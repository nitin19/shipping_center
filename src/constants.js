export const US_COUNTRY_CODE = 233;

export const LOGIN_PAGE_PATH = '/login';
export const MAIN_PAGE_PATH = '/dashboard';

export const TWENTY_MINUTES_TIME_IN_MILLISECONDS = '1200000';
export const API_TOKEN_NAME = 'api_token';
export const API_TOKEN_EXPIRY_NAME = 'api_token_expiry';
export const USER_ROLE = 'USER_ROLE';
export const USER_ID = 'USER_ID';
export const USER_EMAIL = 'USER_EMAIL';
export const IS_ACTIVE = 'IS_ACTIVE';
export const IMAGE_URL = 'https://www.binarydata.in/devshippingcenterbackend/public/uploads/';