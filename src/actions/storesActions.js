import * as types from './actionTypes';
import { API_TOKEN_NAME } from '../constants';
import { scAxios } from '..';


const getStoresListAction = () => ({ type: types.GET_STORES_LIST });
const checkAccountStatusAction = (isAmazonSellerIdVerified) => ({ type: types.CHECK_ACCOUNT_STATUS, isAmazonSellerIdVerified });
const saveStoreAction = () => ({ type: types.SAVE_STORE });
const syncStoresAction = () => ({ type: types.SYNC_STORES });

/* helper functions */
const getStoresListApi = () => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/stores/myStores', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
        });
        req.then(res => resolve(res.data)).catch(err => reject(err));
    });
}

const checkAccountStatusApi = (data) => {
    return new Promise((resolve, reject) => {
        const params = { id: data.id, identifier: data.identifier }
        // const params = { id: 1, identifier: data }
        const req = scAxios.request('/stores/checkAccountStatus', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }, params: { ...params }
        });
        req.then(res => resolve(res.data)).catch(err => reject(err));
    });
}

const saveStoreApi = (data) => {
    return new Promise((resolve, reject) => {
        const params = { id: data.id, identifier: data.identifier, storeName: data.storeName, key: data.key }
        // const params = { id: 1, identifier: data.amazonSellerId, storeName: 'sample store', key: data.mwsToken }
        const req = scAxios.request('/stores/save', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: { ...params }
        });
        req.then(res => resolve(res.data)).catch(err => reject(err));
    });
}

const syncStoresApi = (data) => {
    return new Promise((resolve, reject) => {
        const params = { slug: data.slug, index: data.index }
        const req = scAxios.request('/stores/syncStores', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
            , params: { ...params }
        });
        req.then(res => resolve(res.data)).catch(err => reject(err));
    });
}

/* thunk functions */
export const getStoresList = () => {
    return async dispatch => {
        /* to place a api call and dispatch action*/
        getStoresListApi
            .then(res => { dispatch(getStoresListAction()); })
            .catch(err => { console.log(err); });
    }
}

export const checkAccountStatus = (data) => {
    return async dispatch => {
        /* to place a api call and dispatch action*/
        checkAccountStatusApi(data)
            .then(res => { dispatch(checkAccountStatusAction(res)); })
            .catch(err => { console.log(err); });
    }
}

export const saveStore = (data) => {
    return async dispatch => {
        /* to place a api call and dispatch action*/
        saveStoreApi(data)
            .then(res => { dispatch(saveStoreAction()); })
            .catch(err => { console.log(err); });
    }
}

export const syncStores = (data) => {
    return async dispatch => {
        /* to place a api call and dispatch action*/
        syncStoresApi(data)
            .then(res => { dispatch(syncStoresAction()); })
            .catch(err => { console.log(err); });
    }
}
