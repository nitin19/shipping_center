import * as types from './actionTypes';
import { API_TOKEN_NAME } from '../constants';


const getSingleStoreOrdersAction = () => ({ type: types.GET_SINGLE_STORE_ORDERS });
const getStoreOrderDetailsByIdAction = () => ({ type: types.GET_STORE_ORDER_DETAILS_BY_ID });
const searchStoreAction = () => ({ type: types.SEARCH_STORE });
const searchStoreOrdersAction = () => ({ type: types.SEARCH_STORE_ORDERS });

/* helper functions */
const getSingleStoreOrdersApi = (slug) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request(`/stores/storeOrders/${slug}`, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
        });
        req.then(res => resolve(res.data)).catch(err => reject(err));
    });
}

const getStoreOrderDetailsByIdApi = (id) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request(`stores/order/details/${id}`, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
        });
        req.then(res => resolve(res.data)).catch(err => reject(err));
    });
}

const searchStoreApi = (data) => {
    return new Promise((resolve, reject) => {
        const params = { keyword: data.keyword };
        const req = scAxios.request('/stores/search', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }, params: { ...params }
        });
        req.then(res => resolve(res.data)).catch(err => reject(err));
    });
}

const searchStoreOrdersApi = (data) => {
    return new Promise((resolve, reject) => {
        const params = { keyword: data.keyword };
        const req = scAxios.request('/orders/search', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }, params: { ...params }
        });
        req.then(res => resolve(res.data)).catch(err => reject(err));
    });
}

/* thunk functions */
export const getSingleStoreOrders = (slug) => {
    return async dispatch => {
        /* to place a api call and dispatch action*/
        getSingleStoreOrdersApi(slug)
            .then(res => { dispatch(getSingleStoreOrdersAction()); })
            .catch(err => { console.log(err); });
    }
}

export const getStoreOrderDetailsById = (id) => {
    return async dispatch => {
        /* to place a api call and dispatch action*/
        getStoreOrderDetailsByIdApi(id)
            .then(res => { dispatch(getStoreOrderDetailsByIdAction()); })
            .catch(err => { console.log(err); });
    }
}

export const searchStore = (data) => {
    return async dispatch => {
        /* to place a api call and dispatch action*/
        searchStoreApi(data)
            .then(res => { dispatch(searchStoreAction()); })
            .catch(err => { console.log(err); });
    }
}

export const searchStoreOrders = (data) => {
    return async dispatch => {
        /* to place a api call and dispatch action*/
        searchStoreOrdersApi(data)
            .then(res => { dispatch(searchStoreOrdersAction()); })
            .catch(err => { console.log(err); });
    }
}