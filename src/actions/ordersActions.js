import * as types from './actionTypes';
import { API_TOKEN_NAME } from '../constants';


const getAllStoreOrdersAction = () => ({ type: types.GET_ALL_STORE_ORDERS });

/* helper functions */
const getAllStoreOrdersApi = () => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/stores/storeOrders', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
        });
        req.then(res => resolve(res.data)).catch(err => reject(err));
    });
}

/* thunk functions */
export const getAllStoreOrders = () => {
    return async dispatch => {
        /* to place a api call and dispatch action*/
        getAllStoreOrdersApi
            .then(res => { dispatch(getAllStoreOrdersAction()); })
            .catch(err => { console.log(err); });
    }
}