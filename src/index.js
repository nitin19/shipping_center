import React from 'react';
import { Provider } from 'react-redux';
import ReactDOM from 'react-dom';
import axios from 'axios';

import * as serviceWorker from './serviceWorker';

import App from './components/App';
import configureStore from './store/configureStore';
import { initUser } from './actions/userActions';
import './index.css';
import './css/animate.css';



export const scAxios = axios.create({
    baseURL: 'https://www.binarydata.in/devshippingcenterbackend/api/',
});

const store = configureStore();
store.dispatch(initUser());

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);

serviceWorker.unregister();
