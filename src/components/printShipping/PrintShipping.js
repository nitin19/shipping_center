import React from 'react';
import './printShipping.css';
import { scAxios } from '../..';
import { LOGIN_PAGE_PATH, API_TOKEN_NAME,USER_ROLE } from '../../constants';
import ReactTooltip from 'react-tooltip';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import loadingimage from '../../images/small_loader.gif';
import Skeleton from 'react-loading-skeleton';

const getMailClasses = () => {
  return new Promise((resolve, reject) => {
      const req = scAxios.request('/getMailClasses', {
          method: 'get',
          headers: {
              'Accept': 'application/json',
              'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
          },
          
      });
      req.then(res => resolve(res.data))
          .catch(err => reject(err));
  });
}

const Calculate = (data) => {
  return new Promise((resolve, reject) => {
      const req = scAxios.request('/label/calculate', {
          method: 'get',
          headers: {
              'Accept': 'application/json',
              'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
          },
          params: {
              ...data
          }
      });
      req.then(res => resolve(res.data))
          .catch(err => reject(err));
  });
}

const getOrders = (url, data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request(url, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

class PrintShipping extends React.Component {
  state = {
    shipping_date : new Date(),
    MailClasses : [],
    order: [],
    orderitems: [],
    total: '',
    currentPage: '',
    LastPage:'',
    PerPage: '',
    FirstPageUrl:'',
    NextPageUrl:'',
    PrevPageUrl:'',
    LastPageUrl:'',
    TotalPages:'',
    searchdata:'',
    loadingFirst: true,
    mailClass: '',
    calculatedPrice: '',
    USPSPrice: '',
    deliverydays: '',
  }
 
  ChangeShippingDate(date) {
    this.setState({
      shipping_date: date
    });
  }

  refreshMailClassList = () => {
    getMailClasses()
      .then(res => {
          if(res.status==1){
            this.setState({
        MailClasses: res.records,
            });
          } else {
            this.setState({
        MailClasses: '',
            });
          }
      })
    .catch(err => {
        console.log(err);
    });
  }

  refreshOrderItemList = () => {
    var id =  this.props.match.params.id;
    var url = 'stores/order/details/'+id;

    const data = {
        searchdata: this.state.searchdata,
    }
    getOrders(url, data)
      .then(res => {
          this.setState({ loadingFirst: false });
          if(res.status==1){
          var order = res.Order;
          var orderitems = res.OrderItems.data;
          this.setState({ 
              order: order,
              orderitems: orderitems,
              total: res.OrderItems.total,
              currentPage: res.OrderItems.current_page,
              PerPage: res.OrderItems.per_page,
              FirstPageUrl: res.OrderItems.first_page_url,
              NextPageUrl: res.OrderItems.next_page_url,
              PrevPageUrl: res.OrderItems.prev_page_url,
              LastPageUrl: res.OrderItems.last_page_url,
              LastPage: res.OrderItems.last_page,
              TotalPages: Math.ceil(this.state.total / this.state.PerPage)
           });
          } else {
              this.setState({ orderitems: '' }); 
              this.setState({ order: '' });
          }
      })
      .catch(err => {
          console.log(err);
          
      });
    }

priceCalculate = () => {
  this.setState({ calculatedPrice: '' });
   const data = {
      orderid: this.state.order.id,
      mailClass: this.state.mailClass,
      labelType: 'domestic',
      senderpostalCode: '95825',
    }

    if(this.state.mailClass!=''){
    Calculate(data)
      .then(res => {
          if(res.status==1){
              this.setState({ calculatedPrice: res.value, USPSPrice: res.valueUsps, deliverydays: res.deliverDays });
              toast.success('successfully calculated', {
                position: toast.POSITION.BOTTOM_RIGHT
              });
          } else {
              this.setState({ calculatedPrice: '' });
              toast.error(res.message, {
                position: toast.POSITION.BOTTOM_RIGHT
              });
          }
      })
    .catch(err => {
        console.log(err);
       
    });
  } else {
    toast.error("Please select Mail Class", {
      position: toast.POSITION.BOTTOM_RIGHT
    });
  }
}

  componentDidMount() {
    this.refreshMailClassList();
    this.refreshOrderItemList();
  }


    render() {
      let srno = 1;
        return (
          <>
          {this.state.loadingFirst==true ?
                    <div className="content-wrap">
                    <div className="content-scale">
                        <h1>{this.props.title || <Skeleton count={5} duration={2} />}</h1>
                        <p>{this.props.title || <Skeleton count={1} duration={2} />}</p>
                        <div className="row">
                            <div className="col-sm-4">
                                {this.props.title || <Skeleton count={1} duration={2} height={40} />}
                            </div>
                            <div className="col-sm-4">
                                
                            </div>
                            <div className="col-sm-4">
                               
                            </div>
                        </div>
                        <div className="button2">
                            <h6>{this.props.title || <Skeleton />}</h6>
                            {this.props.body || <Skeleton count={5} duration={2}  circle={true} color="red" height={40} highlightColor="red" />}
                        </div>
                        {/*{this.props.body || <Skeleton count={1} duration={2} circle={true} />}*/}
                    </div>
                    </div>
                    :
            <div className="content-wrap">
                <div className="storemiddle-sec">
                <div className="row addstore-list-text">
                        <div className="col-sm-2 text-right">
                            <p className="ship-date">Select Shipping Date</p>
                        </div>
                        <div className="col-sm-2 text-right">
                          <div className="list-date">
                            <DatePicker 
                              onChange={this.ChangeShippingDate.bind(this)} 
                              selected={this.state.shipping_date}
                              dateFormat="yyyy-MM-dd"
                              minDate={new Date()}
                            /> 
                          </div>
                        </div>
                        <div className="col-sm-2 text-right">
                          <select className="order-slectbox print-slect" name="mailClass" value={this.state.mailClass} onChange={event => this.setState({mailClass: event.target.value})}>
                            <option value="">Select Mail Class</option>
                            {
                              this.state.MailClasses.length > 0
                               ? 
                               this.state.MailClasses.map(MailClass => {
                                  return (<option value={MailClass.sub_type_key} key={MailClass.sub_type_key}>{MailClass.sub_type_title}</option>);
                              }) 
                               : 
                               ''
                            }
                          </select>
                        </div>
                        <div className="col-sm-2 text-right">
                            <a onClick={this.priceCalculate} data-toggle="modal" data-target="#houseModal" className="green-btn"><i className="fa fa-calculator"></i> Calculation</a>
                          </div>
                        <div className="col-sm-2 text-right">
                            <a href="#" className="yellow-btn"><i className="fa fa-tags"></i> Label Generation</a>
                        </div>
                        <div className="col-sm-2 text-right">
                            <a href="#" className="blue-btn"><i className="fa fa-print" aria-hidden="true"></i> Print</a>
                        </div>
                    </div>
                    <div className="row addstore-list-text">
                        <div className="col-sm-12">
                            <h1>Order Number : #{this.state.order.order_id}</h1>
                        </div>
                    </div>
                <div className="custom_tabel wow bounceIn animated" data-wow-duration="4s">
                    <table className="table ">
                      <thead>
                        <tr className="bg">
                          <th className="collumn1">Sr.</th>
                          <th className="collumn5">Product Name</th>
                          <th className="collumn3">SKU No.</th>
                          <th className="collumn2">Weight</th>
                          <th className="collumn2">Qty.</th>
                          <th className="collumn2">Status</th>
                          <th className="collumn5">Shipping Address</th>
                        </tr>
                      </thead>
                      <tbody>
                        {
                        this.state.orderitems.length > 0
                    ?
                        this.state.orderitems.map(orderitem => {
                    return(
                        <tr key={orderitem.id} className="wow animate fadeInLeft" data-wow-delay="0.4s">
                          <td className="view-stor collumn1"><span className="customfloatvip">{srno++}</span></td>
                          <td className="view-stor collumn5">
                              <span className="customfloatvip">
                                 {orderitem.item_name}
                              </span>
                          </td>
                          <td className="view-stor collumn3"><span className="customfloatvip">{orderitem.sellersku}</span></td>
                          <td className="view-stor collumn2"><span className="customfloatvip">{orderitem.weight}</span></td>
                          <td className="view-stor collumn2"><span className="customfloatvip">{orderitem.item_qty}</span></td>
                          <td className="view-stor collumn2"><span className="customfloatvip">{orderitem.item_status}</span></td>
                          <td className="view-stor collumn5">
                            <span className="edit_icon customfloatvip">
                                {this.state.order.shipping_address}
                            </span>
                            <ReactTooltip />
                          </td>
                        </tr>
                        );
                })
                :
            <tr className="wow animate fadeInLeft " data-wow-delay="0.4s">
               <td className="view-stor collumn20">No Records founds!</td>
            </tr>

                    }
                          
                      </tbody>
                    </table>
                </div>

                    
                </div>


                {/* // <!--productModal-->       */}
                <div id="houseModal" className="modal">

                    {/* <!-- Modal content --> */}
                    <div className="house-content wow animated fadeInDown" data-wow-delay=".9s">
                        <div className="house-header">
                            <p>Calculated Results</p>
                            <button type="button" className="close-sign" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                        </div>
                        <div className="house-middle">
                            
                            <div className="row">
                                <div className="col-sm-8">
                                    <p>Default shipping charge</p>
                                </div>
                                <div className="col-sm-4">
                                    <span>${this.state.calculatedPrice}</span>
                                </div>
                                <hr />
                            </div>
                            <div className="row">
                                <div className="col-sm-8">
                                    <p>USPS Shipping charge</p>
                                </div>
                                <div className="col-sm-4">
                                  <span>${this.state.USPSPrice}</span>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-8">
                                    <p>Delivery in Days</p>
                                </div>
                                <div className="col-sm-4">
                                  <span>{this.state.deliverydays}</span>
                                </div>
                            </div>
                        </div>
                        <div className="house-footer">
                        </div>

                    </div>
                </div>
                 <footer className="add_list_footer">
                      <div className="row">
                          <div className="col-sm-12">
                              <p>© 2019 <a href="#" style={{color: 'rgb(7, 168, 222)'}}>VIP PARCEL</a></p>
                          </div>
                          
                      </div>
                    </footer>
                    <ToastContainer autoClose={5000} />
            </div>
          }
            </>
        );
    }
}

export default PrintShipping;