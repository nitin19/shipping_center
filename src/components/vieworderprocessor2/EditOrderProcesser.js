import React from 'react';
import ReactDOM from 'react-dom';

import profile from '../../images/prflimg.png'

class EditOrderProcesser extends React.Component { 
render(){
	return(

		<div className="container-fuild left-margin">
			<div className="order-view2"> 
	        	<div className="order-view-head">
	        		<h2>Order Processor Detail</h2>
	        	</div>
	        	<div className="order-view2-body">
	        	<div className="position-text">
	        		<p>Last Modified Date:</p>
	        		<p>Nov 26, 2018</p>
	        		<a href="#" className="onetwo">Edit</a>
	        	</div>
	        	<div className="row">
	        		<div className="col-sm-3">
	        			<img src={ profile } alt="roundimg" className="profileimg2" />
	        			{/*<div className="upload-btn-wrapper-img">
						  <button className="btn-gourav">Upload a file</button>
						  <input type="file" name="myfile" />
						</div>*/}
	        		</div>
	        		<div className="col-sm-9">
	        			<h3>Marlo speedwagon</h3> 
	        			<a href="mailto:geeber@me.com" className="geebermail">geeber@me.com</a>
	        			<hr />
	        			<p><span className="increment">Phone Number : </span><a href="tel:(326)-369-7896">(326)-369-7896</a></p>
	        			<hr />
	        			{/*<p><span className="increment">ID auto Increment : </span>369</p>
	        			<hr />
	        			<p><span className="increment">Is Fulfillment Center : </span> <button className="btn btn-primary yes">Yes</button></p>
	        			<hr />
	        			<p><span className="increment">Status : </span>Active</p>
	        			<hr />*/}
	        			<div className="row">
	        				<div className="col-sm-12">
	        					<p className="increment">Store Assigned : </p>
	        				</div>
	        				 
	        					<div className="store-ul col-sm-4">
	        						<h4 className="storul-head">Amazon</h4>
	        						<li>
	        							<lable>Clothing</lable>
	        						</li>
	        						{/*<li>
	        							<input type="text" className="form-control custom" name="clothing"/>
	        						</li>*/}
	        						<li>
	        							<lable>Electronic Accessories</lable>
	        						</li>
	        						{/*<li>
	        							<input type="text" className="form-control custom" name="clothing"/>
	        						</li>*/}
	        					</div>
	        					<div className="store-ul col-sm-4">
	        						<h4 className="storul-head">eBay</h4>
	        						<li>
	        							<lable>Mobile</lable>
	        						</li>
	        						{/*<li>
	        							<input type="text" className="form-control custom" name="clothing"/>
	        						</li>*/}
	        						<li>
	        							<lable>Sport</lable>
	        						</li>
	        						{/*<li>
	        							<input type="text" className="form-control custom" name="clothing"/>
	        						</li>*/}
	        					</div>
	        					<div className="store-ul col-sm-4">
	        						<h4 className="storul-head">Shopify</h4>
	        						<li>
	        							<lable>Clothes</lable>
	        						</li>
	        						{/*<li>
	        							<input type="text" className="form-control custom" name="clothing"/>
	        						</li>*/}
	        					</div>

	        				
	        				{/*<hr />
	        				<a href="" className="assign-btn"> <i className="fa fa-plus plusplus" aria-hidden="true"></i> What to assign more store?</a>*/}
	        			</div>
	        
	        		</div>
	        	</div>
	        	<hr />
	        	<div className=""><button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">Close</span></button></div>
	        	
	        	</div>
        	</div>
    	</div>
 
		);
	}
}

export default EditOrderProcesser; 

