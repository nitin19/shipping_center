import React from 'react'
import ReactDOM from 'react-dom'
import './vieworderprocessor2.css';

import profile from '../../images/prflimg.png';
import { validateUserToken } from '../PrivateRoute';
import { scAxios } from '../..';
import { Redirect } from 'react-router-dom';
import { MAIN_PAGE_PATH, LOGIN_PAGE_PATH, API_TOKEN_NAME } from '../../constants';
import Skeleton from 'react-loading-skeleton';

const getProcessor = (url) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request(url, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

class Vieworderprocessor2 extends React.Component { 
            state = {
                processor:[],
            }
  componentDidMount() {
		var processor_id = this.props.match.params.id;
		var url = 'orderprocessor/viewprocessors/'+processor_id;
	getProcessor(url)
	  .then(res => {
                if (res.message) {
                    this.setState({ processor: res.message });
                }
            })
            .catch(err => {
                console.log(err);
            });
}
render(){
	return(

		<div className="content-wrap universelclass">
				
			<div className="order-view2">
	        	<div className="order-view-head">
	        		<h2>Order Processor Detail</h2>
	        	</div>
	        	<div className="order-view2-body">
	        	<div className="position-text">
	        		<p>Last Modified Date:</p>
	        		<p>{this.state.processor.updated_at}</p>
	        		<a href={'/EditOrderProcesser/'+this.state.processor.id} className="onetwo">Edit</a>
	        	</div>
	        	<div className="row">
	        		<div className="col-sm-3">
	        		
					{
						this.state.processor.imgurl != 'noimage'
						? <img src = { this.state.processor.imgurl } alt='roundimg' className= 'profileimg2'/>
						: <img src = { profile } alt='roundimg' className='profileimg2'/>
					}
	        			
	        		</div>
	        		<div className="col-sm-9">
	        			<h3>{this.state.processor.first_name} {this.state.processor.last_name}</h3>
	        			<a href={'mailto:'+this.state.processor.email} className="geebermail">{this.state.processor.email}</a>
	        			<hr />
	        			<p><span className="increment">Phone Number : </span>{this.state.processor.phone}</p>
	        			<hr />
	        		{	/*<p><span className="increment">Is Fulfillment Center : </span> {
                               this.state.processor.is_fulfillment_center == '1'
                                ? <button className="btn btn-primary yes">Yes</button> 
                                : <button className="btn btn-danger no">No</button> 
                            }</p>
	        			<hr />*/ }
	        			<p><span className="increment">Status :</span> {
                               this.state.processor.status == '1'
                                ? <span>Active</span> 
                                : <span>Suspended</span> 
                            }</p>
	        			<hr />
	        			<div className="row">
	        				<div className="col-sm-3">
	        					<p className="increment">Store Assigned : </p>
	        				</div>
	        				<div className="col-sm-9">
	        					<ul className="store-ul">
	        						<h4 className="storul-head">{
                               this.state.processor.storename != ''
                                ? <span>{this.state.processor.storename}</span> 
                                : <span>No store assigned</span> 
                            }</h4>
	        						
	        					</ul>
	        				</div>
	        				<hr />
	        			{	/*<a href="" className="assign-btn"> <i className="fa fa-plus plusplus" aria-hidden="true"></i> Want to assign more store(s)?</a>*/ }
	        			</div>
	        
	        		</div>
	        	</div>
	        	<hr />
	        		        	
	        	</div>
	        	<footer className="add_list_footer">
	        		<div className="row">
	        			<div className="col-sm-12">
	        				<p>© 2018 <a href="#" style={{color: 'rgb(7, 168, 222)' }}>VIP PARCEL</a></p>
	        			</div>
	        		</div>
	        	</footer>
        	</div>
    	</div>
 
		);
	}
}

export default Vieworderprocessor2; 

