import React from 'react';

import DashboardCards from '../dashboardCards/DashboardCards';
import DashboardOverallSummary from '../dashboardOverallSummary/DashboardOverallSummary';
import DashboardOrderStatus from '../dashboardOrderStatus/DashboardOrderStatus';
import DashboardWarehouseList from '../dashboardWarehouseList/DashboardWarehouseList';
import DashboardAllShipments from '../dashboardAllShipments/DashboardAllShipments';
import DashboardTopDestinations from '../dashboardTopDestinations/DashboardTopDestinations';
import Footer from '../footer/Footer';


class Main extends React.Component {
    render() {
        return (
            <div className="content-wrap">
                <div className="main main-content">
                    <div className="container-fluid">
                        <DashboardCards />
                        <DashboardOverallSummary />

                        <div className="col-sm-12">
                            <div className="row">
                                <DashboardOrderStatus />
                                <DashboardWarehouseList />
                            </div>
                            <div className="bottombox">
                                <div className="row">
                                    <DashboardAllShipments />
                                    <DashboardTopDestinations />
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <Footer />
            </div>
        )
    };
}

export default Main;