import React from 'react';

import uploadImg from '../../images/1.png';
import './products.css';

class Products extends React.Component {

    state = {
        myFile: '',
        fname: '',
        sku: '',
        location: '',
        stockValue: '',
        productWeight: '',
    }


    handleChange = event => {
        this.setState({ [event.target.name]: event.target.value });
    }

    handleSubmit = event => {
        event.preventDefault();

    }

    render() {
        return (
            <div className="content-wrap">
                <div className="storemiddle-sec">
                    <div className="row addstore-list-text">
                        <div className="col-sm-8">
                            <h1>Products Listing</h1>
                        </div>
                        <div className="col-sm-4 text-right">
                            <a href="#" data-toggle="modal" data-target="#productModal" className="Connect-btn">Add New</a>
                        </div>
                    </div>
                    <hr />
                    <div className="products-list-sec">
                        <div className="row single-prdt-sec">
                            <div className="col-xl-4 col-lg-6 col-md-6 col-sm-12">

                                <div className="row prdt-back-gd">
                                    <div className="col-sm-3 ptdt-img">

                                    </div>
                                    <div className="col-sm-9">
                                        <h6 className="prdt-mainhd">Nike lunacharge Essential</h6>
                                        <span className="prdt-headtext">Sku</span> <span className="prdt-nortext">32566L</span>
                                        <p className="prdt-location"><i className="fa fa-map-marker" style={{ color: 'red' }}></i> <span><span>8254 S. Garfield Street. Grand Island, NE 68801</span></span></p>
                                        <span className="prdt-headtext">Stock</span><span className="prdt-nortext">20</span>  <span className="prdt-headtext">Weight</span>  <span className="prdt-nortext">120g</span>
                                        <a href="#" className="products-ellipsis"><i className="fa fa-ellipsis-h"></i></a>
                                    </div>
                                </div>

                            </div>
                            <div className="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                                <div className="row prdt-back-gd">
                                    <div className="col-sm-3 ptdt-img-two">

                                    </div>
                                    <div className="col-sm-9">
                                        <h6 className="prdt-mainhd">Adidas Kampung</h6>
                                        <span className="prdt-headtext">Sku</span> <span className="prdt-nortext">32566L</span>
                                        <p className="prdt-location"><i className="fa fa-map-marker" style={{ color: '#ff3333' }}></i> <span>8254 S. Garfield Street. Grand Island, NE 68801</span></p>
                                        <span className="prdt-headtext">Stock</span><span className="prdt-nortext">20</span>  <span className="prdt-headtext">Weight</span>  <span className="prdt-nortext">120g</span>
                                        <a href="#" className="products-ellipsis"><i className="fa fa-ellipsis-h"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                                <div className="row prdt-back-gd">
                                    <div className="col-sm-3 ptdt-img-ballet">

                                    </div>
                                    <div className="col-sm-9">
                                        <h6 className="prdt-mainhd">Ballet shoe. Pointe shoe</h6>
                                        <span className="prdt-headtext">Sku</span> <span className="prdt-nortext">32566L</span>
                                        <p className="prdt-location"><i className="fa fa-map-marker" style={{ color: '#ff3333' }}></i> <span>8254 S. Garfield Street. Grand Island, NE 68801</span></p>
                                        <span className="prdt-headtext">Stock</span><span className="prdt-nortext">20</span>  <span className="prdt-headtext">Weight</span>  <span className="prdt-nortext">120g</span>
                                        <a href="#" className="products-ellipsis"><i className="fa fa-ellipsis-h"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row single-prdt-sec">
                            <div className="col-xl-4 col-lg-6 col-md-6 col-sm-12">

                                <div className="row prdt-back-gd">
                                    <div className="col-sm-3 ptdt-img-athletic">

                                    </div>
                                    <div className="col-sm-9">
                                        <h6 className="prdt-mainhd">Athletic Shoes Vocabulary</h6>
                                        <span className="prdt-headtext">Sku</span> <span className="prdt-nortext">32566L</span>
                                        <p className="prdt-location"><i className="fa fa-map-marker" style={{ color: '#ff3333' }}></i> <span>8254 S. Garfield Street. Grand Island, NE 68801</span></p>
                                        <span className="prdt-headtext">Stock</span><span className="prdt-nortext">20</span>  <span className="prdt-headtext">Weight</span>  <span className="prdt-nortext">120g</span>
                                        <a href="#" className="products-ellipsis"><i className="fa fa-ellipsis-h"></i></a>
                                    </div>
                                </div>

                            </div>
                            <div className="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                                <div className="row prdt-back-gd">
                                    <div className="col-sm-3 ptdt-img-chuck">

                                    </div>
                                    <div className="col-sm-9">
                                        <h6 className="prdt-mainhd">Chuck Taylor</h6>
                                        <span className="prdt-headtext">Sku</span> <span className="prdt-nortext">32566L</span>
                                        <p className="prdt-location"><i className="fa fa-map-marker" style={{ color: '#ff3333' }}></i> <span>8254 S. Garfield Street. Grand Island, NE 68801</span></p>
                                        <span className="prdt-headtext">Stock</span><span className="prdt-nortext">20</span>  <span className="prdt-headtext">Weight</span>  <span className="prdt-nortext">120g</span>
                                        <a href="#" className="products-ellipsis"><i className="fa fa-ellipsis-h"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                                <div className="row prdt-back-gd">
                                    <div className="col-sm-3 ptdt-img-sneakers">

                                    </div>
                                    <div className="col-sm-9">
                                        <h6 className="prdt-mainhd">Sneakers (U.S)/ Trainers (U.K)</h6>
                                        <span className="prdt-headtext">Sku</span> <span className="prdt-nortext">32566L</span>
                                        <p className="prdt-location"><i className="fa fa-map-marker" style={{ color: '#ff3333' }}></i> <span>8254 S. Garfield Street. Grand Island, NE 68801</span></p>
                                        <span className="prdt-headtext">Stock</span><span className="prdt-nortext">20</span>  <span className="prdt-headtext">Weight</span>  <span className="prdt-nortext">120g</span>
                                        <a href="#" className="products-ellipsis"><i className="fa fa-ellipsis-h"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row single-prdt-sec">
                            <div className="col-xl-4 col-lg-6 col-md-6 col-sm-12">

                                <div className="row prdt-back-gd">
                                    <div className="col-sm-3 ptdt-img-basketball">

                                    </div>
                                    <div className="col-sm-9">
                                        <h6 className="prdt-mainhd">Basketball shoes</h6>
                                        <span className="prdt-headtext">Sku</span> <span className="prdt-nortext">32566L</span>
                                        <p className="prdt-location"><i className="fa fa-map-marker" style={{ color: '#ff3333' }}></i> <span>8254 S. Garfield Street. Grand Island, NE 68801</span></p>
                                        <span className="prdt-headtext">Stock</span><span className="prdt-nortext">20</span>  <span className="prdt-headtext">Weight</span>  <span className="prdt-nortext">120g</span>
                                        <a href="#" className="products-ellipsis"><i className="fa fa-ellipsis-h"></i></a>
                                    </div>
                                </div>

                            </div>
                            <div className="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                                <div className="row prdt-back-gd">
                                    <div className="col-sm-3 ptdt-img-knee">

                                    </div>
                                    <div className="col-sm-9">
                                        <h6 className="prdt-mainhd">Knee high boots</h6>
                                        <span className="prdt-headtext">Sku</span> <span className="prdt-nortext">32566L</span>
                                        <p className="prdt-location"><i className="fa fa-map-marker" style={{ color: '#ff3333' }}></i> <span>8254 S. Garfield Street. Grand Island, NE 68801</span></p>
                                        <span className="prdt-headtext">Stock</span><span className="prdt-nortext">20</span>  <span className="prdt-headtext">Weight</span>  <span className="prdt-nortext">120g</span>
                                        <a href="#" className="products-ellipsis"><i className="fa fa-ellipsis-h"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                                <div className="row prdt-back-gd">
                                    <div className="col-sm-3 ptdt-img-combat">

                                    </div>
                                    <div className="col-sm-9">
                                        <h6 className="prdt-mainhd">Combat boots</h6>
                                        <span className="prdt-headtext">Sku</span> <span className="prdt-nortext">32566L</span>
                                        <p className="prdt-location"><i className="fa fa-map-marker" style={{ color: '#ff3333' }}></i> <span>8254 S. Garfield Street. Grand Island, NE 68801</span></p>
                                        <span className="prdt-headtext">Stock</span><span className="prdt-nortext">20</span>  <span className="prdt-headtext">Weight</span>  <span className="prdt-nortext">120g</span>
                                        <a href="#" className="products-ellipsis"><i className="fa fa-ellipsis-h"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row single-prdt-sec">
                            <div className="col-xl-4 col-lg-6 col-md-6 col-sm-12">

                                <div className="row prdt-back-gd">
                                    <div className="col-sm-3 ptdt-img-solly">

                                    </div>
                                    <div className="col-sm-9">
                                        <h6 className="prdt-mainhd">Solly Alison Jean</h6>
                                        <span className="prdt-headtext">Sku</span> <span className="prdt-nortext">32566L</span>
                                        <p className="prdt-location"><i className="fa fa-map-marker" style={{ color: '#ff3333' }}></i> <span>8254 S. Garfield Street. Grand Island, NE 68801</span></p>
                                        <span className="prdt-headtext">Stock</span><span className="prdt-nortext">20</span>  <span className="prdt-headtext">Weight</span>  <span className="prdt-nortext">120g</span>
                                        <a href="#" className="products-ellipsis"><i className="fa fa-ellipsis-h"></i></a>
                                    </div>
                                </div>

                            </div>
                            <div className="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                                <div className="row prdt-back-gd">
                                    <div className="col-sm-3 ptdt-img-timberland">

                                    </div>
                                    <div className="col-sm-9">
                                        <h6 className="prdt-mainhd">Timberland Shirt</h6>
                                        <span className="prdt-headtext">Sku</span> <span className="prdt-nortext">32566L</span>
                                        <p className="prdt-location"><i className="fa fa-map-marker" style={{ color: '#ff3333' }}></i> <span>8254 S. Garfield Street. Grand Island, NE 68801</span></p>
                                        <span className="prdt-headtext">Stock</span><span className="prdt-nortext">20</span>  <span className="prdt-headtext">Weight</span>  <span className="prdt-nortext">120g</span>
                                        <a href="#" className="products-ellipsis"><i className="fa fa-ellipsis-h"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                                <div className="row prdt-back-gd">
                                    <div className="col-sm-3 ptdt-img-court">

                                    </div>
                                    <div className="col-sm-9">
                                        <h6 className="prdt-mainhd">Court shoes(U.K)–Pumps(U.S)</h6>
                                        <span className="prdt-headtext">Sku</span> <span className="prdt-nortext">32566L</span>
                                        <p className="prdt-location"><i className="fa fa-map-marker" style={{ color: '#ff3333' }}></i> <span>8254 S. Garfield Street. Grand Island, NE 68801</span></p>
                                        <span className="prdt-headtext">Stock</span><span className="prdt-nortext">20</span>  <span className="prdt-headtext">Weight</span>  <span className="prdt-nortext">120g</span>
                                        <a href="#" className="products-ellipsis"><i className="fa fa-ellipsis-h"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row single-prdt-sec">
                            <div className="col-xl-4 col-lg-6 col-md-6 col-sm-12">

                                <div className="row prdt-back-gd">
                                    <div className="col-sm-3 ptdt-img-mary">

                                    </div>
                                    <div className="col-sm-9">
                                        <h6 className="prdt-mainhd">Mary Jane platforms</h6>
                                        <span className="prdt-headtext">Sku</span> <span className="prdt-nortext">32566L</span>
                                        <p className="prdt-location"><i className="fa fa-map-marker" style={{ color: '#ff3333' }}></i> <span>8254 S. Garfield Street. Grand Island, NE 68801</span></p>
                                        <span className="prdt-headtext">Stock</span><span className="prdt-nortext">20</span>  <span className="prdt-headtext">Weight</span>  <span className="prdt-nortext">120g</span>
                                        <a href="#" className="products-ellipsis"><i className="fa fa-ellipsis-h"></i></a>
                                    </div>
                                </div>

                            </div>
                            <div className="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                                <div className="row prdt-back-gd">
                                    <div className="col-sm-3 ptdt-img-platform">

                                    </div>
                                    <div className="col-sm-9">
                                        <h6 className="prdt-mainhd">Platform heels</h6>
                                        <span className="prdt-headtext">Sku</span> <span className="prdt-nortext">32566L</span>
                                        <p className="prdt-location"><i className="fa fa-map-marker" style={{ color: '#ff3333' }}></i> <span>8254 S. Garfield Street. Grand Island, NE 68801</span></p>
                                        <span className="prdt-headtext">Stock</span><span className="prdt-nortext">20</span>  <span className="prdt-headtext">Weight</span>  <span className="prdt-nortext">120g</span>
                                        <a href="#" className="products-ellipsis"><i className="fa fa-ellipsis-h"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                                <div className="row prdt-back-gd">
                                    <div className="col-sm-3 ptdt-img-almond-toe">

                                    </div>
                                    <div className="col-sm-9">
                                        <h6 className="prdt-mainhd">Almond-toe pumps</h6>
                                        <span className="prdt-headtext">Sku</span> <span className="prdt-nortext">32566L</span>
                                        <p className="prdt-location"><i className="fa fa-map-marker" style={{ color: '#ff3333' }}></i> <span>8254 S. Garfield Street. Grand Island, NE 68801</span></p>
                                        <span className="prdt-headtext">Stock</span><span className="prdt-nortext">20</span>  <span className="prdt-headtext">Weight</span>  <span className="prdt-nortext">120g</span>
                                        <a href="#" className="products-ellipsis"><i className="fa fa-ellipsis-h"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="" style={{ position: 'relative' }}>
                            <div className="loader"></div>
                        </div>

                    </div>
                </div>

                {/* // <!--productModal-->       */}
                <div id="productModal" className="modal">

                    {/* <!-- Modal content --> */}
                    <div className="modal-content">
                        <div className="add-prdt-header">
                            <p>Add Product</p>
                            <button type="button" className="close-sign" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                        </div>
                        <div className="add-prdt-middle">
                            <ul>
                                <li>
                                    <div className="row">
                                        <div className="col-sm-4">
                                            <p>Product Image</p>
                                        </div>
                                        <div className="col-sm-8">
                                            <img src={{ uploadImg }} alt="upload-img" className="prdt-upload-img" />
                                            <div className="upload-btn-wrapper">
                                                <button className="btn">Upload Images</button>
                                                <input type="file" name="myFile" value={this.state.myFile} onChange={this.handleChange} />
                                            </div>

                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div className="row">
                                        <div className="col-sm-4">
                                            <p>Product Name</p>
                                        </div>
                                        <div className="col-sm-8">
                                            <input type="text" name="fname" placeholder="Enter your product name" value={this.state.fname} onChange={this.handleChange} />
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div className="row">
                                        <div className="col-sm-4">
                                            <p>SKU No.</p>
                                        </div>
                                        <div className="col-sm-8">
                                            <input type="text" name="sku" placeholder="Enter sku number" value={this.state.sku} onChange={this.handleChange} />
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div className="row">
                                        <div className="col-sm-4">
                                            <p>Location</p>
                                        </div>
                                        <div className="col-sm-8">
                                            <input type="text" name="location" placeholder="Location" value={this.state.location} onChange={this.handleChange} />
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div className="row">
                                        <div className="col-sm-4">
                                            <p>Stock</p>
                                        </div>
                                        <div className="col-sm-8">
                                            <input type="text" name="stockValue" placeholder="Enter stock value" value={this.state.stockValue} onChange={this.handleChange} />
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div className="row">
                                        <div className="col-sm-4">
                                            <p>Weight</p>
                                        </div>
                                        <div className="col-sm-8">
                                            <input type="text" name="productWeight" placeholder="Enter product weight" value={this.state.productWeight} onChange={this.handleChange} />
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div className="row">
                                        <div className="col-sm-4">

                                        </div>
                                        <div className="col-sm-8">
                                            <a href="" className="Connect-btn">Submit</a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Products;