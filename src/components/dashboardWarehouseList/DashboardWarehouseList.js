import React from 'react';
import * as userActions from '../../actions/userActions';
import { scAxios } from '../..';
import { API_TOKEN_NAME, USER_ID} from '../../constants';
import { startUserSession } from '../userSession';

const getWarehousesList = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/dashboard/warehouseslist', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}
class DashboardWarehouseList extends React.Component {
    state = {
        warehouse_list: [],
    };
    componentDidMount() {
        const data = {
            userid: localStorage.getItem(USER_ID)
        }
        getWarehousesList(data)
            .then(res => {
                    this.setState({ warehouse_list: res.total_warehouses });
                    console.log(res.total_warehouses);
            })
            .catch(err => {
                    console.log(err);
            })     
    }

    render() {
        return (
            <div className="col-sm-4 tablesection tablerightsection joinedsec wow fadeInRightBig animated" data-wow-delay="0.4s" data-wow-duration="2s">
                <div className="card tablecard orderstatusdiv warehosediv card-title">
                    <h4 className="boxtitles text-uppercase"><span>Warehouse Lists </span><a href={'/warehouse-lists'} className="viewallbtn">View All</a></h4>
                    {this.state.warehouse_list.length > 0 ?
                         this.state.warehouse_list.map(warehouse => {
                    return (<div className="warehouselisting_div wow fadeInRight animated" data-wow-delay="0.2s" key={warehouse.id}>
                        <h4>{warehouse.warehouse_name}</h4>
                        <p><i className="fa fa-map-marker-alt"></i><span>{warehouse.warehouse_address}, {warehouse.warehouse_city}, {warehouse.warehouse_state}, {warehouse.warehouse_country}</span></p>
                    </div>);
                    })
                    :
                    <div className="warehouse_list">No Record Found</div>
                    }
                    {/*<div className="warehouselisting_div wow fadeInRight animated" data-wow-delay="0.4s" >
                        <h4>Caruso Logistics </h4>
                        <p><i className="fa fa-map-marker-alt"></i><span>8254 S. Garfield Street. Grand Island, NE 68801</span></p>
                    </div>
                    <div className="warehouselisting_div wow fadeInRight animated" data-wow-delay="0.6s">
                        <h4>Caruso Logistics </h4>
                        <p><i className="fa fa-map-marker-alt"></i><span>8254 S. Garfield Street. Grand Island, NE 68801</span></p>
                    </div>
                    <div className="warehouselisting_div wow fadeInRight animated" data-wow-delay="0.8s">
                        <h4>Caruso Logistics </h4>
                        <p><i className="fa fa-map-marker-alt"></i><span>8254 S. Garfield Street. Grand Island, NE 68801</span></p>
                    </div>
                    <div className="warehouselisting_div wow fadeInRight animated" data-wow-delay="0.9s">
                        <h4>Caruso Logistics </h4>
                        <p><i className="fa fa-map-marker-alt"></i><span>8254 S. Garfield Street. Grand Island, NE 68801</span></p>
                    </div>*/}
                </div>
            </div>
        )
    }
}

export default DashboardWarehouseList;