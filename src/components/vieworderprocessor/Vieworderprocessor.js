import React from 'react';
import ReactDOM from 'react-dom' ;
import { connect } from 'react-redux'; 
import { Link } from 'react-router-dom'
import { bindActionCreators } from 'redux';
import { Redirect } from 'react-router-dom';
import './vieworderprocessor.css';


import crbtn from '../../images/crbtn.png';
import { scAxios } from '../..';
import { LOGIN_PAGE_PATH, API_TOKEN_NAME, USER_ID } from '../../constants';
import { startUserSession } from '../userSession';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import loadingimage from '../../images/small_loader.gif';
import ReactTooltip from 'react-tooltip';
import Skeleton from 'react-loading-skeleton';

const getProcessors = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/orderprocessor/myprocessors', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const deleteprocessor = (url) => {

    return new Promise((resolve, reject) => {

        const req = scAxios.request(url, {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
        });

        req.then(res => resolve(res.data))
           .catch(err => reject(err));
    });
}

class Vieworderprocessor extends React.Component {
            state = {
                processors:[],
                searchdata: '',
                total: '',
                currentPage: '',
                LastPage:'',
                PerPage: '',
                FirstPageUrl:'',
                NextPageUrl:'',
                PrevPageUrl:'',
                LastPageUrl:'',
                TotalPages:'',
                loadingFirst: true,
            }

        refreshProcessorList = (page) => {
            const data = {
                    searchdata: this.state.searchdata,
                    userid: localStorage.getItem(USER_ID),
                    page: page,
                }
            getProcessors(data)
            .then(res => {
                this.setState({ loadingFirst: false });
                var records = res.records.data;
                this.setState({ processors: records});
                this.setState({ total: res.records.total });
                this.setState({ currentPage: res.records.current_page });
                this.setState({ PerPage: res.records.per_page });
                this.setState({ FirstPageUrl: res.records.first_page_url });
                this.setState({ NextPageUrl: res.records.next_page_url });
                this.setState({ PrevPageUrl: res.records.prev_page_url });
                this.setState({ LastPageUrl: res.records.last_page_url });
                this.setState({ LastPage: res.records.last_page });
                this.setState({ TotalPages: Math.ceil(this.state.total / this.state.PerPage) });
            })
            .catch(err => {
                console.log(err);
            });
        }

    delete(id){
      var url = '/orderprocessor/suspend/'+id;
      deleteprocessor(url)
        .then(res => {
          if(res.status=='Success'){
            toast.success(res.message, {
              position: toast.POSITION.BOTTOM_RIGHT
            });
          } else if(res.status=='Warning'){
            toast.warn(res.message, {
              position: toast.POSITION.BOTTOM_RIGHT
            });
            
          } else {
            toast.error(res.message, {
              position: toast.POSITION.BOTTOM_RIGHT
            });
          }
          this.refreshProcessorList();
        })
        .catch(err => {
            console.log(err);
        });
  }

    handleChange = event => {
        this.setState({ [event.target.name]: event.target.value });
    }

    componentDidMount() {
            this.refreshProcessorList();
        }

  render() {

    const currentPage  = this.state.currentPage;
    const previousPage = currentPage - 1;
    const NextPage     = currentPage + 1;
    const LastPage     = this.state.LastPage;
    const pageNumbers  = [];
        for (let i = 1; i <= this.state.TotalPages; i++) {
          pageNumbers.push(i);
        }

        return (

            <>     
            {this.state.loadingFirst==true ?
                <div className="content-wrap backcollor">
                    <div className="ms-stor">
                        <div className="row mstop">
                            <div className="col-sm-4">
                                {this.props.title || <Skeleton count={1} duration={2} height={30} />}
                            </div>
                            <div className="col-sm-4">
                                
                            </div>
                            <div className="col-sm-4">
                                {this.props.title || <Skeleton count={1} duration={2} height={30} />}
                            </div>
                        </div>
                        <div className="button2">
                            
                            <h6>{this.props.title || <Skeleton />}</h6>
                            {this.props.body || <Skeleton count={5} duration={2}  circle={true} color="red" height={50} highlightColor="red" />}
                        </div>
                    </div>
                </div>
                : 

        <div className="content-wrap universelclass">
        <div className="col-lg-12">
           <div className="sorting-list">
                <h1>Order Processor Listing</h1>
                <div className="view-serch">
                  <form className="form_section">
                    <input type="text" placeholder="Search" id="search_keyword" name="searchdata" value={this.state.searchdata} onChange={this.handleChange} />
                    <input type="button" className="Connect-btn" value="Search" id="submit_btn" onClick={this.refreshProcessorList} />
                  </form>
                </div>
            </div>
            <div className="custom_tabel oderlist">

                <table className="table">
                    <thead>
                        <tr className="bg">
                            <th className="collumn3">Name</th>
                            <th className="collumn4">Email</th>
                            <th className="collumn3">Phone</th>
                            <th className="collumn4">Assign Stores</th>
                         { /*  <th>Is Fulfillment</th> */ }
                            <th className="collumn3">Status</th>
                            <th className="collumn3">Action</th>
                        </tr>
                    </thead>
                    <tbody>
  {
    this.state.processors.length > 0
    ?
        this.state.processors.map(processor =>{
                    return (
                        <tr className="wow animate fadeInLeft" data-wow-delay="0.4s" key={processor.id}>
                            <td className="viewname collumn3"><span className="customfloatvip">{processor.first_name}</span><span className="customfloatvip"></span></td>
                            <td className="viewname collumn4"><span className="customfloatvip">{processor.email}</span></td>
                            <td className="viewname collumn3"><span className="customfloatvip">{processor.phone}</span></td>
                            <td className="viewname collumn4"><span className="customfloatvip">{processor.storesystems}</span></td>

                      {   /*   <td className="viewname collumn3"><span className="customfloatvip">
                            {
                               processor.is_fulfillment_center == '1'
                                ? <button className="btn btn-primary yes">Yes</button> 
                                : <button className="btn btn-danger no">No</button> 
                            }
                              </span>
                            </td> */ }

                            <td className="viewname collumn3"> <span className="customfloatvip">
                            {
                               processor.status == '1'
                                ? <span>Active</span> 
                                : <span>Suspended</span> 
                            }</span></td>
                            
                            <td className="viewname collumn3"><span className="customfloatvip done-space">
                                <span className="edit_icon" data-tip="Edit">
                                    <a href={'./Editorderprocesser/'+processor.id}><i className="fas fa-pencil-alt "></i></a>
                                    <ReactTooltip />
                                </span>
                             {  /* <span className="edit_icon" data-tip="Delete">
                                    <a style={{cursor:'pointer'}} onClick={this.delete.bind(this, processor.id)}><i className="far fa-trash-alt "></i> </a>
                                    <ReactTooltip />
                                </span> */  }
                                <span className="edit_icon" data-tip="View">
                                    <a href={'./Vieworderprocessor2/'+processor.id}><i className="fas fa-eye "></i></a>
                                    <ReactTooltip />
                                </span>
                                </span>
                            </td>
                        
                        </tr>
                        );
                    })
                  :
  <tr>
      <td>No Record Found</td>
  </tr>
}           
                    </tbody>
                </table>

{ pageNumbers.length > 1 ?

<div className="paggination-text">
  <ul className="pagination pagination-sm">
    {this.state.PrevPageUrl != null ? 
  <li className="page-item"><a className="page-link" style={{cursor:'pointer'}} onClick={this.refreshProcessorList.bind(this, previousPage)}>Previous</a></li>
  : ''
}
      {pageNumbers.map(page => {
      return (<li className={currentPage == page ? 'activelink page-item' : 'page-item' } ><a className="page-link" style={{cursor:'pointer'}} onClick={this.refreshProcessorList.bind(this, page)}>{page}</a></li>);
    })}

   {this.state.NextPageUrl != null ? 
  <li className="page-item"><a className="page-link" style={{cursor:'pointer'}} onClick={this.refreshProcessorList.bind(this, NextPage)}>Next</a></li>
  : ''
}
    {this.state.LastPage != null && currentPage!=LastPage? 
  <li className="page-item"><a className="page-link" style={{cursor:'pointer'}} onClick={this.refreshProcessorList.bind(this, LastPage)} >Last</a></li>
  : ''
}
  
  </ul>
</div> : ''
}
  
            <footer className="add_list_footer">
                <div className="row">
                    <div className="col-sm-12">
                        <p>© 2019 <a href="#" style={{color: 'rgb(7, 168, 222)'}}>VIP PARCEL</a></p>
                    </div>
                </div>
            </footer>
            </div>
            
        </div>
        <ToastContainer autoClose={5000} />
    </div>

        }
    </>
    
        );
    }

}



export default Vieworderprocessor;
     