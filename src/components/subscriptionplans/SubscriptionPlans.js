import React from 'react';
import ReactDOM from 'react-dom';

class SubscriptionPlans extends React.Component {

	render() {

		return(

				<div className="content-wrap universelclass">
			    	<div className="col-sm-12">
			    		<div className="shiconnect">
			    			<h1>Subscription </h1>
			    			<p>Update your plan, billing information, or download your monthly data archives.</p>
			    			<hr />
			    			<div className="subsplans">
			    			<h2>Current Subscription</h2>
			    			<div className="row">
			    				<div className="col-sm-2">
			    					<h5>Your Plan</h5>
			    				</div>
			    				<div className="col-sm-10">
			    					<ul className="trial-month">
			    						<h5>Trial for $0.00 per month</h5>
			    						<li><span style={{fontFamily: 'Aileron-bold', color: '#333'}}>1.</span> Unlimited selling channel(s)</li>
			    						<li><span style={{fontFamily: 'Aileron-bold', color: '#333'}}>2.</span> Unlimited shipments	</li>
										<li><span style={{fontFamily: 'Aileron-bold', color: '#333'}}>3.</span> Unlimited user(s)</li>
			    					</ul>
			    				</div>
			    			</div>
			    			
			    			<div className="row">
			    				<div className="col-sm-2">
			    					<h5>Next Invoice</h5>
			    				</div>
			    				<div className="col-sm-10">
			    					<p>Guess what? You are still within your trial period 
			    					and there are no invoices to pay during the trial subscription period!</p>
			    					<p>Cancel your subscription <a href="" className="namehigh">here.</a></p>
			    				</div>
			    			</div>
			    			
			    			</div>
			    			<hr />
			    			<div className="subsplans">
			    				<h2>Billing Information 
				    				<a href="" className="viewallbtn">
					    				<i className="fas fa-plus"></i>
					    				 Change Your Subscription
					    			</a>
				    			</h2>
			    				<p>You currently do not have billing information on file for your ShipStation account, but don't worry. By clicking on the 'Edit Billing Info' button below you can enter credit card information that will allow you to make changes to your ShipStation billing plan and do other important things within the app.</p>
			    				
			    			</div>
			    			<hr />
			    			<div className="subsplans">
			    				<h2>Data Archive
				    				<a href="" className="viewallbtn">
					    				<i className="fas fa-plus"></i>
					    				 Change Billing Information
					    			</a>
			    				</h2>
			    				<p>Every month, ShipStation archives your order data. Click <a href="" className="namehigh">here</a> to view your monthly archive and download your data.</p>
			    				
			    			</div>


			    		</div>
			        </div>
			        <footer className="add_list_footer">   
	                    <div className="row">  
		                    <div className="col-sm-12">        
		                    	<p>© 2019 <a href="#" style={{color: '#2db4e3', textDecoration: 'none'}}>VIP PARCEL</a></p>    
		                    </div>  
                    	</div>
                	</footer>
			    </div>

			);

	}

}
export default SubscriptionPlans;