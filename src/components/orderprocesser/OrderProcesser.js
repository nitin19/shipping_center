import React from 'react'; 

import home from '../../images/home.png';
import carticon from '../../images/carticon.png';
import sale from '../../images/sale.png';
import gt from '../../images/gt.png';
import cart2 from '../../images/cart2.png';
import amazon from '../../images/amazon.png';
import line from '../../images/line.png';
import arrow from '../../images/arroe.png';
import msg from '../../images/msg.png';
import chasse from '../../images/chasse.png';
import line2 from '../../images/line2.png';
import msg2 from '../../images/msg2.png';
import ebay from '../../images/ebay.png';
import line3 from '../../images/line3.png';
import msg3 from '../../images/msg3.png';
import cart from '../../images/cart.png';
import line4 from '../../images/line4.png';
import ind from '../../images/ind.png';
import one from '../../images/1.png';
import two from '../../images/2.png';
import usa from '../../images/usa.png';
import aus from '../../images/aus.png';
import eng from '../../images/eng.png';
import three from '../../images/3.png';
import four from '../../images/4.png';
import nzl from '../../images/nzl.png';
import five from '../../images/5.png';

class OrderProcesser extends React.Component { 

render() {
        return (
           
      <div className="content-wrap">
         <div className="main main-content">
            <div className="container-fluid">
               <div className="row">
                  <div className="col-lg-4 boxdivsec wow animate fadeInLeft " data-wow-delay="0.4s">
                     <div className="card skybluebox">
                        <div className="boxcontent">
                           <div className="boximgdiv">
                              <img src={home} alt="home" />
                           </div>
                           <div className="boxcontentdiv">
                              <h4>Store</h4>
                              <h3>90%</h3>
                           </div> 
                        </div>
                     </div>
                  </div>
                  <div className="col-lg-4 boxdivsec wow animate fadeInLeft " data-wow-delay="0.6s">
                     <div className="card purplebox">
                        <div className="boxcontent">
                           <div className="boximgdiv">
                              <img src={carticon} alt="carticon" />
                           </div>
                           <div className="boxcontentdiv">
                              <h4>Products</h4>
                              <h3>41,410</h3>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div className="col-lg-4 boxdivsec wow animate fadeInLeft " data-wow-delay="0.8s">
                     <div className="card bluebox">
                        <div className="boxcontent">
                           <div className="boximgdiv">
                              <img src={sale} alt="sale" />
                           </div>
                           <div className="boxcontentdiv">
                              <h4>Sales</h4>
                              <h3>8,543</h3>
                           </div>
                        </div>
                     </div>
                  </div>
                  
                  {/*<-- column -->*/}
               </div>
               
               {/*<!--reffered-sec-->*/}
               <div className="col-sm-12">
                  <div className="row">
                     <div className="col-sm-8 nopadding tablesection tableleftsection wow animate fadeInLeft" >
                        <div className="referredsec">
                           <div className="card tablecard orderstatusdiv card-title">
                              <h4 className="boxtitles text-uppercase"><span>Orders Status </span><a href="#" className="viewallbtn">View All</a></h4>
                              <div className="tablediv">
                                 <table>
                                    <tr className="tabledata wow animated fadeInLeft ">
                                       <td className="column7 column ordersname text-uppercase">Nike LUNARCHARGE ESSENTIAL<span>id 383247</span></td>
                                       <td className="column3 column text-center"><img src={amazon} alt="amazon" /></td>
                                       <td className="location column7 column text-center"><span>Alabama</span><img src={line} alt="line" /><span>Alaska</span></td>
                                       <td className="column2 column"><img src={msg} alt="message" /></td>
                                       <td className="column1 column"><img src={arrow} alt="arrow" /></td>
                                    </tr>
                                    <tr className="tabledata wow animated fadeInLeft " data-wow-delay="0.2s">
                                       <td className="column7 column ordersname text-uppercase">Nike Duel racer<span>id 383247</span></td>
                                       <td className="column3 column text-center"><img src={chasse} /></td>
                                       <td className="location column column7 text-center"><span>California</span><img src={line2} alt="line" /><span>Alaska</span></td>
                                       <td className="column2 column"><img src={msg2} alt="message" /></td>
                                       <td className="column1 column"><img src={arrow} alt="arrow" /></td> 
                                    </tr>  
                                    <tr className="tabledata wow animated fadeInLeft " data-wow-delay="0.4s">
                                       <td className="column7 column ordersname text-uppercase">Nike LUNARCHARGE ESSENTIAL<span>id 383247</span></td>
                                       <td className="column3 column text-center"><img src={ebay} /></td>
                                       <td className="location column column7 text-center"><span>Alabama</span><img src={line3} alt="line" /><span>Los Angeles</span></td>
                                       <td className="column2 column"><img src={msg3} alt="message" /></td>
                                       <td className="column1 column"><img src={arrow} alt="arrow" /></td>
                                    </tr> 
                                    <tr className="tabledata wow animated fadeInLeft " data-wow-delay="0.6s">
                                       <td className="column7 column ordersname text-uppercase">Nike LUNARCHARGE ESSENTIAL<span>id 383247</span></td>
                                       <td className="column3 column text-center"><img src={cart} /></td>
                                       <td className="location column column7 text-center"><span>Alabama</span><img src={line4} alt="line" /><span>Alaska</span></td>
                                       <td className="column2 column"><img src={msg2} alt="message" /></td>
                                       <td className="column1 column"><img src={arrow} alt="arrow" /></td>
                                    </tr>
                                    <tr className="tabledata wow animated fadeInLeft " data-wow-delay="0.8s">
                                       <td className="column7 column ordersname text-uppercase">Nike LUNARCHARGE ESSENTIAL<span>id 383247</span></td>
                                       <td className="column3 column text-center"><img src={cart} /></td>
                                       <td className="location column column7 text-center"><span>Alabama</span><img src={line4} alt="line" /><span>Alaska</span></td>
                                       <td className="column2 column"><img src={msg2} alt="message" /></td>
                                       <td className="column1 column"><img src={arrow} alt="arrow" /></td>
                                    </tr>
                                    <tr className="tabledata wow animated fadeInLeft " data-wow-delay="0.9s">
                                       <td className="column7 column ordersname text-uppercase">Nike LUNARCHARGE ESSENTIAL<span>id 383247</span></td>
                                       <td className="column3 column text-center"><img src={cart} /></td>
                                       <td className="location column column7 text-center"><span>Alabama</span><img src={line4} alt="line" /><span>Alaska</span></td>
                                       <td className="column2 column"><img src={msg2} alt="message" /></td>
                                       <td className="column1"><img src={arrow} alt="arrow" /></td>
                                    </tr>
                                 </table>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div className="col-sm-4 tablesection tablerightsection joinedsec wow animate fadeInRightBig " data-wow-delay="0.4s">
                        <div className="card tablecard orderstatusdiv warehosediv card-title">
                           <h4 className="boxtitles text-uppercase"><span>Warehouse Lists </span><a href="#" className="viewallbtn">View All</a></h4>
                           <div className="warehouselisting_div">
                              <h4>Caruso Logistics </h4>
                              <p><i className="fa fa-map-marker-alt"></i><span>8254 S. Garfield Street. Grand Island, NE 68801</span></p>
                           </div>
                           <div className="warehouselisting_div">
                              <h4>Caruso Logistics </h4>
                              <p><i className="fa fa-map-marker-alt"></i><span>8254 S. Garfield Street. Grand Island, NE 68801</span></p>
                           </div>
                           <div className="warehouselisting_div">
                              <h4>Caruso Logistics </h4>
                              <p><i className="fa fa-map-marker-alt"></i><span>8254 S. Garfield Street. Grand Island, NE 68801</span></p>
                           </div>
                           <div className="warehouselisting_div">
                              <h4>Caruso Logistics </h4>
                              <p><i className="fa fa-map-marker-alt"></i><span>8254 S. Garfield Street. Grand Island, NE 68801</span></p>
                           </div>
                           <div className="warehouselisting_div">
                              <h4>Caruso Logistics </h4>
                              <p><i className="fa fa-map-marker-alt"></i><span>8254 S. Garfield Street. Grand Island, NE 68801</span></p>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div className="bottombox">
                     <div className="row">
                        <div className="col-sm-5 shipmentbox wow animate fadeInLeftBig " data-wow-delay="0.4s">
                           <div className="shipmentbg card-title">
                              <h3 className="boxtitles text-uppercase text-white">All Shipments</h3>
                              <img src={cart2} />
                              <div className="deliverysec row">
                                 <div className="col-sm-4 wow animated fadeInLeft " data-wow-delay="0.2s">
                                    <div className="deliverybox">
                                       <p><span className="greenbox colouredbox"></span>Delivered</p>
                                    </div>
                                 </div>
                                 <div className="col-sm-4 wow animated fadeInLeft " data-wow-delay="0.4s">
                                    <div className="deliverybox">
                                       <p><span className="whitebox colouredbox"></span>Delivered</p>
                                    </div>
                                 </div>
                                 <div className="col-sm-4 wow animated fadeInLeft " data-wow-delay="0.6s">
                                    <div className="deliverybox">
                                       <p><span className="orangebox colouredbox"></span>Delivered</p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div className="col-sm-7 nopadding tablesection wow animate fadeInRightBig " data-wow-delay="0.4s">
                           <div className="referredsec">
                              <div className="card tablecard orderstatusdiv card-title">
                                 <h4 className="boxtitles text-uppercase"><span>Top Destinations </span><a href="#" className="viewallbtn">View All</a></h4>
                                 <div className="tablediv">
                                    <table>
                                       <tr className="tableheader wow animated fadeInLeft " data-wow-delay="0.2s">
                                          <th className="column7 column">Countries</th>
                                          <th className="column8 column"></th>
                                          <th className="column5 column">Total Shipments</th>
                                       </tr>
                                       <tr className="tabledata wow animated fadeInLeft " data-wow-delay="0.4s">
                                          <td className="column7 column"><img src={ind} /><span className="text-uppercase"> India</span></td>
                                          <td className="column8 column barimg"><img src={one} className="img-responsive" /></td>
                                          <td className="column5 column ttlshiping">33,333,023</td>
                                       </tr>
                                       <tr className="tabledata wow animated fadeInLeft " data-wow-delay="0.6s">
                                          <td className="column7 column"><img src={usa} /><span className="text-uppercase"> USA</span></td>
                                          <td className="column8 column barimg"><img src={two} className="img-responsive" /></td>
                                          <td className="column5 column ttlshiping">33,333,023</td>
                                       </tr>
                                       <tr className="tabledata wow animated fadeInLeft " data-wow-delay="0.8s">
                                          <td className="column7 column"><img src={aus} /><span className="text-uppercase"> AUSTRALIA</span></td>
                                          <td className="column8 column barimg"><img src={three} className="img-responsive" /></td>
                                          <td className="column5 column ttlshiping">33,333,023</td>
                                       </tr>
                                       <tr className="tabledata wow animated fadeInLeft " data-wow-delay="0.9s">
                                          <td className="column7 column"><img src={eng} /><span className="text-uppercase"> ENG</span></td>
                                          <td className="column8 column barimg"><img src={four} className="img-responsive" /></td>
                                          <td className="column5 column ttlshiping">33,333,023</td>
                                       </tr>
                                       <tr className="tabledata wow animated fadeInLeft " data-wow-delay="0.9s">
                                          <td className="column7 column"><img src={nzl} /><span className="text-uppercase"> NZL</span></td>
                                          <td className="column8 column barimg"><img src={five} className="img-responsive" /></td>
                                          <td className="column5 column ttlshiping">33,333,023</td>
                                       </tr>
                                    </table>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div className="footerdiv">
            <div className="col-lg-12">
               <div className="footer">
                  <ul>
                     <li>Copyright  Mycityperks. All rights reserved</li>
                     <li><a href="">Careers</a></li>
                     <li><a href="">Privacy Policy</a></li>
                     <li><a href="">Privacy Policy</a></li>
                     <li className="feedback"><a href="#">Feedback<i className="fa fa-angle-up"></i></a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
      

        );

    }

}
 
export default OrderProcesser;      