import React from 'react'
import ReactDOM from 'react-dom'  
import './createorderprocessor.css'; 

import profile from '../../images/prflimg.png'
import clander from '../../images/clandericon.png';
import { scAxios } from '../..';
import { API_TOKEN_NAME, LOGIN_PAGE_PATH, USER_ID, USER_EMAIL } from '../../constants';
import { startUserSession } from '../userSession';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Skeleton from 'react-loading-skeleton';

const processorRequest = (data, imageData) => {

     let processor_image = new FormData();
     processor_image.append('processor_image', imageData);

    return new Promise((resolve, reject) => {
        scAxios.post('/orderprocessor/save', processor_image, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': `multipart/form-data; boundary=${processor_image._boundary}`,
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        }).then((res) => {
            return resolve(res.data)
        }).catch((err) => {
            return reject(err)
        });
    });
}

const getStores = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('stores/allstores', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)

            },
            params: {
                ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

class Createorderprocessor extends React.Component {

       state = {
            first_name: '',
            last_name: '',
            email: '',
            password: '',
            password_confirmation: '',
            phone: '',

            fields: {},
            errors: {},
            stores: [],
            is_fulfillment_center:'0',
            file: '',
            imagePreviewUrl: '',
            assignstores: [],
            store_status:'',
            processor_image: profile,
            processor_image_data: '',
            loadingFirst: true,
       }

    handleChange = event => {
        let fields = this.state.fields;
        fields[event.target.name] = event.target.value;
        this.setState({
        fields
        }, () => this.validateForm());
      }

    handleCheckBoxChange = event => {
        /*this.setState({
            [event.target.name]: event.target.checked
        });*/
        if(event.target.checked){
          this.state.assignstores.push(event.target.value);
        } else {
            var index = this.state.assignstores.indexOf(event.target.value); 
            if (index >= 0) {
             this.state.assignstores.splice( index, 1 );
            }
        }
    }

    onImageChange = event => {
        if (event.target.files[0]) {
            const file = event.target.files[0];
            let reader = new FileReader();
            reader.readAsDataURL(file);

            reader.onload = event => {
                this.setState({ processor_image: event.target.result, processor_image_data: file });
            }
        }
    }   

    validateForm() {
      let fields = this.state.fields;
      let errors = {};
      let formIsValid = true;

      if (!fields["first_name"]) {
        formIsValid = false;
        errors["first_name"] = "*Please enter your first name.";
      }

      if (typeof fields["first_name"] !== "undefined") {
        if (!fields["first_name"].match(/^[a-zA-Z0-9 ]*$/)) {
          formIsValid = false;
          errors["first_name"] = "*Please enter alphanumeric characters only.";
        }
      }


      if (!fields["last_name"]) {
        formIsValid = false;
        errors["last_name"] = "*Please enter your last name.";
      }

      if (typeof fields["last_name"] !== "undefined") {
        if (!fields["last_name"].match(/^[a-zA-Z0-9 ]*$/)) {
          formIsValid = false;
          errors["last_name"] = "*Please enter alphanumeric characters only.";
        }
      }

      if (!fields["email"]) {
        formIsValid = false;
        errors["email"] = "*Please enter your first name.";
      }

      if (typeof fields["email"] !== "undefined") {
          let lastAtPos = fields["email"].lastIndexOf('@');
          let lastDotPos = fields["email"].lastIndexOf('.');
          if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["email"].indexOf('@@') == -1 && lastDotPos > 2 && (fields["email"].length - lastDotPos) > 2)) {
              formIsValid = false;
              errors["email"] = "Email is not valid";
            }
      }

      if (!fields["phone"]) {
        formIsValid = false;
        errors["phone"] = "*Please enter your Phone no.";
      }

      if (typeof fields["phone"] !== "undefined") {
        if (!fields["phone"].match(/^[0-9]{10}$/)) {
          formIsValid = false;
          errors["phone"] = "*Please enter valid Phone no.";
        }
      }

      if(!fields["user_password"]){
           formIsValid = false;
           errors["user_password"] = "*Please enter password";
        }

      if(!fields["user_password_confirm"]){
           formIsValid = false;
           errors["user_password_confirm"] = "*Please enter confirm password";
        }  

      if(fields["user_password_confirm"]!=fields["user_password"]){
           formIsValid = false;
           errors["user_password_confirm"] = "Password and confirm password must be same";
        }   

      this.setState({
        errors: errors
      });
      return formIsValid;

    }  

   
  handleSubmit = event => {
        event.preventDefault();

       if (this.validateForm()) {

          let fields = {};
          fields["first_name"] = "";
          fields["last_name"] = "";
          fields["email"] = "";
          fields["user_password"] = "";
          fields["user_password_confirm"] = "";
          fields["phone"] = "";
          this.setState({fields:fields});

          const processor = {
            first_name: this.state.fields.first_name,
            last_name: this.state.fields.last_name,
            email: this.state.fields.email,
            password: this.state.fields.user_password,
            password_confirmation: this.state.fields.user_password_confirm,
            phone: this.state.fields.phone,
            assignedstores: this.state.assignstores,
            userid: localStorage.getItem(USER_ID),
            useremail: localStorage.getItem(USER_EMAIL)
          }

           processorRequest(processor, this.state.processor_image_data)
                .then(res => {
                  this.setState({
                     fields: {},
                    });

                  if(res.status=='Success'){
                    toast.success(res.message, {
                      position: toast.POSITION.BOTTOM_RIGHT
                    });
                    setTimeout(function(){
                      window.location.href = '/vieworderprocessor';
                    }, 5000);
                  } else if(res.status=='Warning'){
                    toast.warn(res.message, {
                      position: toast.POSITION.BOTTOM_RIGHT
                    });
                    
                  } else {
                    toast.error(res.message, {
                      position: toast.POSITION.BOTTOM_RIGHT
                    });
                  }

                })
                .catch(err => {
                    console.log(err);
                });
        
         }
        else
           toast.error('Validation failed !', {
              position: toast.POSITION.BOTTOM_RIGHT
            });   

    }


  componentDidMount() {

    const data = {
            userid: localStorage.getItem(USER_ID),
          }

        getStores(data)
            .then(res => {
                this.setState({ stores: res.records,store_status: res.status, loadingFirst: false });
            })
            .catch(err => {
            });
  }

  render() {

        return (
<>     
                {this.state.loadingFirst==true ?
          <div className="content-wrap backcollor">
                  <div className="ms-stor">
                    <div className="row">
                      <div className="col-sm-3">
                          <p className="circle">{this.props.title || <Skeleton circle={true} count={1} duration={2} height={200} width={200} />}</p>
                      </div>
                      <div className="col-sm-9">
                          <div className="button2">
                              {this.props.body || <Skeleton count={10} duration={2} height={40} />}
                          </div>
                      </div>
                    </div>    
                  </div>
                </div>
                :

          <div className="content-wrap universelclass">
          <div className="col-lg-12">
            <div className="sorting-list">
                <h1>Order Processor</h1>
            </div>
            <div>           
        <form name="processorform" className="processorform" onSubmit={this.handleSubmit}>
        
        <div className="row orderprocess">

            <div className="col-sm-3 creatpro text-center pakwidth">
                    <img src={this.state.processor_image} alt="roundimg" className="viewprofileimg" />
              <div className="upload-btn-wrapper-img">
              <button className="btn-gourav">Upload a file</button>
              <input className="fileInput" type="file" name="processor_image" d='processor_image' onChange={this.onImageChange} />
              </div>
            </div>

            <div className="col-sm-9 pakwidth">
              <fieldset>

              <div className="idauto">  
                <div className="labsec">
                  <p>First Name</p>  
                </div>
                <div className="writetext">
                  <input type="text" size="30" placeholder="Enter FirstName" className="form-control custom" name="first_name" value={this.state.fields.first_name} onChange={this.handleChange}/>
                  <span className="errorMsg">{this.state.errors.first_name}</span>
                </div>
              </div>

              <div className="idauto">  
                <div className="labsec">
                  <p>Last Name</p>  
                </div>
                <div className="writetext">
                  <input type="text" size="30" placeholder="Enter LastName" className="form-control custom" name="last_name" value={this.state.fields.last_name} onChange={this.handleChange} />
                  <span className="errorMsg">{this.state.errors.last_name}</span>
                </div>
              </div>

                <div className="idauto">  
                  <div className="labsec">
                    <p>Email Address</p>  
                  </div>
                  <div className="writetext">
                    <input type="text" size="30" placeholder="Enter Email" className="form-control custom" name="email" value={this.state.fields.email} onChange={this.handleChange} />
                    <span className="errorMsg">{this.state.errors.email}</span>
                  </div>
                </div> 

                <div className="idauto">  
                  <div className="labsec">
                    <p>PhoneNumber</p>  
                  </div>
                  <div className="writetext">
                    <input type="text" size="30" placeholder="Enter Phone" className="form-control custom" name="phone" value={this.state.fields.phone} onChange={this.handleChange} />
                    <span className="errorMsg">{this.state.errors.phone}</span>
                  </div>
                </div>


                <div className="idauto">  
                  <div className="labsec">
                    <p>Password</p>  
                  </div>
                  <div className="writetext">
                    <input type="password" placeholder="Enter Password" className="form-control custom" name="user_password" value={this.state.fields.user_password} onChange={this.handleChange} />  
                    <span className="errorMsg">{this.state.errors.user_password}</span>
                    
                  </div>
                </div>

                <div className="idauto">  
                  <div className="labsec">
                    <p>Confirm Password</p>  
                  </div>
                  <div className="writetext">
                    <input type="password" placeholder="Confirm Password" className="form-control custom" name="user_password_confirm" value={this.state.fields.user_password_confirm} onChange={this.handleChange} />  
                    <span className="errorMsg">{this.state.errors.user_password_confirm}</span>
                  </div>
                </div>

                <div className="idauto AssignStore">  
                  <div className="labsec">
                    <p>Assign Store(s)</p>  
                  </div>
                  <div className="writetext pack-head-ing">
                        { this.state.store_status != '0'
                          ?  this.state.stores.map(store => {
                          return <label key={store.id} className="container-radio">{store.store_name}<input type="checkbox" name="assignstores" id={'chk_'+store.id} value={store.id} onChange={this.handleCheckBoxChange} />
                          <span className="checkmark"></span></label>
                                           
                         }) : <span> no store is created</span>}
                 
                  </div>
                </div>

                {/*<div className="idauto">  
                                    <div className="labsec">
                                      <p>Is Fulfillment</p>  
                                    </div>
                                  <div className="writetext center">
                                    <span>
                                      <input type="radio" id="test3" name="is_fulfillment_center" value= "1" checked={this.state.fields.is_fulfillment_center === '1'} onChange={this.handleChange.bind(this, "is_fulfillment_center")} />
                                      <label for="test3">Yes</label>
                                      <span className="error">{this.state.errors["is_fulfillment_center"]}</span>
                                    </span>
                                    <span>
                                      <input type="radio" id="test2" name="is_fulfillment_center" value= "0" checked={this.state.fields.is_fulfillment_center === '0'} onChange={this.handleChange.bind(this, "is_fulfillment_center")}/>
                                      <label for="test2">No</label>
                                      <span className="error">{this.state.errors["is_fulfillment_center"]}</span>
                                    </span>
                                  </div>
                                </div>*/}

                <div className="idauto">
                  <div className="writetext btnright">
                      <button type="reset" className="createbtn Cancel">Cancel</button>
                      <button type= "submit" className="mainbtn">Submit</button>
                  </div>
                </div>

              </fieldset>
            </div>

         </div>
        </form>
            </div>
            <footer className="add_list_footer">
                <div className="row">
                    <div className="col-sm-12">
                        <p>© 2019 <a href="#" style={{color: 'rgb(7, 168, 222)'}}>VIP PARCEL</a></p>
                    </div>
                </div>
            </footer>
      </div>
      <ToastContainer autoClose={5000} />
</div>  

}
        </>

        );
    }

}

export default Createorderprocessor;      