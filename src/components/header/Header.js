import React from 'react';

import mailIcon from '../../images/mail-icon.png';
import noticon from '../../images/noticon.png';
import prflimg from '../../images/prflimg.png';
import { scAxios } from '../..';
import { API_TOKEN_NAME, USER_ROLE, USER_ID } from '../../constants';

const fetchHeaderInfo = (data) => {
    if(localStorage.getItem(USER_ROLE) !== "OP"){
        var url = '/user';
    } else {
        var url = '/orderprocessor/viewprocessors/'+localStorage.getItem(USER_ID);
    }
    return new Promise((resolve, reject) => {
        const req = scAxios.request(url, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const fetchNotifyInfo = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/notification/notify', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getUserdetails = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('user', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                userid: localStorage.getItem(USER_ID)
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const readNotifyInfo = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/notification/readnotify', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}


class Header extends React.Component {
    state = {
        username: '',
        role: '',
        countnotify:'',
        notifications:[],
        totalbalance:'',

    }

    componentDidMount() {
        const data = {
            userid: localStorage.getItem(USER_ID)
        }
        fetchHeaderInfo(data)
            .then(res => {
               if(localStorage.getItem(USER_ROLE) !== "OP"){
                    const fullname = res.profile.first_name + ' ' + res.profile.last_name;
                    this.setState({ username: fullname, userid: res.id });
                } else {
                    const fullname = res.message.first_name + ' ' + res.message.last_name;
                    this.setState({ username: fullname, userid: res.message.id });
                }
            })
            .catch(err => {
                console.log('Error receiving user info.', err);
            });
        fetchNotifyInfo(data)
            .then(res => {
               this.setState({ countnotify: res.total_notification , notifications:res.read_notification});
               })
            .catch(err => {
               console.log('Error receiving notification info.', err);
            });

        getUserdetails()
            .then(res => {
                this.setState({ totalbalance: res.balance });
            })
            .catch(err => {
                console.log(err);
        });
    }
    onClick = event => {
        const data = {
            userid: localStorage.getItem(USER_ID)
        }
      readNotifyInfo(data)
        .then(res => {
           this.setState({ countnotify: '0'});
        })
        .catch(err => {
           console.log('Error updating notification info.', err);
        });
    }

    render() {

        return (
            <div className="header">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="media">
                                <div className="col-sm-4 dashboard_title">
                                    {/* <h5>Dashboard</h5> */}
                                </div>
                                <div className="col-sm-8 notificationicons">
                                    <ul className="float-right">
                                    {/* <li className="header-icon headericondiv"><i className="fa fa-search"></i> </li>
                                        <li className="header-icon envelopeicon headericondiv"><img src={mailIcon} alt="mailIcon" /><span className="counter">4</span></li>*/}
                                        <li className="header-icon doller headericondiv"><i className="fas fa-dollar-sign"></i> <span className="counter">{this.state.totalbalance}</span></li>
                                        <li className="header-icon notificationicon headericondiv">
                                            
                                                <button className="noticebtn" type="button" id="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" onClick={this.onClick}>
                                                    <img src={noticon} alt="noticon" /><span className="counter">{this.state.countnotify}</span>
                                                </button>
                                                <div className="dropdown-menu noticmenu" aria-labelledby="dropdownMenuButton">
                                                    <div className="notichead">Notifications<a href="/Notifications" className="noticeview">View all</a></div>
                                                     <ul className="noticul">
                                                    {this.state.notifications.length > 0 
                                                    ?
                                                    this.state.notifications.map(notification => {
                                                      return    <li key={notification.id}>
                                                                    <a className="dropdown-item noticitem" href="/Notifications">{notification.message}</a>
                                                                    {/*<span className="noticetime">34m</span>*/}
                                                                </li>
                                                                     
                                                     }):<li>No notification right now</li>}

                                    </ul>
                                                </div>
                                        </li>
                                        <li className="header-icon profileimg">
                                            <img src={prflimg} alt="peflimg" />
                                            <div className="dropdown">
                                                <button className="btn btn-secondary dropdown-toggle logoutbtn" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{this.state.username}</button>
                                                <div className="dropdown-menu logout-menu" aria-labelledby="dropdownMenuButton">
                                                    <ul className="logoutul">
                                                        <div className="logout-heading">Menus-list <i className="fas fa-cog"></i></div>
                                                        <li><a className="dropdown-item logoutlist" href="/Profile">Profile</a></li>
                                                        <li><a className="dropdown-item logoutlist" href="/renewpassword">Change Password</a></li>
                                                        <li><a className="dropdown-item logoutlist" href="">Business Setting</a></li>
                                                        <li><a className="dropdown-item logoutlist" href="/logout">Logout</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Header;
