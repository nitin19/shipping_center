import React from 'react';

import './inventorymanagement.css';

class InventoryManagement extends React.Component {

	render() {

		return(
 
			<div className="content-wrap universelclass">
				{/*<div className="row">
					<div className="col-sm-12">
						<h1 className="Inventory-head">Inventory Settings</h1>
					</div>
				</div>	*/}
				<div className="InManagg">
					<h1 className="Inventory-head">Inventory Settings</h1>
				<div className="InManag-first">
				<div className="row">
				<div className="col-sm-6 pakwidth">
					<div className="row">
						<div className="col-sm-12">
						<div className="invtryset">
							
							<h4>When shipping products that have NO STOCK remaining:</h4>
							<span><input type="radio" id="test3" name="radio-group" /><label for="test3">Allow the shipment</label></span>
							<span><input type="radio" id="test4" name="radio-group" /><label for="test4">Warn, but allow the shipment</label></span>
							<span><input type="radio" id="test5" name="radio-group" /><label for="test5">Block the shipment</label></span>
						</div>
						</div>
					</div>
					<div className="row">
						<div className="col-sm-12">
						<div className="invtryset">
							<h4>When shipping products that have an UNKNOWN stock level:</h4>
							<span><input type="radio" id="test6" name="radio-group1" /><label for="test6">Allow the shipment</label></span>
							<span><input type="radio" id="test7" name="radio-group1" /><label for="test7">Warn, but allow the shipment</label></span>
							<span><input type="radio" id="test8" name="radio-group1" /><label for="test8">Block the shipment</label></span>
						</div>
						</div>
					</div>
					<div className="row">
						<div className="col-sm-12">
						<div className="invtryset pack-head-ing">
							<h4>When shipping from a location that is not tracking inventory:</h4>
							<div className="run1" style={{position: 'relative'}}><label className="customcheck">Warn that inventory will not be adjusted<input type="checkbox" /><span className="checkmark"></span></label></div>
						</div>
						</div>
					</div>
					<div className="row">
						<div className="col-sm-12">
						<div className="invtryset">
							<h4>Create alerts for SKUs that are below a default threshold:</h4>
							<span className="enable-span">Default Threshold</span>
							
							<input id="input-number-mod" className="mod" type="number" placeholder="0" />
						</div>
						</div>
					</div>
					<div className="row">
						<div className="col-sm-12">
						<div className="invtryset">
							<h4>Enable Inventory Tracking for Products?</h4>
							<span className="enable-span">Enable Inventory Tracking</span>
							<label className="switch">
							  <input type="checkbox" />
							  <span className="slider round chag"></span>
							</label>
						</div>
						</div>
					</div>
				</div>
				
				<div className="col-sm-6 pakwidth">
					<div className="Checklist">
						<h4>Inventory Setup Checklist</h4>
						<div className="local">
						<h6>Link "Ship From" Locations</h6>
						<p>You haven't yet created a "Ship From" location. When you do, you can link it to an Inventory Warehouse as an "Inventory Source"</p>
						</div>
						<div className="local">
						<h6>Establish Stock Levels</h6>
						<p>Establish stock level data from the Products area - either manually, or by CSV import.</p>
						</div>
						<div className="local">
						<h6>Enable Inventory Tracking</h6>
						<p>Enabling Inventory Tracking will begin enforcing the rules you've set when shipping products.</p>
						</div>
					</div>
				</div>
				</div>
						
				</div>
				</div>
						
						<footer className="add_list_footer">
                            <div className="row">
                                <div className="col-sm-12">
                                    <p>© 2019 <a href="#" style={{color: 'rgb(7, 168, 222)'}}>VIP PARCEL</a></p>
                                </div>
                            </div>
                        </footer>
			</div>

			);

	}

}
export default InventoryManagement;