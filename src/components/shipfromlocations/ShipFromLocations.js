import React from 'react';

//import './inventory<a href="" className="namehigh">Warehouse</a>s.css';

class ShipFromLocations extends React.Component {

  render() {
   
    return( 
 
      <div className="container-fuild left-margin">
        <div className="col-lg-12">
            <div className="sorting-list">
                <h1>Ship From Locations</h1>
                <p>Your ship from locations provide origin information and shipment return information for packages shipped through ShipStation.
                 Your default <span style={{fontWeight: 'bold', color: '#000'}}>Ship From Location</span> will be applied to all incoming orders unless otherwise applied by an automation rule.</p>
            </div>
            {/*<div className="automation-para">
              <p>Automation rules help you perform tasks for every order you import. You can manage your rules below or create a new one.</p>
            </div>*/}

            <div className="custom_tabel wow bounceIn animated" data-wow-duration="4s">
 
        <table className="table ">
  <thead>
    <tr className="bg ">
      <th>Name</th>
      <th>Return to Address</th>
      <th>Active</th>
      <th>Edit</th>
      <th>Delete</th>
    </tr>
  </thead>
  <tbody>
    <tr className="wow animate fadeInLeft " data-wow-delay="0.4s">
      <td><a href="" className="namehigh">Warehouse</a></td>
      <td>Gourav,Binarydata,12,chandigarh,PA,123456</td>
      <td><span className="status"></span></td>
      <td><span className="edit_icon "><a href="# "><i className="fas fa-pencil-alt "></i>Edit</a></span></td>
      
      <th><span className="edit_icon "><a href="# "><i className="far fa-trash-alt "></i> Delete</a></span></th>
    </tr>
    <tr className="wow animate fadeInLeft" data-wow-delay="0.8s">
           
      <td><a href="" className="namehigh">Warehouse</a></td>
      <td>Gourav,Binarydata,12,chandigarh,PA,123456</td>
      <td><span className="status"></span></td>
      <td><span className="edit_icon "><a href="# "><i className="fas fa-pencil-alt "></i>Edit</a></span></td>
      
      <th><span className="edit_icon "><a href="# "><i className="far fa-trash-alt "></i> Delete</a></span></th>
    </tr>
    <tr className="wow animate fadeInLeft " data-wow-delay="1.4s">
         
      <td><a href="" className="namehigh">Warehouse</a></td>
      <td>Gourav,Binarydata,12,chandigarh,PA,123456</td>
      <td><span className="status"></span></td>
      <td><span className="edit_icon "><a href="# "><i className="fas fa-pencil-alt "></i>Edit</a></span></td>
     
      <th><span className="edit_icon "><a href="# "><i className="far fa-trash-alt "></i> Delete</a></span></th>
    </tr>
   <tr className="wow animate fadeInLeft " data-wow-delay="1.8s">
          
      <td><a href="" className="namehigh">Warehouse</a></td>
      <td>Gourav,Binarydata,12,chandigarh,PA,123456</td>
      <td><span className="status"></span></td>
      <td><span className="edit_icon "><a href="# "><i className="fas fa-pencil-alt "></i>Edit</a></span></td>
      
      <th><span className="edit_icon "><a href="# "><i className="far fa-trash-alt "></i> Delete</a></span></th>
    </tr>
    <tr className="wow animate fadeInLeft " data-wow-delay="2s"> 
          
      <td><a href="" className="namehigh">Warehouse</a></td>
      <td>Gourav,Binarydata,12,chandigarh,PA,123456</td>
      <td><span className="status"></span></td>
      <td><span className="edit_icon "><a href="# "><i className="fas fa-pencil-alt "></i>Edit</a></span></td>
      
      <th><span className="edit_icon "><a href="# "><i className="far fa-trash-alt "></i> Delete</a></span></th>
    </tr>
    <tr className="wow animate fadeInLeft " data-wow-delay="2s"> 
          
      <td><a href="" className="namehigh">Warehouse</a></td>
      <td>Gourav,Binarydata,12,chandigarh,PA,123456</td>
      <td><span className="status"></span></td>
      <td><span className="edit_icon "><a href="# "><i className="fas fa-pencil-alt "></i>Edit</a></span></td>
      
      <th><span className="edit_icon "><a href="# "><i className="far fa-trash-alt "></i> Delete</a></span></th>
    </tr>
    
    <tr className="wow animate fadeInLeft " data-wow-delay="2s"> 
          
      <td><a href="" className="namehigh">Warehouse</a></td>
      <td>Gourav,Binarydata,12,chandigarh,PA,123456</td>
      <td><span className="status"></span></td>
      <td><span className="edit_icon "><a href="# "><i className="fas fa-pencil-alt "></i>Edit</a></span></td>
      
      <th><span className="edit_icon "><a href="# "><i className="far fa-trash-alt "></i> Delete</a></span></th>
    </tr>
  </tbody>
</table>

       </div>
            
       </div>
      </div>

      );

  }

}
export default ShipFromLocations;