import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import { NavLink, Link, Route, Switch, Redirect } from 'react-router-dom';

import * as userActions from '../../actions/userActions';
import { scAxios } from '../..';
import { API_TOKEN_NAME, USER_ID} from '../../constants';
import { startUserSession } from '../userSession';

import home from '../../images/home.png';
import carticon from '../../images/carticon.png';
import sale from '../../images/sale.png';
import gt from '../../images/gt.png';

const getStoresCount = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/dashboard/storesCount', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getOrdersCount = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/dashboard/ordersCount', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getProductsCount = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/dashboard/productsCount', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getWarehousesCount = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/dashboard/warehousesCount', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

class DashboardCards extends React.Component {

    state = {
        stores_count: '',
        orders_count: '',
        products_count: '',
        warehouses_count: '',
    };

    componentDidMount() {
        const data = {
            userid: localStorage.getItem(USER_ID)
        }
        getStoresCount(data)
            .then(res => {
                this.setState({ stores_count: res.total_stores });
                console.log(res.total_stores);
            })
            .catch(err => {
                console.log(err);
                alert("Error occured fetching stores count. Check console");
        });

        getOrdersCount(data)
            .then(res => {
                this.setState({ orders_count: res.total_orders });
                console.log(res.total_orders);
            })
            .catch(err => {
                console.log(err);
                alert("Error occured fetching orders count. Check console");
        });

        getProductsCount(data)
            .then(res => {
                this.setState({ products_count: res.total_products });
                console.log(res.total_products);
            })
            .catch(err => {
                console.log(err);
                alert("Error occured fetching products count. Check console");
        });

        getWarehousesCount(data)
            .then(res => {
                this.setState({ warehouses_count: res.total_warehouses });
                console.log(res.total_warehouses);
            })
            .catch(err => {
                console.log(err);
                alert("Error occured fetching locations count. Check console");
        });     
             
    }

    render() {
        return (
            <div className="row" >
                <div className="col-lg-3 boxdivsec wow fadeInLeft animated" data-wow-delay="0.2s" data-wow-duration="1s">
                    <div className="card skybluebox">
                        <div className="boxcontent wow fadeInLeft animated" data-wow-delay="0.4s">
                          <a href="/viewmystore">
                            <div className="boximgdiv">
                                <img src={home} alt="home"/>
                            </div>
                            <div className="boxcontentdiv">
                                <h4>Store</h4>
                                <h3>{this.state.stores_count}</h3>
                            </div>
                          </a>
                        </div>
                    </div>
                </div>
                <div className="col-lg-3 boxdivsec wow fadeInLeft animated" data-wow-delay="0.6s" data-wow-duration="2s">
                    <div className="card purplebox">
                        <div className="boxcontent wow fadeInLeft animated" data-wow-delay="0.8s">
                          <a href="/storeorder">
                            <div className="boximgdiv">
                                <img src={carticon} alt="cartIcon"/>
                            </div>
                            <div className="boxcontentdiv">
                                <h4>Products</h4>
                                <h3>{this.state.products_count}</h3>
                            </div>
                           </a>
                        </div>
                    </div>
                </div>
                <div className="col-lg-3 boxdivsec wow fadeInLeft animated" data-wow-delay="0.8s" data-wow-duration="3s">
                    <div className="card bluebox">
                        <div className="boxcontent wow fadeInLeft animated" data-wow-delay="0.10s">
                          <a href="/storeorder">
                            <div className="boximgdiv">
                                <img src={sale} alt="sale"/>
                            </div>
                            <div className="boxcontentdiv">
                                <h4>Orders</h4>
                                <h3>{this.state.orders_count}</h3>
                            </div>
                          </a>
                        </div>
                    </div>
                </div> 
                <div className="col-lg-3 boxdivsec wow fadeInLeft animated" data-wow-delay="0.14s" data-wow-duration="4s">
                    <div className="card pinkbox">
                        <div className="boxcontent wow fadeInLeft animated" data-wow-delay="0.16s">
                           <a href="/warehouse-lists">
                            <div className="boximgdiv">
                                <img src={gt} alt="gt"/>
                            </div>
                            <div className="boxcontentdiv">
                                <h4>Locations</h4>
                                <h3>{this.state.warehouses_count}</h3>
                            </div>
                           </a>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

export default DashboardCards;