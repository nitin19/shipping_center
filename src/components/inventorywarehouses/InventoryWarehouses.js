import React from 'react';

//import './inventorywarehouses.css';

class InventoryWarehouses extends React.Component {

  render() {
   
    return( 
 
      <div className="content-wrap universelclass">
        <div className="col-lg-12">
            <div className="sorting-list">
                <h1>Inventory Warehouses</h1>
                <p>Stock for your products can be distributed across one or more Inventory Warehouses. For each one, provide a unique name, the number of warehouse location dimensions (e.g. A27-05 has two dimensions, but A-27-05 has three), and linked ‘Ship From’ location.</p>
            </div>
           
            <div className="custom_tabel wow bounceIn animated" data-wow-duration="4s">
                <table className="table ">
                  <thead>
                    <tr className="bg ">
                      <th className="collumn5">Name</th>
                      <th className="collumn5">Linked to Ship From</th>
                      <th className="collumn5">Edit</th>
                      <th className="collumn5">Delete</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr className="wow animate fadeInLeft " data-wow-delay="0.4s">
                      <td className="Inventorytabl collumn5"><span className="customfloatvip">Inventory Warehouse</span></td>
                      <td className="Inventorytabl collumn5"><span className="customfloatvip"><a href="" className="namehigh">Manage Locations</a></span></td>
                      <td className="Inventorytabl collumn5"><span className="customfloatvip"><span className="edit_icon "><a href="# "><i className="fas fa-pencil-alt "></i></a></span></span></td>
                      <td className="Inventorytabl collumn5"><span className="customfloatvip"><span className="edit_icon "><a href="# "><i className="far fa-trash-alt "></i> </a></span></span></td>
                    </tr>
                    <tr className="wow animate fadeInLeft" data-wow-delay="0.8s">
                      <td className="Inventorytabl collumn5"><span className="customfloatvip">Inventory Warehouse</span></td>
                      <td className="Inventorytabl collumn5"><span className="customfloatvip"><a href="" className="namehigh">Manage Locations</a></span></td>
                      <td className="Inventorytabl collumn5"><span className="customfloatvip"><span className="edit_icon "><a href="# "><i className="fas fa-pencil-alt "></i></a></span></span></td>
                      <td className="Inventorytabl collumn5"><span className="customfloatvip"><span className="edit_icon "><a href="# "><i className="far fa-trash-alt "></i> </a></span></span></td>
                    </tr>
                    <tr className="wow animate fadeInLeft " data-wow-delay="1.4s">
                      <td className="Inventorytabl collumn5"><span className="customfloatvip">Inventory Warehouse</span></td>
                      <td className="Inventorytabl collumn5"><span className="customfloatvip"><a href="" className="namehigh">Manage Locations</a></span></td>
                      <td className="Inventorytabl collumn5"><span className="customfloatvip"><span className="edit_icon "><a href="# "><i className="fas fa-pencil-alt "></i></a></span></span></td>
                      <td className="Inventorytabl collumn5"><span className="customfloatvip"><span className="edit_icon "><a href="# "><i className="far fa-trash-alt "></i> </a></span></span></td>
                    </tr>
                   <tr className="wow animate fadeInLeft " data-wow-delay="1.8s">
                      <td className="Inventorytabl collumn5"><span className="customfloatvip">Inventory Warehouse</span></td>
                      <td className="Inventorytabl collumn5"><span className="customfloatvip"><a href="" className="namehigh">Manage Locations</a></span></td>
                      <td className="Inventorytabl collumn5"><span className="customfloatvip"><span className="edit_icon "><a href="# "><i className="fas fa-pencil-alt "></i></a></span></span></td>
                      <td className="Inventorytabl collumn5"><span className="customfloatvip"><span className="edit_icon "><a href="# "><i className="far fa-trash-alt "></i> </a></span></span></td>
                    </tr>
                    <tr className="wow animate fadeInLeft " data-wow-delay="2s"> 
                      <td className="Inventorytabl collumn5"><span className="customfloatvip">Inventory Warehouse</span></td>
                      <td className="Inventorytabl collumn5"><span className="customfloatvip"><a href="" className="namehigh">Manage Locations</a></span></td>
                      <td className="Inventorytabl collumn5"><span className="customfloatvip"><span className="edit_icon "><a href="# "><i className="fas fa-pencil-alt "></i></a></span></span></td>
                      <td className="Inventorytabl collumn5"><span className="customfloatvip"><span className="edit_icon "><a href="# "><i className="far fa-trash-alt "></i> </a></span></span></td>
                    </tr>
                    <tr className="wow animate fadeInLeft " data-wow-delay="2s"> 
                      <td className="Inventorytabl collumn5"><span className="customfloatvip">Inventory Warehouse</span></td>
                      <td className="Inventorytabl collumn5"><span className="customfloatvip"><a href="" className="namehigh">Manage Locations</a></span></td>
                      <td className="Inventorytabl collumn5"><span className="customfloatvip"><span className="edit_icon "><a href="# "><i className="fas fa-pencil-alt "></i></a></span></span></td>
                      <td className="Inventorytabl collumn5"><span className="customfloatvip"><span className="edit_icon "><a href="# "><i className="far fa-trash-alt "></i> </a></span></span></td>
                    </tr>
                    
                    <tr className="wow animate fadeInLeft " data-wow-delay="2s"> 
                      <td className="Inventorytabl collumn5"><span className="customfloatvip">Inventory Warehouse</span></td>
                      <td className="Inventorytabl collumn5"><span className="customfloatvip"><a href="" className="namehigh">Manage Locations</a></span></td>
                      <td className="Inventorytabl collumn5"><span className="customfloatvip"><span className="edit_icon "><a href="# "><i className="fas fa-pencil-alt "></i></a></span></span></td>
                      <td className="Inventorytabl collumn5"><span className="customfloatvip"><span className="edit_icon "><a href="# "><i className="far fa-trash-alt "></i> </a></span></span></td>
                    </tr>
                  </tbody>
                </table>
              </div>
              
            <div className="paggination-text">
                <ul className="pagination pagination-sm">
                    <li className="page-item activelink"><a className="page-link" href="">Previous</a></li>
                    <li className="page-item"><a className="page-link" href="">1</a></li>
                    <li className="page-item"><a className="page-link" href="">2</a></li>
                    <li className="page-item"><a className="page-link" href="">3</a></li>
                    <li className="page-item"><a className="page-link" href="">Next</a></li>
                </ul>
            </div>
       </div>
          <footer className="add_list_footer">
              <div className="row">
                  <div className="col-sm-12">
                      <p>© 2019 <a href="#" style={{color: 'rgb(7, 168, 222)'}}>VIP PARCEL</a></p>
                  </div>
              </div>
          </footer>
      </div>

      );

  }

}
export default InventoryWarehouses;