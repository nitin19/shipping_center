import React from 'react';
import graphimg from '../../images/graphimg.png';
import * as userActions from '../../actions/userActions';
import { scAxios } from '../..';
import { API_TOKEN_NAME, USER_ID} from '../../constants';
import { startUserSession } from '../userSession';
import {Line} from 'react-chartjs-2';
import { MDBContainer } from "mdbreact";

const getTotalOrder = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/dashboard/ordersCount', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getCompletedOrder = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/dashboard/completedordercount', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getPendingOrder = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/dashboard/pendingordersCount', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getPartialOrder = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/dashboard/partialordersCount', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }

        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getNewOrder = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/dashboard/newordersCount', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getOrderProcessor = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/dashboard/orderprocessorcount', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getTotalOrderCount = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/dashboard/totalordercount', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

class DashboardOverallSummary extends React.Component {
    state={
        totalorders: '',
        totalcompletedorders:'',
        totalpendingorders:'',
        totalpartialorders:'',
        totalneworders:'',
        totalorderprocessor:'',
        dataDoughnut: {
            labels: ["TotalOrders", "Completed", "Pending", "New", "Partial", "Shippers"],
            datasets: [
              {
                data: [],
                backgroundColor: ["#46BFBD","#FDB45C","#4D5360","#F7464A", "#ff0000", '#000000'],
                hoverBackgroundColor: [
                  "#46BFBD",
                  "#FDB45C",
                  "#4D5360",
                  "#F7464A",
                  "#ff0000",
                  '#000000'
                ]
              }
            ]
        }
    };

    componentDidMount() {
        const data = {
            userid: localStorage.getItem(USER_ID)
        }
        getTotalOrder(data)
            .then(res => {
                this.setState({ totalorders: res.total_orders });
                console.log(res.total_orders);
            })
            .catch(err => {
                console.log(err);
                alert("Error occured fetching orders count. Check console");
        });

        getCompletedOrder(data)
            .then(res => {
                this.setState({ totalcompletedorders: res.total_orders });
                console.log(res.total_orders);
            })
            .catch(err => {
                console.log(err);
                alert("Error occured fetching orders count. Check console");
        });

        getPendingOrder(data)
            .then(res => {
                this.setState({ totalpendingorders: res.total_orders });
                console.log(res.total_orders);
            })
            .catch(err => {
                console.log(err);
                alert("Error occured fetching orders count. Check console");
        });

        getPartialOrder(data)
            .then(res => {
                this.setState({ totalpartialorders: res.total_orders });
                console.log(res.total_orders);
            })
            .catch(err => {
                console.log(err);
                alert("Error occured fetching orders count. Check console");
        });

        getNewOrder(data)
            .then(res => {
                this.setState({ totalneworders: res.total_orders });
                console.log(res.total_orders);
            })
            .catch(err => {
                console.log(err);
                alert("Error occured fetching orders count. Check console");
        });

        getOrderProcessor(data)
            .then(res => {
                this.setState({ totalorderprocessor: res.total_order_processor });
                console.log(res.total_order_processor);
            })
            .catch(err => {
                console.log(err);
                alert("Error occured fetching orders count. Check console");
        });

        getTotalOrderCount(data)
            .then(res => {
                let newState = this.state;
                newState.dataDoughnut.datasets[0].data = res.total_order;
                this.setState({ ...newState });
                console.log('yours',res.total_order);
                console.log('mine',this.state);
            })
            .catch(err => {
                console.log(err);
                alert("Error occured fetching Completed orders count. Check console");
        });
    }
    render() {
        return (
            <div className="col-lg-12 tablesection">
                <div className="card tablecard wow fadeInLeftBig animated" data-wow-delay="0.4s" data-wow-duration="2s">
                    <div className="row card-title wow fadeInLeftBig animated" data-wow-delay="0.2s" data-wow-duration="2s"> 
                        <div className="col-sm-4">
                            <h4 className="boxtitles text-uppercase"><span>Overall Summary</span> </h4>
                        </div>
                        {/*<div className="col-sm-8 dashboardrightbar">
                            <ul>
                                <li><a href="#">Week</a></li>
                                <li className="active"><a href="#">Month</a></li>
                            </ul>
                            <div className="datepickerdiv">
                                <div className="form-group">
                                    <input type="date" name="date" id="date" placeholder="DD / MM / YY" />
                                </div>
                                <span>To</span>
                                <div className="form-group">
                                    <input type="date" name="date" id="date" placeholder="DD / MM / YY" />
                                </div>
                            </div>
                        </div>*/}
                    </div>
                    <div className="row">
                        <div className="col-sm-2 summaryboxdiv wow fadeInLef animated" data-wow-delay="0.2s">
                            <div className="summarybox">
                                <h5>Total Order</h5>
                                <h2>{this.state.totalorders}</h2>
                            </div>
                        </div>
                        <div className="col-sm-2 summaryboxdiv wow fadeInLeft animated" data-wow-delay="0.4s">
                            <div className="summarybox">
                                <h5>Complete Orders</h5>
                                <h2>{this.state.totalcompletedorders}</h2>
                            </div>
                        </div>
                        <div className="col-sm-2 summaryboxdiv wow fadeInLeft animated" data-wow-delay="0.6s">
                            <div className="summarybox">
                                <h5>Pending Orders</h5>
                                <h2>{this.state.totalpendingorders}</h2>
                            </div>
                        </div>
                        <div className="col-sm-2 summaryboxdiv wow fadeInLeft animated" data-wow-delay="0.8s">
                            <div className="summarybox">
                                <h5>Partial Orders</h5>
                                <h2>{this.state.totalpartialorders}</h2>
                            </div>
                        </div>
                        <div className="col-sm-2 summaryboxdiv wow fadeInLeft animated" data-wow-delay="0.9s">
                            <div className="summarybox">
                                <h5>New Orders</h5>
                                <h2>{this.state.totalneworders}</h2>
                            </div>
                        </div>
                        <div className="col-sm-2 summaryboxdiv wow fadeInLeft animated" data-wow-delay="0.9s">
                            <div className="summarybox">
                                <h5>Shippers </h5>
                                <h2>{this.state.totalorderprocessor}</h2>
                            </div>
                        </div>
                    </div>
                    <div className="row wow fadeInBig animated" data-wow-delay="0.4s" data-wow-duration="4s">
                        <div className="col-sm-12 graphimg">
                            < Line data={this.state.dataDoughnut} options={{ responsive: true }} height={200} width={700}/>
                        </div>
                    </div>
                </div>
            </div>
        );
    };
}

export default DashboardOverallSummary;