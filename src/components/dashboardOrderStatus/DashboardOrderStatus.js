import React from 'react';
import amazon from '../../images/amazon.png';
import line from '../../images/line.png';
import arrow from '../../images/arroe.png';
import msg from '../../images/msg.png';
import chasse from '../../images/chasse.png';
import line2 from '../../images/line2.png';
import msg2 from '../../images/msg2.png';
import ebay from '../../images/ebay.png';
import line3 from '../../images/line3.png';
import msg3 from '../../images/msg3.png';
import cart from '../../images/cart.png';
import line4 from '../../images/line4.png';

import * as userActions from '../../actions/userActions';
import { scAxios } from '../..';
import { API_TOKEN_NAME, USER_ID} from '../../constants';
import { startUserSession } from '../userSession';

const getOrderItemList = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/dashboard/orderstatus', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }                                                                                                                              
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}
class DashboardOrderStatus extends React.Component {
    state = {
        order_status_data: []
    };
    componentDidMount() {
        const data = {
            userid: localStorage.getItem(USER_ID)
        }
        getOrderItemList(data)
            .then(res => {
                    this.setState({ order_status_data: res.order_status });
                    console.log(res.order_status);
            })
            .catch(err => {
                    console.log(err);
                 //   alert("Error occured fetching order status. Check console");
            })     
    }

renderStoreSwitch(order_status) {
  switch(order_status) {
    case '1':
      return {amazon};
    case '2':
      return {ebay};
    default:
      return {amazon};
  }
}

    render() {
        return (
            <div className="col-sm-8 nopadding tablesection tableleftsection">
                <div className="referredsec">
                    <div className="card tablecard orderstatusdiv card-title wow fadeInLeftBig" data-wow-delay="0.4s">
                        <h4 className="boxtitles text-uppercase"><span>Orders Status </span><a href="#" className="viewallbtn">View All</a></h4>
                        <div className="tablediv">
                            <table>
                                <tbody>
                                    { this.state.order_status_data.length > 0 ?
                                        this.state.order_status_data.map(order_status => {
                                    return(<tr className="tabledata wow fadeInLeft animated" data-wow-delay="0.2s" data-wow-duration="1s">
                                        <td className="column7 column ordersname text-uppercase" key={order_status.id}>{order_status.item_name}<span>id {order_status.orderid}</span></td>
                                        <td className="column3 column text-center"> 
                                          <img src={this.renderStoreSwitch(order_status)} alt="amazon" /> 
                                           <img src={( '/images/amazon.png' )} alt="amazon" />   
                                        </td>
                                        <td className="location column7 column text-center"><span>{order_status.warehouse_city}</span>
                                            <img src={line} alt="line" /><span></span></td>
                                        <td className="column2 column">
                                            <img src={msg} alt="msg" /></td>
                                        <td className="column1 column">
                                            <img src={arrow} alt="arrow" /></td>
                                    </tr>);
                                    })
                                    :
                                    <tr className="tabledata"><td className="column20 column">No Records Found</td></tr>
                                    }

                                    {/*<tr className="tabledata wow fadeInLeftBig animated" data-wow-delay="0.4s" data-wow-duration="2s">
                                        <td className="column7 column ordersname text-uppercase">Nike Duel racer<span>id 383247</span></td>
                                        <td className="column3 column text-center"><img src={chasse} alt="chasse" /></td>
                                        <td className="location column column7 text-center"><span>California</span><img src={line2} alt="line2" /><span>Alaska</span></td>
                                        <td className="column2 column"><img src={msg2} alt="msg2" /></td>
                                        <td className="column1 column"><img src={arrow} alt="arrow" /></td>
                                    </tr>
                                    <tr className="tabledata wow fadeInLeftBig animated" data-wow-delay="0.6s" data-wow-duration="3s">
                                        <td className="column7 column ordersname text-uppercase">Nike LUNARCHARGE ESSENTIAL<span>id 383247</span></td>
                                        <td className="column3 column text-center"><img src={ebay} alt="ebay" /></td>
                                        <td className="location column column7 text-center"><span>Alabama</span><img src={line3} alt="line3" /><span>Los Angeles</span></td>
                                        <td className="column2 column"><img src={msg3} alt="msg3" /></td>
                                        <td className="column1 column"><img src={arrow} alt="arrow" /></td>
                                    </tr>
                                    <tr className="tabledata wow fadeInLeftBig animated" data-wow-delay="0.8s" data-wow-duration="4s">
                                        <td className="column7 column ordersname text-uppercase">Nike LUNARCHARGE ESSENTIAL<span>id 383247</span></td>
                                        <td className="column3 column text-center"><img src={cart} alt="cart" /></td>
                                        <td className="location column column7 text-center"><span>Alabama</span><img src={line4} alt="line4" /><span>Alaska</span></td>
                                        <td className="column2 column"><img src={msg2} alt="msg2" /></td>
                                        <td className="column1 column"><img src={arrow} alt="arrow" /></td>
                                    </tr>
                                    <tr className="tabledata wow fadeInLeftBig animated" data-wow-delay="0.2s" data-wow-duration="5s">
                                        <td className="column7 column ordersname text-uppercase">Nike LUNARCHARGE ESSENTIAL<span>id 383247</span></td>
                                        <td className="column3 column text-center"><img src={cart} alt="cart" /></td>
                                        <td className="location column column7 text-center"><span>Alabama</span><img src={line4} alt="line4" /><span>Alaska</span></td>
                                        <td className="column2 column"><img src={msg2} alt="msg2" /></td>
                                        <td className="column1 column"><img src={arrow} alt="arrow" /></td>
                                    </tr>
                                    <tr className="tabledata wow fadeInLeftBig animated" data-wow-delay="0.4s" data-wow-duration="6s">
                                        <td className="column7 column ordersname text-uppercase">Nike LUNARCHARGE ESSENTIAL<span>id 383247</span></td>
                                        <td className="column3 column text-center"><img src={cart} alt="cart" /></td>
                                        <td className="location column column7 text-center"><span>Alabama</span><img src={line4} alt="line4" /><span>Alaska</span></td>
                                        <td className="column2 column"><img src={msg2} alt="msg2" /></td>
                                        <td className="column1"><img src={arrow} alt="arrow" /></td>
                                    </tr>*/}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        );
    };
}

export default DashboardOrderStatus;