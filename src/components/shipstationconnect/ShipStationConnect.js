import React from 'react';
import ReactDOM from 'react-dom';
import './shipstationconnect.css';

class ShipStationConnect extends React.Component {

	render() {

		return (

				<div className="content-wrap">
			    	<div className="col-sm-12">
			    		
			    		<div className="shipstation">
			    		<div className="shipstationhead">
			    			<h1>Get Connected to your devices!</h1>
			    		</div>
			    			<div className="shipstationbody">
				    			<p>ShipStation Connect allows you to seamlessly utilize a computers printers, scanners and more. Devices can be shared with any other user of your organization, so they can be accessed from any computer, anywhere! </p>
				    			<h2>If you have not installed ShipStation Connect...</h2>
				    			<p>Install ShipStation Connect below and you'll be able to seamessly access and share your computer's devices from within ShipStation.</p>
				    			<h6>"Unfortunately, we don't have a version of ShipStation Connect appropriate for your OS. Please send us feedback and let us know your OS and version so that we can factor it in to future versions!"</h6>
				    			<h2>If you already have ShipStation Connect installed on your computer...</h2>
				    			<p>Launch ShipStation Connect on your computer to activate it. You'll then be able to manage it here.</p>
				    			<div className="shipconnect-footer"><span style={{fontWeight: '600', color: '#000'}}>Please note:</span> Only the user that installed ShipStation Connect can manage it.</div>
			    			</div>
			    		</div>
			        </div>
			        <footer className="add_list_footer">   
	                    <div className="row">  
		                    <div className="col-sm-12">        
		                    	<p>© 2019 <a href="#" style={{color: '#2db4e3', textDecoration: 'none'}}>VIP PARCEL</a></p>    
		                    </div>  
                    	</div>
                	</footer>
			    </div>

			);

	}

}

export default ShipStationConnect;