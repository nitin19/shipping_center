import React from 'react';

import './setting.css'

import setodr from '../../images/Settings_Orders.svg';
import setpri from '../../images/Settings_Printing.svg';
import setlab from '../../images/Settings_Labels.svg';
import setcus from '../../images/Settings_Customers.svg';
import setacc from '../../images/Settings_Account.svg';


class Setting extends React.Component {
	render() {
		return(

				<div className="content-wrap">
					<div className="setg-page-row">
						<div className="row">
							<div className="col-md-4 wow fadeInLeft animated" data-wow-delay="0.4s">
								<div className="setting-box">
									<img src={setodr} alt="troly" />
									<h5>Import Orders</h5>
									<h6><a href="./sellingchannels" className="Selling-head">Selling Channels</a></h6>
									<p>Connect your stores and marketplaces so ShipStation can import your orders</p>
									<h6><a href="./automationrules" className="Selling-head">Automation Rules</a></h6>
									<p>Upon order import, performs tasks based on criteria (weight, order value, etc.)</p>
								</div>
							</div>
							<div className="col-md-4 wow fadeInLeft animated" data-wow-delay="0.6s">
								<div className="setting-box">
									<img src={setpri} alt="printer" />
									<h5>Printing</h5>
									<h6><a href="./printingsetup" className="Selling-head">Document Options</a></h6>
									<p>Choose documents and formats such as labels, pick lists, and packing slips</p>
									<h6><a href="./shipstationconnect" className="Selling-head">ShipStation Connect</a></h6>
									<p>The best way to connect to printers and USB scales</p>
								</div>
							</div>
							<div className="col-md-4 wow fadeInLeft animated" data-wow-delay="0.8s">
								<div className="setting-box">
									<img src={setacc} alt="account" />
									<h5>Account</h5>
									<h6><a href="./usermanagement" className="Selling-head">Users</a></h6>
									<p>Configure restrictions and permissions for all your users</p>
									<h6><a href="./subscriptionplans" className="Selling-head">Subscription</a></h6>
									<p>Update your billing information or your ShipStation plan</p>
									<h6><a href="./renewpassword" className="Selling-head">Change Password</a></h6>
									
								</div>
							</div>
						</div>
					
						<div className="row">
							<div className="col-md-4 wow fadeInLeft animated" data-wow-delay="0.4s">
								<div className="setting-box">
									<img src={setodr} alt="troly" />
									<h5>Customer Communication</h5>
									<h6><a href="./emailtemplates" className="Selling-head">Email Templates</a></h6>
									<p>Customize shipment and delivery notifications sent to your shoppers</p>
									<h6><a href="./packingslips" className="Selling-head">Packing Slips</a></h6>
									<p>This HTML template editor is available for Silver plans and above, incl. trials</p>
									<h6><a href="./brandedcustomer" className="Selling-head">Branded Customer Pages</a></h6>
									<p>Provide a branded tracking experience for your customers.</p>
								</div>
							</div>
							<div className="col-md-4 wow fadeInLeft animated" data-wow-delay="0.6s">
								<div className="setting-box">
									<img src={setcus} alt="customer" />
									<h5>Labels</h5>
									<h6><a href="" className="Selling-head">Shipping Carriers</a></h6>
									<p>Create or connect accounts and buy USPS postage</p>
									<h6><a href="./ShipFromLocations" className="Selling-head">Ship From Locations</a></h6>
									<p>Create and manage your "Ship From" and "Return To" addresses</p>
									<h6><a href="./packages-lists" className="Selling-head">Package Types</a></h6>
									<p>Customize the list of package types and services available to your users</p>
								</div>
							</div>
							<div className="col-md-4">
								
							</div>
						</div>
							<div className="paggination-text">
	                            <ul className="pagination pagination-sm">
	                                <li className="page-item activelink"><a className="page-link" href="">Previous</a></li>
	                                <li className="page-item"><a className="page-link" href="">1</a></li>
	                                <li className="page-item"><a className="page-link" href="">2</a></li>
	                                <li className="page-item"><a className="page-link" href="">3</a></li>
	                                <li className="page-item"><a className="page-link" href="">Next</a></li>
	                            </ul>
	                        </div>
					</div>
						<footer className="add_list_footer">
                            <div className="row">
                                <div className="col-sm-12">
                                    <p>© 2019 <a href="#" style={{color: 'rgb(7, 168, 222)'}}>VIP PARCEL</a></p>
                                </div>
                                
                            </div>
                        </footer>
				</div>

			);
	}
}

export default Setting;