import React from 'react';
import { validateUserToken } from '../PrivateRoute';
import { scAxios } from '../..';
import { Redirect } from 'react-router-dom';
import { MAIN_PAGE_PATH, LOGIN_PAGE_PATH, API_TOKEN_NAME, USER_ID, IMAGE_URL} from '../../constants';
import profile from '../../images/prflimg.png';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


const updateWarehouse = (data, url, imageData) => {
    let warehouse_image = new FormData();
    warehouse_image.append('warehouse_image', imageData);

    return new Promise((resolve, reject) => {
        scAxios.post(url, warehouse_image, {
            headers: {
                'accept': 'application/json',
                'Content-Type': `multipart/form-data; boundary=${warehouse_image._boundary}`,
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        }).then((response) => {
            return resolve(response.data)
        }).catch((error) => {
            return reject(error)
        });
    });
}

const viewWarehouse = (data, url) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request(url, {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

class EditWareHouse extends React.Component {
    state = {
        warehouse_image: profile,

        warehouse_image_data: '',
        warehouse_name: '',
        warehouse_address: '',
        warehouse_city: '',
        warehouse_state: '',
        warehouse_country: '',
        warehouse_phone: '',
    }

    handleChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    componentDidMount() {
        const data = {
            userid: localStorage.getItem(USER_ID)
        }
        var id = this.props.match.params.id;
        var url = '/warehouse/viewwarehouse/' + id;
        viewWarehouse(data, url)
            .then(res => {
                if (res.message) {
                    this.setState({ ...res.message, warehouse_image: IMAGE_URL + 'warehouseimage/' + res.message.warehouse_image });
                }
            })
            .catch(err => {
                console.log(err);
                alert("Error occured fetching warehouse. Check console");
            });
    }

    handleSubmit = event => {
        event.preventDefault();
        const warehouse = {
            userid: localStorage.getItem(USER_ID),
            warehouse_name: this.state.warehouse_name,
            warehouse_address: this.state.warehouse_address,
            warehouse_city: this.state.warehouse_city,
            warehouse_state: this.state.warehouse_state,
            warehouse_country: this.state.warehouse_country,
            warehouse_phone: this.state.warehouse_phone,
        }
        var id = this.props.match.params.id;
        var url = '/warehouse/update/' + id;
        updateWarehouse(warehouse, url, this.state.warehouse_image_data)
            .then(res => {
                if (res.status == 'Success') {
                    toast.success(res.message, {
                        position: toast.POSITION.BOTTOM_RIGHT
                    });
                    this.props.history.push('/warehouse-lists');
                } else if (res.status == 'Warning') {
                    toast.warn(res.message, {
                        position: toast.POSITION.BOTTOM_RIGHT
                    });
                } else {
                    toast.error(res.message, {
                        position: toast.POSITION.BOTTOM_RIGHT
                    });
                }
                this.setState({
                    warehouse_name: '',
                });
            })
            .catch(err => {
                console.log(err);
                toast.error('save warehouse error', {
                    position: toast.POSITION.BOTTOM_RIGHT
                });
            });
    }

    onImageChange = event => {
        if (event.target.files[0]) {
            const file = event.target.files[0];
            let reader = new FileReader();
            reader.readAsDataURL(file);

            reader.onload = event => {
                this.setState({ warehouse_image: event.target.result, warehouse_image_data: file });
            }
        }
    }

    render() {
        return (

            <div className="content-wrap universelclass">
                <div className="order-view2">
                    <div className="order-ware-head">
                        <h2>Edit WareHouse</h2>
                    </div>
                    <div className="order-view2-body">
                        <div className="position-text">
                            <p>Last Modified Date:</p>
                            <p>Nov 26, 2018</p>
                            {/*<a href="#" className="onetwo">Edit</a>*/}
                        </div>
                        <form onSubmit={this.handleSubmit}>
                            <div className="idauto wow animate fadeInLeft " data-wow-delay="0.2s">
                                <div className="labsec">
                                    <p>Warehouse Name</p>
                                </div>
                                <div className="writetext">
                                    <input type="text" className="form-control custom" name="warehouse_name" defaultValue={this.state.warehouse_name} onChange={this.handleChange} />
                                </div>
                            </div>

                            <div className="idauto wow animate fadeInLeft " data-wow-delay="0.4s">
                                <div className="labsec">
                                    <p>Warehouse Address</p>
                                </div>
                                <div className="writetext">
                                    <input type="text" className="form-control custom" name="warehouse_address" defaultValue={this.state.warehouse_address} onChange={this.handleChange} />
                                </div>
                            </div>

                            <div className="idauto wow animate fadeInLeft " data-wow-delay="0.6s">
                                <div className="labsec">
                                    <p>Warehouse City</p>
                                </div>
                                <div className="writetext">
                                    <input type="text" className="form-control custom" name="warehouse_city" defaultValue={this.state.warehouse_city} onChange={this.handleChange} />
                                </div>
                            </div>

                            <div className="idauto wow animate fadeInLeft " data-wow-delay="0.6s">
                                <div className="labsec">
                                    <p>Warehouse State</p>
                                </div>
                                <div className="writetext">
                                    <input type="text" className="form-control custom" name="warehouse_state" defaultValue={this.state.warehouse_state} onChange={this.handleChange} />
                                </div>
                            </div>

                            <div className="idauto wow animate fadeInLeft " data-wow-delay="0.6s">
                                <div className="labsec">
                                    <p>Warehouse Country</p>
                                </div>
                                <div className="writetext">
                                    <input type="text" className="form-control custom" name="warehouse_country" defaultValue={this.state.warehouse_country} onChange={this.handleChange} />
                                </div>
                            </div>

                            <div className="idauto wow animate fadeInLeft " data-wow-delay="0.6s">
                                <div className="labsec">
                                    <p>Warehouse phone</p>
                                </div>
                                <div className="writetext">
                                    <input type="text" className="form-control custom" name="warehouse_phone" defaultValue={this.state.phone_number} onChange={this.handleChange} />
                                </div>
                            </div>

                            <div className="idauto wow animate fadeInLeft" data-wow-delay="0.4s">
                                <div className="labsec">
                                    <p>upload Image</p>
                                </div>
                                <div className="writetext">
                                    <img src={this.state.warehouse_image} className="prdt-upload-img" alt="upload-img" />
                                    <div className="upload-btn-wrapper">
                                        <button className="btn">Upload Image</button>
                                        <input type='file' id='warehouse_image' onChange={this.onImageChange} />
                                    </div>
                                    
                                </div>
                            </div>

                            <div className="idauto wow animate fadeInLeft " data-wow-delay="0.2s">

                                <div className="writetext btnright">
                                    <button className="mainbtn">Update</button>
                                </div>
                            </div>
                        </form>
                        
                    </div>
                </div>
                <footer className="add_list_footer"><div className="row"><div className="col-sm-12"><p>© 2019 <a href="#" style={{color: 'rgb(7, 168, 222)'}}>VIP PARCEL</a></p></div></div></footer>
                <ToastContainer autoClose={5000} />
            </div>

        );
    }
}

export default EditWareHouse;

