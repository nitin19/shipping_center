import React from 'react';

import ind from '../../images/ind.png';
import one from '../../images/1.png';
import two from '../../images/2.png';
import usa from '../../images/usa.png';
import aus from '../../images/aus.png';
import eng from '../../images/eng.png';
import three from '../../images/3.png';
import four from '../../images/4.png';
import nzl from '../../images/nzl.png';
import five from '../../images/5.png';

class DashboardTopDestinations extends React.Component {
    render() {
        return (
            <div className="col-sm-7 nopadding tablesection wow fadeInRight animated" data-wow-delay="0.4s" data-wow-duration="2s">
                <div className="referredsec">
                    <div className="card tablecard orderstatusdiv card-title">
                        <h4 className="boxtitles text-uppercase"><span>Top Destinations </span><a href="#" className="viewallbtn">View All</a></h4>
                        <div className="tablediv">
                            <table>
                                <tbody>
                                    <tr className="tableheader">
                                        <th className="column7 column">Countries</th>
                                        <th className="column8 column"></th>
                                        <th className="column5 column">Total Shipments</th>
                                    </tr>
                                    <tr className="tabledata wow fadeInRightBig animated" data-wow-delay="0.2s" data-wow-duration="2s" data-wow-duration="1s">
                                        <td className="column7 column"><img src={ind} alt="ind" /><span className="text-uppercase"> India</span></td>
                                        <td className="column8 column barimg"><img src={one} alt="one" className="img-responsive" /></td>
                                        <td className="column5 column ttlshiping">33,333,023</td>
                                    </tr>
                                    <tr className="tabledata wow fadeInRightBig animated" data-wow-delay="0.4s" data-wow-duration="3s" data-wow-duration="2s">
                                        <td className="column7 column"><img src={usa} alt="usa" /><span className="text-uppercase"> USA</span></td>
                                        <td className="column8 column barimg"><img src={two} alt="two" className="img-responsive" /></td>
                                        <td className="column5 column ttlshiping">33,333,023</td>
                                    </tr>
                                    <tr className="tabledata wow fadeInRightBig animated" data-wow-delay="0.6s" data-wow-duration="4s" data-wow-duration="3s">
                                        <td className="column7 column"><img src={aus} alt="aus" /><span className="text-uppercase"> AUSTRALIA</span></td>
                                        <td className="column8 column barimg"><img src={three} alt="three" className="img-responsive" /></td>
                                        <td className="column5 column ttlshiping">33,333,023</td>
                                    </tr>
                                    <tr className="tabledata wow fadeInRightBig animated" data-wow-delay="0.8s" data-wow-duration="5s" data-wow-duration="4s">
                                        <td className="column7 column"><img src={eng} alt="eng" /><span className="text-uppercase"> ENG</span></td>
                                        <td className="column8 column barimg"><img src={four} alt="four" className="img-responsive" /></td>
                                        <td className="column5 column ttlshiping">33,333,023</td>
                                    </tr>
                                    <tr className="tabledata wow fadeInRightBig animated" data-wow-delay="0.9s" data-wow-duration="6s" data-wow-duration="5s">
                                        <td className="column7 column"><img src={nzl} alt="nzl" /><span className="text-uppercase"> NZL</span></td>
                                        <td className="column8 column barimg"><img src={five} alt="five" className="img-responsive" /></td>
                                        <td className="column5 column ttlshiping">33,333,023</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default DashboardTopDestinations;