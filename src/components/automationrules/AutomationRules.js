import React from 'react';
import ReactDOM from 'react-dom';
import './automationrules.css'

import ebay from '../../images/ebay.png';
import plush from '../../images/plush.png';
import amazon from '../../images/amazon-listing.png';
import cart32 from '../../images/cart32.png';
import usps from '../../images/usps-listing.png';
import seocart from '../../images/seocart.png';
import shiprobot from '../../images/logo-shiprobot.png';
import insure from '../../images/insure-listing.png';
import ReactTooltip from 'react-tooltip';

class AutomationRules extends React.Component {

	render() {

		return (

				<div className="content-wrap universelclass">
        <div className="col-lg-12">
            <div className="sorting-list">
                <h1>Automation Rules</h1>
                <div className="view-serch">
                  <form className="form_section">
                    <input name="searchdata" placeholder="Search..." id="search_keyword" value="" type="text" value="" />
                    <input className="Connect-btn" id="submit_btn" value="Search" type="button" />
                  </form>
                </div>
            </div>
            {/*<div className="automation-para">
              <p>Automation rules help you perform tasks for every order you import. You can manage your rules below or create a new one.</p>
            </div>*/}

            <div className="custom_tabel wow bounceIn animated" data-wow-duration="4s">
 
        <table className="table ">
  <thead>
    <tr className="bg ">
      <th>Rule Name</th>
      <th>Last Modified</th>
      <th>Active</th>
      <th>Edit</th>
      <th>Delete</th>
    </tr>
  </thead>
  <tbody>
    <tr className="wow animate fadeInLeft " data-wow-delay="0.4s">
      <td className="collumn4">Default Confirmation</td>
      <td className="collumn4">11/25/2018</td>
      <td className="collumn4"><span className="edit_icon "><span className="status orange "></span></span></td>
      <td className="collumn4">
        <span className="edit_icon" data-tip="Delete">
          <a href="# "><i className="fas fa-pencil-alt "></i></a>
            <ReactTooltip />
        </span>
      </td>
      <td className="collumn4">
        <span className="edit_icon"  data-tip="Delete">
          <a href="# "><i className="far fa-trash-alt "></i> </a>
            <ReactTooltip />
        </span>
      </td>
    </tr>
    <tr className="wow animate fadeInLeft" data-wow-delay="0.8s">
           
      <td className="collumn4">Default Confirmation</td>
      <td className="collumn4">11/25/2018</td>
      <td className="collumn4"><span className="edit_icon "><span className="status orange "></span></span></td>
      <td className="collumn4">
        <span className="edit_icon" data-tip="Delete">
          <a href="# "><i className="fas fa-pencil-alt "></i></a>
            <ReactTooltip />
        </span>
      </td>
      <td className="collumn4">
        <span className="edit_icon"  data-tip="Delete">
          <a href="# "><i className="far fa-trash-alt "></i> </a>
            <ReactTooltip />
        </span>
      </td>
    </tr>
    <tr className="wow animate fadeInLeft " data-wow-delay="1.4s">
         
      <td className="collumn4">Default Confirmation</td>
      <td className="collumn4">11/25/2018</td>
      <td className="collumn4"><span className="edit_icon "><span className="status orange "></span></span></td>
      <td className="collumn4">
        <span className="edit_icon" data-tip="Delete">
          <a href="# "><i className="fas fa-pencil-alt "></i></a>
            <ReactTooltip />
        </span>
      </td>
      <td className="collumn4">
        <span className="edit_icon"  data-tip="Delete">
          <a href="# "><i className="far fa-trash-alt "></i> </a>
            <ReactTooltip />
        </span>
      </td>
    </tr>
   <tr className="wow animate fadeInLeft " data-wow-delay="1.8s">
          
      <td className="collumn4">Default Confirmation</td>
      <td className="collumn4">11/25/2018</td>
      <td className="collumn4"><span className="edit_icon "><span className="status orange "></span></span></td>
      <td className="collumn4">
        <span className="edit_icon" data-tip="Delete">
          <a href="# "><i className="fas fa-pencil-alt "></i></a>
            <ReactTooltip />
        </span>
      </td>
      <td className="collumn4">
        <span className="edit_icon"  data-tip="Delete">
          <a href="# "><i className="far fa-trash-alt "></i> </a>
            <ReactTooltip />
        </span>
      </td>
    </tr>
    <tr className="wow animate fadeInLeft " data-wow-delay="2s"> 
          
      <td className="collumn4">Default Confirmation</td>
      <td className="collumn4">11/25/2018</td>
      <td className="collumn4"><span className="edit_icon "><span className="status orange "></span></span></td>
      <td className="collumn4">
        <span className="edit_icon" data-tip="Delete">
          <a href="# "><i className="fas fa-pencil-alt "></i></a>
            <ReactTooltip />
        </span>
      </td>
      <td className="collumn4">
        <span className="edit_icon"  data-tip="Delete">
          <a href="# "><i className="far fa-trash-alt "></i> </a>
            <ReactTooltip />
        </span>
      </td>
    </tr>
    <tr className="wow animate fadeInLeft " data-wow-delay="2s">
      <td className="collumn4">Default Confirmation</td>
      <td className="collumn4">11/25/2018</td>
      <td className="collumn4"><span className="edit_icon "><span className="status orange "></span></span></td>
      <td className="collumn4">
        <span className="edit_icon" data-tip="Delete">
          <a href="# "><i className="fas fa-pencil-alt "></i></a>
            <ReactTooltip />
        </span>
      </td>
      <td className="collumn4">
        <span className="edit_icon"  data-tip="Delete">
          <a href="# "><i className="far fa-trash-alt "></i></a>
            <ReactTooltip />
        </span>
      </td>
    </tr>
    
    <tr className="wow animate fadeInLeft " data-wow-delay="2s"> 
          
      <td className="collumn4">Default Confirmation</td>
      <td className="collumn4">11/25/2018</td>
      <td className="collumn4"><span className="edit_icon "><span className="status orange "></span></span></td>
      <td className="collumn4">
        <span className="edit_icon" data-tip="Delete">
          <a href="# "><i className="fas fa-pencil-alt "></i></a>
            <ReactTooltip />
        </span>
      </td>
      <td className="collumn4">
        <span className="edit_icon"  data-tip="Delete">
          <a href="# "><i className="far fa-trash-alt "></i> </a>
            <ReactTooltip />
        </span>
      </td>
    </tr>
  </tbody>
</table>

       </div>
            <div className="autorule-para wow fadeInLeftBig animated" data-wow-duration="4s">
              <p>To apply recent changes to product details, store mappings or automation rules to your current open orders, click Reprocess Automation Rules below. Warning: This may overwrite any shipping settings that have been made since the orders were imported.</p>
              <a href="#" className="viewallbtn">  Reprocess Automation Rules </a>
            </div>
       </div>
       <footer className="add_list_footer">   
                    <div className="row"> 
                        <div className="col-sm-12">          
                            <p>© 2019 <a href="#" style={{color: '#07a8de'}}>VIP PARCEL</a></p>    
                        </div>
                    </div>
                </footer>
      </div>

			);

	}

}

export default AutomationRules;