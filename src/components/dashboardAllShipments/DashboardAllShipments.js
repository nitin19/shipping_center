import React from 'react';

import cart2 from '../../images/cart2.png';
import {Doughnut} from 'react-chartjs-2';
import { MDBContainer } from "mdbreact";
import * as userActions from '../../actions/userActions';
import { scAxios } from '../..';
import { API_TOKEN_NAME, USER_ID} from '../../constants';
import { startUserSession } from '../userSession';

const getallOrdercount = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/dashboard/allordercount', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}
class DashboardAllShipments extends React.Component {
    state={
        dataDoughnut: {
            labels: ["Completed", "Pending", "New", "Partial"],
            datasets: [
              {
                data: [300, 50, 100, 40],
                backgroundColor: ["#46BFBD","#FDB45C","#4D5360","#F7464A"],
                hoverBackgroundColor: [
                  "#46BFBD",
                  "#FDB45C",
                  "#4D5360",
                  "#F7464A"
                ]
              }
            ]
        }
    }

    componentDidMount() {
        const data = {
            userid: localStorage.getItem(USER_ID)
        }
        getallOrdercount(data)
            .then(res => {
                let newState = this.state;
                newState.dataDoughnut.datasets[0].data = res.total_order;
                this.setState({ ...newState });
                console.log('yours',res.total_order);
                console.log('mine',this.state);
            })
            .catch(err => {
                console.log(err);
                alert("Error occured fetching Completed orders count. Check console");
        });
    }
    render() {
        return (
            <div className="col-sm-5 shipmentbox allshipment">
                <div className="shipmentbg card-title">
                    <h3 className="boxtitles text-uppercase text-white">All Shipments</h3>
                    <div className="ggg">
                    <MDBContainer>
                        {/*<h3 className="mt-5">Radar chart</h3>*/}
                        <Doughnut data={this.state.dataDoughnut} options={{ responsive: true }}  height={450} width={500} />
                    </MDBContainer>
                    </div>
                </div>
            </div>
        );
    }
}

export default DashboardAllShipments;