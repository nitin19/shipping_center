import React, { Component } from 'react';
import ReactDOM from 'react-dom'
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Redirect } from 'react-router-dom';
import { LOGIN_PAGE_PATH, API_TOKEN_NAME, USER_ROLE, USER_ID } from '../../constants';
import { scAxios } from '../..';
import { startUserSession } from '../userSession';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import './picklist.css';
import * as jsPDF from 'jspdf';
import * as html2canvas from 'html2canvas';

const getItemDetails = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/print/picklist', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
            
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

class PickList extends React.Component {
    state = {
        Details: [],
        total: '',
        currentPage: '',
        LastPage:'',
        PerPage: '',
        FirstPageUrl:'',
        NextPageUrl:'',
        PrevPageUrl:'',
        LastPageUrl:'',
        TotalPages:'',
        searchdata: '',
    }
refreshPickList = (page) => {
    const data = {
        usertype: localStorage.getItem(USER_ROLE),
        orders: this.props.match.params.orders,
        searchdata: this.state.searchdata
    }

    getItemDetails(data)
        .then(res => {
            if(res.status==1){
                var records = res.success.data;
                this.setState({ 
                    Details: records, 
                    total: res.success.total,
                    currentPage: res.success.current_page,
                    PerPage: res.success.per_page,
                    FirstPageUrl: res.success.first_page_url,
                    NextPageUrl: res.success.next_page_url,
                    PrevPageUrl: res.success.prev_page_url,
                    LastPageUrl: res.success.last_page_url,
                    LastPage: res.success.last_page,
                    TotalPages: Math.ceil(this.state.total / this.state.PerPage)
                });
            } else {
                this.setState({
                    Details: '',
                });
            }
        })
    .catch(err => {
        console.log(err);
        toast.error('Some Problem Exist, Check Console', {
              position: toast.POSITION.BOTTOM_RIGHT
        });
    });
}

print(){
    const content = document.getElementById('orderdata');
    window.print();
}

handleChange = () => {
    this.refreshPickList();
}

handlePdf = () => {
    const input = document.getElementById('orderdata');

    html2canvas(input)
        .then((canvas) => {
            const imgData = canvas.toDataURL('image/png');
            const pdf = new jsPDF('p', 'px', 'a4');
            var width = pdf.internal.pageSize.getWidth();
            var height = pdf.internal.pageSize.getHeight();

            pdf.addImage(imgData, 'PNG', 0, 0, width, height);
            pdf.save("Picklist.pdf");
        });
};

componentDidMount() {
    this.refreshPickList();
}

    render(){

        const currentPage = this.state.currentPage;
        const previousPage = currentPage - 1;
        const NextPage = currentPage + 1;
        const LastPage = this.state.LastPage;
        const pageNumbers = [];
        for (let i = 1; i <= this.state.TotalPages; i++) {
          pageNumbers.push(i);
        }
        let srno = 1;
    
        return(
                <div className="content-wrap universelclass">
                <div className="storemiddle-sec">
                    <div className="addstore-list-text">
                        <div className="col-sm-12 p-0">
                        <ul className="storelist">
                            <li><div className="example storeexam" action="/action_page.php">
                                <input type="text" placeholder="Process Batch" name="search2" />
                                <button type="submit"></button>
                            </div>
                            </li>
                            <li><a href="#" className="order-samebtn">Remove From Batch</a></li>
                            <li><a href="#" className="order-samebtn">Cancel Batch</a></li>
                            <li><a href="#" className="order-samebtn specelbtn" style={{ marginLeft: 10, marginRight: 10 }}>Bulk Action</a></li>
                            <li><a style={{cursor:'pointer'}} className="order-samebtn" onClick={this.print}>Print</a></li>
                            <li><a style={{cursor:'pointer'}} onClick={this.handlePdf} className="order-samebtn">Download Pdf</a></li>
                            <li className="pick-li">
                            <form className="form_section">
                            <input name="searchdata" placeholder="Search..." id="search_keyword" type="text" value={this.state.searchdata} onChange={event => this.setState({searchdata: event.target.value})}/>
                            <input className="Connect-btn" id="submit_btn" value="Search" type="button" onClick={this.handleChange}/>
                            </form>
                            </li>
                        </ul>
                        </div>
                    </div>
                    <hr />
                    
                </div>
                <div className="store-listing-table" id="orderdata" style= {{minHeight: '297mm'}}>
                    <div className="row storlisthead">
                       
                        <div className="col-sm-1">
                            <span className="listtxt-1">Sr. No.</span>
                        </div>
                        <div className="col-sm-3">
                            <span className="listtxt-1">Product Name</span>
                        </div>
                        <div className="col-sm-2"> 
                            <span className="listtxt-1">SKU Code</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listtxt-1">Quantity</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listtxt-1">Weight</span>
                        </div>
                        <div className="col-sm-4">
                            <span className="listtxt-1">Location</span>
                        </div>
                        
                    </div>
                    

                    {
                        this.state.Details.length > 0
                    ?
                        this.state.Details.map(itemDetail => {
                   return(<div className="row storlistbody" key={itemDetail.sellersku}>
                        
                        <div className="col-sm-1 pick-tabel-head">
                            <span className="listext-1">{srno++}</span>
                        </div>
                        
                        <div className="col-sm-3 pick-tabel-head"> 
                            <span className="listext-1"><a href="">{itemDetail.item_name}</a></span>
                        </div>
                        <div className="col-sm-2 pick-tabel-head">
                            <span className="listext-1">{itemDetail.sellersku}</span>
                        </div>
                        <div className="col-sm-1 pick-tabel-head">
                            <span className="listext-1">{itemDetail.qtys}</span>
                        </div>
                        
                        <div className="col-sm-1 pick-tabel-head">
                            <span className="listext-1">{itemDetail.weight}</span>
                        </div>

                        <div className="col-sm-4 pick-tabel-head">
                            <span className="listext-1">{itemDetail.address}</span>
                        </div>
                        
                    </div>);
                })
                    :
  
    <div className="no_records">
       <h2>No Records founds!</h2>
    </div>
  
}

{ pageNumbers.length > 1 ?

<div className="paggination-text">
  <ul className="pagination pagination-sm">
    {this.state.PrevPageUrl != null ? 
  <li className="page-item"><a className="page-link" style={{cursor:'pointer'}} onClick={this.refreshStoreOrderList.bind(this, previousPage)}>Previous</a></li>
  : ''
}
      {pageNumbers.map(page => {
      return (<li className={currentPage == page ? 'activelink page-item' : 'page-item' } >
        <a className="page-link" style={{cursor:'pointer'}} onClick={this.refreshStoreOrderList.bind(this, page)}>{page}</a>
        </li>);
    })}

   {this.state.NextPageUrl != null ? 
  <li className="page-item"><a className="page-link" style={{cursor:'pointer'}} onClick={this.refreshStoreOrderList.bind(this, NextPage)}>Next</a></li>
  : ''
}
    {this.state.LastPage != null && currentPage!=LastPage? 
  <li className="page-item"><a className="page-link" style={{cursor:'pointer'}} onClick={this.refreshStoreOrderList.bind(this, LastPage)} >Last</a></li>
  : ''
}
  
  </ul>
</div> : ''
}

                </div>
                <footer className="add_list_footer">   
                    <div className="row"> 
                    <div className="col-sm-12">          
                    <p>© 2018 <a href="#" style={{color: '#07a8de'}}>VIP PARCEL</a></p>    
                    </div>
                    
                    </div>
                </footer>
            </div>

            );
 
    }

}
export default PickList;
