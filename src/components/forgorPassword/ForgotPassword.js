import React from 'react';
import { Link } from 'react-router-dom'

import registerLogo from '../../images/register-logo.png';
import './forgotPassword.css';
import { scAxios } from '../..';
import { API_TOKEN_NAME } from '../../constants';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const forgotPasswordApi = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/password/create', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

class ForgotPassword extends React.Component {

    state = {
        email: '',
        emailSuccess: false,
        fields: {},
        errors: {},
    };

    validateForm() {
      let fields = this.state.fields;
      let errors = {};
      let formIsValid = true;

    if (!fields["email"]) {
        formIsValid = false;
        errors["email"] = "*Please enter your email.";
      }

    if (typeof fields["email"] !== "undefined") {
          let lastAtPos = fields["email"].lastIndexOf('@');
          let lastDotPos = fields["email"].lastIndexOf('.');
          if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["email"].indexOf('@@') == -1 && lastDotPos > 2 && (fields["email"].length - lastDotPos) > 2)) {
              formIsValid = false;
              errors["email"] = "Email is not valid";
            }
      }

      this.setState({
        errors: errors
      });
      return formIsValid;

    }

    handleChange = event => {

       let fields = this.state.fields;
        fields[event.target.name] = event.target.value;
        this.setState({
        fields
        }, () => this.validateForm()); 
    }

    handleCheckBoxChange = event => {
        this.setState({
            [event.target.name]: event.target.checked
        });
    }

    handleSubmit = event => {
        event.preventDefault();

        if (this.validateForm()) {

        let fields = {};
          fields["email"] = "";
          this.setState({fields:fields});

          const user = {
            email: this.state.fields.email,
         }

            forgotPasswordApi(user)
                .then(res => {
                    if(res.status=='1'){
                        toast.success(res.message, {
                            position: toast.POSITION.BOTTOM_RIGHT
                        });
                    } else {
                        toast.error(res.message, {
                            position: toast.POSITION.BOTTOM_RIGHT
                        });
                    }
                    //this.setState({ emailSuccess: true });
                })
                .catch(err => {
                    console.log('Error requesting forgot password email', err);
                });
        }
        else
           toast.error('Please provide email !', {
                  position: toast.POSITION.BOTTOM_RIGHT
            });
    }

    render() {
        return (
            <div className="register-page">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-4 col-md-5 regst-img cus-height">
                            <div className="left-register">
                                <Link to="/signup"><img src={registerLogo} alt="vip-logo" className="vip-logo animated fadeInLefte" /> </Link>
                                <h6 className="animated fadeInLeft">Reset your</h6>
                                <h1 className="animated fadeInLeft">Password</h1>
                                <h5 className="animated fadeInLeft">VIPparcel is the Nation's leading online postage service to buy and print discounted USPS labels - domestic and international - from the comfort of your desk. We guarantee to save you time and money by making your shipping process easy, affordable, and efficient.</h5>
                            </div>
                        </div>
                        <div className="col-lg-8 col-md-7 regst-input">
                            <h2 className="animated fadeInLeft">Forgot Password</h2>

                            {this.state.emailSuccess && <h6 style={{ textAlign: 'center' }}>Password Reset email sent successfully.</h6>}

                            <form onSubmit={this.handleSubmit} className="login-div">
                                <div className="row mar-bot">
                                    <div className="col-sm-12 padd-normal">
                                        <input type="text" name="email" value={this.state.fields.email} onChange={this.handleChange} placeholder="Email Address *" className="email-one animated fadeInLeft" />
                                        <div className="errorMsg">{this.state.errors.email}</div>
                                    </div>
                                </div>
                                <div className="row mar-bot">
                                    <div className="col-sm-12 padd-normal">
                                        <div style={{ textAlign: "center" }}>
                                            <button type="submit" className="graydent-color animated fadeInDown" value="Send reset Password Link" >Send reset Password Link</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <ToastContainer autoClose={5000} />
            </div>
        );
    }
}

export default ForgotPassword;