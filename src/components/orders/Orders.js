import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Redirect } from 'react-router-dom';
import { LOGIN_PAGE_PATH, API_TOKEN_NAME,USER_ID } from '../../constants';
import { scAxios } from '../..';
import { startUserSession } from '../userSession';
import eBay from '../../images/myStores/ebayLogo.png';
import amazonLogoTra from '../../images/myStores/amazon-logo_tra.png';
import './orders.css';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import * as jsPDF from 'jspdf';
import * as html2canvas from 'html2canvas';

import loadingimage from '../../images/small_loader.gif';
import ReactTooltip from 'react-tooltip';
import Popover from "react-awesome-popover";
import "react-awesome-popover/dest/react-awesome-popover.css";
import Skeleton from 'react-loading-skeleton';
import {Animated} from "react-animated-css";
import WOW from "wowjs";

const getOrders = (url, data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request(url, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
            
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getStatus = () => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/orders/status', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const changeOrderStatus = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/orders/changeItemstatus', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
            
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

class Orders extends React.Component {

    state = {
        order: [],
        orderitems: [],
        total: '',
        currentPage: '',
        LastPage:'',
        PerPage: '',
        FirstPageUrl:'',
        NextPageUrl:'',
        PrevPageUrl:'',
        LastPageUrl:'',
        TotalPages:'',
        searchdata:'',
        loadingFirst: true,
    }


    handleChange = event => {
        this.setState({ [event.target.name]: event.target.value });
        this.refreshOrderItemList();
    }

    handleSubmit = event => {
        event.preventDefault();
    }

    refreshOrderItemList = () => {
        var id =  this.props.match.params.id;
        var url = 'stores/order/details/'+id;

        const data = {
            searchdata: this.state.searchdata,
            userid: localStorage.getItem(USER_ID)
        }
        getOrders(url, data)
            .then(res => {
                this.setState({ loadingFirst: false });
                if(res.status==1){
                var order = res.Order;
                var orderitems = res.OrderItems.data;
                this.setState({ 
                    order: order,
                    orderitems: orderitems,
                    total: res.OrderItems.total,
                    currentPage: res.OrderItems.current_page,
                    PerPage: res.OrderItems.per_page,
                    FirstPageUrl: res.OrderItems.first_page_url,
                    NextPageUrl: res.OrderItems.next_page_url,
                    PrevPageUrl: res.OrderItems.prev_page_url,
                    LastPageUrl: res.OrderItems.last_page_url,
                    LastPage: res.OrderItems.last_page,
                    TotalPages: Math.ceil(this.state.total / this.state.PerPage)
                 });
                } else {
                    this.setState({ orderitems: '' }); 
                    this.setState({ order: '' });
                }
            })
            .catch(err => {
                console.log(err);
            });
    }

print(){
const content = document.getElementById('orderdata');
    window.print();
}

handlePdf = () => {
    const input = document.getElementById('orderdata');

    html2canvas(input)
        .then((canvas) => {
            const imgData = canvas.toDataURL('image/png');
            const pdf = new jsPDF('p', 'px', 'a4');
            var width = pdf.internal.pageSize.getWidth();
            var height = pdf.internal.pageSize.getHeight();

            pdf.addImage(imgData, 'PNG', 0, 0, width, height);
            pdf.save("Order.pdf");
        });
}

componentDidMount() {
    this.refreshOrderItemList();

    getStatus()
        .then(res => {
          var orderStatus = res.success;
            this.setState({ orderStatus: orderStatus });
        })
        .catch(err => {
            console.log(err);
        });
}

StatusChange = (event) => {
    const data = {
        itemid: event.target.id,
        orderstatus: event.target.value
    }

    changeOrderStatus(data)
        .then(res => {
            if(res.status==1){
                toast.success(res.message, {
                  position: toast.POSITION.BOTTOM_RIGHT
                });
            } else {
                toast.error(res.message, {
                  position: toast.POSITION.BOTTOM_RIGHT
                });
            }
            this.refreshOrderItemList();
        })
        .catch(err => {
            console.log(err);
        });
}

renderSwitch(param) {
  switch(param) {
    case 'new':
      return 'New';
    case 'in_process':
      return 'In Process';
    case 'shipped':
      return 'Shipped';
    case 'cancel':
      return 'Cancel';
    case 'full_refund':
      return 'Full Refund';
    case 'partial_refund':
      return 'Partial Refund';
    case 'partial_shipped':
      return 'Partial Shipped';
    case 'completed':
      return 'Completed';
    default:
      return 'new';
  }
}

    render() {
    const wow = new WOW.WOW();
    wow.init();
    const currentPage = this.state.currentPage;
    const previousPage = currentPage - 1;
    const NextPage = currentPage + 1;
    const LastPage = this.state.LastPage;
    const pageNumbers = [];
    for (let i = 1; i <= this.state.TotalPages; i++) {
      pageNumbers.push(i);
    }
    let srno = 1;

        return (
           
            <>     
            {this.state.loadingFirst==true ?
                <div className="content-wrap backcollor">
                    <div className="ms-stor">
                        <div className="row mstop">
                            <div className="col-sm-4">
                                {this.props.title || <Skeleton count={1} duration={2} height={30} />}
                            </div>
                            <div className="col-sm-4">
                                
                            </div>
                            <div className="col-sm-4">
                                {this.props.title || <Skeleton count={1} duration={2} height={30} />}
                            </div>
                        </div>
                        <div className="row mstop">
                            <div className="col-sm-2">
                                {this.props.title || <Skeleton count={1} duration={2} height={30} />}
                            </div>
                            <div className="col-sm-2">
                                {this.props.title || <Skeleton count={1} duration={2} height={30} />}
                            </div>
                            <div className="col-sm-8">
                                
                            </div>
                        </div>
                      <p>{this.props.title || <Skeleton count={1} duration={2} />}</p>
                        <div className="row mstop">
                            <div className="col-sm-12">
                                {this.props.title || <Skeleton count={1} duration={2} height={300} />}
                            </div>
                        </div>
                      <div className="mystoreboxs">
                          <h1>{this.props.title || <Skeleton count={5} duration={2} />}</h1>
                      </div>
                    </div>
                </div>
                : 

            <div className="content-wrap universelclass">   
                <div className="order-summary-back">   
                <div className="order-summary">  
                    <div className="row">
                        <div className="col-sm-12"> 
                            <div className="sorting-list viewstoree wow animated fadeInLeft" data-wow-duration="2s" data-wow-delay="2s">
                                <h1>Order Summary</h1>
                                <div className="view-serch">
                                  <form className="form_section">
                                    <input name="searchdata" placeholder="Search..." id="search_keyword" value="" type="text" value={this.state.searchdata} onChange={this.handleChange}/>
                                    <input className="Connect-btn" id="submit_btn" value="Search" type="button" onClick={this.refreshUserStoreList}/>
                                  </form>
                                </div>
                            </div> 
                            
                        </div>
                    </div>

                    <div className="row">
                    <div className="col-lg-2 wow animated fadeInLeft" data-wow-delay="0.4s">
                        <div className="">
                            <button className="order-samebtn dropdown-toggle" style={{cursor:'pointer'}} type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <a style={{cursor:'pointer'}} >Print</a>
                            </button>
                            <div className="dropdown-menu printmenu" aria-labelledby="dropdownMenuButton">
                                <a className="dropdown-item printlist" onClick={this.print}>Packing Slipt</a>
                                <a className="dropdown-item printlist" onClick={this.print}>Order Summary</a>
                                <a className="dropdown-item printlist" onClick={this.print}>Pick List</a>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-2 wow animated fadeInLeft" data-wow-delay="0.8s">
                        <a style={{cursor:'pointer', textAlign: 'center'}} onClick={this.handlePdf} className="order-samebtn">Download Pdf</a>
                    </div>
                    <div className="col-lg-2">
                        
                    </div>
                    <div className="col-lg-2">
                        
                    </div>
                    <div className="col-lg-2">
                        
                    </div>
                    <div className="col-lg-2">
                        {/*<div className="view-serch view-serch2" style={{width:'100%'}}>
                                                    <form className="form_section">
                                                        <input type="text" placeholder="Search..." name="searchdata" id="search_keyword" value={this.state.searchdata} onChange={this.handleChange.bind(this)}/>
                                                        <input className="Connect-btn" id="submit_btn" value="Search" type="button" onClick={this.refreshStoreOrderList}/>
                                                    </form>  
                                                </div>*/}
                    </div>
                    </div>
                    <hr />

                </div>



                <div className="order-sumary-body">  
                       
                    <div className="row">  
                        <div className="col-sm-3 wow animated fadeInLeft" data-wow-delay="0.2s">
                            <h6>Warehouse</h6>   
                            <h5>Caruso Logistics </h5> 
                            <h4>8254 S. Garfield Street,Grand Island, NE 68801</h4>
                                  
                        </div>
                        <div className="col-sm-3 wow animated fadeInLeft" data-wow-delay="0.4s">
                            <h6>Shipping to</h6>       
                            <h5>kjyh</h5>     
                            <h4>65465, kjj, 5465 </h4>    
                                
                        </div>
                        
                        <div className="col-sm-3 wow animated fadeInLeft" data-wow-delay="0.6s">  
                            <h6>Delivery</h6> 
                            <h5>Estimated Between</h5>   
                            <h4>Wednesday, October 7 to Ocober 12</h4>
                                   
                        </div>  
                        <div className="col-sm-3 wow animated fadeInLeft" data-wow-delay="0.8s">  
                            <h6>Date of submission</h6>   
                            <h5>huyguyff</h5>                
                        </div>      
                    </div>
                    <div className="row">  
                        <div className="col-sm-3 wow animated fadeInLeft" data-wow-delay="0.4s">
                            <a href="mailto:r.micheal02@gmail.com" className="oredr-email">r.micheal02@gmail.com</a> 
                            <a href="tel:+1 256-896-3256" className="oredr-email">+1 256 - 896 - 3256</a>          
                        </div>
                        <div className="col-sm-3 wow animated fadeInLeft" data-wow-delay="0.6s">   
                            <a href="mailto:r.micheal02@gmail.com" className="oredr-email">iugut@gmail.com</a>
                            <a href="tel:+1 256-896-3256" className="oredr-email">52465651651321</a>         
                        </div>
                        
                        <div className="col-sm-6 wow animated fadeInLeft" data-wow-delay="0.8s">  
                            <h5>INSTRUCTION</h5>   
                            <p className="ipsum-ins">1. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            <p className="ipsum-ins">2. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>       
                        </div>  
                              
                    </div>
                    <ul className="step-bar">  
                        <li className="step-one wow animated fadeInLeft" data-wow-delay="0.2s"><p></p></li>  
                        <li className="step-two wow animated fadeInLeft" data-wow-delay="0.4s"><p></p></li>
                        <li className="step-three wow animated fadeInLeft" data-wow-delay="0.6s"><p></p></li>  
                        <li className="step-four wow animated fadeInLeft" data-wow-delay="0.8s"><p></p></li>  
                        <li className="step-five wow animated fadeInLeft" data-wow-delay="0.9s"><p></p></li>    
                    </ul>
                </div>
                

            </div>


                <div className="storemiddle-sec">
                    <div className="addstore-list-text">
                        {/*<div className="col-sm-12 p-0">
                                                <ul className="storelist">
                                                    <li>
                                                        <div className="example" action="/action_page.php">
                                                            <input type="text" placeholder="Process Batch" name="search2" value={this.state.search2} />
                                                            <button type="submit"></button>
                                                        </div>
                                                    </li>
                                                    <li><a href="#" className="order-samebtn">Remove From Batch</a></li>
                                                    <li><a href="#" className="order-samebtn">Cancel Batch</a></li>
                                                    <li><a href="#" className="order-samebtn">Scan Batch</a></li>
                                                    <li><a href="#" className="order-samebtn" style={{ marginLeft: 10, marginRight: 10 }}>Bulk Action</a></li>
                                                    <li><div className="dropdown">
                                                        <button className="order-samebtn dropdown-toggle" style={{cursor:'pointer'}} type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <a style={{cursor:'pointer'}} >Print</a>
                                                        </button>
                                                        <div className="dropdown-menu printmenu" aria-labelledby="dropdownMenuButton">
                                                            <a className="dropdown-item printlist" onClick={this.print}>Packing Slipt</a>
                                                            <a className="dropdown-item printlist" onClick={this.print}>Order Summary</a>
                                                            <a className="dropdown-item printlist" onClick={this.print}>Pick List</a>
                                                        </div>
                                                    </div>
                                                    </li>
                                                    <li><a style={{cursor:'pointer'}} onClick={this.handlePdf} className="order-samebtn">Download Pdf</a></li>
                                                    <li className="order-srh"><form className="example serch-form"><input type="text" placeholder="Search Store" name="searchdata" value={this.state.searchdata} onChange={this.handleChange.bind(this)}/></form></li>
                                                </ul>
                                                </div>*/}
                        

                </div>  
                </div>
                <div className="store-listing-table" id="orderdata" style= {{minHeight: '297mm'}}>
                    <div className="row storlisthead">
                        <div className="col-sm-1 wow animated fadeInLeft" data-wow-delay="0.2s">
                            <span className="listtxt-1">S.No.</span>
                        </div>
                        <div className="col-sm-2 wow animated fadeInLeft" data-wow-delay="0.4s">
                            <span className="listtxt-1">Store Name</span>
                        </div>
                        <div className="col-sm-2 wow animated fadeInLeft" data-wow-delay="0.6s">
                            <span className="listtxt-1">Product Name</span>
                        </div>
                        <div className="col-sm-1 wow animated fadeInLeft" data-wow-delay="0.8s">
                            <span className="listtxt-1">SKU No.</span>
                        </div>
                        <div className="col-sm-1 wow animated fadeInLeft" data-wow-delay="0.9s">
                            <span className="listtxt-1">Item ID</span>
                        </div>
                        <div className="col-sm-1 wow animated fadeInLeft" data-wow-delay="1s">
                            <span className="listtxt-1">Qty.</span>
                        </div>
                        <div className="col-sm-1 wow animated fadeInLeft" data-wow-delay="2s">
                            <span className="listtxt-1">Order Date</span>
                        </div>
                        <div className="col-sm-2 wow animated fadeInLeft" data-wow-delay="3s">
                            <span className="listtxt-1">Item Status</span>
                        </div>
                        <div className="col-sm-1 wow animated fadeInLeft" data-wow-delay="4s">
                            <span className="listtxt-1">Action</span>
                        </div>
                    </div>



                    {
                        this.state.orderitems.length > 0
                    ?
                        this.state.orderitems.map(orderitem => {
                    return(
                        
                        <div className="row storlistbody">
                        <div className="col-sm-1 orderlistable">
                            <span className="listext-1">{srno++}</span>
                        </div>
                        <div className="col-sm-2 orderlistable">
                            <span className="listext-1">{this.state.order.store.store_name}</span>
                        </div>
                        <div className="col-sm-2 orderlistable">
                            <span className="listext-1">{orderitem.item_name}</span>
                        </div>
                        <div className="col-sm-1 orderlistable">
                            <span className="listext-1">{orderitem.sellersku}</span>
                        </div>
                        <div className="col-sm-1 orderlistable">
                            <span className="listext-1">#{orderitem.order_item_id}</span>
                        </div>
                        <div className="col-sm-1 orderlistable">
                            <span className="listext-1">{orderitem.item_qty}</span>
                        </div>
                        <div className="col-sm-1 orderlistable">
                            <span className="listext-1">{this.state.order.order_date}</span>
                        </div>
                        <div className="col-sm-2 orderlistable">
                            <span className="listext-1 downarrow">
                                <Popover placement="top">
                                    {this.renderSwitch(orderitem.item_status)}
                                    <select id={orderitem.id}  onChange={this.StatusChange.bind(this)} value={orderitem.item_status}>
                                    {this.state.orderStatus.map(status => {
                                        return (<option value={status.slug}>{status.name}</option>);
                                    })}
                                    </select>
                                </Popover>
                                <i className="fa fa-caret-down " aria-hidden="true" title="Toggle dropdown menu"></i>
                            </span>
                        </div>
                        <div className="col-sm-1 orderlistable">
                           <span className="edit_icon customfloatvip"><a href={`/Singleorderdetail/${orderitem.id}`}><i className="fas fa-eye"></i></a></span>
                        </div>
                    </div>
                    );
                })
                :
  
    <div className="no_records">
       <h2>No Records founds!</h2>
    </div>

            }

{ pageNumbers.length > 1 ?

<div className="paggination-text">
  <ul className="pagination pagination-sm">
    {this.state.PrevPageUrl != null ? 
  <li className="page-item"><a className="page-link" style={{cursor:'pointer'}} onClick={this.refreshStoreOrderList.bind(this, previousPage)}>Previous</a></li>
  : ''
}
      {pageNumbers.map(page => {
      return (<li className={currentPage == page ? 'activelink page-item' : 'page-item' } >
        <a className="page-link" style={{cursor:'pointer'}} onClick={this.refreshStoreOrderList.bind(this, page)}>{page}</a>
        </li>);
    })}

   {this.state.NextPageUrl != null ? 
  <li className="page-item"><a className="page-link" style={{cursor:'pointer'}} onClick={this.refreshStoreOrderList.bind(this, NextPage)}>Next</a></li>
  : ''
}
    {this.state.LastPage != null && currentPage!=LastPage? 
  <li className="page-item"><a className="page-link" style={{cursor:'pointer'}} onClick={this.refreshStoreOrderList.bind(this, LastPage)} >Last</a></li>
  : ''
}
  
  </ul>
</div> : ''
}

                    <div className="" style={{ position: 'relative' }}>
                        <div className="loader"></div>
                    </div>
                    
                </div>
                <footer className="add_list_footer">   
                    <div className="row"> 
                        <div className="col-sm-12">          
                            <p>© 2019 <a href="#" style={{color: '#07a8de'}}>VIP PARCEL</a></p>    
                        </div>
                    </div>
                </footer>
                <ToastContainer autoClose={5000} />
            </div>
            

            }
        </>
        
        );
    }
}

export default Orders;



