import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Redirect } from 'react-router-dom';
import { LOGIN_PAGE_PATH, API_TOKEN_NAME, USER_ID } from '../../constants';
import { scAxios } from '../..';
import { startUserSession } from '../userSession';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import * as jsPDF from 'jspdf';
import * as html2canvas from 'html2canvas';
import ReactTooltip from 'react-tooltip';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import loadingimage from '../../images/small_loader.gif';
import Popover from "react-awesome-popover";
import "react-awesome-popover/dest/react-awesome-popover.css";
import Skeleton from 'react-loading-skeleton';
import {Animated} from "react-animated-css";
import WOW from "wowjs";

const getAllOrders = (data, url) => {

    return new Promise((resolve, reject) => {

        const req = scAxios.request(url, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }

        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getStatus = () => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/orders/status', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const changeOrderStatus = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/orders/changestatus', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
            
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const storeDetails = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/stores/getdetail', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

class SingleStoreOrder extends React.Component {
    state = {
        storeOrders: [],
        stores:[],
        orderStatus:[],
        start_date: '',
        end_date: '',
        searchdata: '',
        total: '',
        currentPage: '',
        LastPage:'',
        PerPage: '',
        FirstPageUrl:'',
        NextPageUrl:'',
        PrevPageUrl:'',
        LastPageUrl:'',
        TotalPages:'',
        store_name:'',
        order_status: '',
        count:0,
        open: false,
        storesdetails:[],
        picklistorders: [],
        last_date: new Date(),
        loadingFirst: true,
    }

handleChange = () => {
    this.refreshStoreOrderList();
}

ChangelastDate(date) {
    this.setState({
      last_date: date
    });
  }

handleCheckBoxChange = event => {
        if(event.target.checked){
          this.state.picklistorders.push(event.target.value);
        } else {
            var index = this.state.picklistorders.indexOf(event.target.value); 
            if (index >= 0) {
             this.state.picklistorders.splice( index, 1 );
            }
        }
    }

printPicklist = () => {
    if(this.state.picklistorders!=''){
        this.props.history.push('/singlestorepicklist/'+this.state.storesdetails.slug+'/'+this.state.picklistorders);
    } else {
        this.props.history.push('/singlestorepicklist/'+this.state.storesdetails.slug+'/all');
    }
};

refreshStoreDetails = () => {

    const data = {
        slug: this.props.match.params.slug
    }
    
    storeDetails(data)
        .then(res => {
            if(res.status==1){
                this.setState({ 
                    storesdetails: res.details, 
                });
            } else {
                this.setState({ storesdetails: '' }); 
            }
        })
        .catch(err => {
            console.log(err);
        });
}

handleClick(e) {
    this.setState({open: !this.state.open});
  }

  handleClose(e) {
    this.setState({open: false});
  }

refreshStoreOrderList = (page) => {

    var slug =  this.props.match.params.slug;
    var url = '/stores/storeOrders/'+slug;
    
    const data = {
        searchdata: this.state.searchdata,
        page: page,
        order_status: this.state.order_status,
        start_date: this.state.start_date,
        end_date: this.state.end_date,
        userid: localStorage.getItem(USER_ID)
    }

    getAllOrders(data, url)
        .then(res => {
            this.setState({ loadingFirst: false });
            if(res.status==1){
                var records = res.success.data;
                this.setState({ 
                    storeOrders: records, 
                    total: res.success.total,
                    currentPage: res.success.current_page,
                    PerPage: res.success.per_page,
                    FirstPageUrl: res.success.first_page_url,
                    NextPageUrl: res.success.next_page_url,
                    PrevPageUrl: res.success.prev_page_url,
                    LastPageUrl: res.success.last_page_url,
                    LastPage: res.success.last_page,
                    TotalPages: Math.ceil(res.success.total / res.success.per_page)
                });
            } else {
                this.setState({ storeOrders: '' }); 
            }
        })
        .catch(err => {
            console.log(err);
        });
}

print(){
const content = document.getElementById('orderdata');
    window.print();
}

handlePdf = () => {
    const input = document.getElementById('orderdata');

    html2canvas(input)
        .then((canvas) => {
            const imgData = canvas.toDataURL('image/png');
            const pdf = new jsPDF('p', 'px', 'a4');
            var width = pdf.internal.pageSize.getWidth();
            var height = pdf.internal.pageSize.getHeight();

            pdf.addImage(imgData, 'PNG', 0, 0, width, height);
            pdf.save("StoreOrder.pdf");
        });
}




componentDidMount() {
    this.refreshStoreOrderList();
    this.refreshStoreDetails();

    getStatus()
        .then(res => {
          var orderStatus = res.success;
            this.setState({ orderStatus: orderStatus });
        })
        .catch(err => {
            console.log(err);
        });

}

StatusChange = (event) => {
    const data = {
        orderid: event.target.id,
        orderstatus: event.target.value
    }

    changeOrderStatus(data)
        .then(res => {
            if(res.status==1){
                toast.success(res.message, {
                  position: toast.POSITION.BOTTOM_RIGHT
                });
            } else {
                toast.error(res.message, {
                  position: toast.POSITION.BOTTOM_RIGHT
                });
            }
            this.refreshStoreOrderList();
        })
        .catch(err => {
            console.log(err);
        });
} 

renderSwitch(param) {
  switch(param) {
    case 'new':
      return 'New';
    case 'in_process':
      return 'In Process';
    case 'shipped':
      return 'Shipped';
    case 'cancel':
      return 'Cancel';
    case 'full_refund':
      return 'Full Refund';
    case 'partial_refund':
      return 'Partial Refund';
    case 'partial_shipped':
      return 'Partial Shipped';
    case 'completed':
      return 'Completed';
    default:
      return 'new';
  }
}


    render(){


        const wow = new WOW.WOW();
        const currentPage = this.state.currentPage;
        const previousPage = currentPage - 1;
        const NextPage = currentPage + 1;
        const LastPage = this.state.LastPage;
        const pageNumbers = [];
            for (let i = 1; i <= this.state.TotalPages; i++) {
              pageNumbers.push(i);
            }
        let srno = 1;

        return(
                <>     
                {this.state.loadingFirst==true ?
                    <div className="content-wrap">
                    <div className="content-scale">
                        <h1>{this.props.title || <Skeleton count={5} duration={2} />}</h1>
                        <p>{this.props.title || <Skeleton count={1} duration={2} />}</p>
                        <div className="row">
                            <div className="col-sm-4">
                                {this.props.title || <Skeleton count={1} duration={2} height={40} />}
                            </div>
                            <div className="col-sm-4">
                                
                            </div>
                            <div className="col-sm-4">
                               
                            </div>
                        </div>
                        <div className="button2">
                            <h6>{this.props.title || <Skeleton />}</h6>
                            {this.props.body || <Skeleton count={5} duration={2}  circle={true} color="red" height={40} highlightColor="red" />}
                        </div>
                        {/*{this.props.body || <Skeleton count={1} duration={2} circle={true} />}*/}
                    </div>
                    </div>
                    :

                <div className="content-wrap universelclass">

                <div className="storemiddle-sec">
                    {/*<div className="addstore-list-text">
                        <div className="col-sm-12 p-0">
                           
                        <ul className="storelist">
                            <li>
                                                            <div className="example" action="/action_page.php">
                                                                <input type="text" placeholder="Process Batch" name="search2" />
                                                                <button type="submit"></button>
                                                            </div>
                                                        </li>
                                                        <li><a href="#" className="order-samebtn">Remove From Batch</a></li>
                                                        <li><a href="#" className="order-samebtn">Cancel Batch</a></li>
                                                        <li><a href="#" className="order-samebtn">Scan Batch</a></li>
                                                        <li><a href="#" className="order-samebtn" style={{ marginLeft: 10, marginRight: 10 }}>Bulk Action</a></li>
                                                        
                            <li>
                                <div className="">
                                    <button className="order-samebtn dropdown-toggle" style={{cursor:'pointer'}} type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><a style={{cursor:'pointer'}} >Print</a></button>
                                    <div className="dropdown-menu printmenu" aria-labelledby="dropdownMenuButton">
                                        <a className="dropdown-item printlist" onClick={this.print}>Packing Slipt</a>
                                        <a className="dropdown-item printlist" onClick={this.print}>Order Summary</a>
                                        <a className="dropdown-item printlist" onClick={this.printPicklist}>Pick List</a>
                                    </div>
                                    <a style={{cursor:'pointer'}} onClick={this.handlePdf} className="order-samebtn">Download Pdf</a>
                                </div>
                            </li>
                            
                            <li>
                                <select className="order-slectbox" name="order_status" value={this.state.order_status} onChange={event => this.setState({order_status: event.target.value})}>
                                    <option value="">Select Status</option>
                                    {this.state.orderStatus.map(status => {
                                    return (<option value={status.slug}>{status.name}</option>);
                                    })}
                                </select>
                            </li>
                            <li>
                                <div className="list-date">
                                    Dates: <input type="date" name="start_date" className="first-date" value={this.state.start_date} onChange={event => this.setState({start_date: event.target.value})} />
                                </div>
                            </li>
                            <li>
                                <div className="list-date">
                                    to  <input type="date" name="end_date" className="second-date" value={this.state.end_date} onChange={event => this.setState({end_date: event.target.value})} />
                                </div>
                            </li>
                            <li>
                                <div className="view-serch view-serch2" style={{width:'100%'}}>
                                  <form className="form_section">
                                    <input name="searchdata" placeholder="Search..." id="search_keyword"  type="text" value={this.state.searchdata} onChange={event => this.setState({searchdata: event.target.value})}/>
                                    <input className="Connect-btn" id="submit_btn" value="Search" type="button" onClick={this.refreshStoreOrderList}/>
                                  </form>
                                </div>
                            </li>
                             
                        </ul>
                        
                        </div>
                    </div>
                    <hr />*/}
                    {/*<div className="row">
                                            <div className="col-sm-3">
                                                <div className="row ">
                                                    <div className="col-sm-12 p-1">
                                                        <select className="order-slectbox" name="order_status" value={this.state.order_status} onChange={event => this.setState({order_status: event.target.value})}>
                                                            <option value="">Select Order Status</option>
                                                            {this.state.orderStatus.map(status => {
                                                            return (<option value={status.slug}>{status.name}</option>);
                                                            })}
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-sm-9">
                                                <div className="row add-list-slet">
                                                    <div className="col-sm-4 paadd">
                                                        <div className="list-date">
                                                            Dates: <input type="date" name="start_date" className="first-date" value={this.state.start_date} onChange={event => this.setState({start_date: event.target.value})} />
                                                        </div>
                                                    </div>
                                                    <div className="col-sm-4 paadd">
                                                        <div className="list-date">
                                                            to  <input type="date" name="end_date" className="second-date" value={this.state.end_date} onChange={event => this.setState({end_date: event.target.value})} />
                                                        </div>
                                                    </div>
                                                    <div className="col-sm-4 paadd add-text-sort">
                                                    <div className="view-serch view-serch2" style={{width:'100%'}}>
                                                      <form className="form_section">
                                                        <input name="searchdata" placeholder="Search..." id="search_keyword"  type="text" value={this.state.searchdata} onChange={event => this.setState({searchdata: event.target.value})}/>
                                                        <input className="Connect-btn" id="submit_btn" value="Search" type="button" onClick={this.refreshStoreOrderList}/>
                                                      </form>
                                                    </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>*/}
                <div className="row">
                    <div className="col-lg-2 wow animated fadeInLeft" >
                        <div className="">
                            <button className="order-samebtn dropdown-toggle" style={{cursor:'pointer'}} type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><a style={{cursor:'pointer'}} >Print</a></button>
                            <div className="dropdown-menu printmenu" aria-labelledby="dropdownMenuButton">
                                <a className="dropdown-item printlist" onClick={this.print}>Packing Slipt</a>
                                <a className="dropdown-item printlist" onClick={this.print}>Order Summary</a>
                                <a className="dropdown-item printlist" onClick={this.printPicklist}>Pick List</a>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-2 wow animated fadeInLeft" >
                        <a style={{cursor:'pointer', textAlign: 'center'}} onClick={this.handlePdf} className="order-samebtn">Download Pdf</a>
                    </div>
                    <div className="col-lg-2 wow animated fadeInLeft" >
                        <select className="order-slectbox" name="order_status" value={this.state.order_status} onChange={event => this.setState({order_status: event.target.value})}>
                            <option value="">Select Status</option>
                            {this.state.orderStatus.map(status => {
                            return (<option value={status.slug}>{status.name}</option>);
                            })}
                        </select>
                    </div>
                    <div className="col-lg-2 wow animated fadeInLeft" >
                        <div className="list-date">
                            <DatePicker 
                              onChange={this.ChangelastDate.bind(this)} 
                              selected={this.state.last_date}
                              dateFormat="yyyy-MM-dd"
                            /> 
                        </div>
                    </div>
                    <div className="col-lg-2 wow animated fadeInLeft" >
                        <div className="list-date">
                            <DatePicker 
                              onChange={this.ChangelastDate.bind(this)} 
                              selected={this.state.last_date}
                              dateFormat="yyyy-MM-dd"
                            /> 
                        </div>
                    </div>
                    <div className="col-lg-2 wow animated fadeInLeft" >
                        <div className="view-serch view-serch2" style={{width:'100%'}}>
                          <form className="form_section">
                            <input name="searchdata" placeholder="Search..." id="search_keyword"  type="text" value={this.state.searchdata} onChange={event => this.setState({searchdata: event.target.value})}/>
                            <input className="Connect-btn" id="submit_btn" value="Search" type="button" onClick={this.refreshStoreOrderList}/>
                          </form>
                        </div>
                    </div>
                </div>
                <hr />
                </div>
                <div className="storname wow animated fadeInLeft" data-wow-duration="7s" >
                    <h1>Store Name : <span className="storena">{this.state.storesdetails.store_name}</span></h1>
                </div>

                <div className="store-listing-table wow animated fadeInLeft" id="orderdata" style= {{minHeight: '297mm'}}>
                    <div className="row storlisthead">
                         <div className="col-sm-1">
                            <span className="listtxt-1" data-tip="If no order is checked, then default all order picklist will create."><i className="fa fa-info-circle" aria-hidden="true"></i></span>
                            <ReactTooltip />
                        </div>
                        {/*<div className="col-sm-2">
                                                    <span className="listtxt-1">Store Name</span>
                                                </div>*/}
                        <div className="col-sm-2">
                            <span className="listtxt-1">Order ID</span>
                        </div>
                        <div className="col-sm-1"> 
                            <span className="listtxt-1">Qty.</span>
                        </div>
                        <div className="col-sm-2">
                            <span className="listtxt-1">Order Date</span>
                        </div>
                        <div className="col-sm-2">
                            <span className="listtxt-1">Asignee</span>
                        </div>
                        <div className="col-sm-2">
                            <span className="listtxt-1">Order Status</span>
                        </div>
                        <div className="col-sm-2">
                            <span className="listtxt-1">Action</span>
                        </div>
                    </div>
                
                    {
                        this.state.storeOrders.length > 0
                        ?
                        this.state.storeOrders.map(storeOrder => {
                    return (<div className="row storlistbody">
                        <div className="col-sm-1 single-store">
                            <span className="listext-1 pack-head-ing">
                                <form action="#">
                                    <span className="customfloatvip12">
                                        <p className="package-checkmar">
                                            <label className="customcheck">
                                                <input type="checkbox" name="picklistorders" id={'chk_'+storeOrder.id} value={storeOrder.id} onChange={this.handleCheckBoxChange} />
                                                <span className="checkmark"></span>
                                            </label>
                                        </p>
                                    </span>
                                </form>
                            </span>
                        </div>
                        {/*<div className="col-sm-2 single-store">
                                                    <span className="listext-1">{storeOrder.store.store_name}</span>
                                                </div>*/}
                        
                        <div className="col-sm-2 single-store"> 
                            <span className="listext-1"><a href={`../orders-lists/${storeOrder.id}`}>#{storeOrder.order_id}</a></span>
                        </div>
                        <div className="col-sm-1 single-store">
                            <span className="listext-1">2</span>
                        </div>
                        <div className="col-sm-2 single-store">
                            <span className="listext-1">{storeOrder.order_date}</span>
                        </div>
                        <div className="col-sm-2 single-store">
                            <span className="listext-1">{storeOrder.orderprocessors != '' && storeOrder.orderprocessors != null ? storeOrder.orderprocessors: 'Not Assigned'}</span>
                        </div>
                       
                        <div className="col-sm-2 single-store">
                            <span className="listext-1  downarrow" >
                                <Popover placement="top">
                                    {this.renderSwitch(storeOrder.order_status)}
                                    <select id={storeOrder.id}  onChange={this.StatusChange.bind(this)} value={storeOrder.order_status}>
                                    {this.state.orderStatus.map(status => {
                                        return (<option value={status.slug}>{status.name}</option>);
                                    })}
                                    </select>
                                    <i className="fa fa-caret-down " aria-hidden="true" title="Toggle dropdown menu"></i>
                                </Popover>
                                <i className="fa fa-caret-down" aria-hidden="true" title="Toggle dropdown menu"></i>
                            </span>
                            
                        </div>
                        <div className="col-sm-2 single-store padding-right">
                           <span className="edit_icon customfloatvip" data-tip="View"><a href={`../orders-lists/${storeOrder.id}`}><i className="fas fa-eye"></i></a></span>
                            <ReactTooltip />
                            <span className="edit_icon customfloatvip" data-tip="Create-Label"><a href={`../print-shipping/${storeOrder.id}`}><i className="fa fa-tags"></i></a></span>
                            <ReactTooltip />
                        </div>
                    </div>);

                })
                    :
                    <div className="no_records">
                       <h2>No Records founds!</h2>
                    </div>
                    }                    

{ pageNumbers.length > 1 ?

<div className="paggination-text">
  <ul className="pagination pagination-sm">
    {this.state.PrevPageUrl != null ? 
  <li className="page-item"><a className="page-link" style={{cursor:'pointer'}} onClick={this.refreshStoreOrderList.bind(this, previousPage)}>Previous</a></li>
  : ''
}
      {pageNumbers.map(page => {
      return (<li className={currentPage == page ? 'activelink page-item' : 'page-item' } >
        <a className="page-link" style={{cursor:'pointer'}} onClick={this.refreshStoreOrderList.bind(this, page)}>{page}</a>
        </li>);
    })}

   {this.state.NextPageUrl != null ? 
  <li className="page-item"><a className="page-link" style={{cursor:'pointer'}} onClick={this.refreshStoreOrderList.bind(this, NextPage)}>Next</a></li>
  : ''
}
    {this.state.LastPage != null && currentPage!=LastPage? 
  <li className="page-item"><a className="page-link" style={{cursor:'pointer'}} onClick={this.refreshStoreOrderList.bind(this, LastPage)} >Last</a></li>
  : ''
}
  
  </ul>
</div> : ''
}

                    
    </div>
    <footer className="add_list_footer">   
        <div className="row"> 
            <div className="col-sm-12">          
                <p>© 2019 <a href="#" style={{color: '#07a8de'}}>VIP PARCEL</a></p>    
            </div>
        </div>
    </footer>
    <ToastContainer autoClose={5000} />
</div>

}
        </>

            );
 
    }

}
export default SingleStoreOrder;