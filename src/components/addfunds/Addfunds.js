import React from 'react';
import ReactDOM from 'react-dom';

import './addfunds.css';

import visaimgs from '../../images/visa-imgs.png';
import bitcoin from '../../images/bitcoin.png';
import paypal1 from '../../images/paypal1.png';
import { validateUserToken } from '../PrivateRoute';
import { scAxios } from '../..';
import { Redirect } from 'react-router-dom';
import { MAIN_PAGE_PATH, LOGIN_PAGE_PATH, API_TOKEN_NAME, USER_ID} from '../../constants';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const addPaypalfund = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('payment/paypal', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const addCreditcardfund = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/payment/card', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getCreditcardList = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/accounts/card/getList', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getCreditcardverifiedlist = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/accounts/card/getverifycard', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getCreditcarddetails = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/accounts/card/details', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getUserDetails = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/user', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

class Addfunds extends React.Component { 
    state = {
        fundtype:'',
        amount:'',
        creditcards: [],
        totalcards:'',
        name: '',
        number: '',
        month: '',
        year: '',
        cvv: '',
        cardId:'',
        singlecarddetails:[],
        creditcardsverify:[],
        userProfile:[],
    }

    handleChange = event => {
        this.setState({ [event.target.name]: event.target.value });
    }

    handlePaypalSubmit = event => {
        event.preventDefault();
        const addfundspaypal = {
            amount: this.state.amount
        }

        addPaypalfund(addfundspaypal)
            .then(res => {
                window.location.href = res;
                if(res.status=='Success'){
                    toast.success(res.message, {
                        position: toast.POSITION.BOTTOM_RIGHT
                    });
                } else if(res.status=='Warning'){
                    toast.warn(res.message, {
                        position: toast.POSITION.BOTTOM_RIGHT
                    });
                } else {
                    toast.error(res.message, {
                        position: toast.POSITION.BOTTOM_RIGHT
                    });
                }
                console.log('save success', res);
                this.setState({
                    amount:'',
                });
            })
            .catch(err => {
                console.log(err);
            });
    }

    handleCreditcardSubmit = event => {
        const addfundscredit = {
            amount: this.state.amount,
            cvv: this.state.cvv,
            cardId: this.state.cardId
        }
        addCreditcardfund(addfundscredit)
            .then(res => {
                if(res.status=='1'){
                    toast.success(res.message, {
                        position: toast.POSITION.BOTTOM_RIGHT
                    });
                } else {
                    toast.error(res.message, {
                        position: toast.POSITION.BOTTOM_RIGHT
                    });
                }
                this.setState({ amount:'', cvv:''});
            })
            .catch(err => {
                console.log(err);
            });
    }

    getsinglecarddetails = (event) =>{
        const data = {
            cardId: event.target.value
        } 

        this.setState({ cardId: event.target.value });
        
    getCreditcarddetails(data)
        .then(res => {
            this.setState({ singlecarddetails: res.carddetails });
        })
        .catch(err => {
            console.log(err);
        });
    }

    refreshCreditCardList = () => {
        getCreditcardList()
            .then(res => {
                if (res.totalRecords > 0) {
                    this.setState({ creditcards: res.records });
                } else {
                    this.setState({ creditcards: '' });
                }
                this.setState({ totalcards: res.totalRecords });
            })
            .catch(err => {
                console.log(err);
            });
    }

    refreshCreditCardverifiedlist = () => {
        getCreditcardverifiedlist()
            .then(res => {
                if (res.totalRecords > 0) {
                    this.setState({ creditcardsverify: res.records });
                } else {
                    this.setState({ creditcardsverify: '' });
                }
                this.setState({ totalcards: res.totalRecords });
            })
            .catch(err => {
                console.log(err);
            });
    }

    refreshUserDetails = () => {
        const data = {
            userid: localStorage.getItem(USER_ID)
        }
        getUserDetails(data)
            .then(res => {
                this.setState({ userProfile: res.profile });
            })
            .catch(err => {
                console.log(err);
            });
    }

    componentDidMount() {
        this.refreshCreditCardList();
        this.refreshCreditCardverifiedlist();
        this.refreshUserDetails();
    }


    render() {

        return (

            <div className="content-wrap universelclass">
                <div className="col-lg-12">
                    <div className="visa-account">
                        <form action="process_form.cfm">
                            <div className="row">      
                                <div className="col-sm-4">  
                                    <label className="container-visa">Credit Card  
                                      <input type="radio" value="creditcard" checked="checked" name="fundtype" onChange={event => this.setState({fundtype: event.target.value})} checked={this.state.fundtype=='creditcard' || this.state.fundtype=='' ? 'checked' : ''} />
                                      <span className="checkmark-visa"></span>
                                    </label>
                                </div>  
                                <div className="col-sm-4">
                                    <label className="container-visa">Paypal
                                      <input type="radio" value="paypal" name="fundtype" onChange={event => this.setState({fundtype: event.target.value})}/>
                                      <span className="checkmark-visa"></span>
                                    </label> 
                                </div>   
                                <div className="col-sm-4">
                                    <label className="container-visa">Bitcoin 
                                      <input type="radio" value="bitcoin" name="fundtype" onChange={event => this.setState({fundtype: event.target.value})}/>  
                                      <span className="checkmark-visa"></span>
                                    </label> 
                                </div> 
                            </div>
                            <hr /> 
                        </form> 
                    </div>

                    <div className={this.state.fundtype=='creditcard' || this.state.fundtype=='' ? 'showfunddiv visa-back' : 'hidefunddiv visa-back'}>
                    <form >
                    <div className="visa-account-second">  
                        <div className="row">  
                            <div className="col-sm-12">
                                <img src={ visaimgs } alt="visa-img" className="visa-img" />  
                                <input type="text" name="amount" value={this.state.amount} placeholder="Enter Amount ($10 minimum)" onChange={this.handleChange} className="visa-fild1 animated fadeInDown" autoComplete="off"/>           
                                  
                            </div>  
                        </div>
                    </div>
                    {
                        this.state.creditcardsverify.length > 0 ?
                    <div className="visa-account-form">
                        <div className="visacrlform">
                            <h1>Credit Card Infomation</h1>
                            <div className="row">
                                <div className="col-sm-12">
                                    <select className="visa-drop" name="cardId" id="cardId" value={this.state.cardId} onChange={this.getsinglecarddetails}>
                                    <option value="">---Select Your Card---</option>
                                    
                                        
                                      {  this.state.creditcardsverify.map(creditcard => { 
                                           return( <option value={creditcard.id} key={creditcard.id}>xxxxxxxxxxxx{creditcard.ending}, {creditcard.name}</option>);
                                    }) }      
                                    </select>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-6 p-0">
                                    <input type="text" name="name" placeholder="Enter Name" defaultValue={this.state.singlecarddetails.name} className="visa-fild1 animated fadeInDown" readOnly autoComplete="off"/> 
                                </div>   
                                <div className="col-sm-3 p-0">
                                    <input type="text" name="month" placeholder="Enter Month" defaultValue={this.state.singlecarddetails.exp_month} className="visa-fild1 animated fadeInDown" readOnly autoComplete="off"/>
                                </div>
                                <div className="col-sm-3 p-0">
                                    <input type="text" name="year" placeholder="Enter Year" defaultValue={this.state.singlecarddetails.exp_year} className="visa-fild1 animated fadeInDown" readOnly autoComplete="off"/>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-6 p-0">
                                    <input type="text" name="number" placeholder="Enter Card Number" defaultValue={this.state.singlecarddetails.ending} className="visa-fild1 animated fadeInDown" readOnly autoComplete="off"/>
                                </div>
                                <div className="col-sm-6 p-0">
                                    <input type="text" name="cvv" placeholder="Enter CVV" value={this.state.cvv} onChange={this.handleChange} className="visa-fild1 animated fadeInDown" />
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-sm-12">
                                    <a href="/creditcard" className="manage_link">Manage Card Details</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    :
                    <div className="no_verified_card"><a className="Varified-credit">Varified credit card not found Manage credit cards</a></div>
                    }
                    <div className="visa-account-form">  
                        <div className="row">  
                            <div className="col-sm-12">
                                <h2>Billing Address</h2>  
                                <input type="text" name="user_firstname" defaultValue={this.state.userProfile.first_name} className="visa-fild animated fadeInDown" placeholder="Firstname" readOnly/>
                            </div>  
                        </div>    
                        <div className="row">      
                            <div className="col-sm-12">  
                                <input type="text" name="user_lastname" defaultValue={this.state.userProfile.last_name} className="visa-fild animated fadeInDown" placeholder="Lastname" readOnly/>  
                            </div>  
                        </div>  
                        <div className="row">  
                            <div className="col-sm-6">
                                 
                                <input type="text" name="user_address" defaultValue={this.state.userProfile.address} className="visa-fild animated fadeInDown" placeholder="Address" readOnly/>  
                            </div> 
                            <div className="col-sm-6">
                                
                                <input type="text" name="user_pincode" defaultValue={this.state.userProfile.zip} className="visa-fild animated fadeInDown" placeholder="Postal code" readOnly/>    
                            </div>    
                        </div>
                        <div className="row">  
                            <div className="col-sm-6">
                                  
                                <input type="text" name="user_city" defaultValue={this.state.userProfile.city} className="visa-fild animated fadeInDown" placeholder="City" readOnly/>  
                            </div>   
                            <div className="col-sm-6">
                                  
                                <input type="text" name="user_state" defaultValue={this.state.userProfile.state} className="visa-fild animated fadeInDown" placeholder="State" readOnly/>    
                            </div>    
                        </div>  
                        <div className="row">    
                            <div className="col-sm-12">      
                                <input type="text" name="user_phone" defaultValue={this.state.userProfile.phone} className="visa-fild animated fadeInDown" placeholder="Phone" readOnly/>  
                            </div>  
                        </div>
                        <div className="row">    
                            <div className="col-sm-12">      
                                <input type="button" className="Resend-sms" value="Pay" onClick={this.handleCreditcardSubmit}/>  
                            </div>  
                        </div>
                    </div>
                    </form>
                </div>

                <div className={this.state.fundtype=='paypal' ? 'showfunddiv paypalpag' : 'hidefunddiv paypalpag'}>
                    <form onSubmit={this.handlePaypalSubmit}>
                        <img src={paypal1} alt="Paypal" />
                        <input type="text" name="amount" id="amount" value={this.state.amount} placeholder="Enter Amount ($10 minimum)" className="visa-fild animated fadeInDown" onChange={this.handleChange} autoComplete="off" />
                        <input type="submit" value="Pay" className="Resend-sms" />
                        {/*<button type="button" className="Resend-sms">Pay</button>*/}
                    </form>
                </div>

                <div className={this.state.fundtype=='bitcoin' ? 'showfunddiv paypalpag' : 'hidefunddiv paypalpag'}>
                    <form action="#">
                        <img src={bitcoin} alt="bitcoin" />
                        <input type="text" name="user_email" placeholder="Enter Amount ($10 minimum)" className="visa-fild animated fadeInDown" />
                        <button type="button" className="Resend-sms">Pay</button>
                    </form>
                </div>

                </div>  
                <footer className="add_list_footer">   
                    <div className="row">  
                    <div className="col-sm-12">        
                    <p>© 2019 <a href="#" style={{color: '#2db4e3', textDecoration: 'none'}}>VIP PARCEL</a></p>    
                    </div>  
                    </div>
                </footer> 
                <ToastContainer autoClose={5000} />        
            </div>


        );
    }
}

export default Addfunds;
