import React from 'react';

//import './allocationstrategy.css';

class AllocationStrategy extends React.Component {

	render() {

		return(
 
			<div className="content-wrap">
				{/*<div className="row">
									<div className="col-sm-12">
										
									</div>
								</div>	*/}
				<div className="InManagg">
					<h1 className="Inventory-head">Allocation Strategy</h1>
				<div className="InManag-first">
				
					<div className="row">
						<div className="col-sm-12">
						<div className="invtryset">
							
							<p>Allocation refers to the process of reserving inventory for orders based on priority. You can customize this process by creating steps (or Tasks) and specifying how priority is given to orders in each set.</p>
						</div>
						</div>
					</div>
					<div className="row">
						<div className="col-sm-12">
						<div className="invtryset">
							<h4>No allocation strategy has been defined. By default, inventory will be allocated to the oldest orders first.</h4>
							<p>Create an allocation strategy by selecting a method below</p>
							<p style={{overflow: 'hidden'}}><a href="" className="viewallbtn">Prioritize inventory for...</a></p>
							<p style={{textAlign: 'center'}}>For allocation strategy ideas, <a href="" style={{color: '#04a5dc',fontWeight: '600'}}>click here.</a></p>
						</div>
						</div>
					</div> 
					
				</div>
				</div>
					<footer className="add_list_footer">
                        <div className="row">
                            <div className="col-sm-12">
                                <p>© 2019 <a href="#" style={{color: 'rgb(7, 168, 222)'}}>VIP PARCEL</a></p>
                            </div>
                            
                        </div>
                    </footer>
			</div>

			);

	}

}
export default AllocationStrategy;