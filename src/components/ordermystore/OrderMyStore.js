import React from 'react';

import ebay from '../../images/ebay.png';
import plush from '../../images/plush.png';
import amazon from '../../images/amazon-listing.png';
import cart32 from '../../images/cart32.png';
import usps from '../../images/usps-listing.png';
import seocart from '../../images/seocart.png';
import shiprobot from '../../images/logo-shiprobot.png';
import insure from '../../images/insure-listing.png';

class OrderMyStore extends React.Component {

	render(){

		return (

			<div className="container-fuild left-margin">
        <div className="col-lg-12">
            <div className="sorting-list">
                <h1>Store Listing</h1>
                <form className="example serch-form" action="/action_page.php"><label>Search Store</label><input type="text" placeholder="Search" name="search2" value="" /></form>
            </div>
            <div className="custom_tabel">
 
        <table className="table ">
  <thead>
    <tr className="bg ">
      <th>Store Image</th>
      <th>Store Name</th>
      <th>Store Sync</th>
      <th>Status</th>
      <th>View Orders</th>
    </tr>
  </thead>
  <tbody>
    <tr className="wow animate fadeInLeft " data-wow-delay="0.4s">
      <td ><span> <label className="customcheck">
  <input type="checkbox" />
  <span className="checkmark"></span>
</label>
      <label><img src={ ebay } alt="#" /></label></span></td>
      <td><a href="./OrderView">Dr. Martens Banzai Pull On Boot (Children's)</a></td>
      <td><a href="# " className="btn btn-primary synbtn ">Sync</a></td>
      <td><span className="status orange "></span></td>
      <td><span className="edit_icon "><a href="# "><i className="fas fa-eye "></i>View</a></span></td>
    </tr>
    <tr className="wow animate fadeInLeft" data-wow-delay="0.8s">
          <td ><span> <label className="customcheck">
  <input type="checkbox" /> 
  <span className="checkmark"></span>
</label>
      <label><img src={ amazon } alt="#" /></label></span></td>
      <td><a href="./OrderView">Dr. Martens Banzai Pull On Boot (Children's)</a></td>
      <td><a href="# " className="btn btn-primary synbtn ">Sync</a></td>
      <td><span className="status "></span></td>
      <td><span className="edit_icon "><a href="# "><i className="fas fa-eye "></i>View</a></span></td>
    </tr>
    <tr className="wow animate fadeInLeft " data-wow-delay="1.4s">
         <td ><span> <label className="customcheck">
  <input type="checkbox" />
  <span className="checkmark"></span>
</label>
      <label><img src={ cart32 } alt="#" /></label></span></td>
      <td><a href="./OrderView">Dr. Martens Banzai Pull On Boot (Children's)</a></td>
      <td><a href="# " className="btn btn-primary synbtn ">Sync</a></td>
      <td><span className="status "></span></td>
      <td><span className="edit_icon "><a href="# "><i className="fas fa-eye "></i>View</a></span></td>
    </tr>
   <tr className="wow animate fadeInLeft " data-wow-delay="1.8s">
          <td ><span> <label className="customcheck">
  <input type="checkbox" />
  <span className="checkmark"></span>
</label>
      <label><img src={ usps } alt="#" /></label></span></td>
      <td><a href="./OrderView">Dr. Martens Banzai Pull On Boot (Children's)</a></td>
      <td><a href="# " className="btn btn-primary synbtn ">Sync</a></td>
      <td><span className="status "></span></td>
      <td><span className="edit_icon "><a href="# "><i className="fas fa-eye "></i>View</a></span></td>
    </tr>
    <tr className="wow animate fadeInLeft " data-wow-delay="2s"> 
          <td ><span> <label className="customcheck">
  <input type="checkbox" />
  <span className="checkmark"></span>
</label>
      <label><img src={ seocart } alt="ebayy" /></label></span></td>
      <td><a href="./OrderView">Dr. Martens Banzai Pull On Boot (Children's)</a></td>
      <td><a href="# " className="btn btn-primary synbtn ">Sync</a></td>
      <td><span className="status red "></span></td>
      <td><span className="edit_icon "><a href="# "><i className="fas fa-eye "></i>View</a></span></td>
    </tr>
    <tr className="wow animate fadeInLeft " data-wow-delay="2s"> 
          <td ><span> <label className="customcheck">
  <input type="checkbox" />
  <span className="checkmark"></span>
</label>
      <label><img src={ shiprobot } alt="ebayy" /></label></span></td>
      <td><a href="./OrderView">Dr. Martens Banzai Pull On Boot (Children's)</a></td>
      <td><a href="# " className="btn btn-primary synbtn ">Sync</a></td>
      <td><span className="status red "></span></td>
      <td><span className="edit_icon "><a href="# "><i className="fas fa-eye "></i>View</a></span></td>
    </tr>
    
    <tr className="wow animate fadeInLeft " data-wow-delay="2s"> 
          <td ><span> <label className="customcheck">
  <input type="checkbox" />
  <span className="checkmark"></span>
</label>
      <label><img src={ insure } alt="ebayy" /></label></span></td>
      <td><a href="./OrderView">Dr. Martens Banzai Pull On Boot (Children's)</a></td>
      <td><a href="# " className="btn btn-primary synbtn ">Sync</a></td>
      <td><span className="status red "></span></td>
      <td><span className="edit_icon "><a href="# "><i className="fas fa-eye "></i>View</a></span></td>
    </tr>
  </tbody>
</table>

       </div>
       </div>
      </div>

			);

	}

}
export default OrderMyStore;