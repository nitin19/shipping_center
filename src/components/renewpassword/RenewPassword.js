import React from 'react';
import ReactDOM from 'react-dom';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import "react-tabs/style/react-tabs.css";

import './renewpassword.css';


class RenewPassword extends React.Component {
	render() {  
		return(

				<div className="content-wrap universelclass">
				 {/*<Tabs>
				 					<TabList>
				 					    <Tab>Personal Information</Tab>
				 					    <Tab>Change Password</Tab>
				 				    </TabList>*/}

					<div className="reset-back">

						{/*<div className="change-heading">
													<h1>Change Password From</h1>
												</div>
												<hr />*/}
						
						<div className="repassmain">
						{/*<TabPanel className="renewtab">*/}

			    		
						{/**********Personal-Information***********/}
							{/*<div className="resetpass resetleft">
															<h2>Personal Information</h2>
															<hr />
															<div className="row mar-bot">
																<div className="col-sm-12 padd-normal">
																	<label className="chancol">Full Name</label>
																	<input type="text" name="first_name" placeholder="Full Name *" className="first-one animated fadeInLeft" required="" value="" />
																</div>
															</div>
															<div className="row mar-bot">
																<div className="col-sm-12 padd-normal">
																	<label className="chancol">Email</label>
																	<input type="email" name="email" placeholder="Email Address *" className="email-one animated fadeInDown" required="" value="" />
																</div>
															</div>
															<div className="row mar-bot">
																<div className="col-sm-12 padd-normal animated fadeInLeft">
																	<label className="container-radio chancol">Receive a weekly digest email
																	<input type="checkbox" name="read_terms" />
																	<span className="checkmark"></span>
																	</label>
																</div>
															</div>
															
																<button type="submit" className=" animated fadeInDown renewpass" value="Login">Update Profile</button>
																
														</div>
													
														<div className="renew-backimg">
							
														</div>*/}
						{/*</TabPanel>*/}
						{/**********Change-Your-Password***********/}
						{/*<TabPanel className="renewtab">*/}
			    		
			    		
							<div className="resetpass resetright">
								<h2>Change Your Password</h2>
								<hr />
								<div className="row mar-bot">
									<div className="col-sm-12 padd-normal">
										<label>Current password</label>
										<input type="password" name="pass" placeholder="Enter Password *" className="animated fadeInDown" />
									</div>
								</div>
								<div className="row mar-bot">
									<div className="col-sm-12 padd-normal">
										<label>New password</label>
										<input type="password" name="pass" placeholder="New password *" className="animated fadeInDown" />
									</div>
								</div>
								<div className="row mar-bot">
									<div className="col-sm-12 padd-normal">
										<label>Verify password</label>
										<input type="password" name="pass" placeholder="Verify password *" className="animated fadeInDown" />
									</div>
								</div>

								<button type="submit" className="animated fadeInDown renewpass mt-2" value="">Change Password</button>
							</div>

							{/*<div className="renew-backimg">
							
														</div>*/}
						{/**********end***********/}
						{/*</TabPanel>*/}
						</div>
					</div>
					{/*</Tabs>*/}	
					<footer className="add_list_footer">   
	                    <div className="row"> 
		                    <div className="col-sm-12">          
		                    	<p>© 2018 <a href="#" style={{color: '#07a8de'}}>VIP PARCEL</a></p>    
		                    </div>
	                    </div>
	                </footer>
					
				</div>

			);
	}
}

export default RenewPassword;