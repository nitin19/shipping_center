import React from 'react';
import ReactDOM from 'react-dom';

class PackingSlips extends React.Component{

  render() {

    return(

      <div className="content-wrap universelclass">
        <div className="col-lg-12">
            <div className="sorting-list">
                <h1>Packing Slips</h1>
                <p>Need to have a custom packing slip for each of your stores? Or maybe you have fragile items that need additional instructions? You can set-up your customized packing slips here.</p>
            </div>
            <div className="custom_tabel wow bounceIn animated" data-wow-duration="4s">
            <div className="row wow animate fadeInLeft " data-wow-delay="0.4s">
              <div className="col-sm-12">
                <label>Type</label>
                <select className="packages-slectbox">
                  <option value="volvo">4" x 6"(Default)</option>
                  <option value="saab">4" x 6"(Default)</option>
                  <option value="opel">4" x 6"(Default)</option>
                  <option value="audi">4" x 6"(Default)</option>
                </select>
                <a href="" className="viewallbtn"><i className="fas fa-plus"></i> New Packing Slip Template </a>
              </div>
            </div>
              
              <table className="table ">
                <thead>
                    <tr className="bg ">
                      <th className="collumn5">Name</th>
                      <th className="collumn5">Size</th>
                      <th className="collumn5">Last Modified</th>
                      <th className="collumn5">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr className="wow animate fadeInLeft " data-wow-delay="0.4s">
                      <td className="collumn5">Default 4" x 6"</td>
                      <td className="collumn5">4x6</td>
                      <td className="collumn5">02/01/2017</td>
                      <td className="collumn5"><span className="edit_icon "><a href="# " className="namehigh"><i className="far fa-copy"></i> Copy</a></span></td>
                    </tr>
                    <tr className="wow animate fadeInLeft" data-wow-delay="0.8s"> 
                      <td className="collumn5">Default 4" x 6"</td>
                      <td className="collumn5">4x6</td>
                      <td className="collumn5">02/01/2017</td>
                      <td className="collumn5"><span className="edit_icon "><a href="#" className="namehigh"><i className="far fa-copy"></i> Copy</a></span></td>
                    </tr>
                    <tr className="wow animate fadeInLeft " data-wow-delay="1.4s">
                      <td className="collumn5">Default 4" x 6"</td>
                      <td className="collumn5">4x6</td>
                      <td className="collumn5">02/01/2017</td>
                      <td className="collumn5"><span className="edit_icon "><a href="# " className="namehigh"><i className="far fa-copy"></i> Copy</a></span></td>
                    </tr>
                   <tr className="wow animate fadeInLeft " data-wow-delay="1.8s">
                      <td className="collumn5">Default 4" x 6"</td>
                      <td className="collumn5">4x6</td>
                      <td className="collumn5">02/01/2017</td>
                      <td className="collumn5"><span className="edit_icon "><a href="# " className="namehigh"><i className="far fa-copy"></i> Copy</a></span></td>
                    </tr>
                    <tr className="wow animate fadeInLeft " data-wow-delay="2s">   
                      <td className="collumn5">Default 4" x 6"</td>
                      <td className="collumn5">4x6</td>
                      <td className="collumn5">02/01/2017</td>
                      <td className="collumn5"><span className="edit_icon "><a href="# " className="namehigh"><i className="far fa-copy"></i> Copy</a></span></td>
                    </tr>
                    <tr className="wow animate fadeInLeft " data-wow-delay="2s"> 
                      <td className="collumn5">Default 4" x 6"</td>
                      <td className="collumn5">4x6</td>
                      <td className="collumn5">02/01/2017</td>
                      <td className="collumn5"><span className="edit_icon "><a href="# " className="namehigh"><i className="far fa-copy"></i> Copy</a></span></td>
                    </tr>
                    <tr className="wow animate fadeInLeft " data-wow-delay="2s">     
                      <td className="collumn5">Default 4" x 6"</td>
                      <td className="collumn5">4x6</td>
                      <td className="collumn5">02/01/2017</td>
                      <td className="collumn5"><span className="edit_icon "><a href="# " className="namehigh"><i className="far fa-copy"></i> Copy</a></span></td>
                    </tr>
                </tbody>
              </table>

          </div>
        </div>
            <footer className="add_list_footer">
              <div className="row">
                  <div className="col-sm-12">
                      <p>© 2019 <a href="#" style={{color: 'rgb(7, 168, 222)'}}>VIP PARCEL</a></p>
                  </div>
                  
              </div>
            </footer>
      </div>

            );

      }

}
export default PackingSlips;
