import React from 'react';
import { Link } from 'react-router-dom'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Redirect } from 'react-router-dom';
import { LOGIN_PAGE_PATH, API_TOKEN_NAME,USER_ROLE,USER_ID } from '../../constants';
import { scAxios } from '../..';
import { startUserSession } from '../userSession';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import * as jsPDF from 'jspdf';
import * as html2canvas from 'html2canvas';
import ReactTooltip from 'react-tooltip';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Popover from "react-awesome-popover";
import "react-awesome-popover/dest/react-awesome-popover.css";
import Skeleton from 'react-loading-skeleton';
import loadingimage from '../../images/small_loader.gif';

const getAllOrders = (data) => {

    if(localStorage.getItem(USER_ROLE) !== "OP"){
        var url = '/stores/storeOrders';
    } else {
        var url = '/processor/processorOrders';
    }

    return new Promise((resolve, reject) => {

        const req = scAxios.request(url, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getStores = (data) => {
    if(localStorage.getItem(USER_ROLE) !== "OP"){
        var url = '/stores/allstores';
    } else {
        var url = '/processor/proStores';
    }
    return new Promise((resolve, reject) => {
        const req = scAxios.request(url, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
            
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const changeOrderStatus = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/orders/changestatus', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
            
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getStatus = () => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/orders/status', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

class AllStoreOrder extends React.Component {
    state = {
        storeOrders: [],
        orderStatus:[],
        stores:[],
        start_date: '',
        end_date: '',
        searchdata: '',
        total: '',
        currentPage: '',
        LastPage:'',
        PerPage: '',
        FirstPageUrl:'',
        NextPageUrl:'',
        PrevPageUrl:'',
        LastPageUrl:'',
        TotalPages:'',
        store_name:'',
        order_status: '',
        loadingFirst: true,
        picklistorders: [],
        last_date: new Date()
    }

handleChange = () => {
    this.refreshStoreOrderList();
}

ChangelastDate(date) {
    this.setState({
      last_date: date
    });
  }

handleCheckBoxChange = event => {
        if(event.target.checked){
          this.state.picklistorders.push(event.target.value);
        } else {
            var index = this.state.picklistorders.indexOf(event.target.value); 
            if (index >= 0) {
             this.state.picklistorders.splice( index, 1 );
            }
        }
    }

refreshStoreOrderList = (page) => {
    
    const data = {
        searchdata: this.state.searchdata,
        page: page,
        store_name: this.state.store_name,
        order_status: this.state.order_status,
        start_date: this.state.start_date,
        end_date: this.state.end_date,
        userid: localStorage.getItem(USER_ID)
    }
    getAllOrders(data)
        .then(res => {
            this.setState({ loadingFirst: false });
            if(res.status==1){
                var records = res.success.data;
                this.setState({ 
                    storeOrders: records, 
                    total: res.success.total,
                    currentPage: res.success.current_page,
                    PerPage: res.success.per_page,
                    FirstPageUrl: res.success.first_page_url,
                    NextPageUrl: res.success.next_page_url,
                    PrevPageUrl: res.success.prev_page_url,
                    LastPageUrl: res.success.last_page_url,
                    LastPage: res.success.last_page,
                    TotalPages: Math.ceil(res.success.total / res.success.per_page)
                });
            } else {
               this.setState({ storeOrders: '' }); 
            }
        })
        .catch(err => {
            console.log(err);
        });

    }


  componentDidMount() {
    this.refreshStoreOrderList();
    const data = {
            userid: localStorage.getItem(USER_ID)
        }
    getStores(data)
        .then(res => {
            if(res.status==1){
                var stores = res.records;
                this.setState({ stores: stores });
            } else {
                this.setState({ stores: '' });
            }
        })
        .catch(err => {
            console.log(err);
            alert("Error occured fetching Stores. Check console");
        });

    getStatus()
        .then(res => {
          var orderStatus = res.success;
            this.setState({ orderStatus: orderStatus });
        })
        .catch(err => {
            console.log(err);
        });
}

print(){
    window.print();
}

printPicklist = () => {
    if(this.state.picklistorders!=''){
        this.props.history.push('/picklist/'+this.state.picklistorders);
    } else {
        this.props.history.push('/picklist/all');
    }
   
};

handlePdf = () => {
    const input = document.getElementById('orderdata');

    html2canvas(input)
        .then((canvas) => {
            const imgData = canvas.toDataURL('image/png');
            const pdf = new jsPDF('p', 'px', 'a4');
            var width = pdf.internal.pageSize.getWidth();
            var height = pdf.internal.pageSize.getHeight();

            pdf.addImage(imgData, 'PNG', 0, 0, width, height);
            pdf.save("StoreOrder.pdf");
        });
};

StatusChange = (event) => {
    const data = {
        orderid: event.target.id,
        orderstatus: event.target.value
    }

    changeOrderStatus(data)
        .then(res => {
            if(res.status==1){
                toast.success(res.message, {
                  position: toast.POSITION.BOTTOM_RIGHT
                });
            } else {
                toast.error(res.message, {
                  position: toast.POSITION.BOTTOM_RIGHT
                });
            }
            this.refreshStoreOrderList();
        })
        .catch(err => {
            console.log(err);
        });
} 

renderSwitch(param) {
  switch(param) {
    case 'new':
      return 'New';
    case 'in_process':
      return 'In Process';
    case 'shipped':
      return 'Shipped';
    case 'cancel':
      return 'Cancel';
    case 'full_refund':
      return 'Full Refund';
    case 'partial_refund':
      return 'Partial Refund';
    case 'partial_shipped':
      return 'Partial Shipped';
    case 'completed':
      return 'Completed';
    default:
      return 'New';
  }
}


    render(){
    const currentPage = this.state.currentPage;
    const previousPage = currentPage - 1;
    const NextPage = currentPage + 1;
    const LastPage = this.state.LastPage;
    const pageNumbers = [];
    for (let i = 1; i <= this.state.TotalPages; i++) {
      pageNumbers.push(i);
    }
    let srno = 1;

        return( 
                 <>     
                {this.state.loadingFirst==true ?
                    <div className="content-wrap">
                    <div className="content-scale">
                        <h1>{this.props.title || <Skeleton count={5} duration={2} />}</h1>
                        <div className="button2">
                            <p>{this.props.title || <Skeleton count={1} duration={2} />}</p>

                            <h6>{this.props.title || <Skeleton />}</h6>
                            {this.props.body || <Skeleton count={5} duration={2}  circle={true} color="red" height={40} highlightColor="red" />}
                        </div>
                        {/*{this.props.body || <Skeleton count={1} duration={2} circle={true} />}*/}
                    </div>
                    </div>
                    : 
                    
                <div className="content-wrap universelclass">
                    
                <div className="storemiddle-sec">
                    <div className="addstore-list-text">
                        {/*<div className="col-sm-12 p-0">
                                                <ul className="storelist">
                                                    <li><div className="example storeexam" action="/action_page.php">
                                                        <input type="text" placeholder="Process Batch" name="search2" value="" />
                                                        <button type="submit"></button>
                                                    </div>
                                                    </li>
                                                    <li><a href="#" className="order-samebtn">Remove From Batch</a></li>
                                                    <li><a href="#" className="order-samebtn">Cancel Batch</a></li>
                                                    <li><a href="#" className="order-samebtn">Scan Batch</a></li>
                                                    <li><a href="#" className="order-samebtn specelbtn" style={{ marginLeft: 10, marginRight: 10 }}>Bulk Action</a></li>
                                                    <li>
                                                        <div className="dropdown">
                                                            <button className="order-samebtn dropdown-toggle" style={{cursor:'pointer'}} type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><a style={{cursor:'pointer'}} >Print</a></button>
                                                            <div className="dropdown-menu printmenu" aria-labelledby="dropdownMenuButton">
                                                                <a className="dropdown-item printlist" onClick={this.print}>Packing Slipt</a>
                                                                <a className="dropdown-item printlist" onClick={this.print}>Order Summary</a>
                                                                <a className="dropdown-item printlist" onClick={this.printPicklist}>Pick List</a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li><a style={{cursor:'pointer'}} onClick={this.handlePdf} className="order-samebtn">Download Pdf</a></li>
                                                    <li><form className="example serch-form" ><input type="text" placeholder="Search Order" name="searchdata" value={this.state.searchdata} onChange={event => this.setState({searchdata: event.target.value})}/></form></li>
                                                </ul>
                                                </div>
                                            </div>
                                            <hr />
                                            <div className="row storordpadd">
                                                <div className="col-sm-5">
                                                    <div className="row ">
                                                        <div className="col-sm-6 p-1">
                                                            <select className="order-slectbox" name="store_name" value={this.state.store_name} onChange={event => this.setState({store_name: event.target.value})}>
                                                            <option value="">Select Store Name</option>
                                                            {
                                                                    this.state.stores.length > 0
                                                                     ? 
                                                                     this.state.stores.map(store => {
                                                                        return (<option value={store.id} key={store.id}>{store.store_name}</option>);
                                                                    }) 
                                                                     : 
                                                                     ''
                                                                }
                                                                
                                                            </select>
                                                        </div> 
                                                        
                                                        <div className="col-sm-6 p-1">
                                                            <select className="order-slectbox" name="order_status" value={this.state.order_status} onChange={event => this.setState({order_status: event.target.value})}>
                                                                <option value="">Select Order Status</option>
                                                                {this.state.orderStatus.map(status => {
                                                                return (<option value={status.slug}>{status.name}</option>);
                                                                })}
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-sm-7">
                                                    <div className="row add-list-slet">
                                                        <div className="col-md-5 col-sm-5 paadd">
                                                            <div className="list-date">
                                                                Dates: <input type="date" name="start_date" className="first-date" value={this.state.start_date} onChange={event => this.setState({start_date: event.target.value})} />
                                                            </div>
                                                        </div>
                                                        <div className="col-md-5 col-sm-5 paadd">
                                                            <div className="list-date">
                                                                to  <input type="date" name="end_date" className="second-date" value={this.state.end_date} onChange={event => this.setState({end_date: event.target.value})} />
                                                            </div>
                                                        </div>
                                                        <div className="col-md-2 col-sm-2 paadd add-text-sort">
                                                        <a style={{cursor:'pointer'}} onClick={this.handleChange} className="viewallbtn">Search</a>
                                                       
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>*/}

                                            <div className="row">
                    <div className="col-lg-2">
                        <div className="">
                            <button className="order-samebtn dropdown-toggle" style={{cursor:'pointer'}} type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><a style={{cursor:'pointer'}} >Print</a></button>
                            <div className="dropdown-menu printmenu" aria-labelledby="dropdownMenuButton">
                                <a className="dropdown-item printlist" onClick={this.print}>Packing Slipt</a>
                                <a className="dropdown-item printlist" onClick={this.print}>Order Summary</a>
                                <a className="dropdown-item printlist" onClick={this.printPicklist}>Pick List</a>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-2">
                        <a style={{cursor: 'pointer', textAlign: 'center'}} onClick={this.handlePdf} className="order-samebtn">Download Pdf</a>
                    </div>
                    <div className="col-lg-2">
                        <select className="order-slectbox" name="order_status" value={this.state.order_status} onChange={event => this.setState({order_status: event.target.value})}>
                            <option value="">Select Status</option>
                            {this.state.orderStatus.map(status => {
                            return (<option value={status.slug} key={status.id}>{status.name}</option>);
                            })}
                        </select>
                    </div>
                    <div className="col-lg-2">
                        <select className="order-slectbox" name="store_name" value={this.state.store_name} onChange={event => this.setState({store_name: event.target.value})}>
                            <option value="">Select Store</option>
                            {
                                    this.state.stores!=''
                                     ? 
                                     this.state.stores.map(store => {
                                        return (<option value={store.id} key={store.id}>{store.store_name}</option>);
                                    }) 
                                     :
                                     ''
                                }
                        </select>
                    </div>
                    <div className="col-lg-2">
                        <div className="list-date">
                            <DatePicker 
                              onChange={this.ChangelastDate.bind(this)} 
                              selected={this.state.last_date}
                              dateFormat="yyyy-MM-dd"
                              minDate={new Date()}
                            />
                        </div>
                    </div>
                    <div className="col-lg-2">
                        <div className="list-date">
                            <DatePicker 
                              onChange={this.ChangelastDate.bind(this)} 
                              selected={this.state.last_date}
                              dateFormat="yyyy-MM-dd"
                              minDate={new Date()}
                            />
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-12">
                        <div className="view-serch">
                          
                            <input name="searchdata" placeholder="Search..." id="search_keyword" value="" type="text" value={this.state.searchdata} onChange={this.handleChange}/>
                            <input className="Connect-btn" id="submit_btn" value="Search" type="button" onClick={this.refreshStoreOrderList}/>
                          
                        </div>
                    </div>
                </div>
                </div>                
                <hr />
                </div>
                <div className="store-listing-table" id="orderdata" style= {{minHeight: '297mm'}}>
                    <div className="row storlisthead">
                        <div className="col-sm-1">
                            <span className="listtxt-1" data-tip="If no order is checked, then default all order picklist will create."><i className="fa fa-info-circle" aria-hidden="true"></i></span>
                            <ReactTooltip />
                        </div>
                        <div className="col-sm-2">
                            <span className="listtxt-1">Store Name</span>
                        </div>
                        <div className="col-sm-2">
                            <span className="listtxt-1">Order ID</span>
                        </div>
                        <div className="col-sm-1"> 
                            <span className="listtxt-1">Qty.</span>
                        </div>
                        <div className="col-sm-2">
                            <span className="listtxt-1">Order Date</span>
                        </div>
                        
                        <div className="col-sm-2">
                            <span className="listtxt-1">Asignee</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listtxt-1">Order Status</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listtxt-1">Action</span>
                        </div>
                    </div>
                    

                    
                    {
                        this.state.storeOrders.length > 0
                    ?
                        this.state.storeOrders.map(storeOrder => {

                    return (<div className="row storlistbody" key={storeOrder.id}>
                        <div className="col-sm-1 store-tabel-head" >
                            <span className="listext-1  pack-head-ing" >
                                <form action="#">
                                    <span className="customfloatvip12">
                                        <p className="package-checkmar">
                                            <label className="customcheck">
                                                <input type="checkbox" name="picklistorders" id={'chk_'+storeOrder.id} value={storeOrder.id} onChange={this.handleCheckBoxChange} />
                                                <span className="checkmark"></span>
                                            </label>
                                        </p>
                                    </span>
                                </form>
                            </span>
                        </div>
                        <div className="col-sm-2 store-tabel-head">
                            <span className="listext-1">{storeOrder.store.store_name}</span>
                        </div>
                        
                        <div className="col-sm-2 store-tabel-head"> 
                            <span className="listext-1"><a href={`./orders-lists/${storeOrder.id}`}>#{storeOrder.order_id}</a></span>
                        </div>
                        <div className="col-sm-1 store-tabel-head">
                            <span className="listext-1">{storeOrder.order_qty}</span>
                        </div>
                        <div className="col-sm-2 store-tabel-head">
                            <span className="listext-1">{storeOrder.order_date}</span>
                        </div>
                        
                        <div className="col-sm-2 store-tabel-head">
                            <span className="listext-1">{storeOrder.orderprocessors != '' && storeOrder.orderprocessors != null ? storeOrder.orderprocessors : 'Not Assigned'}</span>
                        </div>
                        <div className="col-sm-1 store-tabel-head">
                            <span className="listext-1 downarrow">
                                <Popover placement="top">
                                    {this.renderSwitch(storeOrder.order_status)}
                                    <select id={storeOrder.id}  onChange={this.StatusChange.bind(this)} value={storeOrder.order_status}>
                                    {this.state.orderStatus.map(status => {
                                        return (<option value={status.slug} key={status.id}>{status.name}</option>);
                                    })}
                                    </select>
                                </Popover>
                                <i className="fa fa-caret-down " aria-hidden="true" title="Toggle dropdown menu"></i>
                            </span>
                        </div>
                        <div className="col-sm-1 store-tabel-head padding-right">
                            <span className="edit_icon customfloatvip" data-tip="View"><a href={`./orders-lists/${storeOrder.id}`}><i className="fas fa-eye"></i></a></span>
                            <ReactTooltip />
                            <span className="edit_icon customfloatvip" data-tip="Create-Label"><a href={`./print-shipping/${storeOrder.id}`}><i className="fa fa-tags"></i></a></span>
                            <ReactTooltip />
                        </div>
                    </div>);
                    
                })
                    :
  
    <div className="no_records">
       <h2>No Records founds!</h2>
    </div>
  
}


{ pageNumbers.length > 1 ?

<div className="paggination-text">
  <ul className="pagination pagination-sm">
    {this.state.PrevPageUrl != null ? 
  <li className="page-item"><a className="page-link" style={{cursor:'pointer'}} onClick={this.refreshStoreOrderList.bind(this, previousPage)}>Previous</a></li>
  : ''
}
      {pageNumbers.map(page => {
      return (<li className={currentPage == page ? 'activelink page-item' : 'page-item' } >
        <a className="page-link" style={{cursor:'pointer'}} onClick={this.refreshStoreOrderList.bind(this, page)}>{page}</a>
        </li>);
    })}

   {this.state.NextPageUrl != null ? 
  <li className="page-item"><a className="page-link" style={{cursor:'pointer'}} onClick={this.refreshStoreOrderList.bind(this, NextPage)}>Next</a></li>
  : ''
}
    {this.state.LastPage != null && currentPage!=LastPage? 
  <li className="page-item"><a className="page-link" style={{cursor:'pointer'}} onClick={this.refreshStoreOrderList.bind(this, LastPage)} >Last</a></li>
  : ''
}
  
  </ul>
</div> : ''
}

                    <div className="" style={{ position: 'relative' }}>
                        <div className="loader"></div>
                    </div>
                    
                </div>
                <footer className="add_list_footer">   
                    <div className="row"> 
                    <div className="col-sm-12">          
                    <p>© 2019 <a href="#" style={{color: '#07a8de'}}>VIP PARCEL</a></p>    
                    </div>
                    
                    </div>
                </footer>
                <ToastContainer autoClose={5000} />
            </div>

            }
        </>

            );
 
    }

}
export default AllStoreOrder;