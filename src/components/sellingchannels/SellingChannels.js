import React from 'react';
import ReactDOM from 'react-dom';
import ebay from '../../images/ebay.png';
import plush from '../../images/plush.png';
import amazon from '../../images/amazon-listing.png';
import cart32 from '../../images/cart32.png';
import usps from '../../images/usps-listing.png';
import seocart from '../../images/seocart.png';
import shiprobot from '../../images/logo-shiprobot.png';
import insure from '../../images/insure-listing.png';
import ReactTooltip from 'react-tooltip';

class ViewWareHouse extends React.Component {

  render() {

    return (

        <div className="content-wrap universelclass">
        <div className="col-lg-12">
            <div className="sorting-list">
                <h1>Store Setup</h1>
                <div className="view-serch">
                  <form className="form_section">
                    <input name="searchdata" placeholder="Search..." id="search_keyword" type="text"  />
                    <input className="Connect-btn" id="submit_btn" value="Search" type="button" />
                  </form>
                </div>
            </div>
            <div className="custom_tabel wow bounceIn animated" data-wow-duration="4s">
 
        <table className="table ">
  <thead>
    <tr className="bg ">
      <th className="collumn5">Store Name</th>
      <th className="collumn5">Last Modified</th>
      <th className="collumn5">Active</th>
      <th className="collumn5">Actions</th>
    </tr>
  </thead>
  <tbody>
    <tr className="wow animate fadeInLeft " data-wow-delay="0.4s">
      <td className="collumn5">Manual Orders</td>
      <td className="collumn5">11/25/2018</td>
      <td className="collumn5"><span className="edit_icon "><span className="status orange "></span></span></td>
      <td className="collumn5 done-space">
        <span className="edit_icon" data-tip="View">
          <a href="# "><i className="fas fa-eye "></i></a>
          <ReactTooltip />
        </span>
        <span className="edit_icon" data-tip="Edit">
          <a href="# "><i className="fas fa-pencil-alt "></i></a>
          <ReactTooltip />
        </span>
      </td>
    </tr>
    <tr className="wow animate fadeInLeft" data-wow-delay="0.8s">
           
      <td className="collumn5">Manual Orders</td>
      <td className="collumn5">11/25/2018</td>
      <td className="collumn5"><span className="edit_icon "><span className="status orange "></span></span></td>
      <td className="collumn5 done-space">
        <span className="edit_icon" data-tip="View">
          <a href="# "><i className="fas fa-eye "></i></a>
          <ReactTooltip />
        </span>
        <span className="edit_icon" data-tip="Edit">
          <a href="# "><i className="fas fa-pencil-alt "></i></a>
          <ReactTooltip />
        </span>
      </td>
    </tr>
    <tr className="wow animate fadeInLeft " data-wow-delay="1.4s">
         
      <td className="collumn5">Manual Orders</td>
      <td className="collumn5">11/25/2018</td>
      <td className="collumn5"><span className="edit_icon "><span className="status orange "></span></span></td>
      <td className="collumn5 done-space">
        <span className="edit_icon" data-tip="View">
          <a href="# "><i className="fas fa-eye "></i></a>
          <ReactTooltip />
        </span>
        <span className="edit_icon" data-tip="Edit">
          <a href="# "><i className="fas fa-pencil-alt "></i></a>
          <ReactTooltip />
        </span>
      </td>
    </tr>
   <tr className="wow animate fadeInLeft " data-wow-delay="1.8s">
          
      <td className="collumn5">Manual Orders</td>
      <td className="collumn5">11/25/2018</td>
      <td className="collumn5"><span className="edit_icon "><span className="status orange "></span></span></td>
      <td className="collumn5 done-space">
        <span className="edit_icon" data-tip="View">
          <a href="# "><i className="fas fa-eye "></i></a>
          <ReactTooltip />
        </span>
        <span className="edit_icon" data-tip="Edit">
          <a href="# "><i className="fas fa-pencil-alt "></i></a>
          <ReactTooltip />
        </span>
      </td>
    </tr>
    <tr className="wow animate fadeInLeft " data-wow-delay="2s"> 
          
      <td className="collumn5">Manual Orders</td>
      <td className="collumn5">11/25/2018</td>
      <td className="collumn5"><span className="edit_icon "><span className="status orange "></span></span></td>
      <td className="collumn5 done-space">
        <span className="edit_icon" data-tip="View">
          <a href="# "><i className="fas fa-eye "></i></a>
          <ReactTooltip />
        </span>
        <span className="edit_icon" data-tip="Edit">
          <a href="# "><i className="fas fa-pencil-alt "></i></a>
          <ReactTooltip />
        </span>
      </td>
    </tr>
    <tr className="wow animate fadeInLeft " data-wow-delay="2s"> 
          
      <td className="collumn5">Manual Orders</td>
      <td className="collumn5">11/25/2018</td>
      <td className="collumn5"><span className="edit_icon "><span className="status orange "></span></span></td>
      <td className="collumn5 done-space">
        <span className="edit_icon" data-tip="View">
          <a href="# "><i className="fas fa-eye "></i></a>
          <ReactTooltip />
        </span>
        <span className="edit_icon" data-tip="Edit">
          <a href="# "><i className="fas fa-pencil-alt "></i></a>
          <ReactTooltip />
        </span>
      </td>
    </tr>
    
    <tr className="wow animate fadeInLeft " data-wow-delay="2s"> 
          
      <td className="collumn5">Manual Orders</td>
      <td className="collumn5">11/25/2018</td>
      <td className="collumn5"><span className="edit_icon "><span className="status orange "></span></span></td>
      <td className="collumn5 done-space">
        <span className="edit_icon" data-tip="View">
          <a href="# "><i className="fas fa-eye "></i></a>
          <ReactTooltip />
        </span>
        <span className="edit_icon" data-tip="Edit">
          <a href="# "><i className="fas fa-pencil-alt "></i></a>
          <ReactTooltip />
        </span>
      </td>
    </tr>
  </tbody>
</table>

       </div>
       </div>
       <footer className="add_list_footer">
        <div className="row">
          <div className="col-sm-12">
            <p>© 2019 <a href="#" style={{color: 'rgb(7, 168, 222)'}}>VIP PARCEL</a></p>
          </div>
        </div>
      </footer>
      </div>

      );

  }

}

export default ViewWareHouse;