import React from 'react';
import { validateUserToken } from '../PrivateRoute';
import { scAxios } from '../..';
import { Redirect } from 'react-router-dom';
import { MAIN_PAGE_PATH, LOGIN_PAGE_PATH, API_TOKEN_NAME, USER_ID, IMAGE_URL} from '../../constants';
import profile from '../../images/prflimg.png';

const viewWarehouse = (data, url) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request(url, {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

class ViewWareHouse extends React.Component { 
    state = {
        warehouses: [],
    }

    componentDidMount() {
    	const data = {
            userid: localStorage.getItem(USER_ID)
        }
    	var id = this.props.match.params.id;
    	var url = '/warehouse/viewwarehouse/'+id;
        viewWarehouse(data, url)
            .then(res => {
                if (res.message) {
                    this.setState({ warehouses: res.message });
                }
            })
            .catch(err => {
                console.log(err);
                alert("Error occured fetching warehouse. Check console");
            });
    }
render(){
	var warehouse_id = this.state.warehouses.id;
	return(

		<div className="content-wrap universelclass">
			<div className="order-view2">
	        	<div className="order-view-head">
	        		<h2>WareHouse Detail</h2>
	        	</div>
	        	<div className="order-view2-body">
	        	<div className="position-text">
	        		<p>Last Modified Date:</p>
	        		{/*<p>{this.state.warehouses.updated_at}</p>*/}
	        		<a href={'/editwarehouse/'+warehouse_id} className="onetwo">Edit</a>
	        	</div>
	        	<div className="row">
	        		<div className="col-md-2">
	        			<img src={ profile } alt="roundimg" className="profileimg2" />
	        			{/*<div className="upload-btn-wrapper-img">
						  <button className="btn-gourav">Upload a file</button>
						  <input type="file" name="myfile" />
						</div>*/}
	        		</div>
	        		<div className="col-md-9">
	        			<h3 className="">{this.state.warehouses.warehouse_name}</h3> 
	        			{/*<a href="mailto:geeber@me.com" className="geebermail">geeber@me.com</a>*/}
	        			<hr />
	        			<p><span className="increment">Warehouse Location : </span>{this.state.warehouses.warehouse_address}</p>
	        			<hr />
	        			<p><span className="increment">Warehouse City : </span>{this.state.warehouses.warehouse_city}</p>
	        			<hr />
	        			<p><span className="increment">Warehouse State : </span>{this.state.warehouses.warehouse_state}</p>
	        			<hr />
	        			<p><span className="increment">Warehouse Country : </span>{this.state.warehouses.warehouse_country}</p>
	        			<hr />
	        			<p><span className="increment">Phone Number : </span><a href="tel:{this.state.warehouses.phone_number}" className="profile-num">{this.state.warehouses.phone_number}</a></p>
	        			<hr />
	        			<p><span className="increment">Status : </span>{
	        				this.state.warehouses.status == '1' ?
	        				<span>Active</span>
	        				: <span>suspend</span>
	        			}</p>
	        			<hr />
	        			
	        				<div className="">
	        					<p className="increment">Order Assigned : </p>
	        				</div>
	        				 <div className="row">
	        					<div className="store-ul col-sm-4">
	        						<h4 className="storul-head">Amazon</h4>
	        						<li>
	        							<label>Clothing</label>
	        						</li>
	        						{/*<li>
	        							<input type="text" className="form-control custom" name="clothing"/>
	        						</li>*/}
	        						<li>
	        							<label>Electronic Accessories</label>
	        						</li>
	        					</div>
	        					<div className="store-ul col-sm-4">
	        						<h4 className="storul-head">eBay</h4>
	        						<li>
	        							<label>Mobile</label>
	        						</li>
	        						<li>
	        							<label>Sport</label>
	        						</li>
	        					</div>
	        					<div className="store-ul col-sm-4">
	        						<h4 className="storul-head">Shopify</h4>
	        						<li>
	        							<label>Clothes</label>
	        						</li>
	        					</div>

	        				
	        				{/*<hr />
	        				<a href="" className="assign-btn"> <i className="fa fa-plus plusplus" aria-hidden="true"></i> What to assign more store?</a>*/}
	        			</div>
	        
	        		</div>
	        	</div>
	        	<hr />
	        	{/*<div className=""><button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">Close</span></button></div>*/}
	        	
	        	</div>
        	</div>
        	<footer className="add_list_footer"><div className="row"><div className="col-sm-12"><p>© 2019 <a href="#" style={{color: 'rgb(7, 168, 222)'}}>VIP PARCEL</a></p></div></div></footer>
    	</div>
 
		);
	}
}

export default ViewWareHouse; 

