import React from 'react';
import ReactDOM from 'react-dom';

import './profile.css';
import Stephen from '../../images/side.jpg';

class Profile extends React.Component { 

    render() {

        return (

            <div className="content-wrap universelclass">
                <div className="col-lg-12">
                	<div className="profile-heading">
                		<h1>Your personal information <a href="/EditProfile" className="viewallbtn">Edit Profile</a></h1>
                	</div>
                    <div className="row per-profile-back">
                    	<div className="col-sm-4">
                            <div className="persen-img">
                                <img src={Stephen} alt="house" />
                            </div>
                        </div>
                    	<div className="col-sm-8">
                                <div className="person-Name">
                                    <h4> Stephen </h4>
                                    <h6> Theoretical physicist </h6>
                                </div>
                            <ul className="per-profile">
                                    
                                <li>
                                    <ul className="persen-info">
                                        <li>Name </li>
                                        <li> <span className="persen-dot"> : </span> Stephen Hawking</li>
                                    </ul>
                                    <ul  className="persen-info">
                                        <li>Email </li>
                                        <li> <span className="persen-dot"> : </span> stephenhawking@gmail.com</li>
                                    </ul>
                                    <ul className="persen-info">
                                        <li>Address </li>
                                        <li> <span className="persen-dot"> : </span> Oxford, United Kingdom</li>
                                    </ul>
                                    <ul className="persen-info">
                                        <li>Phone Number </li>
                                        <li> <span className="persen-dot"> : </span> (+000) 003 003 0052</li>
                                    </ul>
                                    <ul className="persen-info">
                                        <li>State </li>
                                        <li> <span className="persen-dot"> : </span> Oxford</li>
                                    </ul>
                                    <ul className="persen-info">
                                        <li>City </li>
                                        <li><span className="persen-dot"> : </span> Oxford</li>
                                    </ul>
                                    <ul className="persen-info">
                                        <li>Gender </li>
                                        <li><span className="persen-dot"> : </span> Male</li>
                                    </ul>
                                </li>
                            </ul>
                    		
                    	</div>
                    </div>
                </div>  
                <footer className="add_list_footer">   
                    <div className="row">  
                    <div className="col-sm-12">        
                    <p>© 2018 <a href="#" style={{color: '#2db4e3', textDecoration: 'none'}}>VIP PARCEL</a></p>    
                    </div>  
                    </div>
                </footer>         
            </div>


        );
    }
}

export default Profile;
