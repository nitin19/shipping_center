import React from 'react'
import ReactDOM from 'react-dom'

import profile from '../../images/prflimg.png';

class WareHouseDetail extends React.Component { 
render(){
	return(

		<div className="container-fuild left-margin">
			<div className="order-view2">
	        	<div className="order-ware-head">
	        		<h2>Order Processor Detail</h2>
	        	</div>
	        	<div className="order-view2-body">
	        	<div className="position-text">
	        		<p>Last Modified Date:</p>
	        		<p>Nov 26, 2018</p>
	        		{/*<a href="#" className="onetwo">Edit</a>*/}
	        	</div>
	        	
          <div className="idauto wow animate fadeInLeft " data-wow-delay="0.2s">  
          <div className="labsec">
          <p>First Name</p>  
          </div>
          <div className="writetext">
          <input type="text" className="form-control custom" />  
          </div>
          </div> 

           <div className="idauto wow animate fadeInLeft " data-wow-delay="0.4s">  
          <div className="labsec">
          <p>Last Name</p>  
          </div>
          <div className="writetext">
          <input type="text" className="form-control custom" />  
          </div>
          </div> 

           <div className="idauto wow animate fadeInLeft " data-wow-delay="0.6s">  
          <div className="labsec">
          <p>Email Address</p>  
          </div>
          <div className="writetext">
          <input type="text" className="form-control custom" />  
          </div>
          </div> 

          <div className="idauto wow animate fadeInLeft " data-wow-delay="0.4s">  
          <div className="labsec">
          <p>upload Image</p>  
          </div>
          <div className="writetext">
            <div className="upload-btn-wrapper-img">
              <button className="btn-gourav">Upload a file</button>
              <input type="file" name="myfile" />
            </div>
          </div>
          </div>

          <div className="idauto wow animate fadeInLeft " data-wow-delay="0.4s">  
          <div className="labsec">
          <p>Status</p>  
          </div>
          <div className="writetext">
          <input type="text" name="Status" name="" className="form-control custom" />  
          </div>
          </div>

          <div className="idauto wow animate fadeInLeft " data-wow-delay="0.8s">  
          <div className="labsec">
          <p>Phone Number</p>  
          </div>
          <div className="writetext">
          <input type="Number" className="form-control custom" />  
          </div>
          </div>

          {/* <div className="idauto wow animate fadeInLeft " data-wow-delay="0.2s">  
          <div className="labsec">
          <p>Password</p>  
          </div>
          <div className="writetext">
          <input type="Password" className="form-control custom" />  
          </div>
          </div>

          <div className="idauto wow animate fadeInLeft " data-wow-delay="0.4s">  
          <div className="labsec">
          <p>Confirm Password</p>  
          </div>
          <div className="writetext">
          <input type="Password" name="" className="form-control custom" />  
          </div>
          </div>*/}

        <div className="idauto wow animate fadeInLeft " data-wow-delay="0.6s">  
          <div className="labsec">
          <p>Is Fulfillment</p>  
          </div>
          <div className="writetext center">
          <span>
            <input type="radio" id="test3" name="radio-group" />
            <label for="test3">Yes</label>
          </span>
        <span>
          <input type="radio" id="test2" name="radio-group" />
          <label for="test2">No</label>
        </span>
          </div>
          </div>

            <div className="idauto wow animate fadeInLeft " data-wow-delay="0.8s">  
          <div className="labsec">
          <p>Asign Store</p>  
          </div>
          <div className="writetext">
          <input type="text" className="form-control custom" /> 
          <span className="shop">Amazon, Shopify, Ebay</span> 
          </div>
          </div>

          <div className="idauto wow animate fadeInLeft " data-wow-delay="0.2s">  

            <div className="writetext btnright">
                <button type="button" className="btn btn-secondary Cancel" data-dismiss="modal">Cancel</button><button className="btn btn-primary submit">Submit</button>
            </div>

          
          
          </div>

          
	        	<hr />
	        	<div className=""><button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">Close</span></button></div>
	        	
	        	</div>

	        	{/* // <!--productModal-->       */}
                    <div id="houseModal" className="modal">

                        {/* <!-- Modal content --> */}
                        <div className="modal-content">
                            <div className="add-prdt-header">
                                <p>Add Location</p>
                                <button type="button" className="close-sign" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                            </div>
                            <div className="add-prdt-middle">
                                <form onSubmit={this.handleSubmit}>
                                    <ul>
                                        <li>
                                            <div className="row">
                                                <div className="col-sm-4">
                                                    <p>Warehouse Image</p>
                                                </div>
                                                <div className="col-sm-8">
                                                    <img src={profile} alt="upload-img" className="prdt-upload-img" />
                                                    <div className="upload-btn-wrapper">
                                                        <button className="btn">Upload Images</button>
                                                        <input type="file" name="myfile" />
                                                    </div>

                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="row">
                                                <div className="col-sm-4">
                                                    <p>Warehouse</p>
                                                </div>
                                                <div className="col-sm-8">
                                                    <input type="text" name="name" value="" placeholder="Enter your warehouse name" />
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="row">
                                                <div className="col-sm-4">
                                                    <p>Address</p>
                                                </div>
                                                <div className="col-sm-8">
                                                    <input type="text" name="address" value="" placeholder="Enter your address" />
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="row">
                                                <div className="col-sm-4">
                                                    <p>City</p>
                                                </div>
                                                <div className="col-sm-8">
                                                    <input type="text" name="city" value="" placeholder="Your city name" />
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="row">
                                                <div className="col-sm-4">
                                                    <p>State</p>
                                                </div>
                                                <div className="col-sm-8">
                                                    <input type="text" name="state" value="" placeholder="Enter your state" />
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="row">
                                                <div className="col-sm-4">
                                                    <p>Country</p>
                                                </div>
                                                <div className="col-sm-8">
                                                    <input type="text" name="country" value="" placeholder="Enter your country" />
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="row">
                                                <div className="col-sm-4">
                                                    <p>Phone No.</p>
                                                </div>
                                                <div className="col-sm-8">
                                                    <input type="text" name="phone" value="" placeholder="eg. 326-789-0236" />
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="row">
                                                <div className="col-sm-4">

                                                </div>
                                                <div className="col-sm-8">
                                                    <button type="submit" className="Connect-btn" >Submit</button>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </form>
                            </div>
                            <div className="mod-footer">
                                {/* <!-- <span className="close1">Cancel</span>  --> */}
                            </div>
                        </div>
                    </div>
        	</div>
    	</div>
 
		);
	}
}

export default WareHouseDetail; 

