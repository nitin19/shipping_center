import ReactDOM from 'react-dom'  
import './singleorderdetail.css';
import amazon from '../../images/amazon-listing.png';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Redirect } from 'react-router-dom';
import { LOGIN_PAGE_PATH, API_TOKEN_NAME } from '../../constants';
import { scAxios } from '../..';
import { startUserSession } from '../userSession';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


const getItemDetails = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/stores/orderitemdetail', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
            
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}


class Singleorderdetail extends React.Component {
	state = {
		Details: [],
		system: [],
		order: [],
		store: [],
		orderprocessor: [],
		shippingaddress: '',
	}

componentDidMount() {
	const data = {
        itemId: this.props.match.params.id,
    }

	getItemDetails(data)
        .then(res => {
            if(res.status==1){
                var Details = res.detail;
                var system = Details.system;
                this.setState({
					Details: Details,
					system: system, 
					order: Details.order,
					orderprocessor: Details.orderprocessor
             	});
            }
        })
        .catch(err => {
            console.log(err);
        });

}

renderSwitch(param) {
  switch(param) {
    case 'new':
      return 'New';
    case 'in_process':
      return 'In Process';
    case 'shipped':
      return 'Shipped';
    case 'cancel':
      return 'Cancel';
    case 'full_refund':
      return 'Full Refund';
    case 'partial_refund':
      return 'Partial Refund';
    case 'partial_shipped':
      return 'Partial Shipped';
    case 'completed':
      return 'Completed';
    default:
      return 'New';
  }
}

render() {
        return (
        	<div className="content-wrap">
        	<div className="order-summary-back">   
			    <div className="order-summary">  
			    	<div className="row">
			    		<div className="col-sm-12">  
			    			  <h1>Order Summary</h1>
			    		</div>
			    	</div>
			    </div>
			    <div className="order-sumary-body">  
 					<div className="row">  
 						<div className="col-sm-3">
 							<h6>Tracking Id</h6> 
 							<h3>#{this.state.Details.tracking_id}</h3>    
 						</div>
 						<div className="col-sm-3">
 							<h6>Product Name</h6>   
 							<h3>{this.state.Details.item_name}</h3>      
 						</div>
 						<div className="col-sm-3">  
 							<h6>Store</h6>   
 							<h3><img src={this.state.system.logo_image} alt="#"  /></h3>    
 						</div>   
 						<div className="col-sm-3">
 						</div>
 					</div>
 					<hr />    
 					<div className="row">  
 						<div className="col-sm-3">
 							<h6>Warehouse</h6>   
 							<h5>Caruso Logistics </h5> 
 							<h4>8254 S. Garfield Street,Grand Island, NE 68801</h4>
 							      
 						</div>
 						<div className="col-sm-3">
 							<h6>Shipping to</h6>       
 							<h5>{this.state.order.customer_name}</h5>     
 							<h4>{this.state.order.shipping_address} </h4>    
 							    
 						</div>
 						
 						<div className="col-sm-3">  
 							<h6>Delivery</h6> 
 							<h5>Estimated Between</h5>   
 							<h4>Wednesday, October 7 to Ocober 12</h4>
 							       
 						</div>  
 						<div className="col-sm-3">  
 							<h6>Date of submission</h6>   
 							<h5>{this.state.Details.order_date}</h5>                
 						</div>      
 					</div>
 					<div className="row">  
 						<div className="col-sm-3">
 							<a href="mailto:r.micheal02@gmail.com" className="oredr-email">r.micheal02@gmail.com</a> 
 							<a href="tel:+1 256-896-3256" className="oredr-email">+1 256 - 896 - 3256</a>          
 						</div>
 						<div className="col-sm-3">   
 							<a href="mailto:r.micheal02@gmail.com" className="oredr-email">{this.state.order.customer_email}</a>
 							<a href="tel:+1 256-896-3256" className="oredr-email">{this.state.order.customer_phone}</a>         
 						</div>
 						
 						<div className="col-sm-6">  
 							<h5>INSTRUCTION</h5>   
 							<p className="ipsum-ins">1. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
 							<p className="ipsum-ins">2. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>       
 						</div>  
 						      
 					</div>
 					<ul className="step-bar">  
 						<li className="step-one"><p></p></li>  
 						<li className="step-two"><p></p></li>
 						<li className="step-three"><p></p></li>  
 						<li className="step-four"><p></p></li>  
 						<li className="step-five"><p></p></li>    
 					</ul>
			    </div>
			    <div className="order-sumary-body-footer">   
			    	  
			    		<div className="order-sumary-body-width">
			    			<h6>Package</h6>  
			    			<p>
			    				<select className="order-slectbox singl-slect">      
									<option value="volvo"> Select-1 </option>  
									<option value="saab"> Select-2 </option>
									<option value="opel"> Select-3 </option>
									<option value="audi"> Select-4 </option>  
								</select>
							</p> 
			    		</div>
			    		<div className="order-sumary-body-width">  
			    			<h6>Order ID</h6>
			    			<p>#{this.state.order.order_id}</p>  
			    		</div>
			    		<div className="order-sumary-body-width">  
			    			<h6>Sku Number</h6> 
			    			<p>{this.state.Details.sellersku}</p> 
			    		</div>  
			    		<div className="order-sumary-body-width">  
			    			<h6>weight</h6> 
			    			<p>{this.state.Details.weight}</p> 
			    		</div>  
			    		<div className="order-sumary-body-width">    
			    			<h6>Quantity</h6>
			    			<p>{this.state.Details.item_qty}</p>
			    		</div>
			    		<div className="order-sumary-body-width">  
			    			<h6>Order Status</h6> 
			    			<p>{this.renderSwitch(this.state.Details.item_status)}</p>
			    		</div>  
			    		<div className="order-sumary-body-width">
			    			<h6>Date of paid</h6>    
			    			<p>{this.state.Details.shipped_date}</p>     
			    		</div>
			    		<div className="order-sumary-body-width">  
			    			<h6>Asign to</h6>   
			    			<p>{this.state.Details.processor_id != '' && this.state.Details.processor_id != null ? this.state.orderprocessor.first_name+' '+this.state.orderprocessor.last_name: 'Not Assigned'}</p>    
			    		</div>  
			    	
			    </div>
			    <div className="order-sumary-body-btn">
				    <div className="row">     
				    	<div className="col-sm-12">   
				    		<p className="text-right"><a href={'../print-shipping/'+this.state.Details.order_item_id} className="Connect-btn" >Confirm</a> </p>   
						</div>
					</div>  
				</div>
			</div>
			</div>       
        );
    }

}

export default Singleorderdetail;      