import React from 'react';


import eBay from '../../images/myStores/ebayLogo.png';
import amazonLogoTra from '../../images/myStores/amazon-logo_tra.png';


class SingleOrderView extends React.Component {

    state = {
        search2: '',
        bday: '',
        bday2: ''
    }


    handleChange = event => {
        this.setState({ [event.target.name]: event.target.value });
    }

    handleSubmit = event => {
        event.preventDefault();

    }

    render() {
        return (
            <div className="content-wrap">
                <div className="storemiddle-sec">
                    <div className="addstore-list-text">
                        <div className="col-sm-12 p-0">
                            <div className="example" action="/action_page.php">
                                <input type="text" placeholder="Process Batch" name="search2" value={this.state.search2} onChange={this.handleChange} />
                                <button type="submit"></button>
                            </div>
                            <a href="#" className="order-samebtn">Remove From Batch</a>
                            <a href="#" className="order-samebtn">Cancel Batch</a>
                            <a href="#" className="order-samebtn">Scan Batch</a>
                            <a href="#" className="order-samebtn" style={{ marginLeft: 10, marginRight: 10 }}>Bulk Action</a>
                            <a href="#" className="order-samebtn" >Print</a>
                            <a href="#" className="order-samebtn">Download Pdf</a>
                            <form className="example serch-form" action="/action_page.php"><input type="text" placeholder="Search Order" name="search2" value="" /></form>
                        </div>
                    </div>
                    <hr />
                    <div className="row pl-3">
                        <div className="col-sm-5">
                            <div className="row ">
                                <div className="col-sm-2 p-1">
                                    <select className="order-slectbox">
                                        <option value="volvo">Stores</option>
                                        <option value="saab">Stores</option>
                                        <option value="opel">Stores</option>
                                        <option value="audi">Stores</option>
                                    </select>
                                </div>
                                <div className="col-sm-3 p-1">
                                    <select className="order-slectbox">
                                        <option value="volvo">Destination</option>
                                        <option value="saab">Destination</option>
                                        <option value="opel">Destination</option>
                                        <option value="audi">Destination</option>
                                    </select>
                                </div>
                                <div className="col-sm-3 p-1">
                                    <select className="order-slectbox">
                                        <option value="volvo">Order Dates</option>
                                        <option value="saab">Order Dates</option>
                                        <option value="opel">Order Dates</option>
                                        <option value="audi">Order Dates</option>
                                    </select>
                                </div>
                                <div className="col-sm-3 p-1">
                                    <select className="order-slectbox">
                                        <option value="volvo">Order Status</option>
                                        <option value="saab">Order Status</option>
                                        <option value="opel">Order Status</option>
                                        <option value="audi">Order Status</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-7">
                            <div className="row add-list-slet">
                                <div className="col-sm-4 paadd">
                                    <div className="list-date">
                                        Dates: <input type="date" name="bday" className="first-date" value={this.state.bday} onChange={this.handleChange} />
                                    </div>
                                </div>
                                <div className="col-sm-4 paadd">
                                    <div className="list-date">
                                        to  <input type="date" name="bday" className="second-date" value={this.state.bday2} onChange={this.handleChange} />
                                    </div>
                                </div>
                                <div className="col-sm-4 paadd add-text-sort">
                                    Sort By:
									<select className="list-slectbox">
                                        <option value="volvo">A to Z</option>
                                        <option value="saab">A to Z</option>
                                        <option value="opel">A to Z</option>
                                        <option value="audi">A to Z</option>
                                    </select>

                                    <select className="list-slectbox">
                                        <option value="volvo">20</option>
                                        <option value="saab">20</option>
                                        <option value="opel">20</option>
                                        <option value="audi">20</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="store-listing-table">
                    <div className="row storlisthead">
                        <div className="col-sm-1">
                            <span className="listtxt-1">S.No.</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listtxt-1">Store Name</span>
                        </div>
                        <div className="col-sm-2">
                            <span className="listtxt-1">Product Name</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listtxt-1">SKU No.</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listtxt-1">Order ID</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listtxt-1">Qty.</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listtxt-1">Order Date</span>
                        </div>
                        <div className="col-sm-2">
                            <span className="listtxt-1">Order Status</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listtxt-1">Asignee</span>
                        </div>
                        
                        <div className="col-sm-1">
                            <span className="listtxt-1">Action</span>
                        </div>
                    </div>
                    <div className="row storlistbody">
                        <div className="col-sm-1">
                            <span className="listext-1">01</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">Amazon.com</span>
                        </div>
                        <div className="col-sm-2">
                            <span className="listext-1">Nike Lunarcharge Essential</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">365L</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">#3265895</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">2</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">10/26/2018</span>
                        </div>
                        <div className="col-sm-2">
                            <span className="listext-1">Partially Complete</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">George T. Austin</span>
                        </div>
                        
                        <div className="col-sm-1">
                            <select className="list-slectboxs">
                                <option value="volvo">Action</option>
                                <option value="saab">Edit</option>
                                <option value="opel">Delete</option>
                                <option value="audi">View Detail</option>
                            </select>
                        </div>
                    </div>

                    <div className="row storlistbody">
                        <div className="col-sm-1">
                            <span className="listext-1">02</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">Amazon.com</span>
                        </div>
                        <div className="col-sm-2">
                            <span className="listext-1">Nike Lunarcharge Essential</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">365L</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">#3265895</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">2</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">10/26/2018</span>
                        </div>
                        <div className="col-sm-2">
                            <span className="listext-1">Partially Complete</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">George T. Austin</span>
                        </div>
                        
                        <div className="col-sm-1">
                            <select className="list-slectboxs">
                                <option value="volvo">Action</option>
                                <option value="saab">Edit</option>
                                <option value="opel">Delete</option>
                                <option value="audi">View Detail</option>
                            </select>
                        </div>
                    </div>

                    <div className="row storlistbody">
                        <div className="col-sm-1">
                            <span className="listext-1">03</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">Amazon.com</span>
                        </div>
                        <div className="col-sm-2">
                            <span className="listext-1">Nike Lunarcharge Essential</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">365L</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">#3265895</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">2</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">10/26/2018</span>
                        </div>
                        <div className="col-sm-2">
                            <span className="listext-1">Partially Complete</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">George T. Austin</span>
                        </div>
                        
                        <div className="col-sm-1">
                            <select className="list-slectboxs">
                                <option value="volvo">Action</option>
                                <option value="saab">Edit</option>
                                <option value="opel">Delete</option>
                                <option value="audi">View Detail</option>
                            </select>
                        </div>
                    </div>

                    <div className="row storlistbody">
                        <div className="col-sm-1">
                            <span className="listext-1">04</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">Amazon.com</span>
                        </div>
                        <div className="col-sm-2">
                            <span className="listext-1">Nike Lunarcharge Essential</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">365L</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">#3265895</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">2</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">10/26/2018</span>
                        </div>
                        <div className="col-sm-2">
                            <span className="listext-1">Partially Complete</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">George T. Austin</span>
                        </div>
                        
                        <div className="col-sm-1">
                            <select className="list-slectboxs">
                                <option value="volvo">Action</option>
                                <option value="saab">Edit</option>
                                <option value="opel">Delete</option>
                                <option value="audi">View Detail</option>
                            </select>
                        </div>
                    </div>

                    <div className="row storlistbody">
                        <div className="col-sm-1">
                            <span className="listext-1">05</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">Amazon.com</span>
                        </div>
                        <div className="col-sm-2">
                            <span className="listext-1">Nike Lunarcharge Essential</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">365L</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">#3265895</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">2</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">10/26/2018</span>
                        </div>
                        <div className="col-sm-2">
                            <span className="listext-1">Partially Complete</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">George T. Austin</span>
                        </div>
                        
                        <div className="col-sm-1">
                            <select className="list-slectboxs">
                                <option value="volvo">Action</option>
                                <option value="saab">Edit</option>
                                <option value="opel">Delete</option>
                                <option value="audi">View Detail</option>
                            </select>
                        </div>
                    </div>
                    <div className="row storlistbody">
                        <div className="col-sm-1">
                            <span className="listext-1">06</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">Amazon.com</span>
                        </div>
                        <div className="col-sm-2">
                            <span className="listext-1">Nike Lunarcharge Essential</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">365L</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">#3265895</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">2</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">10/26/2018</span>
                        </div>
                        <div className="col-sm-2">
                            <span className="listext-1">Partially Complete</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">George T. Austin</span>
                        </div>
                        
                        <div className="col-sm-1">
                            <select className="list-slectboxs">
                                <option value="volvo">Action</option>
                                <option value="saab">Edit</option>
                                <option value="opel">Delete</option>
                                <option value="audi">View Detail</option>
                            </select>
                        </div>
                    </div>
                    <div className="row storlistbody">
                        <div className="col-sm-1">
                            <span className="listext-1">07</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">Amazon.com</span>
                        </div>
                        <div className="col-sm-2">
                            <span className="listext-1">Nike Lunarcharge Essential</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">365L</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">#3265895</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">2</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">10/26/2018</span>
                        </div>
                        <div className="col-sm-2">
                            <span className="listext-1">Partially Complete</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">George T. Austin</span>
                        </div>
                        
                        <div className="col-sm-1">
                            <select className="list-slectboxs">
                                <option value="volvo">Action</option>
                                <option value="saab">Edit</option>
                                <option value="opel">Delete</option>
                                <option value="audi">View Detail</option>
                            </select>
                        </div>
                    </div>
                    <div className="row storlistbody">
                        <div className="col-sm-1">
                            <span className="listext-1">08</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">Amazon.com</span>
                        </div>
                        <div className="col-sm-2">
                            <span className="listext-1">Nike Lunarcharge Essential</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">365L</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">#3265895</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">2</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">10/26/2018</span>
                        </div>
                        <div className="col-sm-2">
                            <span className="listext-1">Partially Complete</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">George T. Austin</span>
                        </div>
                        
                        <div className="col-sm-1">
                            <select className="list-slectboxs">
                                <option value="volvo">Action</option>
                                <option value="saab">Edit</option>
                                <option value="opel">Delete</option>
                                <option value="audi">View Detail</option>
                            </select>
                        </div>
                    </div>
                    <div className="row storlistbody">
                        <div className="col-sm-1">
                            <span className="listext-1">09</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">Amazon.com</span>
                        </div>
                        <div className="col-sm-2">
                            <span className="listext-1">Nike Lunarcharge Essential</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">365L</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">#3265895</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">2</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">10/26/2018</span>
                        </div>
                        <div className="col-sm-2">
                            <span className="listext-1">Partially Complete</span>
                        </div>
                        <div className="col-sm-1">
                            <span className="listext-1">George T. Austin</span>
                        </div>
                        
                        <div className="col-sm-1">
                            <select className="list-slectboxs">
                                <option value="volvo">Action</option>
                                <option value="saab">Edit</option>
                                <option value="opel">Delete</option>
                                <option value="audi">View Detail</option>
                            </select>
                        </div>
                    </div>
                    <div className="paggination-text">
                        <ul className="pagination pagination-sm">
                            <li className="page-item"><a className="page-link" href="#">Previous</a></li>
                            <li className="page-item"><a className="page-link" href="#">1</a></li>
                            <li className="page-item"><a className="page-link" href="#">2</a></li>
                            <li className="page-item"><a className="page-link" href="#">3</a></li>
                            <li className="page-item"><a className="page-link" href="#">Next</a></li>
                        </ul>
                    </div>

                    <div className="" style={{ position: 'relative' }}>
                        <div className="loader"></div>
                    </div> 
                    
                </div>
                <footer className="add_list_footer">   
                    <div className="row"> 
                    <div className="col-sm-6">          
                    <p>© 2018 <a href="#" style={{color: '#07a8de'}}>VIP PARCEL</a></p>    
                    </div>
                    <div className="col-sm-6">  
                        <a href="https://www.binarydata.in/" target="_blank" className="Binary_design">Designed By Binary Data</a>    
                    </div>
                    </div>
                </footer>
            </div>
        );
    }
}

export default SingleOrderView;
