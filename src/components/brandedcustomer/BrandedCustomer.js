import React from 'react';
import { SketchPicker } from 'react-color';

import './brandedcustomer.css';

class BrandedCustomer extends React.Component {

	render() {

		return (

				<div className="content-wrap universelclass">
         <div className="col-lg-12">
            <div className="sorting-list">
               <h1>Branding Default</h1>
               
            </div>
            <div className="storeset">
               <p>You have 0 of 1 store(s) using the Branded Tracking Page. Go to <span>Store Setup</span> to switch the rest of your stores to ShipStation's Branded Tracking Page. The following stores are not connected:<span>Manual Orders</span></p>
            </div>
            <div className="Brandedcus">
               <p>Branded Customer Pages include the Tracking Page and the Returns Portal. These pages allow you to re-engage your customers after their packages have shipped, and save time by allowing your customers to initiate the return process themselves.</p>
            </div>
            <div className="stioreac">
               <h1>Each store for your account can use the settings below or have its own settings. The settings below will be used if a store doesn't override them specifically.</h1>
            </div>
            <div className="customcolorbox">
               <div className="colorbox1">
                  <div className="storelogo">
                     <label className="customcheck">
                     <input type="checkbox" name="" value=""/>
                     <span className="checkmark"></span><span className="showstore">Show Store Logo</span>
                     </label>
                     <p>Configured for each store</p>
                  </div>
                  <div className="storelogo">
                     <label className="customcheck">
                     <input type="checkbox" name="" value="" />
                     <span className="checkmark"></span><span className="showstore">Custom Colors</span>
                     </label>
                     <p>Use custom colors that match your brand</p>
                  </div>
                  <div className="morecheck">
                  	<div className="row">
                  		<div className="col-sm-6">
                  			<label>Status Bar:</label>
                  			<SketchPicker />
                  		</div>
                  		<div className="col-sm-6">
                  			<label>Title Text:</label>
                  			<SketchPicker />
                  		</div>
                  	</div>
                  	<div className="row">
                  		<div className="col-sm-6">
                  			<label>Map Markers:</label>
                  			<SketchPicker />
                  		</div>
                  		<div className="col-sm-6">
                  			
                  		</div>
                  	</div>
                  	
                  </div>
                  <div className="storelogo">
                     <label className="customcheck">
                     <input type="checkbox" name="" value="" />
                     <span className="checkmark"></span><span className="showstore">Show Social Media Links</span>
                     </label>
                     <p>For each social media profile that is configured for each store</p>
                  </div>
                  <div className="storelogo">
                     <label className="customcheck">
                     <input type="checkbox" name="" value="" />
                     <span className="checkmark"></span><span className="showstore">Show Order Detail</span>
                     </label>
                     <p>For each social media profile that is configured for each store</p>
                  </div>
                  <div className="morecheck">
                     <div className="showitem storelg">
                        <label className="customcheck">
                        <input type="checkbox" name="" value="" />
                        <span className="checkmark"></span><span className="showstore">Show Item Images</span>
                        </label>
                        <label className="customcheck">
                        <input type="checkbox" name="" value="" />
                        <span className="checkmark"></span><span className="showstore">Show Item Options</span>
                        </label>
                     </div>
                     <div className="showimg storelg">
                        <label className="customcheck">
                        <input type="checkbox" name="" value="" />
                        <span className="checkmark"></span><span className="showstore">Show Item SKUs</span>
                        </label>
                        <label className="customcheck">
                        <input type="checkbox" name="" value="" />
                        <span className="checkmark"></span><span className="showstore">Show Price</span>
                        </label>
                     </div>
                  </div>
               </div>
               <div className="colorbox2">
                  <div className="storelogo">
                     <label className="customcheck">
                     <input type="checkbox" name="" value="" />
                     <span className="checkmark"></span><span className="showstore">Show Service Menu</span>
                     </label>
                     <p>For each social media profile that is configured for each store</p>
                  </div>
                  <div className="webshow">
                     <div className="showinline">
                        <div className="website storelg">
                           <label className="customcheck">
                           <input type="checkbox" name="" value="" />
                           <span className="checkmark"></span><span className="showstore">Website</span>
                           </label>
                        </div>
                        <div className="config">
                           <p>Configured for each store</p>
                        </div>
                     </div>
                     <div className="showinline">
                        <div className="website storelg">
                           <label className="customcheck">
                           <input type="checkbox" name="" value="" />
                           <span className="checkmark"></span><span className="showstore">Aditional Links</span>
                           </label>
                        </div>
                        <div className="config">
                           <p>Provide additional links to your slae items, reviews, pages, etc</p>
                        </div>
                     </div>
                     <div className="showinline urlmain">
                        <div className="weburlshow">
                           <div className="website">
                              <form className="form-inline">
                                 <label>Text:</label>
                                 <input type="text" className="form-control" name="" value="" />
                              </form>
                           </div>
                           <div className="config">
                              <form className="form-inline">
                                 <label>Url:</label>
                                 <input type="text" className="form-control" name="" value="" />
                              </form>
                           </div>
                        </div>
                        <div className="weburlshow">
                           <div className="website">
                              <form className="form-inline">
                                 <label>Text:</label>
                                 <input type="text" className="form-control" name="" value="" />
                              </form>
                           </div>
                           <div className="config">
                              <form className="form-inline">
                                 <label>Url:</label>
                                 <input type="text" className="form-control" name="" value="" />
                              </form>
                           </div>
                        </div>
                        <div className="weburlshow">
                           <div className="website">
                              <form className="form-inline">
                                 <label>Text:</label>
                                 <input type="text" className="form-control" name="" value="" />
                              </form>
                           </div>
                           <div className="config">
                              <form className="form-inline">
                                 <label>Url:</label>
                                 <input type="text" className="form-control" name="" value="" />
                              </form>
                           </div>
                        </div>
                     </div>
                     <div className="returnpolicy">
                        <div className="storelogo">
                           <label className="customcheck">
                           <input type="checkbox" name="" value="" />
                           <span className="checkmark"></span><span className="showstore">Return Policy</span>
                           </label>
                           <p>Configured for each store</p>
                        </div>
                        <div className="storelogo">
                           <label className="customcheck">
                           <input type="checkbox" name="" value="" />
                           <span className="checkmark"></span><span className="showstore">Contact Us</span>
                           </label>
                           <p>Configured for each store</p>
                        </div>
                     </div>
                     <div className="showinline">
                        <div className="website storelg">
                           <label className="customcheck">
                           <input type="checkbox" name="" value="" />
                           <span className="checkmark"></span><span className="showstore">Contact Email</span>
                           </label>
                        </div>
                        <div className="config">
                           <p>Configured for each store</p>
                        </div>
                     </div>
                     <div className="showinline">
                        <div className="website storelg">
                           <label className="customcheck">
                           <input type="checkbox" name="" value="" />
                           <span className="checkmark"></span><span className="showstore">Contact Phone</span>
                           </label>
                        </div>
                        <div className="config">
                           <p>Configured for each store</p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div className="privebtn">
               <button className="btn btn-secondary preview">Preview Changes</button> 
               <button type="button" className="btn btn-primary changes">Save Changes</button>
            </div>
         </div>
         <footer className="add_list_footer" style={{float: 'left', width: '100%'}}>
             <div className="row">
                 <div className="col-sm-6">
                     <p>© 2018 <a href="#" style={{color: 'rgb(7, 168, 222)'}}>VIP PARCEL</a></p>
                 </div>
                 <div className="col-sm-6">
                     <a href="https://www.binarydata.in/" target="_blank" className="Binary_design">Designed By Binary Data</a>
                 </div>
             </div>
         </footer>
      </div>

			);

	}

}

export default BrandedCustomer;