import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import * as userActions from '../actions/userActions';
import Login from './login/Login';
import PrivateRoute from './PrivateRoute';

import Signup from './signup/Signup';
import Dashboard from './dashboard/Dashboard';

import ForgotPassword from './forgorPassword/ForgotPassword';
import './app.css';
import Logout from './logout/Logout';
import VerifyPhoneNumber from './verifyPhoneNumber/VerifyPhoneNumber';
import { LOGIN_PAGE_PATH } from '../constants';
import ResetPassword from './resetPassword/ResetPassword';





class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route path='/signup' component={Signup} />
          <Route path='/phone-verification' component={VerifyPhoneNumber} />

          <Route path={LOGIN_PAGE_PATH} component={Login} />
          <Route path='/logout' component={Logout} />
          <Route path='/forgot-password' component={ForgotPassword} />
          <Route path='/reset-password/:token/:email' component={ResetPassword} />

          <PrivateRoute path='/' component={Dashboard} user={this.props.user} />
          
        </Switch>
      </Router>
    );
  }
}


const mapStateToProps = (state /*, ownProps*/) => {
  return {
    user: state.user,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(userActions, dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
