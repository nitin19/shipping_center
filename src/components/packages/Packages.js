import React from 'react';
import './packages.css';
import Skeleton from 'react-loading-skeleton';

class Packages extends React.Component {

    state = {
        test11: '',
        test10: '',
        test9: '',
        test8: '',
        test7: '',
        test6: '',
        test5: '',
        test4: '',
        test3: '',
        test2: '',
        test1: '',
        fname: '',
        quantity: 1,
        quantity2: 1,
        quantity3: 1,
    }

    handleChange = event => {
        this.setState({ [event.target.name]: event.target.value });
    }

    handleSubmit = event => {
        event.preventDefault();

    }

    render() {
        return (

            <div className="content-wrap universelclass">

                <div className="content-wrap backcollor">
                    <div className="ms-stor">
                        <div className="row mstop">
                            <div className="col-sm-4">
                                {this.props.title || <Skeleton count={1} duration={2} height={40} />}
                            </div>
                            <div className="col-sm-4">
                                
                            </div>
                            <div className="col-sm-4">
                               
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-8">
                                <div className="button2">
                                    <h6>{this.props.title || <Skeleton />}</h6>
                                    {this.props.body || <Skeleton count={10} duration={2} height={30} />}
                                </div>
                            </div>
                            <div className="col-sm-4">
                                {this.props.title || <Skeleton count={1} duration={2} height={200} />}
                            </div>
                        </div>    
                    </div>
                </div>

                <div className="package-back">
                    <div className="package-head">
                        <div className="row wow animate fadeInLeft " data-wow-delay="0.4s">
                            <div className="col-sm-12">
                                <h1>Manage package type</h1>
                                <select className="packages-slectbox">
                                    <option value="volvo">Order Status</option>
                                    <option value="saab">Order Status</option>
                                    <option value="opel">Order Status</option>
                                    <option value="audi">Order Status</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div className="package-table">
                        <div className="row">
                            <div className="col-sm-9 wow animate fadeInLeft pakwidth" data-wow-delay="0.4s">
                                <div className="row package-table-head">
                                    <div className="col-sm-2">
                                        <span className="customfloatvip12"> 
                                            <span className="package-list-head">S.No.</span>
                                        </span>
                                    </div>
                                    <div className="col-sm-10">
                                        <span className="customfloatvip12"> 
                                            <span className="package-list-head">Name</span>
                                        </span>
                                    </div>
                                </div>
                                <div className="row package-table-body">
                                    <div className="col-sm-2 pack-head-ing">
                                        <span className="customfloatvip12"> 
                                            <span className="package-list-body">Package</span>
                                        </span>
                                    </div>
                                    <div className="col-sm-10 pack-head-ing">
                                        <span className="customfloatvip12"> 
                                            <span className="package-list-body">Package</span>
                                        </span>
                                    </div>
                                </div>
                                <div className="row package-table-body">
                                    <div className="col-sm-2 pack-head-ing">
                                        <form action="#">
                                            <span className="customfloatvip12"> 
                                                <p className="package-checkmar">
                                                    <label className="customcheck">
                                                         <input type="checkbox" name="test11" value={this.state.test11} onChange={this.handleChange} />
                                                        <span className="checkmark"></span>
                                                    </label>
                                                </p>
                                            </span>
                                        </form>
                                    </div>
                                    <div className="col-sm-10 pack-head-ing">
                                        <span className="customfloatvip12"> 
                                            <span className="package-list-body">Cubic</span>
                                        </span>
                                    </div>
                                </div>
                                <div className="row package-table-body">
                                    <div className="col-sm-2 pack-head-ing">
                                        <form action="#">
                                            <span className="customfloatvip12"> 
                                                <p className="package-checkmar">
                                                    <label className="customcheck">
                                                    <input type="checkbox" name="test10" value={this.state.test10} onChange={this.handleChange} />
                                                        <span className="checkmark"></span>
                                                    </label>
                                                </p>
                                            </span>
                                        </form>
                                    </div>
                                    <div className="col-sm-10 pack-head-ing">
                                        <span className="customfloatvip12"> 
                                            <span className="package-list-body">Flat Rate Envelope</span>
                                        </span>
                                    </div>
                                </div>
                                <div className="row package-table-body">
                                    <div className="col-sm-2 pack-head-ing">
                                        <form action="#">
                                            <span className="customfloatvip12"> 
                                                <p className="package-checkmar">
                                                    <label className="customcheck">
                                                     <input type="checkbox" name="test9" value={this.state.test9} onChange={this.handleChange} />
                                                        <span className="checkmark"></span>
                                                    </label>
                                                </p>
                                            </span>
                                        </form>
                                    </div>
                                    <div className="col-sm-10 pack-head-ing">
                                        <span className="customfloatvip12">
                                            <span className="package-list-body">Flat Rate Legal Envelope</span>
                                        </span>
                                    </div>
                                </div>
                                <div className="row package-table-body">
                                    <div className="col-sm-2 pack-head-ing">
                                        <form action="#">
                                            <span className="customfloatvip12"> 
                                                <p className="package-checkmar">
                                                    <label className="customcheck">
                                                      <input type="checkbox" name="test8" value={this.state.test8} onChange={this.handleChange} />
                                                        <span className="checkmark"></span>
                                                    </label>
                                                </p>
                                            </span>
                                        </form>
                                    </div>
                                    <div className="col-sm-10 pack-head-ing">
                                        <span className="customfloatvip12"> 
                                            <span className="package-list-body">Flat Rate Padded Envelope</span>
                                        </span>
                                    </div>
                                </div>
                                <div className="row package-table-body">
                                    <div className="col-sm-2 pack-head-ing">
                                        <form action="#">
                                            <span className="customfloatvip12"> 
                                                <p className="package-checkmar">
                                                    <label className="customcheck">
                                                      <input type="checkbox" name="test7" value={this.state.test7} onChange={this.handleChange} />
                                                        <span className="checkmark"></span>
                                                    </label>
                                                </p>
                                            </span>
                                        </form>
                                    </div>
                                    <div className="col-sm-10 pack-head-ing">
                                        <span className="customfloatvip12"> 
                                            <span className="package-list-body">Large Envelope or Flat</span>
                                        </span>
                                    </div>
                                </div>
                                <div className="row package-table-body">
                                    <div className="col-sm-2 pack-head-ing">
                                        <form action="#">
                                            <span className="customfloatvip12"> 
                                                <p className="package-checkmar">
                                                    <label className="customcheck">
                                                        <input type="checkbox" name="test6" value={this.state.test6} onChange={this.handleChange} />
                                                        <span className="checkmark"></span>
                                                    </label>
                                                </p>
                                            </span>
                                        </form>
                                    </div>
                                    <div className="col-sm-10 pack-head-ing">
                                        <span className="customfloatvip12"> 
                                            <span className="package-list-body">Large Flat Rate Box</span>
                                        </span>
                                    </div>
                                </div>
                                <div className="row package-table-body">
                                    <div className="col-sm-2 pack-head-ing">
                                        <form action="#">
                                            <span className="customfloatvip12"> 
                                                <p className="package-checkmar">
                                                    <label className="customcheck">
                                                     <input type="checkbox" name="test5" value={this.state.test5} onChange={this.handleChange} />
                                                        <span className="checkmark"></span>
                                                    </label>
                                                </p>
                                            </span>
                                        </form>
                                    </div>
                                    <div className="col-sm-10 pack-head-ing">
                                        <span className="customfloatvip12"> 
                                            <span className="package-list-body">Large Package (any side > 12")</span>
                                        </span>
                                    </div>
                                </div>
                                <div className="row package-table-body">
                                    <div className="col-sm-2 pack-head-ing">
                                        <form action="#">
                                            <span className="customfloatvip12"> 
                                                <p className="package-checkmar">
                                                    <label className="customcheck">
                                                       <input type="checkbox" name="test4" value={this.state.test4} onChange={this.handleChange} />
                                                        <span className="checkmark"></span>
                                                    </label>
                                                </p>
                                            </span>
                                        </form>
                                    </div>
                                    <div className="col-sm-10 pack-head-ing">
                                        <span className="customfloatvip12"> 
                                            <span className="package-list-body">Medium Flat Rate Box</span>
                                        </span>
                                    </div>
                                </div>
                                <div className="row package-table-body">
                                    <div className="col-sm-2 pack-head-ing">
                                        <form action="#">
                                            <span className="customfloatvip12"> 
                                                <p className="package-checkmar">
                                                    <label className="customcheck">
                                                        <input type="checkbox" name="test4" value={this.state.test4} onChange={this.handleChange} />
                                                        <span className="checkmark"></span>
                                                    </label>
                                                </p>
                                            </span>
                                        </form>
                                    </div>
                                    <div className="col-sm-10 pack-head-ing">
                                        <span className="customfloatvip12">
                                            <span className="package-list-body">Regional Rate Box A</span>
                                        </span>
                                    </div>
                                </div>
                                <div className="row package-table-body">
                                    <div className="col-sm-2 pack-head-ing">
                                        <form action="#">
                                            <span className="customfloatvip12"> 
                                                <p className="package-checkmar">
                                                    <label className="customcheck">
                                                        <input type="checkbox" name="test3" value={this.state.test3} onChange={this.handleChange} />
                                                        <span className="checkmark"></span>
                                                    </label>
                                                </p>
                                            </span>
                                        </form>
                                    </div>
                                    <div className="col-sm-10 pack-head-ing">
                                        <span className="customfloatvip12">
                                            <span className="package-list-body">Regional Rate Box B</span>
                                        </span>
                                    </div>
                                </div>
                                <div className="row package-table-body">
                                    <div className="col-sm-2 pack-head-ing">
                                        <form action="#">
                                            <span className="customfloatvip12"> 
                                                <p className="package-checkmar">
                                                    <label className="customcheck">
                                                       <input type="checkbox" name="test2" value={this.state.test2} onChange={this.handleChange} />
                                                        <span className="checkmark"></span>
                                                    </label>
                                                </p>
                                            </span>
                                        </form>
                                    </div>
                                    <div className="col-sm-10 pack-head-ing">
                                        <span className="customfloatvip12">
                                            <span className="package-list-body">Small Flat Rate Box</span>
                                        </span>
                                    </div>
                                </div>
                                <div className="row package-table-body">
                                    <div className="col-sm-2 pack-head-ing">
                                        <form action="#">
                                            <span className="customfloatvip12"> 
                                                <p className="package-checkmar">
                                                    <label className="customcheck">
                                                        <input type="checkbox" name="test1" value={this.state.test1} onChange={this.handleChange} />
                                                        <span className="checkmark"></span>
                                                    </label>
                                                </p>
                                            </span>
                                        </form>
                                    </div> 
                                    <div className="col-sm-10 pack-head-ing">
                                        <span className="customfloatvip12">
                                            <span className="package-list-body">Thick Envelope</span>
                                        </span>
                                    </div>
                                </div>
                                <div className="row package-table-body">
                                    <div className="col-sm-2">

                                    </div>
                                    <div className="col-sm-10 text-right">
                                        <a href="" className="Connect-btn">View All</a>
                                    </div>
                                </div>
                                <div className="package-middle-footer wow animate fadeInLeft " data-wow-delay="0.4s">
                                    <h4>Your Packaging</h4>
                                    <p>Create custom packages to use in addition to standard carrier packaging. These custom packages will be available to  your users when shipping.</p>
                                    <p className="text-right"><a href="#" data-toggle="modal" data-target="#myModal" className="Connect-btn"><i className="fa fa-plus-circle"></i> Create A New Package</a> </p>
                                </div>
                                

                                <div className="custom_tabel">
                                    <table className="table">
                                      <thead>
                                        <tr className="bg">
                                          <th>Name</th>
                                          <th>Length</th>
                                          <th>Width</th>
                                          <th>Height</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <tr className="wow animate fadeInLeft " data-wow-delay="0.4s">
                                          <td className="packtable"><span className="customfloatvip">Clothes</span></td>
                                          <td className="packtable"><span className="customfloatvip">Clothes</span></td>
                                          <td className="packtable"><span className="customfloatvip">Clothes</span></td>
                                          <td className="packtable"><span className="customfloatvip">Clothes</span></td>
                                        </tr>
                                      </tbody>
                                    </table>
                                    
                                   </div>


                            </div>
                            <div className="col-sm-3 wow fadeInRight animated pakwidth" data-wow-delay="0.8s">
                                <div className="package-table-sidebar">
                                    <p>Chances are, your business doesn't use all available package types for all carriers. You can restrict the list of package types available to your users by selecting the carrier below and then selecting which package types to show.</p>
                                </div>
                            </div>
                        </div>
                        <div className="paggination-text">
                            <ul className="pagination pagination-sm">
                                <li className="page-item activelink"><a className="page-link" href="">Previous</a></li>
                                <li className="page-item"><a className="page-link" href="">1</a></li>
                                <li className="page-item"><a className="page-link" href="">2</a></li>
                                <li className="page-item"><a className="page-link" href="">3</a></li>
                                <li className="page-item"><a className="page-link" href="">Next</a></li>
                            </ul>
                        </div>
                    </div>
                    
                </div>
                <footer className="add_list_footer">
                    <div className="row">
                        <div className="col-sm-12">
                            <p>© 2019 <a href="#" style={{color: 'rgb(7, 168, 222)'}}>VIP PARCEL</a></p>
                        </div>
                        
                    </div>
                </footer>

                {/* // <!--model-popup-->   */}
                <div id="myModal" className="modal">

                    {/* <!-- Modal content --> */}
                    <div className="package-content wow animated fadeInDown" >
                        <div className="modal-header">
                            <h5 className="mod-ptext">Add order</h5>
                        </div>
                        <div className="row packagelist-middle">
                            <p>Please enter the details of your package below.  This package will be available in addition to the standard packaging.Selecting this package will automatically apply the dimensions below.</p>
                            <div className="package-inputfild">
                                <div className="row">
                                    <div className="col-sm-3">
                                        <span>Name</span>
                                    </div>
                                    <div className="col-sm-9">
                                        <input type="text" name="fname" placeholder="Type package name" value={this.state.fname} onChange={this.handleChange} />
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-sm-3">
                                        <span>Dimension</span>
                                    </div>
                                    <div className="col-sm-9">
                                        <div className="row">
                                            <div className="col-sm-4">
                                                <div className="quantity">
                                                    <input type="number" min="1" max="9" step="1" placeholder="L" name="quantity" value={this.state.quantity} onChange={this.handleChange} />
                                                </div>
                                            </div>
                                            <div className="col-sm-4">
                                                <div className="quantity2">
                                                    <input type="number" min="1" max="9" step="2" placeholder="W" name="quantity2" value={this.state.quantity2} onChange={this.handleChange} />
                                                </div>
                                            </div>
                                            <div className="col-sm-4">
                                                <div className="quantity3">
                                                    <input type="number" min="1" max="9" step="3" placeholder="H" name="quantity3" value={this.state.quantity3} onChange={this.handleChange} />
                                                </div>
                                            </div>
                                        </div>
                                        <p className="pos-inch">(Inch)</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="mod-footer">
                            
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">Cencel</span></button>
                            <a href="" className="Connect-btn">Save Order</a>
                        </div>
                    </div>
                </div>

            </div>
        
        );
    }

}

export default Packages;