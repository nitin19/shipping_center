import React from 'react';
import { Link } from 'react-router-dom';

import registerLogo from '../../images/register-logo.png';
import { scAxios } from '../..';
import { API_TOKEN_NAME } from '../../constants';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const createNewPasswordApi = ({ email, password, confirm_password, token }) => {
    return new Promise((resolve, reject) => {
        const params = {
            email: email,
            password: password,
            password_confirmation: confirm_password,
            token: token
        }

        const req = scAxios.request('/password/reset', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...params
            },
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

class ResetPassword extends React.Component {
    state = {
        email: '',
        password: '',
        confirm_password: '',
        token: '',
        isResetSuccess: false
    }

    validateForm = () => {
        if (this.state.password.length > 0 && this.state.confirm_password.length > 0) {
            if (this.state.password === this.state.confirm_password)
                return true;
        }

        return false;
    }

    handleChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        }, () => this.validateForm());
    }

    handleResetPasswordSubmit = event => {
        event.preventDefault();

        if (this.validateForm()) {
            createNewPasswordApi(this.state)
                .then(res => {
                    if (res.status=='1') {
                        toast.success(res.message, {
                            position: toast.POSITION.BOTTOM_RIGHT
                        });
                        this.setState({password: '', confirm_password: '' });
                        setTimeout(function(){
                            window.location.href = '/login';
                          }, 2000);
                    } else {
                        toast.error(res.message, {
                            position: toast.POSITION.BOTTOM_RIGHT
                        });
                    }
                })
                .catch(err => {

                });
        }
    }

    componentDidMount() {
        this.setState({ email: this.props.match.params.email, token: this.props.match.params.token });
    }

    render() {
        return (
            <div className="content-wrap">
                <div className="register-page">
                    <div className="container">
                        <div className="row">
                            <div className="col-sm-4 regst-img cus-height">
                                <div className="left-register">
                                    <Link to="/signup"><img src={registerLogo} alt="vip-logo" className="vip-logo animated fadeInLefte" /> </Link>
                                    <h6 className="animated fadeInLeft">Reset your</h6>
                                    <h1 className="animated fadeInLeft">Password</h1>
                                    <h5 className="animated fadeInLeft">VIPparcel is the Nation's leading online postage service to buy and print discounted USPS labels - domestic and international - from the comfort of your desk. We guarantee to save you time and money by making your shipping process easy, affordable, and efficient.</h5>
                                </div>
                            </div>
                            {this.state.isResetSuccess && 
                                <h6 style={{ textAlign: 'center' }}>
                                    Password has been reset Successfully. Click <Link to="/login">Login </Link>
                                </h6>
                            
                            }
                            {
                                !this.state.isResetSuccess &&

                                <div className="col-sm-8 regst-input">
                                    <h2 className="animated fadeInLeft">Reset Password</h2>
                                    <form onSubmit={this.handleResetPasswordSubmit} className="login-div">
                                        <p>Reset your new password</p>

                                        <div className="row mar-bot">
                                            <div className="col-sm-12 padd-normal">
                                                <input type="email" name="email" value={this.props.match.params.email} onChange={this.handleChange} className="pass-one animated fadeInLeft" disabled />
                                            </div>
                                        </div>
                                        <div className="row mar-bot">
                                            <div className="col-sm-12 padd-normal">
                                                <input type="password" name="password" placeholder="New password *" onChange={this.handleChange} className="pass-one animated fadeInLeft" />
                                            </div>
                                        </div>
                                        <div className="row mar-bot">
                                            <div className="col-sm-12 padd-normal">
                                                <div style={{ textAlign: 'center' }}>
                                                    <input type="password" name="confirm_password" placeholder="Confirm password *" onChange={this.handleChange} className="pass-one animated fadeInLeft" />
                                                </div>
                                            </div>
                                        </div>


                                        <input disabled={this.state.isResetSuccess} type="submit" className="reset-pass animated fadeInRight" style={{ marginTopop: 20 }} value="Reset Password" />
                                    </form>
                                </div>

                            }
                        </div>
                    </div>
                </div>
                <ToastContainer autoClose={5000} />
            </div>
        );
    }

}

export default ResetPassword;