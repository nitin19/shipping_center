import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import { Redirect } from 'react-router-dom';
import { Link } from 'react-router-dom';

import * as userActions from '../../actions/userActions';

import './login.css';
import registerLogo from '../../images/register-logo.png';
import { scAxios } from '../..';
import { validateUserToken } from '../PrivateRoute';
import { LOGIN_PAGE_PATH, API_TOKEN_NAME, IS_ACTIVE } from '../../constants';
import { startUserSession } from '../userSession';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const loginUser = (data) => {
    return new Promise((resolve, reject) => {

        const req = scAxios.request(LOGIN_PAGE_PATH, {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            },
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}


class Login extends Component {

    state = {
        user_email: '',
        user_password: '',
        remember_me: false,
        enableLoginBtn: false,
        signin_success: false,
        user_active: false,
        fields: {},
        errors: {},
    };

    validateForm() {
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;

        if (!fields["user_email"]) {
            formIsValid = false;
            errors["user_email"] = "*Please enter your email.";
        }

        if (typeof fields["user_email"] !== "undefined") {
            let lastAtPos = fields["user_email"].lastIndexOf('@');
            let lastDotPos = fields["user_email"].lastIndexOf('.');
            if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["user_email"].indexOf('@@') == -1 && lastDotPos > 2 && (fields["user_email"].length - lastDotPos) > 2)) {
                formIsValid = false;
                errors["user_email"] = "Email is not valid";
            }
        }

        if (!fields["user_password"]) {
            formIsValid = false;
            errors["user_password"] = "*Please enter password";
        }

        this.setState({
            errors: errors
        });
        return formIsValid;

    }

    handleChange = event => {

        let fields = this.state.fields;
        fields[event.target.name] = event.target.value;
        this.setState({
            fields
        }, () => this.validateForm());
    }

    handleCheckBoxChange = event => {
        this.setState({ [event.target.name]: event.target.checked }, () => this.validateForm());
    }

    handleSubmit = event => {
        event.preventDefault();

        if (this.validateForm()) {

            let fields = {};
            fields["user_email"] = "";
            fields["user_password"] = "";
            this.setState({ fields: fields });

            const user = {
                email: this.state.fields.user_email,
                password: this.state.fields.user_password,
            }

            loginUser(user)
                .then(res => {
                    if (!!res.token) {
                        startUserSession(res.token, res.auth_user.type, res.auth_user.id, res.auth_user.email, res.auth_user.active);
                        this.setState({ signin_success: true, user_active: res.auth_user.active === 0 ? false : true });
                        this.props.actions.userLoginSuccess(res.auth_user);
                    }
                    else
                        toast.error(res.errorMsg, {
                            position: toast.POSITION.BOTTOM_RIGHT
                        });
                })
                .catch(err => {
                    toast.error('Error occured', {
                        position: toast.POSITION.BOTTOM_RIGHT
                    });
                });
        }
        else
            toast.error('Please provide username and password !', {
                position: toast.POSITION.BOTTOM_RIGHT
            });
    }

    componentDidMount() {
        localStorage.clear();
    }

    render() {
        const { from } = this.props.location.state || { from: { pathname: "/my-stores" } };

        if (validateUserToken()) {
            if (!this.state.user_active)
                return <Redirect to="/phone-verification" />

            return <Redirect to={from} />
        }

        return (
            <div className="register-page">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-4 col-md-5 regst-img cus-height">
                            <div className="left-register">
                                <Link to="/signup"><img src={registerLogo} alt="vip-logo" className="vip-logo animated fadeInLeft" /></Link>
                                <h6 className="animated fadeInLeft">Hello,</h6>
                                <h1 className="animated fadeInLeft">Welcome</h1>
                                <h5 className="animated fadeInLeft">VIPparcel is the Nation's leading online postage service to buy and print discounted USPS labels - domestic and international - from the comfort of your desk. We guarantee to save you time and money by making your shipping process easy, affordable, and efficient.</h5>
                                <p className="animated fadeInRight">SIgn in</p>
                            </div>
                        </div>
                        <div className="col-lg-8 col-md-7 regst-input">
                            <h2 className="animated fadeInLeft">sign in to Your Account</h2>
                            <form onSubmit={this.handleSubmit} className="login-div">

                                <div className="row mar-bot">
                                    <div className="col-sm-12 padd-normal">
                                        <input type="email" name="user_email" value={this.state.fields.user_email} onChange={this.handleChange} className="email-one animated fadeInLeft" />
                                        <div className="errorMsg">{this.state.errors.user_email}</div>
                                    </div>
                                </div>

                                <div className="row mar-bot">
                                    <div className="col-sm-12 padd-normal">
                                        <input type="password" name="user_password" value={this.state.fields.user_password} onChange={this.handleChange} className="pass-two animated fadeInRight" />
                                        <div className="errorMsg">{this.state.errors.user_password}</div>
                                    </div>

                                </div>

                                <div className="row mar-bot">
                                    <div className="col-sm-12 padd-normal animated fadeInRight">
                                        <label className="container-radio">Remember Me
                                        <input name="remember_me" type="checkbox" onChange={this.handleCheckBoxChange} />
                                            <span className="checkmark"></span>
                                        </label>
                                        <Link className="for-pass animated fadeInLeft" to="/forgot-password">Forgot Password</Link>
                                    </div>
                                </div>
                                <div className="row mar-bot">
                                    <div className="col-sm-12 padd-normal">
                                        <div style={{ textAlign: "center" }}>
                                            <button type="submit" className={this.state.enableLoginBtn ? " animated fadeInDown sub-btnn" : "animated fadeInDown sub-btnn"} value="Login" >Login</button>
                                            <Link className="account-sign-up animated fadeInDown" to="/signup"><span style={{ color: '#00a0da' }}>Don't have an Account?</span> <br /> Please Sign up</Link>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
                <ToastContainer autoClose={5000} />
            </div >
        );
    }
}

const mapStateToProps = (state /*, ownProps*/) => {
    return {
        user: state.user,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        actions: bindActionCreators(userActions, dispatch),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);