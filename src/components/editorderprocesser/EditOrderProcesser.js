import React from 'react';
import ReactDOM from 'react-dom';
import profile from '../../images/prflimg.png'
import clander from '../../images/clandericon.png';
import { scAxios } from '../..';
import { API_TOKEN_NAME, LOGIN_PAGE_PATH , USER_ID, IMAGE_URL} from '../../constants';
import { startUserSession } from '../userSession';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Skeleton from 'react-loading-skeleton';

const processorRequest = (processor, processor_id, imageData) => {

  let processor_image = new FormData();
  processor_image.append('processor_image', imageData);

  return new Promise((resolve, reject) => {
    const params = {
      userid : processor.userid,
      first_name: processor.first_name,
      last_name: processor.last_name,
      email: processor.email,
      phone: processor.phone,
      //   is_fulfillment_center:processor.is_fulfillment_center,
      status: processor.status,
      assignedstores: processor.assignstores,
    }

  //  console.log(params);
    const req = scAxios.post('orderprocessor/update/' + processor_id, processor_image, {
      headers: {
        'Accept': 'application/json',
        'Content-Type': `multipart/form-data; boundary=${processor_image._boundary}`,
        'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
      },
      params: {
        ...params
      },
    });

    req.then(res => resolve(res.data))
      .catch(err => reject(err));
  });
}

const getStores = (data) => {
  return new Promise((resolve, reject) => {
    const req = scAxios.request('stores/allstores', {
      method: 'get',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
      },
      params: {
        ...data
      },
    });

    req.then(res => resolve(res.data))
      .catch(err => reject(err));
  });
}

const getProcessor = (url) => {
  return new Promise((resolve, reject) => {
    const req = scAxios.request(url, {
      method: 'get',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
      },
    });

    req.then(res => resolve(res.data))
      .catch(err => reject(err));
  });
}

class EditOrderProcesser extends React.Component {

  state = {
    processor_image: profile,
    processor_image_data: '',
    enableSaveBtn: false,
    signup_success: false,
    stores: [],
    processor: [],
    assignstores: [],
    userid:localStorage.getItem(USER_ID),
  };


  validateForm() {
    if (this.state.processor.first_name != '' && this.state.processor.last_name != '' && this.state.processor.email != '' && this.state.processor.phone != '') {
      this.setState({ enableSaveBtn: true });
      return true;
    }
    this.setState({ enableSaveBtn: false });
    return false;
  }

  handleChange = event => {
    this.setState({
      [event.target.name]: event.target.value
    }, () => this.validateForm());
  }
  handleCheckBoxChange = event => {
    if (event.target.checked) {
      const stores = this.state.assignstores;
      stores.push(parseInt(event.target.value));
      this.setState({ assignedstores: stores });
    } else {
      const stores = this.state.assignstores;
      var index = stores.indexOf(parseInt(event.target.value));
      if (index >= 0) {
        stores.splice(index, 1);
        this.setState({ assignedstores: stores });
      }
    }
  }

  onImageChange = event => {
        if (event.target.files[0]) {
            const file = event.target.files[0];
            let reader = new FileReader();
            reader.readAsDataURL(file);

            reader.onload = event => {
                this.setState({ processor_image: event.target.result, processor_image_data: file });
            }
        }
    }

  handleSubmit = event => {
    event.preventDefault();
    var processor_id = this.props.match.params.id;
    //  if (this.validateForm()) {
    processorRequest(this.state, processor_id, this.state.processor_image_data)
      .then(res => {
        this.setState({
          first_name: '',
          last_name: '',
          email: '',
          phone: '',
        });

        if (res.status == 'Success') {
          toast.success(res.message, {
            position: toast.POSITION.BOTTOM_RIGHT
          });
          setTimeout(function(){
            window.location.href = '/vieworderprocessor';
          }, 5000);
        } else if (res.status == 'Warning') {
          toast.warn(res.message, {
            position: toast.POSITION.BOTTOM_RIGHT
          });

        } else {
          toast.error(res.message, {
            position: toast.POSITION.BOTTOM_RIGHT
          });
        }

      })
      .catch(err => {
        console.log(err);
        alert('Error occured');
      });
    /* }
     else
         alert('Validation failed !');*/
  }

  chkMystores(myStoreid) {
    var index = this.state.assignstores.indexOf(myStoreid);
    return index >= 0;
  }

  componentDidMount() {
       const data = { userid: localStorage.getItem(USER_ID) }
    getStores(data)
      .then(res => {
        this.setState({ stores: res.records });
      })
      .catch(err => {
        console.log(err);
        alert("Error occured fetching Stores. Check console");
      });

    var processor_id = this.props.match.params.id;
    var url = 'orderprocessor/viewprocessors/' + processor_id;
    getProcessor(url)
      .then(res => {
        if (res.message) {
          this.setState({
            processor: res.message,
            assignstores: res.message.storeid,
            processor_image: IMAGE_URL + 'processorimage/' + res.message.processor_image
          });
        }
      })
      .catch(err => {
        console.log(err);
        alert("Error occured fetching processor. Check console");
      });
  }

  render() {
    return (
              <>     
                {this.state.loadingFirst==true ?
                <div className="content-wrap backcollor">
                  <div className="ms-stor">
                      <div className="row">
                        <div className="col-sm-3">
                            <p>{this.props.title || <Skeleton count={10} duration={2} height={40} width={200} />}</p>
                        </div>
                        <div className="col-sm-9">
                            <div className="button2">
                                {this.props.body || <Skeleton count={10} duration={2} height={40} />}
                            </div>
                        </div>
                      </div>    
                  </div>
                </div>
              :
      <div className="content-wrap universelclass">

        <div className="order-view2">
          <div className="order-view-head">
            <h2>Update Order Processor Detail</h2>
          </div>
          <div className="order-view2-body">
            <div className="position-text">
              <p>Last Modified Date:</p>
              <p>{this.state.processor.updated_at}</p>
            </div>
            <form onSubmit={this.handleSubmit} >
              <div className="">
                <div className="idauto wow animate fadeInLeft " data-wow-delay="0.4s">
                  <div className="labsec">
                    <p>Upload Image</p>
                  </div>
                  <div className="writetext">
                  <img src={this.state.processor_image} className="prdt-upload-img" alt="upload-img" />
                    <div className="upload-btn-wrapper-img">
                      <button className="btn-gourav">Upload a file</button>
                      <input type='file' id='processor_image' onChange={this.onImageChange} />
                    </div>
                  </div>
                </div>
                <div className="idauto">
                  <div className="labsec">
                    <p>First Name</p>
                  </div>
                  <div className="writetext">
                    <input type="text" name="first_name" placeholder="Enter First Name" defaultValue={this.state.processor.first_name} onChange={this.handleChange} className="form-control custom" required />
                  </div>
                </div>
                <div className="idauto">
                  <div className="labsec">
                    <p>Last Name</p>
                  </div>
                  <div className="writetext">
                    <input type="text" name="last_name" placeholder="Enter Last Name" defaultValue={this.state.processor.last_name} onChange={this.handleChange} className="form-control custom" required />
                  </div>
                </div>
                <div className="idauto">
                  <div className="labsec">
                    <p>Email Address</p>
                  </div>
                  <div className="writetext">
                    <input type="email" name="email" placeholder="Enter Email Address" defaultValue={this.state.processor.email} onChange={this.handleChange} className="form-control custom" required />
                  </div>
                </div>
                <div className="idauto">
                  <div className="labsec">
                    <p>PhoneNumber</p>
                  </div>
                  <div className="writetext">
                    <input type="text" name="phone" placeholder="Enter Phone Number" defaultValue={this.state.processor.phone} onChange={this.handleChange} className="form-control custom" required />
                  </div>
                </div>
                {/*                    <div className="idauto">  
                      <div className="labsec">
                        <p>Password</p>  
                      </div>
                      <div className="writetext">
                        <input type="Password" name="user_password" placeholder="Enter Password" value={this.state.processor.password} onChange={this.handleChange} className="form-control custom" required />  
                      </div>
                   </div>*/}
                {/*                    <div className="idauto">  
                      <div className="labsec">
                        <p>Confirm Password</p>  
                      </div>
                      <div className="writetext">
                        <input type="Password" name="user_password_confirm" placeholder="Confirm Password" value={this.state.processor.password} onChange={this.handleChange} className="form-control custom" required />  
                      </div>
                    </div>*/}

                {  /*  <div className="idauto">  
                        <div className="labsec">
                          <p>Is Fulfillment</p>  
                        </div>
                      <div className="writetext center">
                        <span>

        
                            {   this.state.processor.is_fulfillment_center == '1'
                                ? <input type="radio" id="test3" name="is_fulfillment_center" value= "1" checked onChange={(e) => this.setState({ is_fulfillment_center: e.target.value })} />
                                : <input type="radio" id="test3" name="is_fulfillment_center" value= "1" checked={this.state.is_fulfillment_center === '1'} onChange={(e) => this.setState({ is_fulfillment_center: e.target.value })} />
                            }
                          <label for="test3">Yes</label>
                        </span>
                        <span>
                            {   this.state.processor.is_fulfillment_center == '0'
                                ? <input type="radio" id="test2" name="is_fulfillment_center" value= "0" checked onChange={(e) => this.setState({ is_fulfillment_center: e.target.value })} />
                                : <input type="radio" id="test2" name="is_fulfillment_center" value= "0" checked={this.state.is_fulfillment_center === '0'} onChange={(e) => this.setState({ is_fulfillment_center: e.target.value })} />
                            }
                          <label for="test2">No</label>
                        </span>
                      </div>
                    </div>  */ }


                <div className="idauto">
                  <div className="labsec">
                    <p>Status</p>
                  </div>
                  <div className="writetext center">
                    <span>

                      {this.state.processor.status == '1'
                        ? <input type="radio" id="test4" name="status" value="1" checked onChange={(e) => this.setState({ status: e.target.value })} />
                        : <input type="radio" id="test4" name="status" value="1" checked={this.state.status === '1'} onChange={(e) => this.setState({ status: e.target.value })} />
                      }
                      <label for="test4">Active</label>
                    </span>
                    <span>
                      {this.state.processor.status == '0'
                        ? <input type="radio" id="test5" name="status" value="0" checked onChange={(e) => this.setState({ status: e.target.value })} />
                        : <input type="radio" id="test5" name="status" value="0" checked={this.state.status === '0'} onChange={(e) => this.setState({ status: e.target.value })} />
                      }
                      <label for="test5">Suspended</label>
                    </span>
                  </div>
                </div>

                <div className="idauto AssignStore">
                  <div className="labsec">
                    <p>Assign Store(s)</p>
                  </div>
                  <div className="writetext pack-head-ing">
                    {this.state.stores.map(store => {
                      return <label key={store.id} className="container-radio">{store.store_name}<input type="checkbox" name="assignstores" id={'chk_' + store.id} value={store.id} checked={this.chkMystores(store.id)} onChange={this.handleCheckBoxChange} />
                        <span className="checkmark"></span>
                      </label>

                    })}
                  </div>
                </div>

                <div className="idauto">
                  <div className="writetext btnright">
                    <button type="reset" className="createbtn Cancel">Cancel</button>
                    <button type="submit" className="mainbtn">Updates</button>
                  </div>
                </div>
              </div>

            </form>
          </div>
        </div>
        <footer className="add_list_footer">   
            <div className="row"> 
                <div className="col-sm-12">          
                    <p>© 2019 <a href="#" style={{color: '#07a8de'}}>VIP PARCEL</a></p>    
                </div>
            </div>
        </footer>
        <ToastContainer autoClose={5000} />
      </div>

}
        </>
    );
  }
}

export default EditOrderProcesser;

