import React from 'react';
import ReactDOM from 'react-dom';
import Skeleton from 'react-loading-skeleton';

import './error.css';

class Error500 extends React.Component {

	render(){

		return(

			<div className="content-wrap">

      {/*<div style={{ fontSize: 20, lineHeight: 2 }}>
                    <h1>{this.props.title || <Skeleton />}</h1>
                    {this.props.body || <Skeleton count={30} />}
                  </div>*/}
        
          <div className="errer-page">    
        <div className="row">
          <div className="col-sm-12 errer-page-505">    
            <div className="">  
                <h1>500</h1>      
                <h5>That’s an Error</h5>    
                <p>There was an error. Please try again later, Thats all we know</p>    
                <a href="" className="help-link">Perhaps searching can help.</a>   
              </div>     
          </div>
          </div>
      </div>

        
        <footer className="add_list_footer">   
          <div className="row"> 
            <div className="col-sm-12">          
              <p>© 2019 <a href="#" style={{color: '#07a8de'}}>VIP PARCEL</a></p>    
            </div> 
          </div>
        </footer>
      </div>

			);

	}

}
export default Error500;