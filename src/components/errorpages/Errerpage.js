import React from 'react';
import ReactDOM from 'react-dom';
import Skeleton from 'react-loading-skeleton';
//import './usermanagement.css';

class Errorpage extends React.Component {

	render(){

		return(

			<div className="content-wrap universelclass">

      <div style={{ fontSize: 20, lineHeight: 2 }}>
              <h1>{this.props.title || <Skeleton />}</h1>
              {this.props.body || <Skeleton count={30} />}
            </div>
        <div className="col-lg-12">
          <div className="errer-page">    
            <div className="row">
              <div className="col-sm-12 errer-page-505">    
                <div className="">   
                    <h1>404</h1>    
                    <h5>Oops, This Page Not Be Found!</h5>    
                    <p>We are really sorry but the page you requested is missing :(</p>    
                    <a href="" className="help-link">Perhaps searching can help.</a>   
                  </div>     
              </div>
            </div>   
          </div>

        </div>
        <footer className="add_list_footer">   
          <div className="row"> 
            <div className="col-sm-12">          
              <p>© 2019 <a href="#" style={{color: '#07a8de'}}>VIP PARCEL</a></p>    
            </div> 
          </div>
        </footer>
      </div>

			);

	}

}
export default Errorpage;