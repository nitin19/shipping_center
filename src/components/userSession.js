import { TWENTY_MINUTES_TIME_IN_MILLISECONDS, API_TOKEN_NAME, API_TOKEN_EXPIRY_NAME, USER_ROLE, USER_ID, USER_EMAIL, IS_ACTIVE } from "../constants";
import { scAxios } from "..";

let userTokenRefreshIntervalId = '';

const userTokenRefreshApi = () => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/refresh', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

export const userTokenRefreshInterval = () => {
    userTokenRefreshApi()
        .then(res => {
            startUserSession(res.access_token);
        })
        .catch(err => {
            console.log('Error refreshing user token.', err);
            alert('Error refreshing user token.', err);
        })
}

export const startUserSession = (token, user_role , user_id, user_email, isActive) => {
    localStorage.setItem(API_TOKEN_NAME, token);
    localStorage.setItem(USER_ROLE, user_role);
    localStorage.setItem(USER_ID, user_id);
    localStorage.setItem(USER_EMAIL, user_email);
    localStorage.setItem(IS_ACTIVE, isActive);

    let datetime = new Date();
    datetime.setMinutes(datetime.getMinutes() + 29);
    localStorage.setItem(API_TOKEN_EXPIRY_NAME, datetime);

    clearInterval(userTokenRefreshIntervalId);
    userTokenRefreshIntervalId = setInterval(userTokenRefreshInterval, TWENTY_MINUTES_TIME_IN_MILLISECONDS);
}

export const endUserSession = () => {
    clearInterval(userTokenRefreshIntervalId);
    localStorage.clear();
}