import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Redirect } from 'react-router-dom';

import * as userActions from '../../actions/userActions';

import './signup.css';
import registerLogo from '../../images/register-logo.png';
import { scAxios } from '../..';
import { US_COUNTRY_CODE, LOGIN_PAGE_PATH, IS_ACTIVE } from '../../constants';
import { startUserSession } from '../userSession';


const signupRequest = user => {
    return new Promise((resolve, reject) => {
        const params = {
            firstName: user.first_name,
            middleName: user.middle_name,
            lastName: user.last_name,
            email: user.email,
            password: user.user_password,
            password_confirmation: user.user_password_confirm,
            phone: user.phone_number,
            countryId: user.idCountry,
            streetAddress1: user.address_1,
            streetAddress2: user.address_2,
            city: user.city,
            postalCode: user.zip_code,
            hearAboutUs: user.hear_about_us,
            state: user.state,
            province: user.state,
            referralCode: user.referral_code,
        }

        console.log(params);

        const req = scAxios.request('/register', {
            method: 'post',
            params: {
                ...params
            },
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getCountries = () => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/getCountries', {
            method: 'get',
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getStates = () => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/getStates', {
            method: 'get',
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}


class Signup extends Component {

    state = {
        first_name: '',
        middle_name: '',
        last_name: '',
        email: '',
        phone_number: '',
        user_password: '',
        user_password_confirm: '',

        referral_code: '',
        promo_code: '',
        hear_about_us: '',
        idCountry: '',
        address_1: '',
        address_2: '',
        city: '',
        state: '',
        zip_code: '',

        read_terms: false,
        enableSignupBtn: false,
        signup_success: false,

        countries: [],
        states: [],
    };

    validateForm() {
        if (this.state.first_name.length > 0 && this.state.last_name.length > 0 && this.state.email.length > 0 && this.state.phone_number.length > 0) {
            if (this.state.user_password === this.state.user_password_confirm) {
                if (this.state.read_terms) {
                    this.setState({ enableSignupBtn: true });
                    return true;
                }
            }
        }
        this.setState({ enableSignupBtn: false });
        return false;
    }

    handleChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        }, () => this.validateForm());
    }

    handleCheckBoxChange = event => {
        this.setState({
            [event.target.name]: event.target.checked
        }, () => this.validateForm());
    }

    handleSubmit = event => {
        event.preventDefault();

        if (this.validateForm()) {
            signupRequest(this.state)
                .then(res => {
                    if (!!res.token) {
                        startUserSession(res.token, res.auth_user.type, res.auth_user.id, res.auth_user.active);
                        this.props.actions.userLoginSuccess(res.auth_user);
                        this.setState({ signup_success: true });
                    }
                })
                .catch(err => {
                    console.log(err);
                    alert('Error occured');
                });
        }
        else
            alert('Validation failed !');
    }

    componentDidMount() {

        getCountries()
            .then(res => {
                this.setState({ countries: res.records });
            })
            .catch(err => {
                console.log(err);
                alert("Error occured fetching Countries. Check console");
            });

        getStates()
            .then(res => {
                this.setState({ states: res.records });
            })
            .catch(err => {
                console.log(err);
                alert("Error occured fetching States. Check console");
            });

    }

    render() {
        if (this.state.signup_success) return <Redirect to="/phone-verification" />

        return (
            <div className="register-page">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-4 col-md-5 regst-img">
                            <div className="left-register">
                                <Link to="/signup"><img src={registerLogo} alt="vip-logo" className="vip-logo animated fadeInLeft" /> </Link>
                                <h6 className="animated fadeInLeft">Let’s Get</h6>
                                <h1 className="animated fadeInLeft">Started</h1>
                                <h5 className="animated fadeInLeft">VIPparcel is the Nation's leading online postage service to buy and print discounted USPS labels - domestic and international - from the comfort of your desk. We guarantee to save you time and money by making your shipping process easy, affordable, and efficient.</h5>
                                <p className="animated fadeInRight">Sign up Here</p>
                            </div>
                        </div>

                        <div className="col-lg-8 col-md-7  regst-input">
                            <h2 className="animated fadeInLeft">Register With Your Work Email Address</h2>
                            <form onSubmit={this.handleSubmit} className="singn-up-width">
                                <div className="row mar-bot">
                                    <div className="col-sm-4 padd-normal">
                                        <input type="text" name="first_name" placeholder="First Name *" value={this.state.first_name} onChange={this.handleChange} className="first-one animated fadeInLeft" required />
                                    </div>
                                    <div className="col-sm-4 padd-normal">
                                        <input type="text" name="middle_name" placeholder="Middle Name *" value={this.state.middle_name} onChange={this.handleChange} className="animated fadeInUp"/>
                                    </div>
                                    <div className="col-sm-4 padd-normal">
                                        <input type="text" name="last_name" placeholder="Last Name *" value={this.state.last_name} onChange={this.handleChange} className="animated fadeInRight" required />
                                    </div>
                                </div>
                                <div className="row mar-bot">
                                    <div className="col-sm-12 padd-normal email">
                                        <input type="email" name="email" placeholder="Email Address *" value={this.state.email} onChange={this.handleChange} className="email-one animated fadeInDown" required />
                                    </div>
                                </div>
                                <div className="row mar-bot">
                                    <div className="col-sm-12 padd-normal">
                                        <input type="text" name="phone_number" placeholder="Phone Number *" value={this.state.phone_number} onChange={this.handleChange} className="phone-one animated fadeInUp" required />
                                    </div>
                                </div>
                                <div className="row mar-bot">
                                    <div className="col-sm-12 padd-normal">
                                        <p>Please provide a valid phone number in order to verify your account. We will send you a verification code either via SMS or voice message in order to confirm you own a phone number.</p>
                                    </div>
                                </div>
                                <div className="row mar-bot">
                                    <div className="col-sm-6 padd-normal">
                                        <input type="password" name="user_password" value={this.state.user_password} onChange={this.handleChange} className="pass-one animated fadeInLeft" required />
                                    </div>
                                    <div className="col-sm-6 padd-normal">
                                        <input type="password" name="user_password_confirm" value={this.state.user_password_confirm} onChange={this.handleChange} className="pass-one animated fadeInRight" required />
                                    </div>
                                </div>
                                <div className="row mar-bot">
                                    <div className="col-sm-12 padd-normal">
                                        <h6>Other Information</h6>
                                    </div>
                                </div>
                                <div className="row mar-bot">
                                    <div className="col-sm-6 padd-normal">
                                        <input type="text" name="referral_code" placeholder="Have a referral code?" value={this.state.referral_code} onChange={this.handleChange} className="animated fadeInLeft" />
                                    </div>
                                    <div className="col-sm-6 padd-normal">
                                        <input type="text" name="promo_code" placeholder="Promo Code" value={this.state.promo_code} onChange={this.handleChange} className="animated fadeInRight" />
                                    </div>
                                </div>
                                <div className="row mar-bot">
                                    <div className="col-sm-12 padd-normal">
                                        <input type="text" name="hear_about_us" placeholder="How did you hear about us?" value={this.state.hear_about_us} onChange={this.handleChange} className="animated fadeInDown" />
                                    </div>
                                </div>
                                <div className="row mar-bot">
                                    <div className="col-sm-12 padd-normal">
                                        <select className="cuntry-slect" name="idCountry" value={this.state.idCountry} onChange={this.handleChange}>
                                            {this.state.countries.map(country => {
                                                return <option key={country.idCountry} value={country.idCountry}>{country.countryName}</option>
                                            })}
                                        </select>
                                    </div>
                                </div>
                                <div className="row mar-bot">
                                    <div className="col-sm-6 padd-normal">
                                        <input type="text" name="address_1" placeholder="Address Line 1" value={this.state.address_1} onChange={this.handleChange} className="animated fadeInLeft" />
                                    </div>
                                    <div className="col-sm-6 padd-normal">
                                        <input type="text" name="address_2" placeholder="Apartment, Unit, Office" value={this.state.address_2} onChange={this.handleChange} className="animated fadeInRight" />
                                    </div>
                                </div>
                                <div className="row mar-bot">
                                    <div className="col-sm-4 padd-normal">
                                        <input type="text" name="city" placeholder="City" value={this.state.city} onChange={this.handleChange} className="animated fadeInLeft" />
                                    </div>
                                    <div className="col-sm-4 padd-normal">
                                        {
                                            this.state.idCountry == US_COUNTRY_CODE
                                                ? <select name="state" value={this.state.state} onChange={this.handleChange}>
                                                    {this.state.states.map(state => {
                                                        return <option key={state.id} value={state.abbr}>{state.name}</option>
                                                    })}
                                                </select>
                                                : <input type="text" name="state" placeholder="State/Province" value={this.state.state} onChange={this.handleChange} className="animated fadeInDoewn" />
                                        }

                                    </div>
                                    <div className="col-sm-4 padd-normal">
                                        <input type="text" name="zip_code" placeholder="Zip Code" value={this.state.zip_code} onChange={this.handleChange} className="animated fadeInRight" />
                                    </div>
                                </div>
                                <div className="row mar-bot">
                                    <div className="col-sm-12 padd-normal animated fadeInLeft skype-field-sec">
                                        {  this.state.idCountry == US_COUNTRY_CODE ? <span></span>
                                         : 
                                            <input type="text" name="skypeid" placeholder="Skype Id" value={this.state.state} onChange={this.handleChange} className="animated fadeInDoewn" />
                                        }
                                    </div>
                                </div>
                                <div className="row mar-bot">
                                    <div className="col-sm-12 padd-normal animated fadeInLeft">
                                        <label className="container-radio">I read and agree to <span style={{ color: "#00a0da" }}>term and conditions.</span>
                                            <input type="checkbox" name="read_terms" checked={this.state.read_terms} onChange={this.handleCheckBoxChange} />
                                            <span className="checkmark"></span>
                                         </label>
                                    </div>
                                </div>
                                <div className="row mar-bot">        
                                    <div className="col-sm-12 padd-normal">
                                        <div style={{ textAlign: "center" }}>
                                            <button disabled={!this.state.enableSignupBtn} type="submit" className={this.state.enableSignupBtn ? "sub-btnn animated fadeInDown" : "animated fadeInDown sub-btnn"} value="SIGN UP" >SIGN UP</button>
                                            <Link className="account-sign-up animated fadeInUp" to={LOGIN_PAGE_PATH}><span style={{ color: "#00a0da" }}>Have an Account?</span> Please Sign In</Link>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        user: state.user
    }
}

const mapDispatchToProps = dispatch => {
    return {
        actions: bindActionCreators(userActions, dispatch),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Signup);