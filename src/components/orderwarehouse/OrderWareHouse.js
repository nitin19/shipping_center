import React from 'react';

import clientimg from '../../images/client.png';
import uploadImg from '../../images/1.png';
import profile from '../../images/prflimg.png';

class OrderWareHouse extends React.Component {

	render() {

		return (

			<div className="content-wrap">
                <div className="storemiddle-sec">      
                <div className="row addstore-list-text">      
                    <div className="col-sm-8">  
                        <h1>Warehouse Lists</h1>      
                    </div>     
                    <div className="col-sm-4 text-right">        
                        <a href="#" data-toggle="modal" data-target="#houseModal" className="Connect-btn">Add New</a>      
                    </div>  
                </div>  
                <hr />  
                <div className="products-list-sec">   
                    <div className="row single-prdt-sec">    
                        <div className="col-xl-4 col-lg-6 col-md-6 col-sm-12">               
                                       
                                <div className="row prdt-back-gd">  
                                    <div className="col-sm-3 house-img">    
                                        {/*{/*<!-- <img src="imgs/nike-shoes.png" alt="Nike-lunacharge-Essential" width="100%" /> -->*/}*/}    
                                    </div>  
                                    <div className="col-sm-9">      
                                        <h6 className="prdt-mainhd">Caruso Logistics</h6>      
                                        <span className="prdt-headtext">Sku</span> <span className="prdt-nortext">32566L</span>  
                                        <p className="prdt-location"><i className="fa fa-map-marker" style={{color: 'red' }}></i> <span><span>8254 S. Garfield Street. Grand Island, NE 68801</span></span></p>      
                                        <p className="house-phonenum"><i className="fa fa-phone" aria-hidden="true"></i> (805) - 369 - 3214</p>      
                                        <a href="#" className="products-ellipsis"><i className="fa fa-ellipsis-h"></i></a>  
                                    </div>  
                                </div>      
                        
                        </div>
                        <div className="col-xl-4 col-lg-6 col-md-6 col-sm-12">  
                            <div className="row prdt-back-gd">
                                    <div className="col-sm-3 house-img-two">          
                                        {/*<!-- <img src="imgs/adidas-kampung.png" alt="Nike-lunacharge-Essential" width="100%" /> -->*/}    
                                    </div>    
                                    <div className="col-sm-9">      
                                        <h6 className="prdt-mainhd">Champion Logistics Group</h6>       
                                        <span className="prdt-headtext">Sku</span> <span className="prdt-nortext">32566L</span>
                                        <p className="prdt-location"><i className="fa fa-map-marker" style={{color: '#ff3333'}}></i> <span>8254 S. Garfield Street. Grand Island, NE 68801</span></p>
                                        <p className="house-phonenum"><i className="fa fa-phone" aria-hidden="true"></i> (805) - 369 - 3214</p>
                                        <a href="#" className="products-ellipsis"><i className="fa fa-ellipsis-h"></i></a>  
                                    </div>  
                                </div>  
                        </div>
                        <div className="col-xl-4 col-lg-6 col-md-6 col-sm-12">   
                            <div className="row prdt-back-gd">
                                    <div className="col-sm-3 house-img-ballet">        
                                        {/*{/*<!-- <img src="imgs/nike-shoes.png" alt="Nike-lunacharge-Essential" width="100%" /> -->*/}*/}    
                                    </div>  
                                    <div className="col-sm-9">    
                                        <h6 className="prdt-mainhd">Clark Logistic Services</h6>      
                                        <span className="prdt-headtext">Sku</span> <span className="prdt-nortext">32566L</span>
                                        <p className="prdt-location"><i className="fa fa-map-marker" style={{color: '#ff3333'}}></i> <span>8254 S. Garfield Street. Grand Island, NE 68801</span></p>
                                        <p className="house-phonenum"><i className="fa fa-phone" aria-hidden="true"></i> (805) - 369 - 3214</p>
                                        <a href="#" className="products-ellipsis"><i className="fa fa-ellipsis-h"></i></a>  
                                    </div>  
                                </div>    
                        </div>  
                    </div>
                    <div className="row single-prdt-sec">    
                        <div className="col-xl-4 col-lg-6 col-md-6 col-sm-12">               
                                     
                                <div className="row prdt-back-gd">  
                                    <div className="col-sm-3 house-img-athletic">    
                                        {/*{/*<!-- <img src="imgs/nike-shoes.png" alt="Nike-lunacharge-Essential" width="100%" /> -->*/}*/}    
                                    </div>  
                                    <div className="col-sm-9">  
                                        <h6 className="prdt-mainhd">Common Wealth Inc. </h6>      
                                        <span className="prdt-headtext">Sku</span> <span className="prdt-nortext">32566L</span>
                                        <p className="prdt-location"><i className="fa fa-map-marker" style={{color: '#ff3333'}}></i> <span>8254 S. Garfield Street. Grand Island, NE 68801</span></p>
                                        <p className="house-phonenum"><i className="fa fa-phone" aria-hidden="true"></i> (805) - 369 - 3214</p>
                                        <a href="#" className="products-ellipsis"><i className="fa fa-ellipsis-h"></i></a>  
                                    </div>  
                                </div>  
                        
                        </div>
                        <div className="col-xl-4 col-lg-6 col-md-6 col-sm-12">  
                            <div className="row prdt-back-gd">
                                    <div className="col-sm-3 house-img-chuck">      
                                        {/*{/*<!-- <img src="imgs/nike-shoes.png" alt="Nike-lunacharge-Essential" width="100%" /> -->*/}*/}    
                                    </div>  
                                    <div className="col-sm-9">    
                                        <h6 className="prdt-mainhd">Continental Warehouse </h6>      
                                        <span className="prdt-headtext">Sku</span> <span className="prdt-nortext">32566L</span>
                                        <p className="prdt-location"><i className="fa fa-map-marker" style={{color: '#ff3333'}}></i> <span>8254 S. Garfield Street. Grand Island, NE 68801</span></p>
                                        <p className="house-phonenum"><i className="fa fa-phone" aria-hidden="true"></i> (805) - 369 - 3214</p>
                                        <a href="#" className="products-ellipsis"><i className="fa fa-ellipsis-h"></i></a> 
                                    </div>  
                                </div>  
                        </div>
                        <div className="col-xl-4 col-lg-6 col-md-6 col-sm-12">   
                            <div className="row prdt-back-gd">
                                    <div className="col-sm-3 house-img-sneakers">      
                                        {/*{/*<!-- <img src="imgs/nike-shoes.png" alt="Nike-lunacharge-Essential" width="100%" /> -->*/}*/}    
                                    </div>  
                                    <div className="col-sm-9">      
                                        <h6 className="prdt-mainhd">The standard Lorem Ipsum</h6>    
                                        <span className="prdt-headtext">Sku</span> <span className="prdt-nortext">32566L</span>
                                        <p className="prdt-location"><i className="fa fa-map-marker" style={{color: '#ff3333'}}></i> <span>8254 S. Garfield Street. Grand Island, NE 68801</span></p>
                                        <p className="house-phonenum"><i className="fa fa-phone" aria-hidden="true"></i> (805) - 369 - 3214</p>
                                        <a href="#" className="products-ellipsis"><i className="fa fa-ellipsis-h"></i></a>  
                                    </div>  
                                </div>    
                        </div>  
                    </div>
                    <div className="row single-prdt-sec">    
                        <div className="col-xl-4 col-lg-6 col-md-6 col-sm-12">               
                                     
                                <div className="row prdt-back-gd">
                                    <div className="col-sm-3 house-img-basketball">      
                                        {/*{/*<!-- <img src="imgs/nike-shoes.png" alt="Nike-lunacharge-Essential" width="100%" /> -->*/}*/}    
                                    </div>  
                                    <div className="col-sm-9">      
                                        <h6 className="prdt-mainhd">Finibus Bonorum et Malorum</h6>    
                                        <span className="prdt-headtext">Sku</span> <span className="prdt-nortext">32566L</span>
                                        <p className="prdt-location"><i className="fa fa-map-marker" style={{color: '#ff3333'}}></i> <span>8254 S. Garfield Street. Grand Island, NE 68801</span></p>
                                        <p className="house-phonenum"><i className="fa fa-phone" aria-hidden="true"></i> (805) - 369 - 3214</p>
                                        <a href="#" className="products-ellipsis"><i className="fa fa-ellipsis-h"></i></a>  
                                    </div>  
                                </div>  
                        
                        </div>
                        <div className="col-xl-4 col-lg-6 col-md-6 col-sm-12">  
                            <div className="row prdt-back-gd">
                                    <div className="col-sm-3 house-img-knee">        
                                        {/*{/*<!-- <img src="imgs/nike-shoes.png" alt="Nike-lunacharge-Essential" width="100%" /> -->*/}*/}    
                                    </div>  
                                    <div className="col-sm-9">        
                                        <h6 className="prdt-mainhd">translation by H. Rackham</h6>    
                                        <span className="prdt-headtext">Sku</span> <span className="prdt-nortext">32566L</span>
                                        <p className="prdt-location"><i className="fa fa-map-marker" style={{color: '#ff3333'}}></i> <span>8254 S. Garfield Street. Grand Island, NE 68801</span></p>
                                        <p className="house-phonenum"><i className="fa fa-phone" aria-hidden="true"></i> (805) - 369 - 3214</p>
                                        <a href="#" className="products-ellipsis"><i className="fa fa-ellipsis-h"></i></a>  
                                    </div>  
                                </div>  
                        </div>
                        <div className="col-xl-4 col-lg-6 col-md-6 col-sm-12">   
                            <div className="row prdt-back-gd">
                                    <div className="col-sm-3 house-img-combat">       
                                        {/*{/*<!-- <img src="imgs/nike-shoes.png" alt="Nike-lunacharge-Essential" width="100%" /> -->*/}*/}    
                                    </div>  
                                    <div className="col-sm-9">    
                                        <h6 className="prdt-mainhd">Caruso Logistics </h6>      
                                        <span className="prdt-headtext">Sku</span> <span className="prdt-nortext">32566L</span>
                                        <p className="prdt-location"><i className="fa fa-map-marker" style={{color: '#ff3333'}}></i> <span>8254 S. Garfield Street. Grand Island, NE 68801</span></p>
                                        <p className="house-phonenum"><i className="fa fa-phone" aria-hidden="true"></i> (805) - 369 - 3214</p>
                                        <a href="#" className="products-ellipsis"><i className="fa fa-ellipsis-h"></i></a> 
                                    </div>  
                                </div>    
                        </div>  
                    </div>
                    <div className="row single-prdt-sec">    
                        <div className="col-xl-4 col-lg-6 col-md-6 col-sm-12">               
                                     
                                <div className="row prdt-back-gd">
                                    <div className="col-sm-3 house-img-solly">      
                                        {/*{/*<!-- <img src="imgs/nike-shoes.png" alt="Nike-lunacharge-Essential" width="100%" /> -->*/}*/}    
                                    </div>  
                                    <div className="col-sm-9">      
                                        <h6 className="prdt-mainhd">de Finibus Bonorum et Malorum</h6>    
                                        <span className="prdt-headtext">Sku</span> <span className="prdt-nortext">32566L</span>
                                        <p className="prdt-location"><i className="fa fa-map-marker" style={{color: '#ff3333'}}></i> <span>8254 S. Garfield Street. Grand Island, NE 68801</span></p>
                                        <p className="house-phonenum"><i className="fa fa-phone" aria-hidden="true"></i> (805) - 369 - 3214</p>
                                        <a href="#" className="products-ellipsis"><i className="fa fa-ellipsis-h"></i></a>  
                                    </div>  
                                </div>  
                        
                        </div>
                        <div className="col-xl-4 col-lg-6 col-md-6 col-sm-12">  
                            <div className="row prdt-back-gd">
                                    <div className="col-sm-3 house-img-timberland">      
                                        {/*{/*<!-- <img src="imgs/nike-shoes.png" alt="Nike-lunacharge-Essential" width="100%" /> -->*/}*/}    
                                    </div>  
                                    <div className="col-sm-9">      
                                        <h6 className="prdt-mainhd">Advent International</h6>    
                                        <span className="prdt-headtext">Sku</span> <span className="prdt-nortext">32566L</span>
                                        <p className="prdt-location"><i className="fa fa-map-marker" style={{color: '#ff3333'}}></i> <span>8254 S. Garfield Street. Grand Island, NE 68801</span></p>
                                        <p className="house-phonenum"><i className="fa fa-phone" aria-hidden="true"></i> (805) - 369 - 3214</p>
                                        <a href="#" className="products-ellipsis"><i className="fa fa-ellipsis-h"></i></a>  
                                    </div>  
                                </div>  
                        </div>
                        <div className="col-xl-4 col-lg-6 col-md-6 col-sm-12">   
                            <div className="row prdt-back-gd">
                                    <div className="col-sm-3 house-img-court">      
                                        {/*{/*<!-- <img src="imgs/nike-shoes.png" alt="Nike-lunacharge-Essential" width="100%" /> -->*/}*/}    
                                    </div>  
                                    <div className="col-sm-9">      
                                        <h6 className="prdt-mainhd">Allen Organ Company</h6>        
                                        <span className="prdt-headtext">Sku</span> <span className="prdt-nortext">32566L</span>
                                        <p className="prdt-location"><i className="fa fa-map-marker" style={{color: '#ff3333'}}></i> <span>8254 S. Garfield Street. Grand Island, NE 68801</span></p>
                                        <p className="house-phonenum"><i className="fa fa-phone" aria-hidden="true"></i> (805) - 369 - 3214</p>
                                        <a href="#" className="products-ellipsis"><i className="fa fa-ellipsis-h"></i></a>   
                                    </div>  
                                </div>    
                        </div>  
                    </div>
                    <div className="row single-prdt-sec">    
                        <div className="col-xl-4 col-lg-6 col-md-6 col-sm-12">               
                                     
                                <div className="row prdt-back-gd">
                                    <div className="col-sm-3 house-img-mary">      
                                        {/*{/*<!-- <img src="imgs/nike-shoes.png" alt="Nike-lunacharge-Essential" width="100%" /> -->*/}*/}    
                                    </div>  
                                    <div className="col-sm-9">        
                                        <h6 className="prdt-mainhd">de Finibus Bonorum et Malorum</h6>    
                                        <span className="prdt-headtext">Sku</span> <span className="prdt-nortext">32566L</span>
                                        <p className="prdt-location"><i className="fa fa-map-marker" style={{color: '#ff3333'}}></i> <span>8254 S. Garfield Street. Grand Island, NE 68801</span></p>
                                        <p className="house-phonenum"><i className="fa fa-phone" aria-hidden="true"></i> (805) - 369 - 3214</p>
                                        <a href="#" className="products-ellipsis"><i className="fa fa-ellipsis-h"></i></a>  
                                    </div>  
                                </div>  
                        
                        </div>
                        <div className="col-xl-4 col-lg-6 col-md-6 col-sm-12">  
                            <div className="row prdt-back-gd">
                                    <div className="col-sm-3 house-img-platform">      
                                        {/*{/*<!-- <img src="imgs/nike-shoes.png" alt="Nike-lunacharge-Essential" width="100%" /> -->*/}*/}    
                                    </div>  
                                    <div className="col-sm-9">       
                                        <h6 className="prdt-mainhd">Advent International</h6>  
                                        <span className="prdt-headtext">Sku</span> <span className="prdt-nortext">32566L</span>
                                        <p className="prdt-location"><i className="fa fa-map-marker" style={{color: '#ff3333'}}></i> <span>8254 S. Garfield Street. Grand Island, NE 68801</span></p>
                                        <p className="house-phonenum"><i className="fa fa-phone" aria-hidden="true"></i> (805) - 369 - 3214</p>
                                        <a href="#" className="products-ellipsis"><i className="fa fa-ellipsis-h"></i></a>  
                                    </div>  
                                </div>  
                        </div>
                        <div className="col-xl-4 col-lg-6 col-md-6 col-sm-12">   
                            <div className="row prdt-back-gd">  
                                    <div className="col-sm-3 house-img-almond-toe">      
                                        {/*<!-- <img src="imgs/nike-shoes.png" alt="Nike-lunacharge-Essential" width="100%" /> -->*/}    
                                    </div>  
                                    <div className="col-sm-9">      
                                        <h6 className="prdt-mainhd">Allen Organ Company</h6>    
                                        <span className="prdt-headtext">Sku</span> <span className="prdt-nortext">32566L</span>
                                        <p className="prdt-location"><i className="fa fa-map-marker" style={{color: '#ff3333'}}></i> <span>8254 S. Garfield Street. Grand Island, NE 68801</span></p>
                                        <p className="house-phonenum"><i className="fa fa-phone" aria-hidden="true"></i> (805) - 369 - 3214</p>
                                        <a href="#" className="products-ellipsis"><i className="fa fa-ellipsis-h"></i></a>  
                                    </div>  
                                </div>    
                        </div>  
                    </div>  
            </div> 

                    {/* // <!--productModal-->       */}
                    <div id="houseModal" className="modal">

                        {/* <!-- Modal content --> */}
                        <div className="modal-content">
                            <div className="add-prdt-header">
                                <p>Add Location</p>
                                <button type="button" className="close-sign" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                            </div>
                            <div className="add-prdt-middle">
                                <form onSubmit={this.handleSubmit} >
                                    <ul>
                                        <li>
                                            <div className="row">
                                                <div className="col-sm-4">
                                                    <p>Warehouse Image</p>
                                                </div>
                                                <div className="col-sm-8">
                                                    <img src={clientimg} alt="upload-img" className="prdt-upload-img" />
                                                    <div className="upload-btn-wrapper">
                                                        <button className="btn">Upload Images</button>
                                                        <input type="file" name="warehouse_image" />
                                                    </div>

                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="row">
                                                <div className="col-sm-4">
                                                    <p>Warehouse</p>
                                                </div>
                                                <div className="col-sm-8">
                                                    <input type="text" name="warehouse_name" placeholder="Enter your warehouse name" />
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="row">
                                                <div className="col-sm-4">
                                                    <p>Address</p>
                                                </div>
                                                <div className="col-sm-8">
                                                    <input type="text" name="warehouse_address" placeholder="Enter your address" />
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="row">
                                                <div className="col-sm-4">
                                                    <p>City</p>
                                                </div>
                                                <div className="col-sm-8">
                                                    <input type="text" name="warehouse_city" placeholder="Your city name" />
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="row">
                                                <div className="col-sm-4">
                                                    <p>State</p>
                                                </div>
                                                <div className="col-sm-8">
                                                    <input type="text" name="warehouse_state" placeholder="Enter your state" />
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="row">
                                                <div className="col-sm-4">
                                                    <p>Country</p>
                                                </div>
                                                <div className="col-sm-8">
                                                    <input type="text" name="warehouse_country" placeholder="Enter your country" />
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="row">
                                                <div className="col-sm-4">
                                                    <p>Phone No.</p>
                                                </div>
                                                <div className="col-sm-8">
                                                    <input type="text" name="warehouse_phone" placeholder="eg. 326-789-0236" />
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="row">
                                                <div className="col-sm-4">

                                                </div>
                                                <div className="col-sm-8">
                                                    <button type="submit" className="Connect-btn" >Submit</button>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </form>
                            </div>
                            <div className="mod-footer">
                                {/* <!-- <span className="close1">Cancel</span>  --> */}
                            </div>
                        </div>
                    </div>

                
            </div>
            </div>
            
			);

	}

}
export default OrderWareHouse;