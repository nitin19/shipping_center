import React from 'react';
import ReactDOM from 'react-dom';

import './creditcard.css';

import visaimgs from '../../images/visa-imgs.png';
import { validateUserToken } from '../PrivateRoute';
import { scAxios } from '../..';
import { Redirect } from 'react-router-dom';
import { MAIN_PAGE_PATH, LOGIN_PAGE_PATH, API_TOKEN_NAME } from '../../constants';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const getCreditcardList = () => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/accounts/card/getList', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}


const saveCreditcard = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/accounts/card/add', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const IsCardVerified = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/accounts/card/iscardverified', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const verifyCreditcard = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/accounts/card/verify', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const deletecard = (data) => {

    return new Promise((resolve, reject) => {
        const req = scAxios.request('/accounts/card/delete', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }

        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const defaultcard = (data) => {

    return new Promise((resolve, reject) => {
        const req = scAxios.request('/accounts/card/default', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }

        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

class CreditCard extends React.Component {
    state = {
        name: '',
        number: '',
        month: '',
        year: '',
        cvv: '',
        default: false,
        creditcards: [],
        totalcards:'',
    }

    validateForm() {
        if (this.state.name.length > 0 && this.state.number.length > 13 && this.state.number.length < 20 && this.state.month.length == 2 && this.state.year.length == 2 && this.state.cvv.length == 3) {
            return true;
        }
        return false;
    }

    handleCheckBoxChange = event => {
        this.setState({
            [event.target.name]: event.target.checked
        });
    }

    handleChange = event => {
        this.setState({ [event.target.name]: event.target.value });
    }

    handleSubmit = event => {
        event.preventDefault();
        const carddetails = {
            name: this.state.name,
            number: this.state.number,
            month: this.state.month,
            year: this.state.year,
            cvv: this.state.cvv,
            default : this.state.default,
        }
    if (this.validateForm()) {
    saveCreditcard(carddetails)
        .then(res => {
            if(res.status=='1'){
                toast.success(res.message, {
                  position: toast.POSITION.BOTTOM_RIGHT
                });
                document.getElementsByClassName('close-sign')[0].click();
                this.setState({ 
                    name: '', 
                    number: '',
                    month: '',
                    year: '',
                    cvv: '',
                    default: '',
                });

            } else {
                toast.error(res.message, {
                  position: toast.POSITION.BOTTOM_RIGHT
                });
            }
            this.refreshCreditCardList();
        })
        .catch(err => {
            console.log(err);
        });
    } else {
        toast.error('Validation Error', {
          position: toast.POSITION.BOTTOM_RIGHT
        });
    }
}

    refreshCreditCardList = () => {
        getCreditcardList()
            .then(res => {
                if (res.totalRecords > 0) {
                    this.setState({ creditcards: res.records });
                } else {
                    this.setState({ creditcards: '' });
                }
                this.setState({ totalcards: res.totalRecords });
            })
            .catch(err => {
                console.log(err);
            });
    }

  cardverify = (cardId) =>{
        const data = {
            cardId: cardId
        } 
        
    verifyCreditcard(data)
        .then(res => {
          if(res.status=='1'){
            toast.success(res.message, {
              position: toast.POSITION.BOTTOM_RIGHT
            });
            this.props.history.push('/cardverify/'+cardId);
          } else {
            toast.error(res.message, {
              position: toast.POSITION.BOTTOM_RIGHT
            });
            this.refreshUserStoreList();
          }
          
        })
        .catch(err => {
            console.log(err);
        });
    }


  IsCardVerified = (cardId) => {
        const data = {
            cardId: cardId
        } 
      IsCardVerified(data)
        .then(res => {
          if(res.status=='1'){
            this.props.history.push('/cardverify/'+cardId);
          } else {
            this.cardverify(cardId);
          }
        })
        .catch(err => {
            console.log(err);
        });
  }

  delete(cardId){
    const data = {
            cardId: cardId
        }

      deletecard(data)
        .then(res => {
          if(res.status=='1'){
            toast.success(res.message, {
              position: toast.POSITION.BOTTOM_RIGHT
            });
          }  else {
            toast.error(res.message, {
              position: toast.POSITION.BOTTOM_RIGHT
            });
          }
          this.refreshCreditCardList();
        })
        .catch(err => {
            console.log(err);
        });
  }

  setdefault(cardId){
    const data = {
            cardId: cardId
        }

      defaultcard(data)
        .then(res => {
          if(res.status=='1'){
            toast.success(res.message, {
              position: toast.POSITION.BOTTOM_RIGHT
            });
          }  else {
            toast.error(res.message, {
              position: toast.POSITION.BOTTOM_RIGHT
            });
          }
          this.refreshCreditCardList();
        })
        .catch(err => {
            console.log(err);
        });
  }

    componentDidMount() {
        this.refreshCreditCardList();
    }


    render() {
 
        return (

            <div className="content-wrap universelclass">
                <div className="col-lg-12">
                    <div className="cards">             
                    <div className="cards-account">  
                        <div className="row">    
                            <div className="col-sm-12">   
                                <a href="#" data-toggle="modal" data-target="#addnewcardModal" className="add-card"><i className="fa fa-plus"></i> add new card</a> 
                            </div>  
                        </div>    
                    </div>    
                    <hr />  
                    <div className="cards-account-second"> 
                        
                        {
                            this.state.creditcards.length > 0
                            ?
                        this.state.creditcards.map(creditcard => {    
                        return(<div className="row" key={creditcard.id}>
                            <div className="col-sm-6">
                                <p>Card:xxxxxxxxxxxx{creditcard.ending}, {creditcard.type}</p>    
                                <p>Created: {creditcard.created}</p>    
                                </div> 

                                <div className="col-sm-6 confirm-close">
                                {creditcard.verified=='false' ?  
                                <a style={{cursor:'pointer'}} onClick={this.IsCardVerified.bind(this, creditcard.id)} className="confirm-card">confirm this card &nbsp;<i className="fa fa-angle-double-right" aria-hidden="true"></i></a>
                                 : 
                                 <a className="confirm-card">confirmed card &nbsp;<i className="fa fa-check" aria-hidden="true"></i></a> 
                             }
                                    {creditcard.default=='true' && creditcard.verified=='true'? <a className="white-btn"><i className="fa fa-star"></i>Default</a> : ''}   
                                    {creditcard.default=='false' && creditcard.verified=='true'? <a style={{cursor:'pointer'}} onClick={this.setdefault.bind(this, creditcard.id)} className="white-btn"><i className="far fa-star"></i>Default</a> : ''}
                                <a style={{cursor:'pointer'}} onClick={(e) => { if (window.confirm('Are you sure you wish to delete this card ?')) this.delete(creditcard.id)} } className="red-close">X</a>
                            </div>                          
                        </div>);
                    })
                        :
                        <div className="row"> </div>
                        }


                        <div className="row video-sec">  
                            <div className="col-sm-12">     
                                <h1>Video Instuction</h1>
                                <hr />          
                                <div className="video-Instuction">
                                    <iframe width="100%" height="400px" src="https://www.youtube.com/embed/ayIA4a0b-ME" frameBorder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen=""></iframe>     
                                </div>   
                            </div>  
                              
                        </div> 
                    </div>  
                    <div className="cards-account-form">  
                         <div className="row">
                            <div className="col-sm-12">
                                <p>What happens when you link a new card to your VIPparcel account:</p> 
                                <ol>
                                    
                                    <li>Click "Add Card".</li>
                                    <li>Fill out the required fields, and click "Submit".</li>
                                    <li>VIPparcel will send two charges between $0.01 and US $5.00 (After the card is confirmed, those two amounts will be deposited into your VIPparcel account;unconfirmed transactions will be refunded back to the credit card).</li>  
                                    <li>The charged amounts appear as two transactions from VIPparcelCo on your card statement. </li>
                                    <li>To verify your card, enter those amounts in two separate fields, and click "Verify". Note, you have three attempts and 24 hours for a card confirmation.</li>  
                                    <li>Our system will confirm the entered amounts, match the amount VIPparcel charged, and then approve the card.</li>
                                    <p>If VIPparcel is unable to charge your card, or if the entered amounts do not match with the charges, you will be asked to provide a new card information.</p>  
                                </ol>
                                
                                <p>Your security is our top priority.</p>     
                            </div>
                         </div>  
                    </div>
                </div> 
                </div>
                <footer className="add_list_footer">   
                    <div className="row">  
                    <div className="col-sm-12">        
                    <p>© 2019 <a href="#" style={{color: '#2db4e3', textDecoration: 'none'}}>VIP PARCEL</a></p>    
                    </div>  
                    </div>
                </footer>  

                {/*<!--productModal-->*/}      
                    <div id="addnewcardModal" className="modal">
                        {/*<!-- Modal content -->*/}
                        <div className="modal-content">
                            <div className="add-prdt-header">  
                                <p>Add Card</p>      
                                <button type="button" className="close-sign" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>      
                            </div>    
                            <div className="add-prdt-middle">
                                <form  id="newcardform">
                                    <div className="row card-row">   
                                        <div className="col-sm-12">  
                                            <input type="text" name="name" value={this.state.name} onChange={this.handleChange} placeholder="Cardholder's name *" />
                                        </div>  
                                    </div>
                                    <div className="row card-row">   
                                        <div className="col-sm-12">  
                                            <input type="text" name="number" value={this.state.number} onChange={this.handleChange} placeholder="Card Number *" pattern="[1-9][0-9]*|0"  />
                                        </div>
                                    </div>
                                    <div className="row card-row">   
                                        <div className="col-sm-4">
                                            <input type="text" name="month" value={this.state.month} onChange={this.handleChange} placeholder="Exp Month *" />  
                                        </div>
                                        <div className="col-sm-4">
                                            <input type="text" name="year" value={this.state.year} onChange={this.handleChange} placeholder="Exp Year *" />
                                        </div>
                                        <div className="col-sm-4">
                                            <input type="text" name="cvv" value={this.state.cvv} onChange={this.handleChange} placeholder="CVV *" />
                                        </div>  
                                    </div>
                                    <div className="row card-row">   
                                        <div className="col-sm-6">      
                                            <input type="button" value="Submit" className="Connect-btn" onClick={this.handleSubmit}/>
                                        </div>
                                        <div className="col-sm-6">    
                                              
                                              <p className="text-right">  
                                                <input type="checkbox" id="test1" value="{this.state.default}" name="default" onChange={this.handleCheckBoxChange}/>  
                                                <label htmlFor="test1">Default(Add Funds)</label>  
                                              </p>  
                                           
                                        </div>      
                                    </div>  
                                </form>
                            </div>       
                        </div>     
                    </div>
                <ToastContainer autoClose={5000} />
            </div>

        );
    }
}

export default CreditCard;
