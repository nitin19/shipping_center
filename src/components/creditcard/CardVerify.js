import React from 'react';
import ReactDOM from 'react-dom';

import './creditcard.css';

import visaimgs from '../../images/visa-imgs.png';
import { validateUserToken } from '../PrivateRoute';
import { scAxios } from '../..';
import { Redirect } from 'react-router-dom';
import { MAIN_PAGE_PATH, LOGIN_PAGE_PATH, API_TOKEN_NAME } from '../../constants';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const GetCardDetails = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/accounts/card/details', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const VerfyCardCharge = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/accounts/card/chargeverify', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const saveCreditcard = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/accounts/card/add', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

class CardVerify extends React.Component {
    state = {
        carddetails: [],
        amount1: '',
        amount2: '',
        name: '',
        number: '',
        month: '',
        year: '',
        cvv: '',
        default: false,
    }

validateForm() {
    if (this.state.name.length > 0 && this.state.number.length > 13 && this.state.number.length < 20 && this.state.month.length == 2 && this.state.year.length == 2 && this.state.cvv.length == 3) {
        return true;
    }
    return false;
}

handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
}

handleCheckBoxChange = event => {
    this.setState({
        [event.target.name]: event.target.checked
    });
}

handleSubmit = event => {
        event.preventDefault();
        const carddetails = {
            name: this.state.name,
            number: this.state.number,
            month: this.state.month,
            year: this.state.year,
            cvv: this.state.cvv,
            default : this.state.default,
        }
    if (this.validateForm()) {
    saveCreditcard(carddetails)
        .then(res => {
            if(res.status=='1'){
                toast.success(res.message, {
                  position: toast.POSITION.BOTTOM_RIGHT
                });
                document.getElementsByClassName('close-sign')[0].click();
                this.props.history.push('/creditcard/');
            } else {
                toast.error(res.message, {
                  position: toast.POSITION.BOTTOM_RIGHT
                });
            }
            this.refreshCreditCardList();
        })
        .catch(err => {
            console.log(err);
        });
    } else {
        toast.error('Validation Error', {
          position: toast.POSITION.BOTTOM_RIGHT
        });
    }
}

chargeverify = () =>{
        const data = {
            cardId: this.props.match.params.id,
            amount1: this.state.amount1,
            amount2: this.state.amount2,
        } 
        
    VerfyCardCharge(data)
        .then(res => {
          if(res.status=='1'){
            toast.success(res.message, {
              position: toast.POSITION.BOTTOM_RIGHT
            });
            this.setState({ 
                    amount1: '', 
                    amount2: '',
                });
            this.props.history.push('/creditcard/');
          } else {
            toast.error(res.message, {
              position: toast.POSITION.BOTTOM_RIGHT
            });
          }
          
        })
        .catch(err => {
            console.log(err);
        });
    }


componentDidMount() {
    const data = {
        cardId: this.props.match.params.id
    }
    
GetCardDetails(data)
    .then(res => {
      if(res.status=='1'){
        this.setState({ carddetails: res.carddetails });
      } else {
        this.setState({ carddetails: '' });
      }
    })
    .catch(err => {
        console.log(err);
    });

}

    render() {
 
        return (

            <div className="content-wrap universelclass">
                <div className="col-lg-12">
                    <div className="cards">             
                    <div className="cards-account">  
                        <div className="row">    
                            <div className="col-sm-12">   
                                <a href="#" data-toggle="modal" data-target="#addnewcardModal" className="add-card"><i className="fa fa-plus"></i> add new card</a> 
                            </div>  
                        </div>    
                    </div>    
                    <hr />  

                        <div className="row">
                            <div className="col-sm-12">
                                <p>Card:xxxxxxxxxxxx{this.state.carddetails.ending}, {this.state.carddetails.type}</p>    
                                <p>This card was charged on {this.state.carddetails.created}</p>    
                            </div>  
                        </div>
                     
                   
                    <hr />
   <form >
        <div className="row">
            <div className="col-sm-4">
                <input type="text" name="amount1" className="cominp-fild mywd option-pass" placeholder="Paid Amount (1)" onChange={event => this.setState({amount1: event.target.value})}/>
            </div>
            <div className="col-sm-4">
                <input type="text" name="amount2" className="cominp-fild mywd option-pass" placeholder="Paid Amount (2)" onChange={event => this.setState({amount2: event.target.value})}/>
            </div>
            <div className="col-sm-4">
                
            </div>
            
        </div> 

        <div className="row">
            <div className="col-sm-4">
               <input type="button" className="Resend-sms abtn" value="Submit" onClick={this.chargeverify}/>
            </div>
            </div>
            </form>

                </div> 
                </div>
                <footer className="add_list_footer">   
                    <div className="row">  
                    <div className="col-sm-12">        
                    <p>© 2019 <a href="#" style={{color: '#2db4e3', textDecoration: 'none'}}>VIP PARCEL</a></p>    
                    </div>  
                    </div>
                </footer>  


               {/*<!--productModal-->*/}      
                    <div id="addnewcardModal" className="modal">
                        {/*<!-- Modal content -->*/}
                        <div className="modal-content">
                            <div className="add-prdt-header">  
                                <p>Add Card</p>      
                                <button type="button" className="close-sign" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>      
                            </div>    
                            <div className="add-prdt-middle">
                                <form  id="newcardform">
                                    <div className="row card-row">   
                                        <div className="col-sm-12">  
                                            <input type="text" name="name" value={this.state.name} onChange={this.handleChange} placeholder="Cardholder's name *" />
                                        </div>  
                                    </div>
                                    <div className="row card-row">   
                                        <div className="col-sm-12">  
                                            <input type="text" name="number" value={this.state.number} onChange={this.handleChange} placeholder="Card Number *" pattern="[1-9][0-9]*|0"  />
                                        </div>
                                    </div>
                                    <div className="row card-row">   
                                        <div className="col-sm-4">
                                            <input type="text" name="month" value={this.state.month} onChange={this.handleChange} placeholder="Exp Month *" />  
                                        </div>
                                        <div className="col-sm-4">
                                            <input type="text" name="year" value={this.state.year} onChange={this.handleChange} placeholder="Exp Year *" />
                                        </div>
                                        <div className="col-sm-4">
                                            <input type="text" name="cvv" value={this.state.cvv} onChange={this.handleChange} placeholder="CVV *" />
                                        </div>  
                                    </div>
                                    <div className="row card-row">   
                                        <div className="col-sm-6">      
                                            <input type="button" value="Submit" className="Connect-btn" onClick={this.handleSubmit}/>
                                        </div>
                                        <div className="col-sm-6">    
                                              
                                              <p className="text-right">  
                                                <input type="checkbox" id="test1" value="{this.state.default}" name="default" onChange={this.handleCheckBoxChange}/>  
                                                <label for="test1">Default(Add Funds)</label>  
                                              </p>  
                                           
                                        </div>      
                                    </div>  
                                </form>
                            </div>       
                        </div>     
                    </div>

               
                <ToastContainer autoClose={5000} />
            </div>

        );
    }
}

export default CardVerify;
