import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import { NavLink, Link, Route, Switch, Redirect } from 'react-router-dom';

import * as userActions from '../../actions/userActions';
import { scAxios } from '../..';
import { API_TOKEN_NAME, USER_ID} from '../../constants';
import { startUserSession } from '../userSession';

import Header from '../header/Header.js';
import logo from '../../images/logo.png';
import './dashboard.css';
import '../../css/bootstrap.min.css';
import '../../css/style.css';
import Main from '../main/Main.js';
import PrivateRoute from '../PrivateRoute.js';
import MyStore from '../myStore/MyStore';
import WarehouseLists from '../warehouseLists/WarehouseLists';
import Orders from '../orders/Orders';
import Packages from '../packages/Packages';
import PrintShipping from '../printShipping/PrintShipping';
import Products from '../products/Products';
import Viewmystore from '../viewmystore/Viewmystore';
import Singleorderdetail from '../singleorderdetail/Singleorderdetail';
import Createorderprocessor from '../createorderprocessor/Createorderprocessor';
import Vieworderprocessor from '../vieworderprocessor/Vieworderprocessor';
import Vieworderprocessor2 from '../vieworderprocessor2/Vieworderprocessor2';
import OrderProcesser from '../orderprocesser/OrderProcesser';
import OrderMyStore from '../ordermystore/OrderMyStore';
import OrderWareHouse from '../orderwarehouse/OrderWareHouse';
import OrderView from '../orderview/OrderView';
import StoreOrder from '../storeorder/StoreOrder';
import SingleOrderView from '../singleorderview/SingleOrderView';
import Setting from '../setting/Setting';
import InventoryManagement from '../inventorymanagement/InventoryManagement';
import AllocationStrategy from '../allocationstrategy/AllocationStrategy';
import InventoryWarehouses from '../inventorywarehouses/InventoryWarehouses';
import EditOrderProcesser from '../editorderprocesser/EditOrderProcesser';
import SellingChannels from '../sellingchannels/SellingChannels';
import AutomationRules from '../automationrules/AutomationRules';
import PrintingSetup from '../printingsetup/PrintingSetup';
import ShipStationConnect from '../shipstationconnect/ShipStationConnect';
import ShipFromLocations from '../shipfromlocations/ShipFromLocations';
import EmailTemplates from '../emailtemplates/EmailTemplates';
import PackingSlips from '../packingslips/PackingSlips';
import UserManagement from '../usermanagement/UserManagement';
import SubscriptionPlans from '../subscriptionplans/SubscriptionPlans';
import ViewWareHouse from '../viewwarehouse/ViewWareHouse';
import EditWareHouse from '../editwarehouse/EditWareHouse';
import RenewPassword from '../renewpassword/RenewPassword';  
import SingleStoreOrder from '../singlestoreorder/SingleStoreOrder';
import BrandedCustomer from '../brandedcustomer/BrandedCustomer';
import CreditCard from '../creditcard/CreditCard';
import CardVerify from '../creditcard/CardVerify';
import Addfunds from '../addfunds/Addfunds';
import PickList from '../picklist/PickList';
import Notifications from '../notifications/Notifications';
import EditProfile from '../editprofile/EditProfile';
import Profile from '../editprofile/Profile';
import SingleStorePickList from '../singlestoreorder/SingleStorePickList';
import Error404 from '../errorpages/Error404';
import Error500 from '../errorpages/Error500';
import Errorpage from '../errorpages/Errorpage';
// import ShippingCarriers from '../shippingcarriers/ShippingCarriers';
import { USER_ROLE } from '../../constants';

const getStoresCount = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/dashboard/storesCount', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getUserdetails = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('user', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                userid: localStorage.getItem(USER_ID)
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getOrdersCount = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/dashboard/ordersCount', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getProductsCount = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/dashboard/productsCount', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getWarehousesCount = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/dashboard/warehousesCount', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

class Dashboard extends React.Component {

    state = {
        stores_count: '',
        orders_count: '',
        products_count: '',
        warehouses_count: '',
        totalbalance:'',
    };

    componentDidMount() {
        const data = {
            userid: localStorage.getItem(USER_ID)
        }
        getStoresCount(data)
            .then(res => {
                this.setState({ stores_count: res.total_stores });
                console.log(res.total_stores);
            })
            .catch(err => {
                console.log(err);
                alert("Error occured fetching stores count. Check console");
        });

        getOrdersCount(data)
            .then(res => {
                this.setState({ orders_count: res.total_orders });
                console.log(res.total_orders);
            })
            .catch(err => {
                console.log(err);
                alert("Error occured fetching orders count. Check console");
        });

        getProductsCount(data)
            .then(res => {
                this.setState({ products_count: res.total_products });
                console.log(res.total_products);
            })
            .catch(err => {
                console.log(err);
                alert("Error occured fetching products count. Check console");
        });

        getWarehousesCount(data)
            .then(res => {
                this.setState({ warehouses_count: res.total_warehouses });
                console.log(res.total_warehouses);
            })
            .catch(err => {
                console.log(err);
                alert("Error occured fetching locations count. Check console");
        });

        getUserdetails()
            .then(res => {
                this.setState({ totalbalance: res.balance });
            })
            .catch(err => {
                console.log(err);
        });     
             
    }

    render() {  
        console.log(this.props.user);
        return (
            <>
                {
                    localStorage.getItem(USER_ROLE) !== "OP" ?
                        <>
                         
                            <div className="sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures">
                                <div className="nano has-scrollbar">
                                    <div className="nano-content" style={{ right: -25 }}>
                                        <div className="logo"><Link to="/dashboard"><img src={logo} alt="vip parcel logo" /> </Link>
                                            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                                <span className="navbar-toggler-icon"></span>
                                                <span className="navbar-toggler-icon"></span>
                                                <span className="navbar-toggler-icon"></span>
                                            </button>
                                        </div>
                                        <ul className="sidebarnav collapse navbar-collapse" id="navbarSupportedContent">
                                            <li ><NavLink to="/dashboard" activeClassName="active"><i className="fa fa-tachometer-alt"></i> Dashboard</NavLink></li>
                                            <li ><NavLink to="#" activeClassName="active"><i className="fa fa-store-alt"></i> Manage Stores <span className="badge orange">{this.state.stores_count}</span></NavLink>
                                                <ul className="subside-bar" id="navbarSupportedContent">
                                                    <li ><NavLink to="/my-stores" activeClassName="active"><i className="fas fa-qrcode"></i> Add New Store</NavLink></li>
                                                    <li ><NavLink to="/viewmystore" activeClassName="active"><i className="fas fa-qrcode"></i> View MyStores</NavLink></li>
                                                </ul>
                                            </li>
                                            <li ><NavLink to="#" activeClassName="active"><i className="fa fa-clipboard"></i> Manage Orders <span className="badge green">{this.state.orders_count}</span></NavLink>
                                                <ul className="subside-bar" id="navbarSupportedContent">
                                                    <li ><NavLink to="/storeorder" activeClassName="active"><i className="fas fa-qrcode"></i> Orders-listing</NavLink></li>
                                                </ul>
                                            </li>
                                            <li ><NavLink to="/warehouse-lists" activeClassName="active"><i className="fa fa-warehouse"></i> Manage Warehouse <span className="badge blue">{this.state.warehouses_count}</span></NavLink>
                                            </li>
                                            <li ><NavLink to="/packages-lists" activeClassName="active"><i className="fa fa-chart-bar"></i> Manage Packages</NavLink></li>
                                            <li ><NavLink to="#" activeClassName="active"><i className="fa fa-database"></i> Manage Order Processor</NavLink>
                                                <ul className="subside-bar" id="navbarSupportedContent">
                                                    <li className=""><NavLink to="/createorderprocessor" activeClassName="active"><i className="fas fa-qrcode"></i> Create Order Processor </NavLink></li>
                                                    <li className=""><NavLink to="/vieworderprocessor" activeClassName="active"><i className="fas fa-qrcode"></i> View Order Processor</NavLink></li>
                                                </ul>
                                            </li>
                                            <li><NavLink to="#" activeClassName="active"><i className="fa fa-map-marker-alt"></i> Inventory Management</NavLink>
                                                <ul className="subside-bar" id="navbarSupportedContent">
                                                    <li className=""><NavLink to="/inventorymanagement" activeClassName="active"><i className="fas fa-qrcode"></i> Inventory Settings </NavLink></li>
                                                    <li className=""><NavLink to="/allocationstrategy" activeClassName="active"><i className="fas fa-qrcode"></i> Allocation Strategy</NavLink></li>
                                                    <li className=""><NavLink to="/inventorywarehouses" activeClassName="active"> <i className="fas fa-qrcode"></i> Inventory Warehouses</NavLink></li>
                                                </ul>
                                            </li>
                                            <li><NavLink to="#" activeClassName="active"><i className="far fa-money-bill-alt"></i> Finance </NavLink>
                                                <ul className="subside-bar" id="navbarSupportedContent">
                                                    <li ><NavLink to="/balance" activeClassName="active"><i className="fas fa-qrcode"></i> ${this.state.totalbalance} </NavLink></li>
                                                    <li ><NavLink to="/addfunds" activeClassName="active"><i className="fas fa-qrcode"></i> Add funds </NavLink></li>
                                                    <li ><NavLink to="/creditcard" activeClassName="active"><i className="fas fa-qrcode"></i> Credit Card </NavLink></li>
                                                </ul>
                                            </li>
                                            <li ><NavLink to="#" activeClassName="active"><i className="fa fa-chart-bar"></i> Customer Analytics</NavLink></li>
                                            <li ><NavLink to="/notifications" activeClassName="active"><i className="fa fa-envelope"></i> Notifications</NavLink></li>
                                            <li className="dropdown-divider"></li>
                                            <li ><NavLink to="/setting" activeClassName="active"><i className="fa fa-cog"></i> Settings <span className="badge yellow">4</span></NavLink></li>
                                            <li><NavLink to="/Error404" activeClassName="active">Error 404</NavLink></li>
                                            <li><NavLink to="/Error500" activeClassName="active">Error 500</NavLink></li>
                                            <li><NavLink to="/Errorpage" activeClassName="active">Error Page</NavLink></li>
                                        </ul>
                                    </div>  
                                </div>
                            </div>

                            <Route component={Header} />

                            <Switch>
                                <PrivateRoute path="/dashboard" component={Main} user={this.props.user} exact />
                                <PrivateRoute path="/my-stores" component={MyStore} user={this.props.user} exact />
                                <PrivateRoute path='/warehouse-lists' component={WarehouseLists} user={this.props.user} exact />
                                <PrivateRoute path='/orders-lists/:id' component={Orders} user={this.props.user} exact />
                                <PrivateRoute path='/packages-lists' component={Packages} user={this.props.user} exact />
                                <PrivateRoute path='/print-shipping/:id' component={PrintShipping} user={this.props.user} exact />
                                <PrivateRoute path='/products-lists' component={Products} user={this.props.user} exact />
                                <PrivateRoute path='/viewmystore' component={Viewmystore} user={this.props.user} exact />
                                <PrivateRoute path='/singleorderdetail/:id' component={Singleorderdetail} user={this.props.user} exact />
                                <PrivateRoute path='/createorderprocessor' component={Createorderprocessor} user={this.props.user} exact />
                                <PrivateRoute path='/vieworderprocessor' component={Vieworderprocessor} user={this.props.user} exact />
                                <PrivateRoute path='/vieworderprocessor2/:id' component={Vieworderprocessor2} user={this.props.user} exact />
                                <PrivateRoute path='/orderprocesser' component={OrderProcesser} user={this.props.user} exact />
                                <PrivateRoute path='/ordermystore' component={OrderMyStore} user={this.props.user} exact />
                                <PrivateRoute path='/orderwarehouse' component={OrderWareHouse} user={this.props.user} exact />
                                <PrivateRoute path='/orderview' component={OrderView} user={this.props.user} exact />
                                <PrivateRoute path='/storeorder' component={StoreOrder} user={this.props.user} exact />
                                <PrivateRoute path='/setting' component={Setting} user={this.props.user} exact />
                                <PrivateRoute path='/inventorymanagement' component={InventoryManagement} user={this.props.user} exact />
                                <PrivateRoute path='/allocationstrategy' component={AllocationStrategy} user={this.props.user} exact />
                                <PrivateRoute path='/inventorywarehouses' component={InventoryWarehouses} user={this.props.user} exact />
                                <PrivateRoute path='/editorderprocesser/:id' component={EditOrderProcesser} user={this.props.user} exact />
                                <PrivateRoute path='/sellingchannels' component={SellingChannels} user={this.props.user} exact />
                                <PrivateRoute path='/automationrules' component={AutomationRules} user={this.props.user} exact />
                                <PrivateRoute path='/printingsetup' component={PrintingSetup} user={this.props.user} exact />
                                <PrivateRoute path='/shipstationconnect' component={ShipStationConnect} user={this.props.user} exact />
                                <PrivateRoute path='/shipfromlocations' component={ShipFromLocations} user={this.props.user} exact />
                                <PrivateRoute path='/emailtemplates' component={EmailTemplates} user={this.props.user} exact />
                                <PrivateRoute path='/packingslips' component={PackingSlips} user={this.props.user} exact />
                                <PrivateRoute path='/usermanagement' component={UserManagement} user={this.props.user} exact />
                                <PrivateRoute path='/subscriptionplans' component={SubscriptionPlans} user={this.props.user} exact />
                                <PrivateRoute path='/viewwarehouse/:id' component={ViewWareHouse} user={this.props.user} exact />
                                <PrivateRoute path='/editwarehouse/:id' component={EditWareHouse} user={this.props.user} exact />
                                <PrivateRoute path='/renewpassword' component={RenewPassword} user={this.props.user} exact />
                                <PrivateRoute path='/singlestoreorder/:slug' component={SingleStoreOrder} user={this.props.user} exact />
                                <PrivateRoute path='/brandedcustomer' component={BrandedCustomer} user={this.props.user} exact />
                                <PrivateRoute path='/creditcard' component={CreditCard} user={this.props.user} exact />
                                <PrivateRoute path='/addfunds' component={Addfunds} user={this.props.user} exact />
                                <PrivateRoute path='/cardverify/:id' component={CardVerify} user={this.props.user} exact />
                                <PrivateRoute path='/picklist/:orders' component={PickList} user={this.props.user} exact />
                                <PrivateRoute path='/singlestorepicklist/:slug/:orders' component={SingleStorePickList} user={this.props.user} exact />
                               {/* <PrivateRoute path='/shippingcarriers' component={ShippingCarriers} user={this.props.user} exact />*/}
                                <PrivateRoute path='/notifications' component={Notifications} user={this.props.user} exact />
                                <PrivateRoute path='/editprofile' component={EditProfile} user={this.props.user} exact />
                                <PrivateRoute path='/profile' component={Profile} user={this.props.user} exact />
                                <PrivateRoute path='/error404' component={Error404} user={this.props.user} exact />
                                <PrivateRoute path='/error500' component={Error500} user={this.props.user} exact />
                                <PrivateRoute path='/errorpage' component={Errorpage} user={this.props.user} exact />
                                <Redirect to="/dashboard" />  
                            </Switch>
                        </>

                        :
                        <>
                    <div className="sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures">
                                <div className="nano has-scrollbar">
                                    <div className="nano-content" style={{ right: -25 }}>
                                        <div className="logo"><Link to="/dashboard"><img src={logo} alt="vip parcel logo" /> </Link>
                                            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                                <span className="navbar-toggler-icon"></span>
                                                <span className="navbar-toggler-icon"></span>
                                                <span className="navbar-toggler-icon"></span>
                                            </button>
                                        </div>
                                        <ul className="sidebarnav collapse navbar-collapse" id="navbarSupportedContent">
                                            <li ><NavLink to="/dashboard" activeClassName="active"><i className="fa fa-tachometer-alt"></i> Dashboard</NavLink></li>
                                             <li ><NavLink to="/viewmystore" activeClassName="active"><i className="fa fa-database"></i> View MyStores</NavLink></li>
                                            <li ><NavLink to="/storeorder" activeClassName="active"><i className="fa fa-chart-bar"></i> Orders-listing</NavLink></li>

                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <Route component={Header} />

                            <Switch>
                                <PrivateRoute path="/dashboard" component={OrderProcesser} user={this.props.user} exact />
                                <PrivateRoute path='/viewmystore' component={Viewmystore} user={this.props.user} exact />
                                <PrivateRoute path='/singlestoreorder/:slug' component={SingleStoreOrder} user={this.props.user} exact />
                                <PrivateRoute path='/storeorder' component={StoreOrder} user={this.props.user} exact />
                                <Redirect to="/dashboard" />
                            </Switch>
                            
                        </>
                }
            </>
        );
    }
}


const mapStateToProps = (state /*, ownProps*/) => {
    return {
        user: state.user,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        actions: bindActionCreators(userActions, dispatch),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
