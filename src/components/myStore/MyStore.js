import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Redirect } from 'react-router-dom';
import * as userActions from '../../actions/userActions';
import { scAxios } from '../..';
import { API_TOKEN_NAME, USER_ID } from '../../constants';
import { startUserSession } from '../userSession';

import './myStore.css';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import loadingimage from '../../images/small_loader.gif';

import newProject from '../../images/myStores/New-Project.png';
import amazonLogo from '../../images/myStores/amazon-logo.png';
import shopify from '../../images/myStores/shopify.png';
import eBay from '../../images/myStores/ebayLogo.png';
import amazonLogo_tra from '../../images/myStores/amazon-logo_tra.png';
import opencart from '../../images/opencart-blog.png';
import dcart from '../../images/3dcart-logo.png';
import oscommerce from '../../images/oscommerceimg.png';
import ubercart from '../../images/ubercart-logo.png';
import wixstore from '../../images/65_wix_store.png';
import prestashop from '../../images/prestashop.png';
import Magento from '../../images/Magento-svg.png';
import volusion from '../../images/volusion.png';
import ecwid from '../../images/52_ecwid.png';
import woocommerce from '../../images/logo-woocommerce.png';
import bigcommerce from '../../images/big-commerce.png';
import bigcartel from '../../images/49_bigcartel_store.png';
import squarespace from '../../images/squarespace.png';
import WeeblyStore from '../../images/46_Weebly_Store.png';
import EtsyStore from '../../images/45_Connecting_Etsy_Store.png';
import Skeleton from 'react-loading-skeleton';

/*const handleAmazonVerifySellerId = event => {
    event.preventDefault();
}*/

/*const handleAmazonConnect = event => {
    event.preventDefault();
}*/

const handleEBayConnect = event => {
    event.preventDefault();
}

const getMarketplace = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/marketplace/list', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const VerifySellerId = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/stores/checkAccountStatus', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const AmazonConnectStore = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/stores/save', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const EbayConnectStore = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/stores/save', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const MagentoConnectStore = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/stores/save', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

class MyStore extends React.Component {

    state = {
        marketplaces: [],
        searchdata: '',
        total: '',
        currentPage: '',
        LastPage:'',
        PerPage: '',
        FirstPageUrl:'',
        NextPageUrl:'',
        PrevPageUrl:'',
        LastPageUrl:'',
        TotalPages:'',

        search2: '',
        firstName: '',
        mwsToken: '',
        send_cost_to_selling_manager: false,
        id: '',
        key: '',
        storeName:'',
        identifier: 'amazonus',
        amacnt_success: false,
        disableAmazonConnectBtn: true,
        ebaystorename: '',
        ebayusername:'',
        magentoversion:'',
        magentousername:'',
        magentopassword:'',
        magentostoreurl:'',
        loadingFirst: true,
    }

    handleChange = event => {
        this.setState({ [event.target.name]: event.target.value })
        this.canConnectAmazon();
    }

    canConnectAmazon() {
    const { id, key, storeName } = this.state
        if (id.length > 0 && key.length > 0 && storeName.length > 0) {
          this.setState({
            disableAmazonConnectBtn: false
          })
        } else {
          this.setState({
            disableAmazonConnectBtn: true
          })
        }
    }

    handleCheckBoxChange = event => {
        this.setState({
            [event.target.name]: event.target.checked
        }, () => this.validateForm());
    }

    refreshMarketplacesList = (page) => {
        const data = {
            searchdata: this.state.searchdata,
            page: page,
        }
        getMarketplace(data)
            .then(res => {
                this.setState({ loadingFirst: false });
                if(res.status==1){
                var records = res.success.data;
                this.setState({ marketplaces: records });
                this.setState({ total: res.success.total });
                this.setState({ currentPage: res.success.current_page });
                this.setState({ PerPage: res.success.per_page });
                this.setState({ FirstPageUrl: res.success.first_page_url });
                this.setState({ NextPageUrl: res.success.next_page_url });
                this.setState({ PrevPageUrl: res.success.prev_page_url });
                this.setState({ LastPageUrl: res.success.last_page_url });
                this.setState({ LastPage: res.success.last_page });
                this.setState({ TotalPages: Math.ceil(this.state.total / this.state.PerPage) });
              } else {
                this.setState({ marketplaces: '' });
              }
            })
            .catch(err => {
                console.log(err);
            });
    }

    handleSubmit = event => {
        event.preventDefault();

    }

    handleAmazonVerifySellerId = event => {
    event.preventDefault();
    const amavfs = {
            id: this.state.id,
            key: this.state.key,
            storeName: this.state.storeName,
            identifier: this.state.identifier,
            userid: localStorage.getItem(USER_ID)
        }

        VerifySellerId(amavfs)
            .then(res => {

                if(res.status==true){
                    toast.success('Verified seller', {
                      position: toast.POSITION.BOTTOM_RIGHT
                    });
                    
                  } else {
                    toast.error('Not a verified seller', {
                      position: toast.POSITION.BOTTOM_RIGHT
                    });
                  }

                console.log('verify status', res);
            })
            .catch(err => {
                console.log(err);
            });
    }

    handleAmazonConnect = event => {
    event.preventDefault();
        const amacnt = {
            id: this.state.id,
            key: this.state.key,
            storeName: this.state.storeName,
            identifier: this.state.identifier,
            userid: localStorage.getItem(USER_ID)
        }

        AmazonConnectStore(amacnt)
            .then(res => {

                if(res.status==1){
                    console.log('connect success', res);
                    document.getElementById("amazonModal").click();
                    this.setState({ amacnt_success: true });
                    toast.success('Store added successfully', {
                      position: toast.POSITION.BOTTOM_RIGHT
                    });
                    
                  } else {
                    toast.error('Not a verified seller', {
                      position: toast.POSITION.BOTTOM_RIGHT
                    });
                  }
                  
            })
            .catch(err => {
                console.log(err);
            });
    }

    handleEbayConnect = () => {
         const data = {
            ebayusername: '',
            identifier: 'ebayus',
            storeName: this.state.ebaystorename,
            userid: localStorage.getItem(USER_ID)
        }
        EbayConnectStore(data)
            .then(res => {
                console.log('connect success', res);
                window.location.href=res.url;
            })
            .catch(err => {
                console.log(err);
                toast.error('Connect eBay error', {
                      position: toast.POSITION.BOTTOM_RIGHT
                    });
            });
    }

    handleMagentoConnect = () => {
        this.setState({ loadingFirst: true });
        const data = {
            identifier: 'magento',
            magentoversion: this.state.magentoversion,
            username: this.state.magentousername,
            password: this.state.magentopassword,
            storeurl: this.state.magentostoreurl,
            storeName: this.state.storeName,
            userid: localStorage.getItem(USER_ID)
        }
    MagentoConnectStore(data)
        .then(res => {
            if(res.status=='1'){
                this.setState({ loadingFirst: false });
                document.getElementById('magentoModal').getElementsByClassName('crose')[0].click();
                this.setState({ 
                  magentousername: '',
                  magentopassword: '', 
                  magentostoreurl: '',
                  storeName: '',
                });
                toast.success('Store added successfully', {
                  position: toast.POSITION.BOTTOM_RIGHT
                });
          }  else {
            toast.error('Some problem exist. Try again later.', {
              position: toast.POSITION.BOTTOM_RIGHT
            });
          }
        })
        .catch(err => {
            console.log(err);
        });
    }

    componentDidMount() {

        /*getMarketplace()
            .then(res => {
                this.setState({ marketplaces: res.success });
            })
            .catch(err => {
                console.log(err);
                alert("Error occured fetching marketplaces. Check console");
            });*/

        this.refreshMarketplacesList();    

    }

    render() {

        const currentPage = this.state.currentPage;
        const previousPage = currentPage - 1;
        const NextPage = currentPage + 1;
        const LastPage = this.state.LastPage;
        const pageNumbers = [];
            for (let i = 1; i <= this.state.TotalPages; i++) {
              pageNumbers.push(i);
            }

        if (this.state.amacnt_success) return <Redirect to="/viewmystore" />

        return (
            <>
                <div className="content-wrap universelclass">

                    {this.state.loadingFirst==true ?
                        <div className="content-wrap backcollor">
                  <div className="ms-stor">
                        <div className="row mstop">
                            <div className="col-sm-4">
                                {this.props.title || <Skeleton count={2} duration={2} height={30} />}
                            </div>
                            <div className="col-sm-4">
                                
                            </div>
                            <div className="col-sm-4">
                                {this.props.title || <Skeleton count={1} duration={2} height={50} />}
                            </div>
                        </div>
                      <p>{this.props.title || <Skeleton count={1} duration={2} />}</p>
                      <div className="mystoreboxs">
                          <h1>{this.props.title || <Skeleton count={5} duration={2} />}</h1>
                      </div>
                      <div className="mystoreboxs">
                          <h1>{this.props.title || <Skeleton count={5} duration={2} />}</h1>
                      </div>
                      <div className="mystoreboxs">
                          <h1>{this.props.title || <Skeleton count={5} duration={2} />}</h1>
                      </div>
                      <div className="mystoreboxs">
                          <h1>{this.props.title || <Skeleton count={5} duration={2} />}</h1>
                      </div>
                  </div>
                </div>
                        : 

                    <div className="storemiddle-sec">

                        <div className="row addstore-head-text">
                            <div className="col-sm-8 wow fadeInLeft animated" data-wow-delay="0.4s">
                                <h1>Connect to selling stores</h1>
                                <p className="head-distext">We integrate with most online storefronts, marketplaces, and shopping carts (and we’re adding more all the time).</p>
                                
                            </div>
                            <div className="col-sm-4 wow fadeInLeft animated" data-wow-delay="0.8s">
                                <div className="mysearch">
                                    <form className="floatnone">
                                        <input className="my-input" type="text" placeholder="Search" name="searchdata" value={this.state.searchdata} onChange={event => this.setState({searchdata: event.target.value})} />
                                        <input type="button" className="Connect-btn my-btn" id="submit_btn" value="Search" onClick={this.refreshMarketplacesList}/>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <hr className="custom-hr" />


                        <div className="row add-store-imgsec">

                       {
                        this.state.marketplaces.length > 0
                        ?  
                        this.state.marketplaces.map(marketplace => {

                        return <div className="col-md-2 col-sm-3 col-xs-6 wow fadeInLeft animated" data-wow-delay="0.2s" key={marketplace.id}>
                                {/* <!--<a href="#" id="myBtn"><img src={shopify} alt="ebay-logo" width="100%" /></a>  --> */}
                                <a href="#" data-toggle="modal" data-target={'#'+marketplace.name+'Modal'}> <img src={marketplace.logo_image} alt={marketplace.name} width="100%" height="72px" /></a>
                            </div>
                               
                        })
                        :
                        <div className="col-md-12 col-sm-12 col-xs-12 wow fadeInLeft animated" data-wow-delay="0.2s">
                          <h2 style={{textAlign: 'center'}}>No Records founds!</h2>
                        </div>
                        }
                            
                        </div>

                        { pageNumbers.length > 1 ?

                                <div className="paggination-text">
                                  <ul className="pagination pagination-sm">
                                    {this.state.PrevPageUrl != null ? 
                                  <li className="page-item"><a className="page-link" style={{cursor:'pointer'}} onClick={this.refreshMarketplacesList.bind(this, previousPage)}>Previous</a></li>
                                  : ''
                                }
                                      {pageNumbers.map(page => {
                                      return (<li className={currentPage == page ? 'activelink page-item' : 'page-item' } ><a className="page-link" style={{cursor:'pointer'}} onClick={this.refreshMarketplacesList.bind(this, page)}>{page}</a></li>);
                                    })}

                                   {this.state.NextPageUrl != null ? 
                                  <li className="page-item"><a className="page-link" style={{cursor:'pointer'}} onClick={this.refreshMarketplacesList.bind(this, NextPage)}>Next</a></li>
                                  : ''
                                }
                                    {this.state.LastPage != null && currentPage!=LastPage? 
                                  <li className="page-item"><a className="page-link" style={{cursor:'pointer'}} onClick={this.refreshMarketplacesList.bind(this, LastPage)} >Last</a></li>
                                  : ''
                                }
                                  
                                  </ul>
                                </div> : ''
                                }        


                        <div className="" style={{ position: "relative" }}>
                            <div className="loader"></div>
                        </div>
                        <div className="storelast-text">
                            <p>We connect easily with wherever you sell </p>
                        </div>
                    </div>

                   }

                </div>
                <footer className="add_list_footer">
                    <div className="row">
                        <div className="col-sm-12">
                            <p>© 2019 <a href="#" style={{color: 'rgb(7, 168, 222)'}}>VIP PARCEL</a></p>
                        </div>
                    </div>
                </footer>

               <ToastContainer autoClose={5000} />   

            {/*<!--model-popup-->*/}  
<div id="opencartModal" className="modal">

  {/*<!-- Modal content -->*/}
  <div className="modal-contented wow animated pulse"> 
    <div className="modal-header">  
        <h5>Connecting Your Store</h5>
        <button type="button" className="crose" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
    </div>       
    <div className="modal-middle">   
    <h6 className="wow animated fadeInleft">Follow these steps to get connected: <img src={ opencart } alt="opencart-imgs" className="opencart-imgs" /></h6>
        
             <p className="wow animated fadeInleft"><span style={{color: '#000', fontWeight: '600'}}>9.</span>Click on <span style={{color: '#000', fontWeight: '600'}}>Finish</span> to make this store active in ShipStation!</p>
        <div className="opctstp">
            <div className="row wow animated fadeInleft">    
             <div className="col-md-5 opn-la">Your OpenCart Version</div>
             <div className="col-md-4"> <select className="form-control template"> 
                <option>Select OpenCart Version</option>
                    <option id="version2">Select OpenCart Version2</option> 
                    <option>Select OpenCart Version3</option>
                    <option>Select OpenCart Version4</option>  
                    <option>Select OpenCart Version5</option>
                </select></div>   
            </div>  
            <div className="row wow animated fadeInleft">  
             <div className="col-md-5 opn-la">Shipping Center Key</div>
                 <div className="col-md-4"> 
                    <input type="text" name="Verificationn" className="cominp-fild option-pass" />
                 </div>   
            </div>  
            <div className="row wow animated fadeInleft">
                <div className="col-md-5 opn-la">Shipping Center Verificationn Key</div>
                 <div className="col-md-4"> 
                    <input type="text" name="Verificationn" className="cominp-fild option-pass" />
                 </div> 
              
            </div> 
            <div className="row wow animated fadeInleft">
                <div className="col-md-5 opn-la">Open Cart UR</div>
                 <div className="col-md-4"> 
                    <input type="text" name="Verificationn" className="cominp-fild option-pass" /> 
                    <span>(e.g. https://www.myopencart.com/ocart)</span>
                 </div>
                 <div className="col-md-3"> 
                    <a href="#" className="find-shop">Find My Shop</a>
                    
                 </div> 
              
            </div> 
            
                
        </div>
        <div className="wow animated wow animated fadeInUp"> 
            <span className="comma-text">NOTE: Separate multiple statuses with a comma.</span>
            <div className="opctstp">
             
            <div className="row wow animated fadeInleft">
                <div className="col-md-5 opn-la">Awaiting Payment Status</div>
                 <div className="col-md-4"> 
                    <input type="text" name="Verificationn" className="cominp-fild option-pass" />
                 </div>
            </div>
            <div className="row wow animated fadeInleft">
                <div className="col-md-5 opn-la">Paid Status</div>
                 <div className="col-md-4"> 
                    <input type="text" name="Verificationn" className="cominp-fild option-pass" />
                 </div>
            </div>
             <div className="row wow animated fadeInleft">
                <div className="col-md-5 opn-la">Shipping Status</div>
                 <div className="col-md-4"> 
                    <input type="text" name="Verificationn" className="cominp-fild option-pass" />
                 </div>
            </div>
             <div className="row wow animated fadeInleft">
                <div className="col-md-5 opn-la">Cancelled Status</div>
                 <div className="col-md-4"> 
                    <input type="text" name="Verificationn" className="cominp-fild option-pass" />
                 </div>
            </div>
            
                 
        </div>
        </div>
    </div>
    <div className="mod-footer"> 
    <a href="" className="Connect-btn">Connect</a>
        <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">Close</span></button> 
         
    </div>
  </div>     
</div>
{/*<!--cartlogoModal1-->*/}  
<div id="cartlogoModal1" className="modal">

  {/*<!-- Modal content -->*/}
  <div className="modal-contented wow animated fadeInDown">
    <div className="modal-header">  
        <h5 className=" wow animated zoomIn">Connecting Your Store</h5>
        <button type="button" className="crose" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
    </div>      
    <div className="modal-middle">    
    <h6 className=" wow animated zoomIn">Follow these steps to get connected: <img src={dcart} alt="cart-logo" className="opencart-imgs" /></h6>
      
    <ol className="oder-list oder-padd">  
        <li className=" wow animated zoomIn"><span style={{color: '#000', fontWeight: '600'}}>1.</span> Login to your 3dcart store admin.</li>
        <li className=" wow animated zoomIn"><span style={{color: '#000', fontWeight: '600'}}>2.</span> Select <span  style={{color: '#000', fontWeight: '600'}}>Modules</span> from the menu on the left.</li>
        <li className=" wow animated zoomIn"><span style={{color: '#000', fontWeight: '600'}}>3.</span> Locate <span  style={{color: '#000', fontWeight: '600'}}>REST API,</span> click + to expand then select <span  style={{color: '#000', fontWeight: '600'}}>Change Settings.</span></li>
        <li className=" wow animated zoomIn"><span style={{color: '#000', fontWeight: '600'}}>4.</span> Click <span  style={{color: '#000', fontWeight: '600'}}>+ Add</span> in the upper right.</li>
        <li className=" wow animated zoomIn"><span style={{color: '#000', fontWeight: '600'}}>5.</span> Copy and paste the following value into the Public Key field and click + Add.</li>
        <li className="bold-extra"><span style={{color: '#000', fontWeight: '600'}}>Public Key:</span> <span  style={{color: '#04a5dc', fontWeight: '600'}}>  9ff5a12b310ec4ea9d22666ecaf249d8</span></li> 
        <li className=" wow animated zoomIn"><span style={{color: '#000', fontWeight: '600'}}>6.</span> Click + <span  style={{color: '#000', fontWeight: '600'}}>Authorize</span> to give ShipStation access to your 3dcart store.</li>
        <li className=" wow animated zoomIn"><span style={{color: '#000', fontWeight: '600'}}>7.</span> 7. A new window will appear and you'll be prompted to select your 3dcart Product Weight Units. <span  style={{color: '#000', fontWeight: '600'}}>Please disable your ad    blocker if this window does not appear.</span></li>
        <li className=" wow animated zoomIn"><span style={{color: '#000', fontWeight: '600'}}>8.</span> Click <span  style={{color: '#000', fontWeight: '600'}}> Connect.</span></li>
        <li className=" wow animated zoomIn"><span style={{color: '#000', fontWeight: '600'}}>9.</span> Your <span  style={{color: '#000', fontWeight: '600'}}>Shipping Center</span> account will open in a new tab where you can complete your store setup.</li>
    </ol>  

    </div>
    <div className= "mod-footer"> 
        <button type="button" className= "close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">Close</span></button> 
        
    </div>
  </div>     
</div>
{/*<!--cartlogoModal1-->*/}  
<div id="oscomersModal1" className= "modal">
 
  {/*<!-- Modal content -->*/}
  <div className= "modal-contented wow animated fadeInLeft">
    <div className= "modal-header">  
        <h5>Connecting Your Store</h5>
        <button type="button" className= "crose" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
    </div>      
    <div className= "modal-middle">    
    <h6>Follow these steps to get connected: <img src={oscommerce} alt="cart-logo" className= "opencart-imgs" /></h6>
    <span className= "comma-text">In order for Shipping Center to import orders from oscommerce and post back tracking information, you wil need to download and install a plugin on your Oscommerce server.</span>
    <ol className= "oder-list oder-padd"> 
        <li><span style={{color: '#000', fontWeight: '600'}}>1.</span> Download <a href="#" style={{color: '#04a5dc', fontWeight: '600'}}>Shipping Center/Plugin for OsCommerce</a> </li>
        <li><span style={{color: '#000', fontWeight: '600'}}>2.</span> <span style={{color: '#000', fontWeight: '600'}}>Unzip</span> the download file.</li>
        <li><span style={{color: '#000', fontWeight: '600'}}>3.</span> Copy the <span style={{color: '#000', fontWeight: '600'}}>extracted PHP file</span> to admin directory of your <span style={{color: '#000', fontWeight: '600'}}>Oscommerce server</span></li>
        <li><span style={{color: '#000', fontWeight: '600'}}>4.</span> Enter a <span style={{color: '#000', fontWeight: '600'}}>Nick Name</span> for this store.</li>
        <li><span style={{color: '#000', fontWeight: '600'}}>5.</span> Enter a <span style={{color: '#000', fontWeight: '600'}}>plugin URL</span> for your store</li>
        <li><span style={{color: '#000', fontWeight: '600'}}>6.</span> Enter the Admin <span style={{color: '#000', fontWeight: '600'}}>User Name</span> for Your store.</li>
        <li><span style={{color: '#000', fontWeight: '600'}}>7.</span> Enter the Admin <span style={{color: '#000', fontWeight: '600'}}>Password</span> for your store.</li>
        <li><span style={{color: '#000', fontWeight: '600'}}>8.</span> Click on the <span style={{color: '#000', fontWeight: '600'}}>“Connect Button”</span> to complete the setup.</li>
        
    </ol> 
            
            <div className= "opctstp oscomer">
             
            <div className= "row">
                <div className= "col-md-3 opn-la">Nickname</div>
                 <div className= "col-md-4"> 
                    <input type="text" name="Verificationn" className= "cominp-fild option-pass" placeholder="Enter your OsCommerce name" />
                 </div>
            </div>
            <div className= "row">
                <div className= "col-md-3 opn-la">Web Store URL</div>
                 <div className= "col-md-4"> 
                    <input type="text" name="Verificationn" className= "cominp-fild option-pass" placeholder="OsCommerce Store URL" />
                 </div>
            </div>
             <div className= "row">
                <div className= "col-md-3 opn-la">Username</div>
                 <div className= "col-md-4"> 
                    <input type="text" name="Verificationn" className= "cominp-fild option-pass" placeholder="Enter your username" />
                 </div>
            </div>
             <div className= "row">
                <div className= "col-md-3 opn-la">Password</div>
                 <div className= "col-md-4"> 
                    <input type="text" name="Verificationn" className= "cominp-fild option-pass" placeholder="Enter your password" />
                 </div>
            </div>
            
                
        </div>
    </div>
    <div className= "mod-footer"> 
        <button type="button" className= "close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">Close</span></button> 
        <a href="" className= "Connect-btn">Connect</a>
        
    </div>
  </div>     
</div>
{/*<!--cartlogoModal1-->*/}  
<div id="ubercart" className= "modal">
 
  {/*<!-- Modal content -->*/}
  <div className= "modal-contented wow animated fadeInRight" data-wow-delay=".9s">
    <div className= "modal-header">  
        <h5>Connecting Your Store</h5> 
        <button type="button" className= "crose" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
    </div>      
    <div className= "modal-middle">     
    <h6>Follow these steps to get connected: <img src={ubercart} alt="cart-logo" className="opencart-imgs" /></h6>
    <span className= "comma-text">UberCart integration is available for Shipping Station accounts that handle their own tax and invoice management.</span>
    <ol className= "oder-list oder-padd"> 
        <li><span style={{color: '#000', fontWeight: '600'}}>1.</span> Login to your account.
        <div className= "row">
                <div className= "col-md-3 opn-la">Username</div>
                 <div className= "col-md-4"> 
                    <input type="text" name="Verificationn" className= "cominp-fild option-pass" placeholder="Enter your username" />
                 </div>
            </div>
            <div className= "row"> 
                <div className= "col-md-3 opn-la">Password</div>
                 <div className= "col-md-4"> 
                    <input type="text" name="Verificationn" className= "cominp-fild option-pass" placeholder="OsCommerce Store URL" />
                 </div>
            </div>
        </li>
        <li><span style={{color: '#000', fontWeight: '600'}}>2. Follow these steps to activate Redirect URL:</span>
            <ul className= "react-ul">
                <li>In the Redirect URL section check <span style={{color: '#000', fontWeight: '600'}}>“Enable return after sale”</span></li>
                <li>For Return method, select <span style={{color: '#000', fontWeight: '600'}}>Header redirect</span></li>
                <li>Set the <span style={{color: '#000', fontWeight: '600'}}>Approved URL</span> to the URL provided in your UberCart admin  (Replace http://yourdomain.com with 
the actual URL to your domain)
                <div className= "row">
                    <div className= "col-md-3 opn-la">Approved URL</div>
                        <div className= "col-md-4"> 
                            <input type="text" name="Verificationn" className= "cominp-fild option-pass" placeholder="Enter approved URL" />
                        </div>
                </div>
                </li>
                <li>Click <span style={{color: '#000', fontWeight: '600'}}>Update</span> to save your settings</li>
                
            </ul>
        </li>
        <li><span style={{color: '#000', fontWeight: '600'}}>3.</span> In the <span style={{color: '#000', fontWeight: '600'}}>Secret Word</span> section enter the <span style={{color: '#000', fontWeight: '600'}}>INS Secret Word </span>(it must be the same value entered in your UberCart admin).</li>
        
        
    </ol> 
            
       
    </div>
    <div className= "mod-footer"> 
        <button type="button" className= "close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">Close</span></button> 
        <a href="" className= "Connect-btn">Connect</a>
        
    </div>
  </div>     
</div>
{/*<!--cartlogoModal1-->*/}  
<div id="wix_store" className= "modal">
 
  {/*<!-- Modal content -->*/}
  <div className= "modal-contented  wow animated pulse">
    <div className= "modal-header">  
        <h5>Connecting Your Store</h5>
        <button type="button" className= "crose" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
    </div>      
    <div className= "modal-middle">    
    <h6>Follow these steps to get connected: <img src={wixstore} alt="cart-logo" className="opencart-imgs" /></h6>
    <span className= "comma-text">UberCart integration is available only for 2Checkout accounts that handle their own tax and invoice management. Contact 2Checkout for additional information.</span>
    <ol className= "oder-list oder-padd">  
        <li><span style={{color: '#000', fontWeight: '600'}}>1.</span> Log into <span style={{color: '#000', fontWeight: '600'}}>Wix.</span></li>
        <li><span style={{color: '#000', fontWeight: '600'}}>2.</span> Click the <span style={{color: '#000', fontWeight: '600'}}>Manage Store tab.</span></li>
        <li><span style={{color: '#000', fontWeight: '600'}}>3.</span> On the left panel, click on Business Setup, then select Payments from the options.</li>
        <li><span style={{color: '#000', fontWeight: '600'}}>4.</span> Click the Connect button next to the Credit/Debit Cards option.</li>
        <li><span style={{color: '#000', fontWeight: '600'}}>5.</span> Click the Connect button next to the <span style={{color: '#000', fontWeight: '600'}}>Ship Station</span> payment option.</li>
        <li><span style={{color: '#000', fontWeight: '600'}}>6.</span> 5. Click <span style={{color: '#000', fontWeight: '600'}}>Update</span> to save your changes.</li>
         
    </ol> 
    </div>
    <div className= "mod-footer"> 
        <a href="#" style={{color: '#0099cc', float: 'left'}}>Help?</a>
        <button type="button" className= "close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">Close</span></button> 
        
        
    </div>
  </div>     
</div>
{/*<!--cartlogoModal1-->*/}  
<div id="prestashop" className="modal">  
 
  {/*<!-- Modal content -->*/}
  <div className="modal-contented wow animated swing">
    <div className="modal-header">  
        <h5>Connecting Your Store</h5>
        <button type="button" className="crose" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
    </div>      
    <div className="modal-middle">     
    <h6>Follow these steps to get connected: <img src={prestashop} alt="cart-logo" className="opencart-imgs" /></h6>
    <p>Your PrestaShop store can be integrated with Shipping Center in just minutes. To grant Shipping Center access to your PrestaShop web site, please follow the instructions below.</p>
    <ol className="oder-list oder-padd"> 
        <li><span style={{color: '#000', fontWeight: '600'}}>1.</span> In your PrestaShop Admin site, go to <span style={{color: '#000', fontWeight: '600'}}>Advanced Parameters > Webservice.</span></li>
        <li><span style={{color: '#000', fontWeight: '600'}}>2.</span> Click the <span style={{color: '#000', fontWeight: '600'}}>Add New</span> button to create a new key.</li>
        <li><span style={{color: '#000', fontWeight: '600'}}>3.</span> Click the <span style={{color: '#000', fontWeight: '600'}}>Generate!</span> button and then enter a description such as <span style={{color: '#000', fontWeight: '600'}}>Shipping Center Access Key.</span></li>
        <li><span style={{color: '#000', fontWeight: '600'}}>4.</span> Copy the <span style={{color: '#000', fontWeight: '600'}}>Key</span> value to the corresponding field below.</li>
        <li><span style={{color: '#000', fontWeight: '600'}}>5.</span> Select the checkbox to <span style={{color: '#000', fontWeight: '600'}}>View ALL</span> resources.</li>
        <li><span style={{color: '#000', fontWeight: '600'}}>6.</span> Select the checkbox to <span style={{color: '#000', fontWeight: '600'}}>Modify</span> the resources orders and order_carriers.</li>
        <li><span style={{color: '#000', fontWeight: '600'}}>7.</span> Click the Save button to save your changes.</li>
        <li><span style={{color: '#000', fontWeight: '600'}}>8.</span> Select the "Yes" option to <span style={{color: '#000', fontWeight: '600'}}>Enable PrestaShop's Webservice.</span></li>
        <li><span style={{color: '#000', fontWeight: '600'}}>9.</span> Ensure that URL Rewriting has been enabled on your server (mod_rewrite on Apache servers). Note, if Shipping CEenter has trouble authenticating with your server, you may need to add a .htaccess file to your <span style={{color: '#000', fontWeight: '600'}}>Webservices Directory.</span> This file should contain the following rule</li>
        <div className="mod-rew"> 
            <pre>

            </pre>
        </div>
        <li><span style={{color: '#000', fontWeight: '600'}}>10.</span> Enter the URL of your store below. Use HTTPS if possible. (e.g. https://www.mystore.com)</li>
    </ol> 
    </div>
    <div className="mod-footer"> 
        <a href="#" style={{color: '#0099cc', float: 'left'}}>Help?</a>
        <a href="" className="Connect-btn">Connect</a>
        <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">Close</span></button>
    </div>
  </div>     
</div>
{/*<!--Magento Modal1-->*/}  
<div id="magentoModal" className="modal">
 
  {/*<!-- Modal content -->*/}
  <div className="modal-contented  wow animated fadeInUp">
    <div className="modal-header">  
        <h5>Connecting Your Store</h5>
        <button type="button" className="crose" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
    </div>       
    <div className="modal-middle">    
    <h6>Follow these steps to get connected: <img src={Magento} alt="cart-logo" className="opencart-imgs" /></h6>
        <div className="messagefirst"> 
          <span className="optionlabel">Select Magento Version</span><span> 
            <select className="form-control template" value={this.state.magentoversion} onChange={event => this.setState({magentoversion: event.target.value})}> 
            <option value="">Select Version</option>
            <option value="version1">Version 1 </option>
            <option value="version2">Version 2 </option> 
            </select></span>    
        </div>
         
        <div className={this.state.magentoversion=='version1' ? 'magentoversionshowdiv' : 'magentoversionhidediv'}>
        <p>Your Magento store can be integrated with Shipping Center in just minutes. To grant Shipping Center access to your Magento Community, Professional, or Enterprise web site, please follow the instructions below.</p>
        <ol className="oder-list oder-padd"> 
            <li><span style={{color: '#000', fontWeight: '600'}}>1.</span> Go to the <span style={{color: '#000', fontWeight: '600'}}>Shipping Center Magento Extension</span> and install it from Magento Connect.</li>
            <li><span style={{color: '#000', fontWeight: '600'}}>2.</span> In your Magento Admin site, select <span style={{color: '#000', fontWeight: '600'}}>System > Web Services > SOAP/XML-RPC – Users.</span></li>
            <li><span style={{color: '#000', fontWeight: '600'}}>3.</span> Click the <span style={{color: '#000', fontWeight: '600'}}>Add New</span> button and create a new user for Shipping Center. Copy the values that you entered for User Name and <span style={{color: '#000', fontWeight: '600'}}>API Key</span> into the corresponding fields below.</li>
            <li><span style={{color: '#000', fontWeight: '600'}}>4.</span> Click the Save User button.</li>
            <li><span style={{color: '#000', fontWeight: '600'}}>5.</span> Enter the Store URL of your store below. Use HTTPS if possible. (e.g. https://www.mystore.com) The Store URL can be found in <span style={{color: '#000', fontWeight: '600'}}>System > Configuration > General – Web > Secure </span>(or Unsecure for http configuration) In a multi-store scenario, you will need to set the Current Configuration Scope value (near the top-left corner of the page) to the store you're reviewing before looking at the Store URL value.</li>
            <li><span style={{color: '#000', fontWeight: '600'}}>6.</span> Click <span style={{color: '#000', fontWeight: '600'}}>Test Connection</span> to see if the steps you followed worked correctly.</li>
            <li><span style={{color: '#000', fontWeight: '600'}}>7.</span> Click on <span style={{color: '#000', fontWeight: '600'}}>Finish</span> to make this store active in Shipping Center!</li>
        </ol> 
        <div className="opctstp oscomer">
            <div className="row">
                <div className="col-md-4 opn-la">Magento Username</div>
                 <div className="col-md-4"> 
                    <input type="text" name="Verificationn" className="cominp-fild option-pass" placeholder="Enter your OsCommerce name" />
                 </div>
            </div>
            <div className="row">
                <div className="col-md-4 opn-la">API Key</div>
                 <div className="col-md-4"> 
                    <input type="text" name="Verificationn" className="cominp-fild option-pass" placeholder="OsCommerce Store URL" />
                 </div>
            </div>
        </div>
        </div>
    <div className={this.state.magentoversion=='version2' ? 'magentoversionshowdiv' : 'magentoversionhidediv'}>
    <p>Your Magento store can be integrated with Shipping Center in just minutes. To grant Shipping Center access to your Magento 2 account, please follow the instructions below.</p>
    <ol className="oder-list oder-padd"> 
        <li><span style={{color: '#000', fontWeight: '600'}}>1.</span> Download this file and extract the contents to your <span style={{color: '#000', fontWeight: '600'}}>Magento Folder.</span></li>
        <li><span style={{color: '#000', fontWeight: '600'}}>2.</span> Login to the <span style={{color: '#000', fontWeight: '600'}}>Magento Admin</span> as your Administrator User.</li>
        <li><span style={{color: '#000', fontWeight: '600'}}>3.</span> Click System on the left, then <span style={{color: '#000', fontWeight: '600'}}>Cache Management</span> under Tools.</li>
        <li><span style={{color: '#000', fontWeight: '600'}}>4.</span> Select <span style={{color: '#000', fontWeight: '600'}}>Flush Magento Cache.</span></li>
        <li><span style={{color: '#000', fontWeight: '600'}}>5.</span> Click System, and choose <span style={{color: '#000', fontWeight: '600'}}>Web Setup Wizard</span> from the left navigation panel.</li>
        <li><span style={{color: '#000', fontWeight: '600'}}>6.</span> Select <span style={{color: '#000', fontWeight: '600'}}>Component Manager</span> and continue with Manage your components.</li>
        <li><span style={{color: '#000', fontWeight: '600'}}>7.</span> Go to the <span style={{color: '#000', fontWeight: '600'}}>Auctane_API module</span> from Listing and select the Enable option from the Actions column.</li>
        <li><span style={{color: '#000', fontWeight: '600'}}>8.</span> Click <span style={{color: '#000', fontWeight: '600'}}>Next.</span></li>
        <li><span style={{color: '#000', fontWeight: '600'}}>9.</span> We recommend making a backup of existing <span style={{color: '#000', fontWeight: '600'}}>Code, Media and Database</span> files.</li>
        <li><span style={{color: '#000', fontWeight: '600'}}>10.</span> Click <span style={{color: '#000', fontWeight: '600'}}>Enable.</span></li>
        <li><span style={{color: '#000', fontWeight: '600'}}>11.</span> In your Magento Admin site, go to the <span style={{color: '#000', fontWeight: '600'}}>System > All Users</span> to create a new admin user for <span style={{color: '#000', fontWeight: '600'}}>Shipping Center.</span> This step is optional, but recommended so that your new integration keeps working even if you change your primary admin password.</li>
        <li><span style={{color: '#000', fontWeight: '600'}}>12.</span> Click the <span style={{color: '#000', fontWeight: '600'}}>Save User</span> button.</li>
    </ol> 
    

    <div className="opctstp oscomer">
        <div className="row">
            <div className="col-md-4 opn-la">Magento Store URL</div>
             <div className="col-md-4"> 
                <input type="text" name="magentostoreurl" className="cominp-fild option-pass" value={this.state.magentostoreurl} onChange={event => this.setState({magentostoreurl: event.target.value})} placeholder="Enter Store URL" />
             </div>
        </div>
        <div className="row">
            <div className="col-md-4 opn-la">Magento Username</div>
             <div className="col-md-4"> 
                <input type="text" name="magentousername" className="cominp-fild option-pass" value={this.state.magentousername} onChange={event => this.setState({magentousername: event.target.value})} placeholder="Enter Admin Username" />
             </div>
        </div>
        <div className="row">
            <div className="col-md-4 opn-la">Magento Password</div>
             <div className="col-md-4"> 
                <input type="password" name="magentopassword" className="cominp-fild option-pass" value={this.state.magentopassword} onChange={event => this.setState({magentopassword: event.target.value})} placeholder="Enter Admin Password" />
             </div>
        </div>
        <div className="row">
            <div className="col-md-4 opn-la">Magento Store Name</div>
             <div className="col-md-4"> 
                <input type="text" name="storeName" className="cominp-fild option-pass" value={this.state.storeName} onChange={event => this.setState({storeName: event.target.value})} placeholder="Enter Your Store Name" />
             </div>
        </div>
    </div>
    </div>
    
    </div>
    <div className="mod-footer"> 
        <a href="#" style={{color: '#0099cc', float: 'left'}}>Help?</a>
        <a onClick={this.handleMagentoConnect} style={{cursor:'pointer'}} className="Connect-btn">Connect</a>
        <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">Close</span></button> 
        
        
    </div>
  </div>     
</div>
{/*<!--volusion Modal1-->*/}  
<div id="volusion" className="modal">
 
  {/*<!-- Modal content -->*/} 
  <div className="modal-contented  wow animated fadeInDown"> 
    <div className="modal-header">  
        <h5>Connecting Your Store</h5>
        <button type="button" className="crose" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
    </div>       
    <div className="modal-middle">    
    <h6>Follow these steps to get connected: <img src={volusion} alt="cart-logo" className="opencart-imgs" /></h6>
        <p>To grant <span style={{color: '#000', fontWeight: '600'}}>Shipping Center </span> access to your Ecwid marketplace account, please follow the instructions below.</p>
    <ol className="oder-list oder-padd"> 
        <li><span style={{color: '#000', fontWeight: '600'}}>1.</span> Log into your <span style={{color: '#000', fontWeight: '600'}}> Volusion administration </span> site.</li>
        <li><span style={{color: '#000', fontWeight: '600'}}>2.</span> Create a new administrator user specifically for <span style={{color: '#000', fontWeight: '600'}}> Shipping C enter’s </span> use by navigating to <span style={{color: '#000', fontWeight: '600'}}> Customers > Administrators.</span></li>
        <li><span style={{color: '#000', fontWeight: '600'}}>3.</span> Log into Volusion with the newly created <span style={{color: '#000', fontWeight: '600'}}> Shipping Center </span> user account.</li>
        <li><span style={{color: '#000', fontWeight: '600'}}>4.</span> Select Inventory and then <span style={{color: '#000', fontWeight: '600'}}> Import / Export </span> from the main menu.</li>
        <li><span style={{color: '#000', fontWeight: '600'}}>5.</span> Click the <span style={{color: '#000', fontWeight: '600'}}> Volusion API button.</span></li>
        <li><span style={{color: '#000', fontWeight: '600'}}>6.</span> Click the <span style={{color: '#000', fontWeight: '600'}}> Run button next </span> to Generic\\Orders.</li>
        <li><span style={{color: '#000', fontWeight: '600'}}>7.</span> Click the <span style={{color: '#000', fontWeight: '600'}}> Run button </span> again.</li>
        <li><span style={{color: '#000', fontWeight: '600'}}>8.</span> You should now see Download or use this URL. Copy that URL into the field below.</li>
        
    
        <div className="opctstp oscomer"> 
            <div className="row">
                <div className="col-md-4 opn-la">Volusion API URL</div>
                 <div className="col-md-4"> 
                    <input type="text" name="Verificationn" className="cominp-fild option-pass" placeholder="OsCommerce Store URL" />
                 </div>
            </div> 
        </div>     
    <h5 className="vol-text">Please note that this integration requires a Volusion <a href="#" style={{color: '#04a5dc'}}>"Pro"</a> or <a href="#" style={{color: '#04a5dc'}}>"Premium" plan</a></h5>
    </ol> 

    </div>
    <div className="mod-footer">  
        <a href="#" style={{color: '#0099cc', float: 'left'}}>Help?</a>
        <a href="" className="Connect-btn">Connect</a>
        <a href="#" className="con-btn">Test Connection</a> 
        <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">Close</span></button> 
        
        
    </div>
  </div>     
</div>
{/*<!--ecwid Modal1-->*/}  
<div id="ecwid" className="modal">
 
  {/*<!-- Modal content -->*/}
  <div className="modal-contented  wow animated fadeInLeft">
    <div className="modal-header">  
        <h5>Connecting Your Store</h5>
        <button type="button" className="crose" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
    </div>       
    <div className="modal-middle">    
    <h6>Follow these steps to get connected: <img src={ecwid} alt="cart-logo" className="opencart-imgs" /></h6>
        <p>To grant Shipping Center access to your Ecwid marketplace account, please follow the instructions below.</p>
    <ol className="oder-list oder-padd"> 
        <li><span style={{color: '#000', fontWeight: '600'}}>1.</span> Confirm that you have a <span style={{color: '#000', fontWeight: '600'}}>Venture plan</span> or higher with <span style={{color: '#000', fontWeight: '600'}}>Ecwid.</span></li>
        <li><span style={{color: '#000', fontWeight: '600'}}>2.</span> Click on the Connect button to be taken to Ecwid's site where you'll authorize <span style={{color: '#000', fontWeight: '600'}}>Shipping Center</span> to access and update        your order data.</li>
        <li><span style={{color: '#000', fontWeight: '600'}}>3.</span> Login to Ecwid using your Ecwid Username and Password.</li>
        <li><span style={{color: '#000', fontWeight: '600'}}>4.</span> When prompted, click Allow to grant <span style={{color: '#000', fontWeight: '600'}}>Shipping Center</span> access to your account. You will then be returned to <span style={{color: '#000', fontWeight: '600'}}>Shipping Center</span> to complete the setup process.</li>
    </ol> 

    </div>
    <div className="mod-footer">  
        <a href="#" style={{color: '#0099cc', float: 'left'}}>Help?</a>
        <a href="" className="Connect-btn">Connect</a> 
        <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">Close</span></button> 
        
        
    </div>
  </div>     
</div>
{/*<!--woocommerce Modal1-->*/}  
<div id="woocommerce" className="modal">
 
  {/*<!-- Modal content -->*/}
  <div className="modal-contented  wow animated fadeInRight">
    <div className="modal-header">  
        <h5>Connecting Your Store</h5>
        <button type="button" className="crose" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
    </div>       
    <div className="modal-middle">    
    <h6>Follow these steps to get connected: <img src={woocommerce} alt="cart-logo" className="opencart-imgs" /></h6>
        
    <ol className="oder-list oder-padd"> 
        <li><span style={{color: '#000', fontWeight: '600'}}>1.</span> Download the <span style={{color: '#000', fontWeight: '600'}}> ShipStation </span> Integration plug-in to your computer.</li>
        <li><span style={{color: '#000', fontWeight: '600'}}>2.</span> In your Wordpress Admin dashboard, navigate to <span style={{color: '#000', fontWeight: '600'}}> Plugins > Add New.</span> Here, click on the Upload link and select the plug-in zip file from your local computer and then click Install Now.</li>
        <li><span style={{color: '#000', fontWeight: '600'}}>3.</span> Activate the new extension by clicking the link on the installation page or through the PlugIns menu in WordPress.</li>
        <li><span style={{color: '#000', fontWeight: '600'}}>4.</span> Within your Wordpress Admin dashboard, navigate to the WooCommerce menu, then <span style={{color: '#000', fontWeight: '600'}}> Settings > Integration.</span></li>
        <li><span style={{color: '#000', fontWeight: '600'}}>5.</span> Copy the Authentication Key from WooCommerce and paste it below.</li>
                <div className="row">
                    <div className="col-md-3 opn-la">Authentication Key</div>
                        <div className="col-md-4"> 
                            <input type="text" name="Verificationn" className="cominp-fild option-pass" placeholder="Enter approved URL" />
                        </div>
                </div>
        <li><span style={{color: '#000', fontWeight: '600'}}>6.</span> Enter your WordPress site's domain (e.g., http://www.yoursite.com) into the field below.</li>
                <div className="row">
                    <div className="col-md-3 opn-la">Store URL</div>
                        <div className="col-md-4"> 
                            <input type="text" name="Verificationn" className="cominp-fild option-pass" placeholder="Enter approved URL" />
                        </div>
                    
                </div>
        <li><span style={{color: '#000', fontWeight: '600'}}>7.</span> Within WooCommerce, choose which order statuses to export to <span style={{color: '#000', fontWeight: '600'}}> ShipStation </span> and map these statuses by placing them in the relevant fields below. Be sure to separate multiple statuses with a comma.
                <div className="row">
                    <div className="col-md-3 opn-la">Awaiting Payment Statuses</div>
                        <div className="col-md-4"> 
                            <input type="text" name="Verificationn" className="cominp-fild option-pass" placeholder="Enter approved URL" />
                        </div>
                </div>
                <div className="row">
                    <div className="col-md-3 opn-la">Awaiting Payment Statuses</div>
                        <div className="col-md-4"> 
                            <input type="text" name="Verificationn" className="cominp-fild option-pass" placeholder="Enter approved URL" />
                        </div>
                </div>
        </li>
    </ol> 

    </div>
    <div className="mod-footer">  
        
         
        <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">Close</span></button> 
        
        
    </div>
  </div>     
</div>
{/*<!--commerce Modal1-->*/}  
<div id="commerce" className="modal">
 
  {/*<!-- Modal content -->*/}
  <div className="modal-contented  wow animated pulse">
    <div className="modal-header">  
        <h5>Connecting Your Store</h5>
        <button type="button" className="crose" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
    </div>       
    <div className="modal-middle">    
    <h6>Follow these steps to get connected: <img src={bigcommerce} alt="cart-logo" className="opencart-imgs" /></h6>
    <p>Your BigCommerce store can be integrated with Shipping Center in just minutes. To grant Shipping Center access to your BigCommerce web site, we recommend creating a new user in your BigCommerce admin system for Shipping Center. This user will need permissions to retrieve and modify orders.</p>    
    <ol className="oder-list oder-padd"> 
        <li><span style={{color: '#000', fontWeight: '600'}}>1.</span> Login to your <span style={{color: '#000', fontWeight: '600'}}>BigCommerce</span> admin <a href="#" style={{color: 'rgb(4, 165, 220)', fontWeight: '600'}}>here.</a></li>
        <li><span style={{color: '#000', fontWeight: '600'}}>2.</span> Navigate to the App Marketplace and search for the <span style={{color: '#000', fontWeight: '600'}}>Shipping Center.</span></li>
        <li><span style={{color: '#000', fontWeight: '600'}}>3.</span> Select the <span style={{color: '#000', fontWeight: '600'}}>Shipping Center,</span> click Install, and then Confirm.</li>
        <li><span style={{color: '#000', fontWeight: '600'}}>4.</span> Click Login to <span style={{color: '#000', fontWeight: '600'}}>Shipping Center,</span> and Connect on the next window to link your store.</li>
    
    </ol> 

    </div>
    <div className="mod-footer">  
    
        <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">Close</span></button> 
    
    </div>
  </div>     
</div>
{/*<!--bigcartel Modal1-->*/}  
<div id="bigcartel" className="modal">
 
  {/*<!-- Modal content -->*/}
  <div className="modal-contented  wow animated fadeInDown">
    <div className="modal-header">  
        <h5>Connecting Your Store</h5>
        <button type="button" className="crose" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
    </div>       
    <div className="modal-middle">    
    <h6>Follow these steps to get connected: <img src={bigcartel} alt="cart-logo" className="opencart-imgs" /></h6>
    <p><span style={{color: '#000', fontWeight: '600'}}>To grant ShipStation access to your Big Cartel marketplace account, please follow the instructions below.</span></p>    
    <ol className="oder-list oder-padd"> 
        <li><span style={{color: '#000', fontWeight: '600'}}>1.</span> Click on the "Finish" button to be taken to <span style={{color: '#000', fontWeight: '600'}}>Big Cartel's</span> site where you'll authorize <span style={{color: '#000', fontWeight: '600'}}>Shipping Center</span> to access and update your order data.</li>
        <li><span style={{color: '#000', fontWeight: '600'}}>2.</span> Login to <span style={{color: '#000', fontWeight: '600'}}>Big Cartel</span> using your Big Cartel Username and Password.</li>
        <li><span style={{color: '#000', fontWeight: '600'}}>3.</span> When prompted, click "<span style={{color: '#04a5dc', fontWeight: '600'}}> Allow </span>" to grant <span style={{color: '#000', fontWeight: '600'}}>Shipping Center</span> access to your account. You are then returned to <span style={{color: '#000', fontWeight: '600'}}>Shipping Center.</span></li>
        
    
    </ol> 

    </div>
    <div className="mod-footer">  
        <a href="" className="Connect-btn">Connect</a>
        <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">Close</span></button> 
    
    </div>
  </div>     
</div>
{/*<!--squarespace Modal1-->*/}  
<div id="squarespace" className="modal">
 
  {/*<!-- Modal content -->*/}
  <div className="modal-contented  wow animated fadeInLeft">
    <div className="modal-header">  
        <h5>Connecting Your Store</h5>
        <button type="button" className="crose" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
    </div>       
    <div className="modal-middle">    
    <h6>Follow these steps to get connected: <img src={squarespace} alt="cart-logo" className="opencart-imgs" /></h6>
       
    <ol className="oder-list oder-padd"> 
        <li><span style={{color: '#000', fontWeight: '600'}}>1.</span> Sign into your <span style={{color: '#000', fontWeight: '600'}}>Squarespace Admin <span style={{color: '#04a5dc', fontWeight: '600', fontStyle: 'italic'}}>here.</span></span></li>
        <li><span style={{color: '#000', fontWeight: '600'}}>2.</span> Enter the <span style={{color: '#000', fontWeight: '600'}}>Settings</span> section.</li>
        <li><span style={{color: '#000', fontWeight: '600'}}>3.</span> Click Shipping and find the section for <span style={{color: '#000', fontWeight: '600'}}>Shipping Center.</span></li>
        <li><span style={{color: '#000', fontWeight: '600'}}>4.</span> Click Set Up Connection.</li>
        <li><span style={{color: '#000', fontWeight: '600'}}>5.</span> Copy the generated <span style={{color: '#000', fontWeight: '600'}}>Site ID</span> and <span style={{color: '#000', fontWeight: '600'}}>Connection Key</span> from Squarespace and paste them into the fields below.</li>
        <div className="opctstp oscomer">
             
            <div className="row">
                <div className="col-md-4 opn-la">Site ID</div>
                 <div className="col-md-4"> 
                    <input type="text" name="Verificationn" className="cominp-fild option-pass" placeholder="Enter your OsCommerce name" />
                 </div>
            </div>
            <div className="row">
                <div className="col-md-4 opn-la">Connection Key</div>
                 <div className="col-md-4"> 
                    <input type="text" name="Verificationn" className="cominp-fild option-pass" placeholder="OsCommerce Store URL" />
                 </div>
            </div>
             
        </div>
        <p>For more detailed Squarespace connection instructions, including screenshots, check out our <span style={{color: '#04a5dc', fontWeight: '600', fontStyle: 'italic'}}>support article.</span></p>
        <span className="comma-text">To learn more about Squarespace commerce and/or set-up an account please <span style={{color: '#04a5dc', fontWeight: '600', fontStyle: 'italic'}}>click here.</span></span>
    </ol> 

    </div>
    <div className="mod-footer">  
        <a href="" className="Connect-btn">Connect</a>
        <a href="#" className="con-btn">Test Connection</a>
        <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">Close</span></button> 
    
    </div>
  </div>     
</div>
{/*<!--Weebly_Store Modal1-->*/}  
<div id="Weebly_Store" className="modal">
 
  {/*<!-- Modal content -->*/}
  <div className="modal-contented  wow animated fadeInRight">
    <div className="modal-header">  
        <h5>Connecting Your Store</h5>
        <button type="button" className="crose" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
    </div>       
    <div className="modal-middle">    
    <h6>Follow these steps to get connected: <img src={WeeblyStore} alt="cart-logo" className="opencart-imgs" /></h6>
       
    <ol className="oder-list oder-padd"> 
        <li><span style={{color: '#000', fontWeight: '600'}}>1.</span> Login to your <span style={{color: '#000', fontWeight: '600'}}>Weebly</span> account <a href="#" style={{color: '#04a5dc', fontWeight: '600'}}>here.</a></li>
        <li><span style={{color: '#000', fontWeight: '600'}}>2.</span> Navigate to the <a href="#" style={{color: '#04a5dc', fontWeight: '600'}}>App Center
        </a> and search for the <span style={{color: '#000', fontWeight: '600'}}>Shipping Center.</span></li>
        <li><span style={{color: '#000', fontWeight: '600'}}>3.</span> Select the <span style={{color: '#000', fontWeight: '600'}}>Shipping Center,</span> click Add, and then <a href="#" style={{color: '#04a5dc', fontWeight: '600'}}>Connect.</a></li>
        <li><span style={{color: '#000', fontWeight: '600'}}>4.</span> 4. After clicking Connect you will be returned to <span style={{color: '#000', fontWeight: '600'}}>Shipping Center</span> to complete the setup.</li>
        
    </ol> 

    </div>
    <div className="mod-footer">  
        
        <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">Close</span></button> 
    
    </div>
  </div>     
</div>

{/*<!--Etsy_Store Modal1-->*/}  
<div id="Etsy_Store" className="modal">
 
  {/*<!-- Modal content -->*/}
  <div className="modal-contented  wow animated fadeInUp">
    <div className="modal-header">  
        <h5>Connecting Your Store</h5>
        <button type="button" className="crose" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
    </div>       
    <div className="modal-middle">    
    <h6>Follow these steps to get connected: <img src={EtsyStore} alt="cart-logo" className="opencart-imgs" /></h6>
       
    <ol className="oder-list oder-padd"> 
        <li><span style={{color: '#000', fontWeight: '600'}}>1.</span> Enter your Etsy Username below and click <span style={{color: '#000', fontWeight: '600'}}>Find My Shops.</span> Need help finding your Etsy Username? <span style={{color: '#04a5dc', fontWeight: '600'}}>Click here.</span></li>
        <div className="opctstp oscomer">
        <div className="row">
            <div className="col-md-3 opn-la">Etsy Username</div>
             <div className="col-md-5"> 
                <input type="text" name="Verificationn" className="cominp-fild option-pass" />
                
             </div>
             <div className="col-md-4"> 
                <a href="#" className="find-shop">Find My Shop</a>
             </div> 
        </div> 
        </div>
        <li><span style={{color: '#000', fontWeight: '600'}}>2.</span> Select the shop you wish to connect.</li>
        <div className="opctstp oscomer"> 
            <div className="messagefirst"> 
              <span className="optionlabel">Available Shop</span><span> 
                <select className="form-control template"> 
                <option>Select OpenCart Version</option>
                    <option id="version2">version2</option> 
                    <option>version3</option>
                    <option>version4</option> 
                    <option>version5</option>
                </select></span>    
            </div>
        </div>
        <li><span style={{color: '#000', fontWeight: '600'}}>3.</span> Click <span style={{color: '#04a5dc', fontWeight: '600'}}>Connect</span> to launch <span style={{color: '#000', fontWeight: '600'}}>Etsy's website,</span> then authorize Shipping Center to retrieve your order information.</li>
        
        
    </ol> 

    </div>
    <div className="mod-footer">  
        <a href="" className="Connect-btn">Connect</a>
        <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">Close</span></button> 
    
    </div>
  </div>     
</div>
{/*<!--Shopify_Store Modal1-->*/}  
<div id="Shopify_Store" className="modal">
 
  {/*<!-- Modal content -->*/}
  <div className="modal-contented  wow animated flipInY">
    <div className="modal-header">  
        <h5>Connecting Your Store</h5>
        <button type="button" className="crose" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
    </div>       
    <div className="modal-middle">    
    <h6>Follow these steps to get connected: <img src={shopify} alt="cart-logo" className="opencart-imgs" /></h6>
       
    <ol className="oder-list oder-padd"> 
        <li><span style={{color: '#000', fontWeight: '600'}}>1.</span> Enter your <span style={{color: '#000', fontWeight: '600'}}>Shopify store's domain name</span> below. (Example: yourstore.myshopify.com)</li>
        <div className="opctstp oscomer">
        <div className="row">
            <div className="col-md-4 opn-la">Shopify Domain</div>
             <div className="col-md-5"> 
                <input type="text" name="Verificationn" className="cominp-fild option-pass" />
                
             </div>
             
        </div>
        </div>
        <li><span style={{color: '#000', fontWeight: '600'}}>2.</span> Click <span style={{color: '#04a5dc', fontWeight: '600'}}>Connect</span> to launch Shopify's website, then authorize <span style={{color: '#000', fontWeight: '600'}}>Shipping Center</span> to retrieve your order information.</li>
    
    </ol> 

    </div>
    <div className="mod-footer">  
        <a href="" className="Connect-btn">Connect</a>
        <a href="#" className="con-btn">Test Connection</a>
        <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">Close</span></button> 
    
    </div>
  </div>     
</div>
{/*<!--cartlogoModal1-->*/}  
<div id="ebayModal" className="modal">
  
  {/*<!-- Modal content -->*/} 
  <div className="modal-contented wow animated pulse">
    <div className="modal-header">  
        <h5>Connecting Your Store</h5>
        <button type="button" className="crose" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
    </div>      
    <div className="modal-middle">    
    <h6>Follow these steps to get connected: <img src={eBay} alt="cart-logo" className="opencart-imgs" /></h6>

    <div className="opctstp">         
        <div className="row">
            <div className="col-md-5 opn-la">Enter eBay Store Name.</div>
             <div className="col-md-6"> 
                <input type="text" name="ebaystorename" className="cominp-fild option-pass" placeholder="" value={this.state.ebaystorename} onChange={this.handleChange} required />
             </div>
        </div>
            
    <p>If you have Selling Manager Pro and want ShipStation to send costs to it, please check the box below. Otherwise, leave the box unchecked.</p>
    <div className="run" style={{position: 'relative'}}>   
            <label className="customcheck">
                Send costs to Selling Manager Pro
                <input type="checkbox" /><span className="checkmark"></span>
            </label>
        </div>
    <span className="comma-text">Click Connect to launch eBay's website, then authorize ShipStation to retrieve your order information.</span>

    </div>   
        
    </div>
    <div className="mod-footer"> 
        <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">Close</span></button> 
        <button type="button" disabled={!this.state.ebaystorename} onClick={this.handleEbayConnect} style={{cursor:'pointer'}} className="Connect-btn">Connect</button>
    </div>
  </div>     
</div>
{/*<!--amazonModal Modal1-->*/}  
<div id="amazonModal" className="modal">
 
  {/*<!-- Modal content -->*/}
  <div className="modal-contented wow animated fadeInDown" data-wow-delay=".9s">
    <div className="modal-header">  
        <h5>Connecting Your Store</h5>
        <button type="button" className="crose" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
    </div>      
    <div className="modal-middle">    
    <h6>Follow these steps to get connected: <img src={amazonLogo} alt="cart-logo" className="opencart-imgs" /></h6>
    
    <ol className="oder-list oder-padd"> 
        <li><span style={{color: '#000', fontWeight: '600'}}>1.</span> Go to http://developer.amazonservices.com, click Sign up for MWS, and log in with your Amazon Seller credentials.</li>
        <li><span style={{color: '#000', fontWeight: '600'}}>2. Select the "I want to give a developer access..." option, and enter:</span>
            <ul className="react-ul">
                <li><span style={{color: '#000', fontWeight: '600'}}>Developer Name:</span> ShippingCenter </li>
                <li><span style={{color: '#000', fontWeight: '600'}}>Developer Account No:</span>XXXX-XXXX-4486</li>
                
            </ul>
        </li>
        <li><span style={{color: '#000', fontWeight: '600'}}>3.</span>Follow the prompts, then copy the <span style={{color: '#000', fontWeight: '600'}}>Seller ID</span> and AWS Auth Token into the fields below, and click Verify <span style={{color: '#000', fontWeight: '600'}}>Seller ID</span>
            <div className="row">
                <div className="col-md-3 opn-la">Amazon Seller ID</div>
                 <div className="col-md-5"> 
                   <input type="text" name="id" className="cominp-fild option-pass" placeholder="Seller ID" value={this.state.id} onChange={this.handleChange} required />
                 </div>
                 <div className="col-md-4"> 
                   <button type="button" disabled={!this.state.id} className="find-shop" onClick={this.handleAmazonVerifySellerId}>Find My Shop</button>  
                 </div>
            </div>
            <div className="row"> 
                <div className="col-md-3 opn-la">AWS Auth Token</div>
                 <div className="col-md-5"> 
                   <input type="text" name="key" className="cominp-fild option-pass" placeholder="aws Key" value={this.state.key} onChange={this.handleChange} required />
                 </div>
            </div>
            <div className="row">
                <div className="col-md-3 opn-la">Store Name</div>
                 <div className="col-md-5"> 
                  <input type="text" name="storeName" className="cominp-fild option-pass" placeholder="Enter store name" value={this.state.storeName} onChange={this.handleChange} required />
                 </div>
            </div>
        </li>
        <li><span style={{color: '#000', fontWeight: '600'}}>4.</span>Choose an Amazon order source. Repeat this setup process to add multiple Amazon <span style={{color: '#000', fontWeight: '600'}}>order sources.</span>
            <div className="messagefirst"> 
                <span className="optionlabel">Amazon Order Source</span><span> 
                    <select className="form-control template"> 
                    <option>version</option>
                        <option id="version2">version2</option> 
                        <option>version3</option>
                        <option>version4</option> 
                        <option>version5</option>
                    </select>
                </span>    
            </div>
        </li>
        <li><span style={{color: '#000', fontWeight: '600'}}>5.</span> <span style={{color: '#000', fontWeight: '600'}}>Choose how ShipStation should identify your Amazon products:</span> either by SKU or by Amazon ASIN. <span style={{color: '#000', fontWeight: '600'}}>Most merchants pick SKU,</span> so go with that if you're not sure.
            <div className="messagefirst"> 
                <span className="optionlabel">Product Identifier</span><span> 
                    <select className="form-control template"> 
                    <option>version</option>
                        <option id="version2">version2</option> 
                        <option>version3</option>
                        <option>version4</option> 
                        <option>version5</option>
                    </select>
                </span>    
            </div>
        </li>
        <li><span style={{color: '#000', fontWeight: '600'}}>5.</span> For more detailed Amazon connection instructions, including screenshots, check out our <span style={{color: '#000', fontWeight: '600'}}>support article.</span></li>

        
    </ol> 
            
       
    </div>
    <div className="mod-footer"> 
        <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">Close</span></button> 
        <button type="button" disabled={this.state.disableAmazonConnectBtn} className="Connect-btn" onClick={this.handleAmazonConnect}>Connect</button>
        
    </div>
  </div>     
</div>

            </>
        );
    }
}

export default MyStore;
