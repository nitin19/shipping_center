import React from 'react';
import ReactDOM from 'react-dom';

import './profile.css';
import house from '../../images/house-2.png';

class Profile extends React.Component { 

    render() {

        return (

            <div className="content-wrap universelclass">
                <div className="col-lg-12">
                	<div className="profile-heading">
                		<h1>Your personal information</h1>
                	</div>
                    <div className="row profile-back">
                    	<div className="col-sm-4">
                    		<div className="profile-left">
                    			<img src={house} alt="" className="profile-img" />
                    			<ul className="profile-ul">
                    				<li>
                    					<h4>John Cena</h4>
                    				</li>
                    				<li>
                    					<h5>Address</h5>
                    					<p>New York City, America</p>
                    				</li>
                    				<li>
                    					<h5>Email</h5>
                    					<p>abc@gmail.com</p>
                    				</li>
                    			</ul>
                    		</div>
                    	</div>
                    	<div className="col-sm-8">
                    		<div className="profile-right">
                    			<div className="row mar-bot">
                    				<div className="col-sm-4">
                    					<input type="text" name="first_name" placeholder="First Name *" className="profileinput animated fadeInLeft" required="" value="" />
                    				</div>
                    				<div className="col-sm-4">
                    					<input type="text" name="middle_name" placeholder="Middle Name *" className="profileinput animated fadeInUp" required="" value="" />
                    				</div>
                    				<div className="col-sm-4">
                    					<input type="text" name="last_name" placeholder="Last Name *" className="profileinput animated fadeInRight" required="" value="" />
                    				</div>
                    			</div>
                    			<div className="row mar-bot">
                    				<div className="col-sm-12">
                    					<input type="email" name="email" placeholder="Email Address *" className="profileinput animated fadeInDown" required="" value="" />
                    				</div>
                    			</div>
                    			<div className="row mar-bot">
                    				<div className="col-sm-12">
                    					<input type="text" name="phone_number" placeholder="Phone Number *" className="profileinput animated fadeInUp" required="" value="" />
                    				</div>
                    			</div>
                    			<div className="row mar-bot">
                    				<div className="col-sm-12">
                    					<input type="text" name="phone_number" placeholder="Address *" className="profileinput animated fadeInUp" required="" value="" />
                    				</div>
                    			</div>
                    			<div className="row mar-bot">
                    				<div className="col-sm-6">
                    					<input type="text" name="first_name" placeholder="State *" className="profileinput animated fadeInLeft" required="" value="" />
                    				</div>
                    				<div className="col-sm-6">
                    					<input type="text" name="middle_name" placeholder="City *" className="profileinput animated fadeInUp" required="" value="" />
                    				</div>
                    			</div>
                    			<div className="row mar-bot">
                    				<div className="col-sm-6">
                    					<input type="password" name="first_name" placeholder="Password *" className="profileinput animated fadeInLeft" required="" value="" />
                    				</div>
                    				<div className="col-sm-6">
                    					<input type="password" name="middle_name" placeholder="Confirm Password *" className="profileinput animated fadeInUp" required="" value="" />
                    				</div>
                    			</div>
                    			<div className="row mar-bot">
                    				<div className="col-sm-12">
		                    			<form action="#">
		                    			  <h5>Gender</h5>
										  <p>
										    <input type="radio" id="test1" name="radio-group" checked />
										    <label for="test1">Male</label>
										  </p>
										  <p>
										    <input type="radio" id="test2" name="radio-group" />
										    <label for="test2">Female</label>
										  </p>
										  
										</form>
									</div>
								</div>
                    			<div className="row mar-bot">
                    				<div className="col-sm-12">
                    					<div className="profilebtn">
                    						<button type="submit" className=" animated fadeInDown sub-btnn" value="Login">Update</button>
                    					</div>
                    				</div>
                    			</div>
                    		</div>
                    	</div>
                    </div>
                </div>  
                <footer className="add_list_footer">   
                    <div className="row">  
                    <div className="col-sm-12">        
                    <p>© 2018 <a href="#" style={{color: '#2db4e3', textDecoration: 'none'}}>VIP PARCEL</a></p>    
                    </div>  
                    </div>
                </footer>         
            </div>


        );
    }
}

export default Profile;
