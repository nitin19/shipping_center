import React from 'react';
import ReactDOM from 'react-dom';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import "react-tabs/style/react-tabs.css";

import './emailtemplates.css';

class EmailTemplates extends React.Component {

	render() {

		return (

			<div className="content-wrap universelclass">
        <div className="col-lg-12">
        <Tabs>
    <TabList>
      <Tab>Shipment Notification</Tab>
      <Tab>Delivery Notification</Tab>
    </TabList>

    <TabPanel>
      
            <div className="sorting-list">
                <h1>Email Templates</h1>
                <p>With our easy to customize email templates, you can make sure your customers get the info they need when their orders are shipped out.</p>
            </div>
            {/*<div className="automation-para">
              <p>Automation rules help you perform tasks for every order you import. You can manage your rules below or create a new one.</p>
            </div>*/}

            <div className="custom_tabel wow bounceIn animated" data-wow-duration="4s">
            <a href="" className="viewallbtn"><i className="fas fa-plus"></i>  New Shipment Email Template</a>
 
        <table className="table ">
  <thead>
    <tr className="bg ">
      <th className="collumn4">Name</th>
      <th className="collumn4">Created</th>
      <th className="collumn4">Last Modified</th>
      <th className="collumn4">Last Modified By</th>
      <th className="collumn4">Action</th>
    </tr>
  </thead>
  <tbody>
    <tr className="wow animate fadeInLeft " data-wow-delay="0.4s">
      <td className="collumn4">Default Shipment Template</td>
      <td className="collumn4">12/10/2018</td>
      <td className="collumn4">12/10/2018</td>
      <td className="collumn4">ShipStation</td>
      <td className="collumn4"><span className="edit_icon "><a href="# " className="namehigh"><i className="far fa-copy"></i> Copy</a></span></td>
    </tr>
    <tr className="wow animate fadeInLeft" data-wow-delay="0.8s"> 
      <td className="collumn4">Default Shipment Template</td>
      <td className="collumn4">12/10/2018</td>
      <td className="collumn4">12/10/2018</td>
      <td className="collumn4">ShipStation</td>  
      <td className="collumn4"><span className="edit_icon "><a href="#" className="namehigh"><i className="far fa-copy"></i> Copy</a></span></td>
    </tr>
    <tr className="wow animate fadeInLeft " data-wow-delay="1.4s">  
      <td className="collumn4">Default Shipment Template</td>
      <td className="collumn4">12/10/2018</td>
      <td className="collumn4">12/10/2018</td>
      <td className="collumn4">ShipStation</td>
      <td className="collumn4"><span className="edit_icon "><a href="# " className="namehigh"><i className="far fa-copy"></i> Copy</a></span></td>
    </tr>
   <tr className="wow animate fadeInLeft " data-wow-delay="1.8s">
      <td className="collumn4">Default Shipment Template</td>
      <td className="collumn4">12/10/2018</td>
      <td className="collumn4">12/10/2018</td>
      <td className="collumn4">ShipStation</td>
      <td><span className="edit_icon "><a href="# " className="namehigh"><i className="far fa-copy"></i> Copy</a></span></td>
    </tr>
    <tr className="wow animate fadeInLeft " data-wow-delay="2s"> 
          
      <td className="collumn4">Default Shipment Template</td>
      <td className="collumn4">12/10/2018</td>
      <td className="collumn4">12/10/2018</td>
      <td className="collumn4">ShipStation</td>
      <td className="collumn4"><span className="edit_icon "><a href="# " className="namehigh"><i className="far fa-copy"></i> Copy</a></span></td>
    </tr>
    <tr className="wow animate fadeInLeft " data-wow-delay="2s">
      <td className="collumn4">Default Shipment Template</td>
      <td className="collumn4">12/10/2018</td>
      <td className="collumn4">12/10/2018</td>
      <td className="collumn4">ShipStation</td>
      <td className="collumn4"><span className="edit_icon "><a href="# " className="namehigh"><i className="far fa-copy"></i> Copy</a></span></td>
    </tr>
    
    <tr className="wow animate fadeInLeft " data-wow-delay="2s">  
      <td className="collumn4">Default Shipment Template</td>
      <td className="collumn4">12/10/2018</td>
      <td className="collumn4">12/10/2018</td>
      <td className="collumn4">ShipStation</td>
      <td className="collumn4"><span className="edit_icon "><a href="# " className="namehigh"><i className="far fa-copy"></i> Copy</a></span></td>
    </tr>
  </tbody>
</table>

       </div>
</TabPanel>
    <TabPanel>
      <div className="sorting-list">
                <h1>Email Templates</h1>
                <p>With our easy to customize email templates, you can make sure your customers get the info they need when their orders are shipped out.</p>
            </div>
            {/*<div className="automation-para">
              <p>Automation rules help you perform tasks for every order you import. You can manage your rules below or create a new one.</p>
            </div>*/}

            <div className="custom_tabel wow bounceIn animated" data-wow-duration="4s">
            <a href="" className="viewallbtn"><i className="fas fa-plus"></i> New Delivery Email Template</a>
 
        <table className="table ">
  <thead>
    <tr className="bg ">
      <th className="collumn4">Name</th>
      <th className="collumn4">Created</th>
      <th className="collumn4">Last Modified</th>
      <th className="collumn4">Last Modified By</th>
      <th className="collumn4">Action</th>
    </tr>
  </thead>
  <tbody>
    <tr className="wow animate fadeInLeft " data-wow-delay="0.4s">
      <td className="collumn4">Default Delivery Template</td>
      <td className="collumn4">12/10/2018</td>
      <td className="collumn4">12/10/2018</td>
      <td className="collumn4">ShipStation</td>
      <td className="collumn4"><span className="edit_icon "><a href="# " className="namehigh"><i className="far fa-copy"></i> Copy</a></span></td>
    </tr>
    <tr className="wow animate fadeInLeft" data-wow-delay="0.8s">
      <td className="collumn4">Default Delivery Template</td>
      <td className="collumn4">12/10/2018</td>
      <td className="collumn4">12/10/2018</td>
      <td className="collumn4">ShipStation</td>
      <td className="collumn4"><span className="edit_icon "><a href="#" className="namehigh"><i className="far fa-copy"></i> Copy</a></span></td>
    </tr>
    <tr className="wow animate fadeInLeft " data-wow-delay="1.4s">
      <td className="collumn4">Default Delivery Template</td>
      <td className="collumn4">12/10/2018</td>
      <td className="collumn4">12/10/2018</td>
      <td className="collumn4">ShipStation</td>
      <td className="collumn4"><span className="edit_icon "><a href="# " className="namehigh"><i className="far fa-copy"></i> Copy</a></span></td>
    </tr>
   <tr className="wow animate fadeInLeft " data-wow-delay="1.8s"> 
      <td className="collumn4">Default Delivery Template</td>
      <td className="collumn4">12/10/2018</td>
      <td className="collumn4">12/10/2018</td>
      <td className="collumn4">ShipStation</td>
      <td className="collumn4"><span className="edit_icon "><a href="# " className="namehigh"><i className="far fa-copy"></i> Copy</a></span></td>
    </tr>
    <tr className="wow animate fadeInLeft " data-wow-delay="2s">    
      <td className="collumn4">Default Delivery Template</td>
      <td className="collumn4">12/10/2018</td>
      <td className="collumn4">12/10/2018</td>
      <td className="collumn4">ShipStation</td>
      <td className="collumn4"><span className="edit_icon "><a href="# " className="namehigh"><i className="far fa-copy"></i> Copy</a></span></td>
    </tr>
    <tr className="wow animate fadeInLeft " data-wow-delay="2s">        
      <td className="collumn4">Default Delivery Template</td>
      <td className="collumn4">12/10/2018</td>
      <td className="collumn4">12/10/2018</td>
      <td className="collumn4">ShipStation</td>
      <td className="collumn4"><span className="edit_icon "><a href="# " className="namehigh"><i className="far fa-copy"></i> Copy</a></span></td>
    </tr>
    
    <tr className="wow animate fadeInLeft " data-wow-delay="2s">     
      <td className="collumn4">Default Delivery Template</td>
      <td className="collumn4">12/10/2018</td>
      <td className="collumn4">12/10/2018</td>
      <td className="collumn4">ShipStation</td>
      <td className="collumn4"><span className="edit_icon "><a href="# " className="namehigh"><i className="far fa-copy"></i> Copy</a></span></td>
    </tr>
  </tbody>
</table>

       </div>
    </TabPanel>
  </Tabs>
       </div>
            <footer className="add_list_footer">
              <div className="row">
                  <div className="col-sm-12">
                      <p>© 2019 <a href="#" style={{color: 'rgb(7, 168, 222)'}}>VIP PARCEL</a></p>
                  </div>
                  
              </div>
            </footer>
      </div>

			);

	}

}
export default EmailTemplates;