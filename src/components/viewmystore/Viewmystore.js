import React from 'react';
import { bindActionCreators } from 'redux';
import { Redirect } from 'react-router-dom';
import './viewmystore.css';
import ebay from '../../images/ebay.png';
import plush from '../../images/plush.png';
import amazon from '../../images/amazon-listing.png';
import cart32 from '../../images/cart32.png';
import usps from '../../images/usps-listing.png';
import seocart from '../../images/seocart.png';
import shiprobot from '../../images/logo-shiprobot.png';
import insure from '../../images/insure-listing.png';
import { LOGIN_PAGE_PATH, API_TOKEN_NAME, USER_ROLE, USER_ID} from '../../constants';
import { scAxios } from '../..';
import { startUserSession } from '../userSession';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import loadingimage from '../../images/small_loader.gif';
import ReactTooltip from 'react-tooltip';
import Skeleton from 'react-loading-skeleton';
import {Animated} from "react-animated-css";
import WOW from "wowjs";

const getStores = (data) => {

    if(localStorage.getItem(USER_ROLE) !== "OP"){
        var url = '/stores/myStores';
    } else {
        var url = '/processor/processorStores';
    }
    return new Promise((resolve, reject) => {
        const req = scAxios.request(url, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const deletestore = (url) => {

    return new Promise((resolve, reject) => {

        const req = scAxios.request(url, {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },

        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const syncstore = (url,data) => {

    return new Promise((resolve, reject) => {

        const req = scAxios.request(url, {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
              ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}


class Viewmystore extends React.Component {
  state = {
        stores: [],
        searchdata: '',
        total: '',
        currentPage: '',
        LastPage:'',
        PerPage: '',
        FirstPageUrl:'',
        NextPageUrl:'',
        PrevPageUrl:'',
        LastPageUrl:'',
        TotalPages:'',
        loading: false,
        loadingFirst: true,
    }

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  } 

  refreshUserStoreList = (page) => {
        const data = {
            searchdata: this.state.searchdata,
            page: page,
            userid: localStorage.getItem(USER_ID)
        }
        getStores(data)
            .then(res => {
                this.setState({ loadingFirst: false });
                if(res.status==1){
                var records = res.success.data;
                this.setState({ 
                  stores: records,
                  total: res.success.total, 
                  currentPage: res.success.current_page,
                  PerPage: res.success.per_page,
                  FirstPageUrl: res.success.first_page_url,
                  NextPageUrl: res.success.next_page_url,
                  PrevPageUrl: res.success.prev_page_url,
                  LastPageUrl: res.success.last_page_url,
                  LastPage: res.success.last_page,
                  TotalPages: Math.ceil(this.state.total / this.state.PerPage)
                });
              } else {
                this.setState({ stores: '' });
              }
            })
            .catch(err => {
                console.log(err);
            });
    }

  delete(storeslug){
      var url = '/stores/deletestore/'+storeslug;
      deletestore(url)
        .then(res => {
          if(res.status=='Success'){
            toast.success(res.message, {
              position: toast.POSITION.BOTTOM_RIGHT
            });
          } else if(res.status=='Warning'){
            toast.warn(res.message, {
              position: toast.POSITION.BOTTOM_RIGHT
            });
            
          } else {
            toast.error(res.message, {
              position: toast.POSITION.BOTTOM_RIGHT
            });
          }
          this.refreshUserStoreList();
        })
        .catch(err => {
            console.log(err);
        });
  }

  sync = (storeslug) => {
    const data = { userid: localStorage.getItem(USER_ID) }
     this.setState({
        loading: true
    });
      var url = '/stores/syncStores/'+storeslug;
      syncstore(url,data)
        .then(res => {
          if(res.status=='1'){
            toast.success(res.orders+' Orders, '+res.orderItems+' Items '+res.message, {
              position: toast.POSITION.BOTTOM_RIGHT
            });
            
          } else {
            toast.error(res.message, {
              position: toast.POSITION.BOTTOM_RIGHT
            });
          }
          this.setState({
            loading: false
          });
          setTimeout(function(){
            window.location.href = '/singlestoreorder/'+storeslug;
          }, 2000);
        })
        .catch(err => {
            console.log(err);
        });
  }


  componentDidMount() {
    this.refreshUserStoreList();
  }

render() {
const wow = new WOW.WOW();
wow.init();
const currentPage = this.state.currentPage;
const previousPage = currentPage - 1;
const NextPage = currentPage + 1;
const LastPage = this.state.LastPage;
const pageNumbers = [];
    for (let i = 1; i <= this.state.TotalPages; i++) {
      pageNumbers.push(i);
    }
let srno = 1;

        return (
          
           <>     
            {this.state.loadingFirst==true ?
                <div className="content-wrap backcollor">
                    <div className="ms-stor">
                        <div className="row mstop">
                            <div className="col-sm-4">
                                {this.props.title || <Skeleton count={1} duration={2} height={30} />}
                            </div>
                            <div className="col-sm-4">
                                
                            </div>
                            <div className="col-sm-4">
                                {this.props.title || <Skeleton count={1} duration={2} height={30} />}
                            </div>
                        </div>
                        <div className="button2">
                            
                            <h6>{this.props.title || <Skeleton />}</h6>
                            {this.props.body || <Skeleton count={5} duration={2}  circle={true} color="red" height={50} highlightColor="red" />}
                        </div>
                    </div>
                </div>
                : 

    <div className="content-wrap universelclass">
                
        <div className="col-lg-12">
            <div className="sorting-list viewstoree">
                <h1 className="wow animated fadeInLeft" data-wow-duration="2s" data-wow-delay="1s">Store Listing</h1>
                <div className="view-serch">
                  <form className="form_section wow animated fadeInLeft" data-wow-duration="2s" data-wow-delay="1s">
                    <input name="searchdata" placeholder="Search..." id="search_keyword" value="" type="text" value={this.state.searchdata} onChange={this.handleChange}/>
                    <input className="Connect-btn" id="submit_btn" value="Search" type="button" onClick={this.refreshUserStoreList}/>
                  </form>
                </div>
            </div>
            <div className="custom_tabel">
      {this.state.loading==true ?     
       <div className="loader">
       <div className="loaderdiv">
       <h2 className="loadingtext">Please Wait, We are fetching new records</h2>
       <img src={loadingimage} style={{width:'100px'}}/></div>
       </div>
       :
        <table className="table ">
          <thead>
            <tr className="bg wow animated fadeInLeft" data-wow-duration="2s" data-wow-delay="1s">
              <th className="collumn3">Sr. No.</th>
              <th className="collumn3">Store Image</th>
              <th className="collumn3">Store Name</th>
              <th className="collumn2">Orders</th>
              <th className="collumn3">Status</th>
              <th className="collumn3">Delete</th>
              <th className="collumn3">View </th>
            </tr>
          </thead>
  <tbody>
  {
    this.state.stores.length > 0
    ? 
    this.state.stores.map(store => {
    return (<tr key={store.slug} className="wow animated fadeInLeft" data-wow-duration="2s" data-wow-delay="1s">
      <td className="view-stor collumn3"><span className="customfloatvip">{srno++}</span></td>
      <td className="view-stor collumn3"><span className="customfloatvip">
      <label><img src={store.system.logo_image} alt="#" /></label></span></td>
      <td className="view-stor collumn3"><span className="customfloatvip"> <a href={`./singlestoreorder/${store.slug}`}>{store.store_name}</a></span></td>
      <td className="view-stor collumn2"><span className="customfloatvip"> {store.orders ? store.orders.length : ''}</span></td>
      <td className="view-stor collumn3"><span className="customfloatvip"> <span className={store.status == '0' ? 'red status' : 'status' }></span></span></td>
      <td className="view-stor collumn3"><span className="edit_icon customfloatvip" data-tip="Delete">
      <a style={{cursor:'pointer'}} onClick={(e) => { if (window.confirm('Are you sure you wish to delete this store ?')) this.delete(store.slug)} } ><i className="far fa-trash-alt "></i></a>
      </span>
      <ReactTooltip />
      </td>
      <td className="view-stor collumn3"><span className="edit_icon customfloatvip" data-tip="View"><a style={{cursor:'pointer'}} onClick={this.sync.bind(this, store.slug)}><i className="fas fa-eye "></i></a></span><ReactTooltip /></td>
    </tr>);
  })
  :
  <tr className="norecord">
       <td>No Records founds!</td>
  </tr>
}
 </tbody>
</table>
}

           { pageNumbers.length > 1 ?

<div className="paggination-text">
  <ul className="pagination pagination-sm">
    {this.state.PrevPageUrl != null ? 
  <li className="page-item"><a className="page-link" style={{cursor:'pointer'}} onClick={this.refreshUserStoreList.bind(this, previousPage)}>Previous</a></li>
  : ''
}
      {pageNumbers.map(page => {
      return (<li className={currentPage == page ? 'activelink page-item' : 'page-item' } ><a className="page-link" style={{cursor:'pointer'}} onClick={this.refreshUserStoreList.bind(this, page)}>{page}</a></li>);
    })}

   {this.state.NextPageUrl != null ? 
  <li className="page-item"><a className="page-link" style={{cursor:'pointer'}} onClick={this.refreshUserStoreList.bind(this, NextPage)}>Next</a></li>
  : ''
}
    {this.state.LastPage != null && currentPage!=LastPage? 
  <li className="page-item"><a className="page-link" style={{cursor:'pointer'}} onClick={this.refreshUserStoreList.bind(this, LastPage)} >Last</a></li>
  : ''
}
  
  </ul>
</div> : ''
}

       </div>

       </div>
            <footer className="add_list_footer">
              <div className="row">
                  <div className="col-sm-12">
                      <p>© 2019 <a href="#" style={{color: 'rgb(7, 168, 222)'}}>VIP PARCEL</a></p>
                  </div>
                  
              </div>
            </footer>
            <ToastContainer autoClose={5000} />
      </div>
            
           }
        </>

        );
    }

}

export default Viewmystore;      