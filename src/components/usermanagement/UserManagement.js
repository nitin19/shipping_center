import React from 'react';
import ReactDOM from 'react-dom';
import Skeleton from 'react-loading-skeleton';

import './usermanagement.css';


class UserManagement extends React.Component {

	render(){

		return(
  
			<div className="content-wrap universelclass">
      
      <div className="content-scale">
              <h1>{this.props.title || <Skeleton count={1} duration={2} />}</h1>
              <p>{this.props.title || <Skeleton count={1} duration={2} />}</p>
              <p>{this.props.title || <Skeleton count={1} duration={2} />}</p>
              <div className="button2">
                <p>{this.props.title || <Skeleton count={1} duration={2} />}</p>
                <p>{this.props.title || <Skeleton count={1} duration={2} />}</p>
                <h6>{this.props.title || <Skeleton />}</h6>
              {this.props.body || <Skeleton count={5} duration={2}  circle={true} color="red" height={40} highlightColor="red" />}
              </div>
              {/*{this.props.body || <Skeleton count={1} duration={2} circle={true} />}*/}
            </div>

        <div className="col-lg-12">
            <div className="sorting-list">
                <h1>User Management</h1>
                <p>As users come and go from your organization, it's important to maintain historical references on their activity. Old user accounts are never fully deleted; instead they are deactivated and prevented from accessing the system.</p>
            </div>
            <div className="alert">
              <span className="closebtn">&times;</span>  
              <strong>1.</strong> active user(s) are being used. Your current subscription supports unlimited user(s).
            </div>
            <div className="custom_tabel wow bounceIn animated" data-wow-duration="4s">
            <div className="row wow animate fadeInLeft " data-wow-delay="0.4s">
              <div className="col-sm-12">
                <div className="run1" style={{position: 'relative', display: 'inline-block'}}>
                	<label className="customcheck">Warn that inventory will not be adjusted
                	<input type="checkbox" />
                	<span className="checkmark"></span>
                	</label>
                </div>
                <a href="" className="viewallbtn"><i className="fas fa-plus"></i> Add a User Account </a>
              </div>
            </div>
              
              <table className="table ">
                <thead>
                    <tr className="bg ">
                      <th className="collumn5">Username</th>
                      <th className="collumn3">Name</th>
                      <th className="collumn5">E-mail</th>
                      <th className="collumn3">Last Login</th>
                      <th className="collumn2">Status</th>
                      <th className="collumn2">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr className="wow animate fadeInLeft " data-wow-delay="0.4s">
                      <td className="collumn5">binarydata.marketing@gmail.com...</td>
                      <td className="collumn3">Darrin Lewis</td>
                      <td className="collumn5">binarydata.marketing@gmail.com	</td>
                      <td className="collumn3">02/01/2017</td>
                      <td className="collumn2"><span className="status"></span></td>
                      <td className="collumn2"><span className="edit_icon "><a href="# " className="namehigh"><i className="fas fa-pencil-alt "></i></a></span></td>
                    </tr>
                    <tr className="wow animate fadeInLeft" data-wow-delay="0.8s"> 
                    	<td className="collumn5">binarydata.marketing@gmail.com...</td>
                      <td className="collumn3">Darrin Lewis</td>
                      <td className="collumn5">binarydata.marketing@gmail.com	</td>
                      <td className="collumn3">02/01/2017</td>
                      <td className="collumn2"><span className="status"></span></td>
                      <td className="collumn2"><span className="edit_icon "><a href="# " className="namehigh"><i className="fas fa-pencil-alt "></i></a></span></td>
                    </tr>
                    <tr className="wow animate fadeInLeft " data-wow-delay="1.4s">
                      <td className="collumn5">binarydata.marketing@gmail.com...</td>
                      <td className="collumn3">Darrin Lewis</td>
                      <td className="collumn5">binarydata.marketing@gmail.com	</td>
                      <td className="collumn3">02/01/2017</td>
                      <td className="collumn2"><span className="status"></span></td>
                      <td className="collumn2"><span className="edit_icon "><a href="# " className="namehigh"><i className="fas fa-pencil-alt "></i></a></span></td>
                    </tr>
                   <tr className="wow animate fadeInLeft " data-wow-delay="1.8s">
                      <td className="collumn5">binarydata.marketing@gmail.com...</td>
                      <td className="collumn3">Darrin Lewis</td>
                      <td className="collumn5">binarydata.marketing@gmail.com	</td>
                      <td className="collumn3">02/01/2017</td>
                      <td className="collumn2"><span className="status"></span></td>
                      <td className="collumn2"><span className="edit_icon "><a href="# " className="namehigh"><i className="fas fa-pencil-alt "></i></a></span></td>
                    </tr>
                    <tr className="wow animate fadeInLeft " data-wow-delay="2s">   
                      <td className="collumn5">binarydata.marketing@gmail.com...</td>
                      <td className="collumn3">Darrin Lewis</td>
                      <td className="collumn5">binarydata.marketing@gmail.com	</td>
                      <td className="collumn3">02/01/2017</td>
                      <td className="collumn2"><span className="status orange"></span></td>
                      <td className="collumn2"><span className="edit_icon "><a href="# " className="namehigh"><i className="fas fa-pencil-alt "></i></a></span></td>
                    </tr>
                    <tr className="wow animate fadeInLeft " data-wow-delay="2s">
                      <td className="collumn5">binarydata.marketing@gmail.com...</td> 
                      <td className="collumn3">Darrin Lewis</td>
                      <td className="collumn5">binarydata.marketing@gmail.com	</td>
                      <td className="collumn3">02/01/2017</td>
                      <td className="collumn2"><span className="status red"></span></td>
                      <td className="collumn2"><span className="edit_icon "><a href="# " className="namehigh"><i className="fas fa-pencil-alt "></i></a></span></td>
                    </tr>
                    <tr className="wow animate fadeInLeft " data-wow-delay="2s"> 
                      <td className="collumn5">binarydata.marketing@gmail.com...</td>    
                      <td className="collumn3">Darrin Lewis</td>
                      <td className="collumn5">binarydata.marketing@gmail.com	</td>
                      <td className="collumn3">02/01/2017</td>
                      <td className="collumn2"><span className="status"></span></td>
                      <td className="collumn2"><span className="edit_icon "><a href="# " className="namehigh"><i className="fas fa-pencil-alt "></i></a></span></td>
                    </tr>
                </tbody>
              </table>

          </div>
        </div>
        <footer className="add_list_footer">   
          <div className="row"> 
            <div className="col-sm-12">          
              <p>© 2019 <a href="#" style={{color: '#07a8de'}}>VIP PARCEL</a></p>    
            </div> 
          </div>
        </footer>
      </div>

			);

	}

}
export default UserManagement;