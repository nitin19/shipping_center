import React from 'react';
import './notifications.css';
import noticlose from '../../images/noticlose.png';
import timer from '../../images/timer.png';
import { scAxios } from '../..';
import { API_TOKEN_NAME, USER_ROLE, USER_ID } from '../../constants';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const fetchNotifyInfo = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/notification/notify', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            },
        });


        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const deleteNotifyInfo = (notify_id) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/notification/deletenotify/'+notify_id, {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

class Notifications extends React.Component {

    state = {
        notifications:[],
        del_id : ''
    }

    componentDidMount() {
        const data = {
            userid: localStorage.getItem(USER_ID)
        }
        fetchNotifyInfo(data)
            .then(res => {
               this.setState({ countnotify: res.total_notification , notifications:res.records.data});
               })
            .catch(err => {
               console.log('Error receiving notification info.', err);
            });
    }

    deletenoti(notify_id){
         deleteNotifyInfo(notify_id)
        .then(res => {
                if(res.status=='1'){
                    toast.success(res.message, {
                      position: toast.POSITION.BOTTOM_RIGHT
                    });
                document.getElementsByClassName('cencelsure')[0].click();
                  } else  {
                    toast.error(res.message, {
                      position: toast.POSITION.BOTTOM_RIGHT
                    });
                  }
        this.componentDidMount();
           })
        .catch(err => {
           console.log('Error updating notification info.', err);
        });
    }

   openmodal(noti_id){
    this.setState({del_id:noti_id});
    }

render() {
        return (
        
        <div className="content-wrap universelclass">
            <div className="notificationicons-back">
            <div className="row">
                <div className="col-lg-12">
                    <div className="notificationhead wow fadeInDown animated animated" style={{visibility: 'visible'}}>
                        <h1>Notifications</h1>
                    </div>
                </div>

                <div className="notificationbox">
                    {this.state.notifications.length > 0
                    ?
                    this.state.notifications.map(notification => {

                    return <div className="firstnotification wow animated fadeInLeft animated animated" >
                        <div className="crossbtnnoti">
                            <span>
                            <a data-toggle="modal" data-target="#notificationpopup" onClick={this.openmodal.bind(this, notification.id)} ><img src={noticlose} alt="#" /></a>
                            </span>
                        </div>
                        <div className="notifidata">
                            <button className="btn btn-primary join">{notification.notification_types.name}</button><span><img src={timer} alt="#" /> {notification.created_at}</span>
                            <p>{notification.message}</p>
                            <h3>Sender: </h3>
                        </div>
                        <hr className="notiboxline" />
                    </div>

                }):<div>No notification right now</div>}

                  </div>

            </div>
        </div> 
        <div id="notificationpopup" className="modal">
            <div className="modal-contented deletepopup wow animated fadeInDown" data-wow-delay=".9s">
                <div className="notic-middle">
                    <button type="button" className="crose" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <i className="fa fa-times-circle"></i>
                    <h6>Are You Sure ?</h6>
                    <p>Do you really want to delete this record?</p>
                    <p>This process cannot be undone.</p>
                    <a className="cencelsure" data-dismiss="modal" aria-label="Close">Cancel</a>
                    <a className="deletesure" onClick={this.deletenoti.bind(this, this.state.del_id)} >Delete</a>
                </div>
            </div>
        </div>
        <footer className="add_list_footer">   
                    <div className="row"> 
                        <div className="col-sm-12">          
                            <p>© 2019 <a href="#" style={{color: '#07a8de'}}>VIP PARCEL</a></p>    
                        </div>
                    </div>
                </footer>
        </div>
        

            );
    }

}

export default Notifications;


