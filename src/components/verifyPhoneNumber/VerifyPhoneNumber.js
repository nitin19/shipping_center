import React from 'react';
import { Link, Redirect } from 'react-router-dom'

import registerLogo from '../../images/register-logo.png';
import './verifyPhoneNumber.css';
import { validateUserToken } from '../PrivateRoute';
import { scAxios } from '../..';
import { MAIN_PAGE_PATH, LOGIN_PAGE_PATH, API_TOKEN_NAME, API_TOKEN_EXPIRY_NAME } from '../../constants';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const resendPhoneCode = () => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/resend-sms', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const verifyPhoneCode = (data) => {
    return new Promise((resolve, reject) => {
        const params = {
            code: data.code,
        }

        const req = scAxios.request('/verify-sms', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...params
            },
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

let expiryTime = 0;

class VerifyPhoneNumber extends React.Component {

    state = {
        code: '',
        isVerified: false
    }

    handleChange = event => {
        this.setState({ [event.target.name]: event.target.value });
    }

    handleVerifyPhoneCode = event => {
        event.preventDefault();
        const pvcode = {
            code: this.state.code,
        }
        verifyPhoneCode(pvcode)
            .then(res => {
                if (res === "0") {
                    localStorage.setItem(API_TOKEN_EXPIRY_NAME, expiryTime);
                    this.setState({ isVerified: true });
                    toast.success('Phone no verified successfully.', {
                        position: toast.POSITION.BOTTOM_RIGHT
                    });

                } else {
                    toast.error('Fail to verify phone no.', {
                        position: toast.POSITION.BOTTOM_RIGHT
                    });
                }
            })
            .catch(err => {
                console.log(err);
                toast.error('Error to verify phone no.', {
                    position: toast.POSITION.BOTTOM_RIGHT
                });
            });
    }

    handlerResendPhoneCode = event => {
        event.preventDefault();
        resendPhoneCode()
            .then(res => {
                if (res.status === true) {
                    // console.log('Verify success', res);
                    // this.setState({ isVerified: true });
                    toast.success('Request submitted', {
                        position: toast.POSITION.BOTTOM_RIGHT
                    });

                } else {
                    toast.error('Fail to resending phone code req.', {
                        position: toast.POSITION.BOTTOM_RIGHT
                    });
                }
            })
            .catch(err => {
                console.log(err);
                toast.error('Error resending phone code req.', {
                    position: toast.POSITION.BOTTOM_RIGHT
                });

            });
    }


    /*handleSubmit = () => {
        resendPhoneCode()
            .then(res => {
                if (res === "0") {
                    this.setState({ isVerified: true });
                }
            })
            .catch(err => {
                alert('Error resending phone code req.', err);
            });
    }*/

    componentDidMount() {
        expiryTime = localStorage.getItem(API_TOKEN_EXPIRY_NAME);
        localStorage.setItem(API_TOKEN_EXPIRY_NAME, '');
    }

    render() {
        if (this.state.isVerified) {
            return <Redirect to={MAIN_PAGE_PATH} />
        }

        return (
            <div className="register-page">
                <div className="container">
                    <div className="row">
                        <div className="col-sm-4 regst-img cus-height">
                            <div className="left-register">
                                <Link to="/signup"><img src={registerLogo} alt="vip-logo" className="vip-logo animated fadeInLeft" /></Link>
                                <h6 className="animated fadeInLeft">Reset your</h6>
                                <h1 className="animated fadeInLeft">Password</h1>
                                <h5 className="animated fadeInLeft">VIPparcel is the Nation's leading online postage service to buy and print discounted USPS labels - domestic and international - from the comfort of your desk. We guarantee to save you time and money by making your shipping process easy, affordable, and efficient.</h5>
                            </div>
                        </div>
                        <div className="col-sm-8 regst-input">
                            <h2 className="animated fadeInLeft">Verify your phone number</h2>
                            <form onSubmit={this.handleSubmit} className="login-div">
                                <div className="row mar-bot">
                                    <div className="col-sm-12 padd-normal">
                                        <p className="verifaction-code">Please type verification code send to your number</p>
                                        <input type="text" name="code" value={this.state.code} className="veri-code animated fadeInLeft" onChange={this.handleChange} />
                                        <input type="submit" className="veribtn animated fadeInRight" value="Verify" onClick={this.handleVerifyPhoneCode} style={{ marginTop: 10 }} />
                                    </div>
                                </div>
                                <div className="row mar-bot">
                                    <div className="col-sm-12 padd-normal">
                                        <input type="submit" className="Resend-sms" value="Resend SMS" onClick={this.handlerResendPhoneCode} />
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
                <ToastContainer autoClose={5000} />
            </div>
        );
    };

}

export default VerifyPhoneNumber;
