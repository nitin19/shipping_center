import React from 'react';
import './warehouseLists.css';
import clientimg from '../../images/client.png';
import uploadImg from '../../images/1.png';
import { validateUserToken } from '../PrivateRoute';
import { scAxios } from '../..';
import { Redirect } from 'react-router-dom';
import { MAIN_PAGE_PATH, LOGIN_PAGE_PATH, API_TOKEN_NAME, USER_ID, IMAGE_URL} from '../../constants';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Skeleton from 'react-loading-skeleton';
import loadingimage from '../../images/small_loader.gif';

//import styles from './warehouseLists.css';

const getWarehousesCount = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/warehouse/getwarehousecount', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const getWarehouse = (data) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request('/warehouse/getwarehouse', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        });

        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}

const deletestore = (data, url) => {
    return new Promise((resolve, reject) => {
        const req = scAxios.request(url, {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }

        });
        req.then(res => resolve(res.data))
            .catch(err => reject(err));
    });
}


const saveWarehouse = (data, imageData) => {
    let warehouse_image = new FormData();
    warehouse_image.append('warehouse_image', imageData);

    return new Promise((resolve, reject) => {
        scAxios.post('/warehouse/save', warehouse_image, {
            headers: {
                'accept': 'application/json',
                'Content-Type': `multipart/form-data; boundary=${warehouse_image._boundary}`,
                'Authorization': 'Bearer ' + localStorage.getItem(API_TOKEN_NAME)
            },
            params: {
                ...data
            }
        }).then((response) => {
            return resolve(response.data)
        }).catch((error) => {
            return reject(error)
        });
    });
}
class WarehouseLists extends React.Component {

    state = {
        warehouse_image: clientimg,

        warehouse_image_data: '',
        warehouse_name: '',
        warehouse_address: '',
        warehouse_city: '',
        warehouse_state: '',
        warehouse_country: '',
        warehouse_phone: '',
        total: '',
        currentPage: '',
        LastPage: '',
        PerPage: '',
        FirstPageUrl: '',
        NextPageUrl: '',
        PrevPageUrl: '',
        LastPageUrl: '',
        TotalPages: '',
        searchdata: '',
        warehouses: [],
        fields: {},
        errors: {},
        loadingFirst: true,
        warehouses_count: false,
    }

    handleChange = event => {
        let fields = this.state.fields;
        fields[event.target.name] = event.target.value;
        this.setState({
            fields
        }, () => this.validateForm());
    }

    delete(id) {
        const data = {
            userid: localStorage.getItem(USER_ID)
        }
        var url = 'warehouse/suspended/' + id;
        deletestore(data, url)
            .then(res => {
                if (res.status == 'Success') {
                    toast.success(res.message, {
                        position: toast.POSITION.BOTTOM_RIGHT
                    });
                } else {
                    toast.error(res.message, {
                        position: toast.POSITION.BOTTOM_RIGHT
                    });
                }
                this.refreshWarehouseList();
            })
            .catch(err => {
                console.log(err);
                toast.error("Error occured fetching Stores. Check console", {
                    position: toast.POSITION.BOTTOM_RIGHT
                });
            });
    }

    onImageChange = event => {
        if (event.target.files[0]) {
            const file = event.target.files[0];
            let reader = new FileReader();
            reader.readAsDataURL(file);

            reader.onload = event => {
                this.setState({ warehouse_image: event.target.result, warehouse_image_data: file });
            }
        }
    }

    validateForm() {
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;

        if (!fields["warehouse_name"]) {
            formIsValid = false;
            errors["warehouse_name"] = "*Please enter your Warehouse Name.";
        }

        if (typeof fields["warehouse_name"] !== "undefined") {
            if (!fields["warehouse_name"].match(/^[a-zA-Z0-9 ]*$/)) {
                formIsValid = false;
                errors["warehouse_name"] = "*Please enter alphabet characters only.";
            }
        }

        if (!fields["warehouse_address"]) {
            formIsValid = false;
            errors["warehouse_address"] = "*Please enter your Warehouse Address.";
        }

        if (typeof fields["warehouse_address"] !== "undefined") {
            if (!fields["warehouse_address"].match(/^[a-zA-Z0-9 ]*$/)) {
                formIsValid = false;
                errors["warehouse_address"] = "*Please enter alphabet characters only.";
            }
        }

        if (!fields["warehouse_city"]) {
            formIsValid = false;
            errors["warehouse_city"] = "*Please enter your Warehouse City.";
        }

        if (typeof fields["warehouse_city"] !== "undefined") {
            if (!fields["warehouse_city"].match(/^[a-zA-Z ]*$/)) {
                formIsValid = false;
                errors["warehouse_city"] = "*Please enter alphabet characters only.";
            }
        }

        if (!fields["warehouse_state"]) {
            formIsValid = false;
            errors["warehouse_state"] = "*Please enter your Warehouse State.";
        }

        if (typeof fields["warehouse_state"] !== "undefined") {
            if (!fields["warehouse_state"].match(/^[a-zA-Z ]*$/)) {
                formIsValid = false;
                errors["warehouse_state"] = "*Please enter alphabet characters only.";
            }
        }

        if (!fields["warehouse_country"]) {
            formIsValid = false;
            errors["warehouse_country"] = "*Please enter your Warehouse Country.";
        }

        if (typeof fields["warehouse_country"] !== "undefined") {
            if (!fields["warehouse_country"].match(/^[a-zA-Z ]*$/)) {
                formIsValid = false;
                errors["warehouse_country"] = "*Please enter alphabet characters only.";
            }
        }

        if (!fields["warehouse_phone"]) {
            formIsValid = false;
            errors["warehouse_phone"] = "*Please enter your Phone no.";
        }

        if (typeof fields["warehouse_phone"] !== "undefined") {
            if (!fields["warehouse_phone"].match(/^[0-9]{10}$/)) {
                formIsValid = false;
                errors["warehouse_phone"] = "*Please enter valid Phone no.";
            }
        }

        this.setState({
            errors: errors
        });
        return formIsValid;

    }

    handleSubmit = event => {
        event.preventDefault();
        if (this.validateForm()) {
            let fields = {};
            fields["warehouse_name"] = "";
            fields["warehouse_address"] = "";
            fields["warehouse_city"] = "";
            fields["warehouse_state"] = "";
            fields["warehouse_country"] = "";
            fields["warehouse_phone"] = "";
            this.setState({ fields: fields });

            const warehouse = {
                userid: localStorage.getItem(USER_ID),
                warehouse_name: this.state.fields.warehouse_name,
                warehouse_address: this.state.fields.warehouse_address,
                warehouse_city: this.state.fields.warehouse_city,
                warehouse_state: this.state.fields.warehouse_state,
                warehouse_country: this.state.fields.warehouse_country,
                warehouse_phone: this.state.fields.warehouse_phone,
            }

            saveWarehouse(warehouse, this.state.warehouse_image_data)
                .then(res => {
                    if (res.status == 'Success') {
                        this.setState({ warehouses_count: true });
                        toast.success(res.message, {
                            position: toast.POSITION.BOTTOM_RIGHT
                        });
                    } else if (res.status == 'Warning') {
                        toast.warn(res.message, {
                            position: toast.POSITION.BOTTOM_RIGHT
                        });
                    } else {
                        toast.error(res.message, {
                            position: toast.POSITION.BOTTOM_RIGHT
                        });
                    }
                    //console.log('save success', res);
                    document.getElementById("warehouseModal").click();
                    this.refreshWarehouseList();
                    this.setState({
                        warehouse_name: '',
                    });
                })
                .catch(err => {
                    console.log(err);
                    toast.error('save warehouse error', {
                        position: toast.POSITION.BOTTOM_RIGHT
                    });
                });

        }
        else
            toast.error('Validation failed !', {
                position: toast.POSITION.BOTTOM_RIGHT
            });

    }

    refreshWarehouseList = (page) => {
        const data = {
            searchdata: this.state.searchdata,
            page: page,
            userid: localStorage.getItem(USER_ID)
        }
        getWarehouse(data)
            .then(res => {
                this.setState({ loadingFirst: false });
                var records = res.message.data;
                this.setState({ warehouses: records });
                this.setState({ total: res.message.total });
                this.setState({ currentPage: res.message.current_page });
                this.setState({ PerPage: res.message.per_page });
                this.setState({ FirstPageUrl: res.message.first_page_url });
                this.setState({ NextPageUrl: res.message.next_page_url });
                this.setState({ PrevPageUrl: res.message.prev_page_url });
                this.setState({ LastPageUrl: res.message.last_page_url });
                this.setState({ LastPage: res.message.last_page });
                this.setState({ TotalPages: Math.ceil(this.state.total / this.state.PerPage) });
                {/*if (res.message.data) {
                    this.setState({ warehouses: res.message.data });
                }*/}
            })
            .catch(err => {
                console.log(err);
                toast.error('Error occured fetching warehouse. Check console', {
                    position: toast.POSITION.BOTTOM_RIGHT
                });
                //alert("Error occured fetching warehouse. Check console");
            });
    }

    componentDidMount() {

        this.refreshWarehouseList();
        const data = {
            userid: localStorage.getItem(USER_ID)
        }
        getWarehousesCount(data)
            .then(res => {
                if (res.total_warehouses > 0) {
                    this.setState({ warehouses_count: true });
                }
                console.log(res.total_warehouses);
            })
            .catch(err => {
                console.log(err);
                alert("Error occured fetching locations count. Check console");
            });
    }

    render() {
        const totalWarehouse = this.state.warehouses ? this.state.warehouses.length : 0;
        const currentPage = this.state.currentPage;
        const previousPage = currentPage - 1;
        const NextPage = currentPage + 1;
        const LastPage = this.state.LastPage;
        const pageNumbers = [];
        for (let i = 1; i <= this.state.TotalPages; i++) {
            pageNumbers.push(i);
        }

        return (

            <>
                {this.state.loadingFirst == true ?
                    <div className="content-wrap backcollor">
                          <div className="ms-stor">
                                <div className="row mstop">
                                    <div className="col-sm-4">
                                        {this.props.title || <Skeleton count={1} duration={2} height={30} />}
                                    </div>
                                    <div className="col-sm-4">
                                        {this.props.title || <Skeleton count={1} duration={2} height={30} />}
                                    </div>
                                    <div className="col-sm-4">
                                        {this.props.title || <Skeleton count={1} duration={2} height={30} />}
                                    </div>
                                </div>
                              <p>{this.props.title || <Skeleton count={1} duration={2} />}</p>
                              <div className="mystoreboxs">
                                  <h1>{this.props.title || <Skeleton count={5} duration={2} />}</h1>
                              </div>
                              <div className="mystoreboxs">
                                  <h1>{this.props.title || <Skeleton count={5} duration={2} />}</h1>
                              </div>
                              <div className="mystoreboxs">
                                  <h1>{this.props.title || <Skeleton count={5} duration={2} />}</h1>
                              </div>
                              <div className="mystoreboxs">
                                  <h1>{this.props.title || <Skeleton count={5} duration={2} />}</h1>
                              </div>
                              <div className="mystoreboxs">
                                  <h1>{this.props.title || <Skeleton count={5} duration={2} />}</h1>
                              </div>
                          </div>
                        </div>
                    :

                    <div className="content-wrap universelclass">
                        <div className="storemiddle-sec">
                            <div className="row addstore-list-text">
                                <div className="col-lg-5">
                                    <h1>Warehouse Lists</h1>
                                </div>
                                <div className="col-lg-7 text-right">
                                    <div className="search_sec">
                                        <form className="form_section">
                                            <input type="text" name="searchdata" placeholder="Search..." value={this.state.searchdata} onChange={this.handleChange} id="search_keyword" />
                                            <input type="button" className="Connect-btn" value="Search" id="submit_btn" onClick={this.refreshWarehouseList} />
                                        </form>
                                    </div>


                                    {this.state.warehouses_count == false ?
                                        <a href="#" data-toggle="modal" data-target="#houseModal" className="Connect-btn wereleft">Add New</a>
                                        :
                                        <> </>
                                    }

                                    <input type="button" className="Connect-btn" value="Refresh" onClick={this.refreshWarehouseList} />
                                </div>
                            </div>
                            <hr />
                            <div className="products-list-sec">
                                <div className="row single-prdt-sec">
                                    {
                                        totalWarehouse > 0
                                            ?
                                            this.state.warehouses.map(warehouse => {
                                                return (<div className="col-xl-4 col-lg-6 col-md-6 col-sm-12" key={warehouse.id}>

                                                    <div className="row prdt-back-gd">
                                                        <div className="col-sm-3">
                                                            <img src={IMAGE_URL + 'warehouseimage/' + warehouse.warehouse_image} alt="Nike-lunacharge-Essential" width="100%" />
                                                        </div>
                                                        <div className="col-sm-9">
                                                            <h6 className="prdt-mainhd"><a href={'./viewwarehouse/' + warehouse.id}>{warehouse.warehouse_name}</a></h6>
                                                            <span className="prdt-headtext">Sku</span> <span className="prdt-nortext">32566L</span>
                                                            <p className="prdt-location"><i className="fa fa-map-marker" style={{ color: 'red' }}></i> <span><span>{warehouse.warehouse_address}</span></span></p>
                                                            <p className="house-phonenum"><i className="fa fa-phone" aria-hidden="true"></i> {warehouse.phone_number}</p>
                                                            <div className="dropdown elips-drop">
                                                                <a href="#" className="products-ellipsis" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i className="fa fa-ellipsis-h"></i></a>
                                                                <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                                    <a className="dropdown-item" href={'./editwarehouse/' + warehouse.id}>Edit</a>
                                                                    <a className="dropdown-item" href={'./viewwarehouse/' + warehouse.id}>View</a>
                                                                    <a className="dropdown-item" style={{ cursor: 'pointer' }} onClick={this.delete.bind(this, warehouse.id)}>Delete</a>
                                                                    <a className="dropdown-item" href="">Orders</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>);
                                            })
                                            :
                                            <div className="no_records">
                                                <h2>No Records founds!</h2>
                                            </div>
                                    }

                                </div>
                                {pageNumbers.length > 1 ?
                                    <div className="paggination-text">
                                        <ul className="pagination pagination-sm">
                                            {this.state.PrevPageUrl != null ?
                                                <li className="page-item"><a className="page-link" style={{ cursor: 'pointer' }} onClick={this.refreshWarehouseList.bind(this, previousPage)}>Previous</a></li>
                                                : ''
                                            }
                                            {pageNumbers.map(page => {
                                                return (<li className={currentPage == page ? 'activelink page-item' : 'page-item'} ><a className="page-link" style={{ cursor: 'pointer' }} onClick={this.refreshWarehouseList.bind(this, page)}>{page}</a></li>);
                                            })}

                                            {this.state.NextPageUrl != null ?
                                                <li className="page-item"><a className="page-link" style={{ cursor: 'pointer' }} onClick={this.refreshWarehouseList.bind(this, NextPage)}>Next</a></li>
                                                : ''
                                            }
                                            {this.state.LastPage != null && currentPage != LastPage ?
                                                <li className="page-item"><a className="page-link" style={{ cursor: 'pointer' }} onClick={this.refreshWarehouseList.bind(this, LastPage)} >Last</a></li>
                                                : ''
                                            }
                                        </ul>
                                    </div> : ''
                                }
                                <div className="" style={{ position: 'relative' }}>
                                    <div className="loader"></div>
                                </div>

                            </div>


                            {/* // <!--productModal-->       */}
                            <div id="houseModal" className="modal">

                                {/* <!-- Modal content --> */}
                                <div className="modal-content">
                                    <div className="add-prdt-header">
                                        <p>Add Location</p>
                                        <button id="warehouseModal" type="button" className="close-sign" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                                    </div>
                                    <div className="add-prdt-middle">
                                        <form onSubmit={this.handleSubmit} >
                                            <ul>
                                                <li>
                                                    <div className="row">
                                                        <div className="col-sm-4">
                                                            <p>Warehouse Image</p>
                                                        </div>
                                                        <div className="col-sm-8">
                                                            <img src={this.state.warehouse_image} alt="upload-img" className="prdt-upload-img" />
                                                            <div className="upload-btn-wrapper">
                                                                <button className="btn">Upload Image</button>
                                                                <input type='file' id='warehouse_image' onChange={this.onImageChange} />
                                                            </div>

                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div className="row">
                                                        <div className="col-sm-4">
                                                            <p>Warehouse</p>
                                                        </div>
                                                        <div className="col-sm-8">
                                                            <input type="text" name="warehouse_name" value={this.state.fields.warehouse_name} placeholder="Enter your warehouse name" onChange={this.handleChange} autoComplete="off" />
                                                            <div className="errorMsg">{this.state.errors.warehouse_name}</div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div className="row">
                                                        <div className="col-sm-4">
                                                            <p>Address</p>
                                                        </div>
                                                        <div className="col-sm-8">
                                                            <input type="text" name="warehouse_address" value={this.state.fields.warehouse_address} placeholder="Enter your address" onChange={this.handleChange} autoComplete="off" />
                                                            <div className="errorMsg">{this.state.errors.warehouse_address}</div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div className="row">
                                                        <div className="col-sm-4">
                                                            <p>City</p>
                                                        </div>
                                                        <div className="col-sm-8">
                                                            <input type="text" name="warehouse_city" value={this.state.fields.warehouse_city} placeholder="Your city name" onChange={this.handleChange} autoComplete="off" />
                                                            <div className="errorMsg">{this.state.errors.warehouse_city}</div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div className="row">
                                                        <div className="col-sm-4">
                                                            <p>State</p>
                                                        </div>
                                                        <div className="col-sm-8">
                                                            <input type="text" name="warehouse_state" value={this.state.fields.warehouse_state} placeholder="Enter your state" onChange={this.handleChange} autoComplete="off" />
                                                            <div className="errorMsg">{this.state.errors.warehouse_state}</div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div className="row">
                                                        <div className="col-sm-4">
                                                            <p>Country</p>
                                                        </div>
                                                        <div className="col-sm-8">
                                                            <input type="text" name="warehouse_country" value={this.state.fields.warehouse_country} placeholder="Enter your country" onChange={this.handleChange} autoComplete="off" />
                                                            <div className="errorMsg">{this.state.errors.warehouse_country}</div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div className="row">
                                                        <div className="col-sm-4">
                                                            <p>Phone No.</p>
                                                        </div>
                                                        <div className="col-sm-8">
                                                            <input type="text" name="warehouse_phone" value={this.state.fields.warehouse_phone} placeholder="eg. 326-789-0236" onChange={this.handleChange} autoComplete="off" />
                                                            <div className="errorMsg">{this.state.errors.warehouse_phone}</div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div className="row">
                                                        <div className="col-sm-4">

                                                        </div>
                                                        <div className="col-sm-8">
                                                            <input type="submit" className="Connect-btn" />
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </form>
                                    </div>
                                    <div className="mod-footer">
                                        {/* <!-- <span className="close1">Cancel</span>  --> */}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <ToastContainer autoClose={5000} />
                        <footer className="add_list_footer">
                            <div className="row">
                                <div className="col-sm-12">
                                    <p>© 2019 <a href="#" style={{ color: 'rgb(7, 168, 222)' }}>VIP PARCEL</a></p>
                                </div>
                            </div>
                        </footer>
                    </div>

                }
            </>

        );
    }
}

export default WarehouseLists;