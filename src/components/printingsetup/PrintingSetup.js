import React from 'react';
import ReactDOM from 'react-dom';
//import './printingsetup.css'
import ebay from '../../images/ebay.png';
import plush from '../../images/plush.png';
import amazon from '../../images/amazon-listing.png';
import cart32 from '../../images/cart32.png';
import usps from '../../images/usps-listing.png';
import seocart from '../../images/seocart.png';
import shiprobot from '../../images/logo-shiprobot.png';
import insure from '../../images/insure-listing.png';

class PrintingSetup extends React.Component {

	render() {

		return (

				<div className="content-wrap universelclass">
        <div className="col-lg-12">
            <div className="sorting-list">
                <h1>Printing Setup</h1>
                <p>Choose how your documents print from ShipStation. Document Options are account-wide settings, but your Print To settings will be saved for this machine and browser only.</p>
            </div>
            {/*<div className="automation-para">
              <p>Automation rules help you perform tasks for every order you import. You can manage your rules below or create a new one.</p>
            </div>*/}

            <div className="custom_tabel wow bounceIn animated" data-wow-duration="4s">
 
        <table className="table ">
  <thead>
    <tr className="bg ">
      <th>Document Type</th>
      <th>Print To</th>
      <th>Document Format</th>
      <th>Options</th>
    </tr>
  </thead>
  <tbody>
    <tr className="wow animate fadeInLeft " data-wow-delay="0.4s">
      <td>Inventory Warehouse</td>
      <td><a href="" className="namehigh">Always prompt</a></td>
      <td><span className="edit_icon ">4″ × 6″ (w/ Packing Slip)</span></td>
      
      <th><span className="edit_icon "><a href="#" className="namehigh"> Document Options</a></span></th>
    </tr>
    <tr className="wow animate fadeInLeft" data-wow-delay="0.8s">
           
      <td>Inventory Warehouse</td>
      <td><a href="" className="namehigh">Always prompt</a></td>
      <td><span className="edit_icon ">4″ × 6″ (w/ Packing Slip)</span></td>
      
      <th><span className="edit_icon "><a href="#" className="namehigh"> Document Options</a></span></th>
    </tr>
    <tr className="wow animate fadeInLeft " data-wow-delay="1.4s">
         
      <td>Inventory Warehouse</td>
      <td><a href="" className="namehigh">Always prompt</a></td>
      <td><span className="edit_icon ">4″ × 6″ (w/ Packing Slip)</span></td>
     
      <th><span className="edit_icon "><a href="#" className="namehigh"> Document Options</a></span></th>
    </tr>
   <tr className="wow animate fadeInLeft " data-wow-delay="1.8s">
          
      <td>Inventory Warehouse</td>
      <td><a href="" className="namehigh">Always prompt</a></td>
      <td><span className="edit_icon ">4″ × 6″ (w/ Packing Slip)</span></td>
      
      <th><span className="edit_icon "><a href="#" className="namehigh"> Document Options</a></span></th>
    </tr>
    <tr className="wow animate fadeInLeft " data-wow-delay="2s"> 
          
      <td>Inventory Warehouse</td>
      <td><a href="" className="namehigh">Always prompt</a></td>
      <td><span className="edit_icon ">4″ × 6″ (w/ Packing Slip)</span></td>
      
      <th><span className="edit_icon "><a href="#" className="namehigh"> Document Options</a></span></th>
    </tr>
    <tr className="wow animate fadeInLeft " data-wow-delay="2s"> 
          
      <td>Inventory Warehouse</td>
      <td><a href="" className="namehigh">Always prompt</a></td>
      <td><span className="edit_icon ">4″ × 6″ (w/ Packing Slip)</span></td>
      
      <th><span className="edit_icon "><a href="#" className="namehigh"> Document Options</a></span></th>
    </tr>
    
    <tr className="wow animate fadeInLeft " data-wow-delay="2s"> 
          
      <td>Inventory Warehouse</td>
      <td><a href="" className="namehigh">Always prompt</a></td>
      <td><span className="edit_icon ">4″ × 6″ (w/ Packing Slip)</span></td>
      
      <th><span className="edit_icon "><a href="#" className="namehigh"> Document Options</a></span></th>
    </tr>
  </tbody>
</table>

       </div>
            
       </div>
       <footer className="add_list_footer">   
                    <div className="row"> 
                        <div className="col-sm-12">          
                            <p>© 2019 <a href="#" style={{color: '#07a8de'}}>VIP PARCEL</a></p>    
                        </div>
                    </div>
                </footer>
      </div>

			);

	}

}

export default PrintingSetup;