export default {
    user: {
        id: -1,
        first_name: '',
        last_name: '',
        email: 0,
        phone: 0,
        status: null,
        is_fulfillment_center: '',
        type: '',
        token: ''
    },
    amazon: {

    },
    stores: {
        isAmazonSellerIdVerified: false,

    },
    orders: {

    }
}