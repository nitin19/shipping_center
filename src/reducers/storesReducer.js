import * as types from '../actions/actionTypes';

import initialState from './initialState';

export default (state = initialState.amazon, action) => {
    switch (action.type) {
        case types.GET_STORES_LIST:
            return {
                ...state,
            };

        case types.CHECK_ACCOUNT_STATUS:
            console.log('calling check accout status',action);
            return {
                ...state,
                isAmazonSellerIdVerified: action.isAmazonSellerIdVerified
            };

        case types.SAVE_STORE:
            return {
                ...state,
            };

        case types.SYNC_STORES:
            return {
                ...state,
            };

        default:
            return state;
    }
}
