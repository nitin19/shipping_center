import * as types from '../actions/actionTypes';

import initialState from './initialState';

export default (state = initialState.amazon, action) => {
    switch (action.type) {
        case types.GET_ALL_STORE_ORDERS:
            return {
                ...state,
            };

        default:
            return state;
    }
}
