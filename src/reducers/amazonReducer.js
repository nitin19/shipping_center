import * as types from '../actions/actionTypes';

import initialState from './initialState';


export default (state = initialState.amazon, action) => {
    switch (action.type) {
        case types.GET_SINGLE_STORE_ORDERS:
            return {
                ...state,
            };

        case types.GET_STORE_ORDER_DETAILS_BY_ID:
            return {
                ...state,
            };

        case types.SEARCH_STORE:
            return {
                ...state,
            };

        case types.SEARCH_STORE_ORDERS:
            return {
                ...state,
            };

        default:
            return state;
    }
}
