import { combineReducers } from "redux";

import user from "./userReducer";
import amazon from './amazonReducer';
import stores from './storesReducer';
import orders from './ordersReducer';


export const rootReducer = combineReducers({
    user,
    amazon,
    stores,
    orders,
});
